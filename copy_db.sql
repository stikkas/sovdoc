create smallfile tablespace SOVDOC
    logging
    datafile '/home/basa/oracle/app/oracle/oradata/SOVDOCUTF8/SOVDOC.dbf' size 64M
    reuse autoextend on next 10M maxsize 5120M
    extent management local
    segment space management auto;

create smallfile tablespace SOVDOC_IND
    logging
    datafile '/home/basa/oracle/app/oracle/oradata/SOVDOCUTF8/SOVDOC_IND.dbf' size 64M
    reuse autoextend on next 10M maxsize 5120M
    extent management local
    segment space management auto;

DROP USER sovdoc CASCADE;
CREATE USER sovdoc IDENTIFIED BY sovok DEFAULT TABLESPACE SOVDOC TEMPORARY TABLESPACE TEMP;
GRANT UNLIMITED TABLESPACE TO sovdoc;
GRANT CONNECT TO sovdoc;
GRANT DBA TO sovdoc;
GRANT JAVAUSERPRIV TO sovdoc;
ALTER USER sovdoc DEFAULT ROLE ALL;
GRANT EXECUTE ON sys.dbms_backup_restore TO sovdoc;
GRANT EXECUTE ON sys.dbms_backup_restore TO sovdoc;

$ exp sovdoc/sovok@remote:1521/nyoko file=sovdoc.dmp
$ imp sovdoc/sovok@local/xe file=sovdoc.dmp fromuser=sovdoc touser=sovdoc
