package test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.insoft.commons.utils.StringUtils;

/**
 *
 * @author stikkas<stikkas@yandex.ru>
 */
public class TestStringUtils {
    @Ignore
    @Test
    public void testLengthOfString() {
        String s = "а";
        Assert.assertEquals(2, StringUtils.getByteLengthUTF8(s));
        s = "";
        Assert.assertEquals(0, StringUtils.getByteLengthUTF8(s));
        s = "ФА";
        Assert.assertEquals(4, StringUtils.getByteLengthUTF8(s));
        s = "ХуЛО";
        Assert.assertEquals(8, StringUtils.getByteLengthUTF8(s));
    }

}
