if (!window.insoft)
	window.insoft = {};

(function($, insoft)
{
	insoft.ebook = insoft.ebook || {};
	
	insoft.ebook.attachCover = function()
		{
			$("#uploadPanel\\:form\\:upload").find(".rf-fu-inp").click();
		};
	
}(jQuery, window.insoft));

$(document).ready(function()
{    
	var rfUpload = document.getElementById("uploadPanel:form:upload").rf.component;
	insoft.ebook.__startUpload = $.proxy(rfUpload.__startUpload, rfUpload);
	rfUpload.__startUpload = insoft.ebook.startUpload;
});