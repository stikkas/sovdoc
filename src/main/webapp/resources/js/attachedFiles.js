if (!window.insoft)
	window.insoft = {};

(function ($, insoft)
{
	insoft.attachedFiles = insoft.attachedFiles || {};

	insoft.attachedFiles.reloadButtons = function(category)
		{
			$("#form\\:" + category + "\\:add").click(
					function(event)
					{
						document.getElementById("uploadPanel:popup").rf.component.show();
						renderMainPage = insoft.attachedFiles.renderPanel(category);
						var upload = document.getElementById("uploadPanel:form:upload").rf.component;
						upload.uploadButton.off("click");
						upload.uploadButton.click(
								function(event)
								{
									insoft.attachedFiles.startUpload(category);
								}
							);
						insoft.attachedFiles.__startUpload = $.proxy(upload.__startUpload, upload);
					}
				);
		};
		
	insoft.attachedFiles.renderPanel = function(category)
		{
			if (category === "FULL_SIZE")
				return insoft.attachedFiles.renderFullsizePanel;
			if (category === "PREVIEW")
				return insoft.attachedFiles.renderPreviewPanel;
		};
		
	insoft.attachedFiles.scrollDown = function(category)
		{
			var scroll = $("#form\\:" + category).find('.scrll');
			scroll.scrollTop(scroll.height());
		};
		
}(jQuery, window.insoft));