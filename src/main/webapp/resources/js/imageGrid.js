if (!window.insoft)
	window.insoft = {};

(function ($, insoft)
{
	insoft.imageGrid = (function ()
	{
		var $cells;
		var current;

		showImage = function (index) {
			current = index;
			var $cell = $cells.eq(index),
					url = $cell.find("embed").attr("src"),
					image = $("#myBigImage"),
					mapIndex = $cell.find('object').attr('x-map-id');
			$("[id^='myBigMap_']").hide();
			if (mapIndex === '-1') {
				image.attr("data", url);
				image.find("embed").attr("src", url);
				image.show();
			} else {
				image.hide();
				$('#myBigMap_' + mapIndex).show();
			}
			$("#modal\\:info").html("Изображение " + (index + 1) + " из " + $cells.length);
			$("#modal\\:panel").get(0).rf.component.show();
		};

		setListeners = function ()
		{
			$cells = $("#form").find("div[id$='cell']");
			$cells.dblclick(function ()
			{
				var index = $cells.index($(this));
				showImage(index);
			});
			$("#modal\\:first").click(function ()
			{
				showImage(0);
			});
			$("#modal\\:prev").click(function ()
			{
				if (current > 0)
					showImage(current - 1);
			});
			$("#modal\\:next").click(function ()
			{
				if (current < $cells.length - 1)
					showImage(current + 1);
			});
			$("#modal\\:last").click(function ()
			{
				showImage($cells.length - 1);
			});
		};

		return {setListeners: setListeners};
	})();

	$(document).ready(insoft.imageGrid.setListeners);

}(jQuery, window.insoft));