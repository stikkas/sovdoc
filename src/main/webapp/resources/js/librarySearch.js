if (!window.insoft)
	window.insoft = {};

(function ($, insoft)
{
	insoft.library = insoft.library || {};
	
	insoft.library.showCover = function(event)
	{
		var pic = $(event.target);		
		var picPosition = pic.position();
		$("<img/>",
				{
					id: 'largeCover',
					height: 100,
					src: pic.attr("src")
				}
			).css(
					{
						"position": "absolute",
						"left": picPosition.left + 75,
						"top": picPosition.top - 38
					}
				).appendTo("#form");
	};
	
	insoft.library.hideCover = function()
	{
		$("#largeCover").remove();
	}
	
}(jQuery, window.insoft));