if (!window.Insoft)
    window.Insoft = {};

(function($, insoft)
{
    insoft.phonodoc = insoft.phonodoc || {};
    
    insoft.phonodoc = $.extend(insoft.phonodoc,
    {
        initPage: function()
        {
            var rfUpload = document.getElementById('uploadPanel:form:upload').rf.component;
            var startUploadFn = $.proxy(rfUpload.__startUpload, rfUpload);
            rfUpload.__startUpload = function()
            {
                document.getElementById('uploadPanel:popup').rf.component.show();
                startUploadFn();
            };
            rfUpload.element.children('.rf-fu-hdr').hide();
            renderMainPage = function()
            {
                $('#form\\:postUpload').click();
            }
        },
        countTotalPlaytime: function()
        {
            var res = 0;
            var show = false;
            var $playtimeValue = $('input[id$="suPlaytime:valueInput"]');
            $playtimeValue.each(function()
            {
                 if ($(this).val() !== '')
                 {
                     res += Number($(this).val());
                     show = true;
                 }
            });
            
            var $totalPlaytime = $('div[id$=":totalPlaytime"] div.col-2');
            if (show)
                $totalPlaytime.html(insoft.playtime.getTimestringFromValue(res));
            else
                $totalPlaytime.html('');
        }
    });
    
}(jQuery, window.Insoft));

$(document).ready(Insoft.phonodoc.initPage);