if (!window.Insoft)
	window.Insoft = {};

(function ($, insoft)
{
	insoft.audioplayer = insoft.audioplayer || {};
	insoft.videoplayer = insoft.videoplayer || {};

	insoft.audioplayer.initPlayer = function (clientId, src, format)
	{
		var mediaInfo = {};
		mediaInfo[format] = src;
		clientId = clientId.replace(/\:/g, '\\:');
		$('#' + clientId + '\\:jquery_jplayer_1').jPlayer(
				{
					ready: function ()
					{
						$(this).jPlayer('setMedia', mediaInfo);
					},
					cssSelectorAncestor: '#' + clientId + '\\:jp_container_1',
					supplied: 'mp3',
					useStateClassSkin: true,
					autoBlur: false,
					smoothPlayBar: true,
					keyEnabled: true,
					remainingDuration: true,
					toggleDuration: true
				});
	};

	insoft.videoplayer.initPlayer = function (clientId, src, format)
	{
		var mediaInfo = {};
		format = format === 'mp4' ? 'm4v' : format;
		mediaInfo[format] = src;
		mediaInfo.poster = src;
		clientId = clientId.replace(/\:/g, '\\:');
		$('#' + clientId + '\\:jquery_jvideo_player_1').jPlayer({
			ready: function ()
			{
				$(this).jPlayer('setMedia', mediaInfo);
			},
			cssSelectorAncestor: '#' + clientId + '\\:jp_container_1',
			swfPath: "/js",
			supplied: "m4v",
			useStateClassSkin: true,
			autoBlur: false,
			smoothPlayBar: true,
			keyEnabled: true,
			remainingDuration: true,
			toggleDuration: true
//			cssSelector: {
//				videoPlay: '.jp-video-play',
//				play: '.jp-play',
//				pause: '.jp-pause',
//				stop: '.jp-stop',
//				seekBar: '.jp-seek-bar',
//				playBar: '.jp-play-bar',
//				volumeBar: '.jp-volume-bar',
//				volumeBarValue: '.jp-volume-bar-value',
//				playbackRateBar: '.jp-playback-rate-bar',
//				playbackRateBarValue: '.jp-playback-rate-bar-value',
//				currentTime: '.jp-current-time',
//				duration: '.jp-duration',
//				title: '.jp-title',
//				fullScreen: '.jp-full-screen',
//				restoreScreen: '.jp-restore-screen',
//				repeatOff: '.jp-repeat-off',
//				gui: '.jp-gui',
//				noSolution: '.jp-no-solution'
//			},
//			errorAlerts: false,
//			warningAlerts: false
		});
	};

}
(jQuery, window.Insoft));