if (!window.Insoft)
    window.Insoft = {};

(function ($, insoft)
{
    insoft.playtime = insoft.playtime || {};
    
    insoft.playtime.keypressControl = function(event)
    {
        var ARROW_RIGHT = 39;
	var ARROW_LEFT = 37;
	var BACKSPACE = 8;
	var DELETE = 46;
	var TAB = 9;
	var HOME = 36;
        var END = 35;

	var ignoreKeys = [ARROW_RIGHT, ARROW_LEFT, BACKSPACE, DELETE, TAB, HOME, END];
        if (ignoreKeys.indexOf(event.keyCode) >= 0)
            return true;
        
        if (event.charCode < 48 || event.charCode > 57)
        {
            event.stopPropagation();
            event.preventDefault();
        }
    };
    
    insoft.playtime.normalizeSecond = function($input)
    {
        var val = $input.val();
        if (val.length === 1)
            $input.val("0" + val);
    };
    
    insoft.playtime.isValueEmpty = function(value)
    {
        return !value && value !== 0;
    };
    
    insoft.playtime.getMinutesFromValue = function(value)
    {
        if (insoft.playtime.isValueEmpty(value))
            return '';
        else
            return Math.floor(value / 60);
    };
    insoft.playtime.getSecondsFromValue = function(value)
    {
        if (!value && value !== 0)
            return '';
        else
        {
            var sec = value % 60;
            if (sec < 10)
                return "0" + sec;
            else
                return sec;
        }
    };
    insoft.playtime.getTimestringFromValue = function(value)
    {
        if (!value && value !== 0)
            return '';
        else
            return insoft.playtime.getMinutesFromValue(value) + ':' +
                insoft.playtime.getSecondsFromValue(value);
    };
    
    insoft.playtime.hackRfComponent = function(cmp)
    {
        var __inputHandlerFn = $.proxy(cmp.__inputHandler, cmp);
        cmp.input.off('change');
        cmp.input.change(function(event)
        {
            if (!insoft.playtime.isValueEmpty(cmp.input.val()))
                __inputHandlerFn(event);
        });
        cmp.input.on('input', function()
        {
            cmp.value = Number(cmp.input.val()); 
        });
    };
    
    insoft.playtime.initComponent = function(componentId, onchange)
    {
        var minuteEl = document.getElementById(componentId + ":minute");
        var $minute = $(minuteEl).find('input');
        $minute.attr('maxlength', 4);
        $minute.on('keypress', insoft.playtime.keypressControl);
        
        var secondEl = document.getElementById(componentId + ":second");
        var $second = $(secondEl).find('input');
        $second.attr('maxlength', 2);
        $second.on('keypress', insoft.playtime.keypressControl);
        $second.on('blur', function()
        {
            insoft.playtime.normalizeSecond($second);
        });        
        
        var valueInputEl = document.getElementById(componentId + ":valueInput");
        var $valueInput = $(valueInputEl);
        var updateValue = function(event)
        {
            if (event.target.tagName === 'INPUT' && insoft.playtime.isValueEmpty(event.target.value))
            {
                $minute.val('');
                $second.val('');
                $valueInput.val('');
            }
            else
            {
                if (insoft.playtime.isValueEmpty($minute.val()))
                    $minute.val('0');
                if (insoft.playtime.isValueEmpty($second.val()))
                    $second.val('00');                
                var value = $minute.val() * 60 + Number($second.val());
                $valueInput.val(value);
            }
            if (onchange)
                onchange.call(event);
        };
        $minute.on('blur', updateValue);
        $second.on('blur', updateValue);
        $minute.siblings('.rf-insp-btns').click(updateValue);
        $second.siblings('.rf-insp-btns').click(updateValue);
        
        var minuteCmp = minuteEl.rf.component;
        insoft.playtime.hackRfComponent(minuteCmp);
        
        var secondCmp = secondEl.rf.component;
        var __setValueFn = $.proxy(secondCmp.__setValue, secondCmp);
        secondCmp.__setValue = function(value, event, skipOnChange)
        {
            __setValueFn(value, event, skipOnChange);
            insoft.playtime.normalizeSecond($second);
        };
        insoft.playtime.hackRfComponent(secondCmp);
        
        var initValue = $valueInput.val();
        $minute.val(insoft.playtime.getMinutesFromValue(initValue));
        $second.val(insoft.playtime.getSecondsFromValue(initValue));
        insoft.playtime.normalizeSecond($second);
    };
}
(jQuery, window.Insoft));