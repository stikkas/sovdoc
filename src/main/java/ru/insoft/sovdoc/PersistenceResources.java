package ru.insoft.sovdoc;


import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.jboss.solder.core.ExtensionManaged;

public class PersistenceResources {
	
	@ExtensionManaged
	@Produces
	@RequestScoped
	
	
	@PersistenceUnit(unitName = "OracleEntityManager")
	//@ConversationScoped
	
	//@PersistenceContext
	private EntityManagerFactory emf;
	
}
