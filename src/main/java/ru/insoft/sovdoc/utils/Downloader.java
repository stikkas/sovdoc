package ru.insoft.sovdoc.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import ru.insoft.commons.jsf.ui.MessageUtils;

/**
 * @author stikkas
 */
@RequestScoped
@Named
public class Downloader {

	private Long archiveId;
	private String fundPrefix;
	private Integer fundNumber;
	private String fundLitera;
	private Integer seriesNumber;
	private String seriesLitera;
	private boolean valid;
	private String url;

	public Long getArchiveId() {
		return archiveId;
	}

	public void setArchiveId(Long archiveId) {
		this.archiveId = archiveId;
	}

	public String getFundPrefix() {
		return fundPrefix;
	}

	public void setFundPrefix(String fundPrefix) {
		this.fundPrefix = fundPrefix;
	}

	public Integer getFundNumber() {
		return fundNumber;
	}

	public void setFundNumber(Integer fundNumber) {
		this.fundNumber = fundNumber;
	}

	public String getFundLitera() {
		return fundLitera;
	}

	public void setFundLitera(String fundLitera) {
		this.fundLitera = fundLitera;
	}

	public Integer getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(Integer seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getSeriesLitera() {
		return seriesLitera;
	}

	public void setSeriesLitera(String seriesLitera) {
		this.seriesLitera = seriesLitera;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void exec() throws UnsupportedEncodingException {
		if (archiveId == null || fundNumber == null || seriesNumber == null) {
			MessageUtils.ErrorMessage("Для выгрузки требуется выбрать архив, указать номер фонда и номер описи");
			valid = false;
		} else {
			HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String charsetName = Charset.defaultCharset().name();
			url = req.getRequestURL().toString();
			url = (url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath() + "/");
			url += "/adm/settings/web/downloader?archive=" + archiveId + "&fund=" + fundNumber + "&series=" + seriesNumber
					+ "&fprefix=" + (fundPrefix == null ? "" : URLEncoder.encode(fundPrefix, charsetName))
					+ "&flitera=" + (fundLitera == null ? "" : URLEncoder.encode(fundLitera, charsetName))
					+ "&slitera=" + (seriesLitera == null ? "" : URLEncoder.encode(seriesLitera, charsetName));
			valid = true;
		}
	}

}
