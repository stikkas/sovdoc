package ru.insoft.sovdoc.constant;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.commons.service.AbstractConstService;

@SuppressWarnings({ "serial", "rawtypes" })
@RequestScoped
@Named("constant")
public class ConstSelect extends AbstractConstService
{
	public List<SelectItem> getValueDisplayTypes()
	{
		return getSelectItems(VALUE_DISPLAY_TYPE.class);
	}
	
	public List<SelectItem> getUserStatusTypes()
	{
		return SelectItemUtils.withEmpty(getSelectItems(USER_STATUS_TYPE.class));
	}
}
