package ru.insoft.sovdoc.constant;

import ru.insoft.commons.model.usertypes.ENUM_TYPE_EX;

public enum USER_STATUS_TYPE implements ENUM_TYPE_EX<Boolean, String> {

	TYPE_FALSE("Активен"),
	TYPE_TRUE("Заблокирован");
	
	private final Boolean id;
	private final String value;
	
	private USER_STATUS_TYPE(String value)	
	{
		this.id = Boolean.valueOf(this.name().replaceFirst("TYPE_", ""));
		this.value = value;
	}
	
	@Override
	public Boolean getId() 
	{
		return id;
	}

	@Override
	public String getValue() 
	{
		return value;
	}

}
