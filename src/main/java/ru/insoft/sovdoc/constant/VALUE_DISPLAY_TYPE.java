package ru.insoft.sovdoc.constant;

import ru.insoft.commons.model.usertypes.ENUM_TYPE_EX;

public enum VALUE_DISPLAY_TYPE implements ENUM_TYPE_EX<String, String> {

	SHORT("Сокращённо"),
	FULL("Полностью");
	
	private final String id;
	private final String value;
	
	private VALUE_DISPLAY_TYPE(String value)	
	{
		this.id = this.name();
		this.value = value;
	}
	
	@Override
	public String getId() 
	{
		return id;
	}

	@Override
	public String getValue() 
	{
		return value;
	}

}
