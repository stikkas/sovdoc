package ru.insoft.sovdoc.constant;

import ru.insoft.commons.model.usertypes.ENUM_TYPE;

public enum MULTILINGUAL_VALUE_TYPE implements ENUM_TYPE<String> 
{
	TYPE_FULL,
	TYPE_SHORT,
	TYPE_ATTR
	;

	private final String id;
	
	private MULTILINGUAL_VALUE_TYPE()	
	{
		this.id = this.name();
	}
	
	@Override
	public String getId() 
	{
		return id;
	}	
}
