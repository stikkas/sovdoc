package ru.insoft.sovdoc.system;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.sovdoc.model.adm.table.AdmGroup;
import ru.insoft.sovdoc.model.adm.table.AdmUser;
import ru.insoft.sovdoc.model.complex.view.VComplexArchiveList;
import ru.insoft.sovdoc.model.desc.table.DescDatatype;
import ru.insoft.sovdoc.model.desc.table.DescLanguage;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.view.VDescAttrvalueWithCode;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescSubsystem;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookSeries;
import ru.insoft.sovdoc.system.complex.SystemQuery;

@RequestScoped
@Named
public class SystemSelectItem {

	@Inject
	SystemEntity se;
	@Inject
	ru.insoft.sovdoc.system.ebook.SystemQuery ebookQuery;
	@Inject
	SystemQuery complexQuery;
	Map<String, String> reqmap = null;

	public List<SelectItem> getDatatypes() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (DescDatatype dt : se.getDatatypes()) {
			siList.add(new SelectItem(dt.getDatatypeCode(), dt.getTypeName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getLanguages() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (DescLanguage dl : se.getLanguages()) {
			siList.add(new SelectItem(dl.getLanguageCode(), dl.getLanguageName()));
		}
		return siList;
	}

	public List<SelectItem> getUserDescGroups() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (VDescGroup group : se.getUserDescGroups()) {
			siList.add(new SelectItem(group.getDescriptorGroupId(), group.getGroupName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getUserGroups() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (AdmGroup ag : se.getUserGroups()) {
			siList.add(new SelectItem(ag.getGroupId(), ag.getGroupName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	/**
	 * Возвращает список архивов для отображения в элементе Combobox
	 *
	 * @return список элементов для Combobox
	 */
	public List<SelectItem> getArchives2() {
		return SelectItemUtils.withEmpty(se.getDescValuesByDescriptorAttribute(Arrays.asList("AUTHORITY", "ARCHIVE")));
	}

	public List<SelectItem> getUserTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("USER_TYPE"));
	}

	public List<SelectItem> getCreatableUserTypes() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (VDescValueWithCode val : se.getLinearValuesByCode("USER_TYPE")) {
			VDescAttrvalueWithCode attrval = se.getAttrValueByCode(val.getDescriptorValueId(), "CREATABLE_BY_ADMIN");
			if (Boolean.valueOf(attrval.getAttrValue())) {
				siList.add(new SelectItem(val.getDescriptorValueId(), val.getFullValue()));
			}
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getPositions() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("POSITION"));
	}

	public List<SelectItem> getDescValuesByCode(String code) {
		return getDescValuesByList(se.getLinearValuesByCode(code));
	}

	public List<SelectItem> getDescValuesByCodeForCardLink(String code, String descriptionValueCode) {
		return getDescValuesByList(se.getLinearValuesByCodeForCardLink(code, descriptionValueCode));
	}

	public List<SelectItem> getDescValuesByList(List<VDescValueWithCode> list) {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (VDescValueWithCode val : list) {
			siList.add(new SelectItem(val.getDescriptorValueId(), val.getFullValue()));
		}
		return siList;
	}

	public List<SelectItem> getDescAttrValuesByList(List<VDescAttrvalueWithCode> list) {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (VDescAttrvalueWithCode attrVal : list) {
			siList.add(new SelectItem(attrVal.getRefDescriptorValueId(),
					se.getDescValue(attrVal.getRefDescriptorValueId()).getFullValue()));
		}
		return siList;
	}

	public List<SelectItem> getSubsystems() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (VDescSubsystem ss : se.getSubsystemModel()) {
			siList.add(new SelectItem(ss.getSubsystemNumber(), ss.getSubsystemName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getFundTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("FUND_TYPE"));
	}

	public List<SelectItem> getDocumentKinds() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("DOCUMENT_KIND"));
	}

	public List<SelectItem> getDescRelationTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("DESC_RELATION_TYPE"));
	}

	public List<SelectItem> getReproductionTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("REPRODUCTION_TYPE"));
	}

	public List<SelectItem> getDocLanguages() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("DOC_LANGUAGE"));
	}

	public List<SelectItem> getDescLevels() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("DESC_LEVEL"));
	}

	public List<SelectItem> getBaseMaterials() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("BASE_MATERIAL"));
	}

	public List<SelectItem> getAuthenticityTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("AUTHENTICITY_TYPE"));
	}

	public List<SelectItem> getSingleDocumentTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("SINGLE_DOCUMENT_TYPE"));
	}

	public List<SelectItem> getEditionTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("EDITION_TYPE"));
	}

	public List<SelectItem> getUnitTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("UNIV_UNIT_TYPE"));
	}

	public List<SelectItem> getBookSeries() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (ArchEbookSeries series : ebookQuery.querySeriesList()) {
			siList.add(new SelectItem(series.getSeriesId(), series.getSeriesName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getPortalSections() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("PORTAL_SECTION"));
	}

	public List<SelectItem> getArchives() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		List<VComplexArchiveList> dvArchives = se.getArchives();
		for (VComplexArchiveList val : dvArchives) {
			siList.add(new SelectItem(val.getArchiveId(), val.getArchiveName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getChangedEntityType() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("CHANGED_ENTITY_TYPE"));
	}

	public List<SelectItem> getNsaGroupsDescriptors() {
		List<VDescGroup> userGroups = se.getUserDescGroups();
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (VDescGroup val : userGroups) {
			siList.add(new SelectItem(val.getDescriptorGroupId(), val.getGroupName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getOperationType() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("OPERATION_TYPE"));
	}

	public List<SelectItem> getUsers() {
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (AdmUser val : se.getUsers()) {
			siList.add(new SelectItem(val.getUserId(), val.getDisplayedName()));
		}
		return SelectItemUtils.withEmpty(siList);
	}

	public List<SelectItem> getWebContentTypes(String contentClass) {
		DescriptorValue contentClassValue = se.getDescValueByCodes("WEB_CONTENT_CLASS", contentClass);
		return getDescAttrValuesByList(se.getAttrValuesByCode(
				contentClassValue.getDescriptorValueId(), "CONTENT_TYPES_ALLOWED"));
	}

	public List<SelectItem> getRecordTypes() {
		return getDescValuesByCode("RECORD_TYPE");
	}

	public List<SelectItem> getRecordTypesForSearch() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("RECORD_TYPE"));
	}

	public List<SelectItem> getPhonodocTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("PHONODOC_TYPE"));
	}

	public List<SelectItem> getVideodocTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("VIDEODOC_TYPE"));
	}

	public List<SelectItem> getKinodocTypes() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("KINODOC_TYPE"));
	}

	public List<SelectItem> getProductionCountries() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("COUNTRY_PRODUCTION"));
	}

	public List<SelectItem> getStorageTypes(Long phonodocTypeId) {
		if (phonodocTypeId == null) {
			return null;
		}

		List<SelectItem> lsi = getDescValuesByCode("STORAGE_TYPE");
		List<SelectItem> deleteList = new ArrayList<SelectItem>();
		for (SelectItem si : lsi) {
			boolean b = false;
			List<VDescAttrvalueWithCode> attrValues
					= se.getAttrValuesByCode((Long) si.getValue(), "STORAGE_PHONODOC_TYPE");
			for (VDescAttrvalueWithCode attrValue : attrValues) {
				if (phonodocTypeId.equals(attrValue.getRefDescriptorValueId())) {
					b = true;
					break;
				}
			}
			if (!b) {
				deleteList.add(si);
			}
		}
		lsi.removeAll(deleteList);
		return SelectItemUtils.withEmpty(lsi);
	}

	public List<SelectItem> getChannelCounts() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("CHANNEL_COUNT"));
	}

	public List<SelectItem> getSoundQualities() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("SOUND_QUALITY"));
	}

	public List<SelectItem> getSounds() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("VIDEO_SOUND"));
	}

	public List<SelectItem> getColors() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("CHROMATICITY"));
	}

	public List<SelectItem> getRecordSystems() {
		return SelectItemUtils.withEmpty(getDescValuesByCode("SYSTEM_RECORD"));
	}
}
