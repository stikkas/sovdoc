package ru.insoft.sovdoc.system;

import java.math.BigDecimal;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import ru.insoft.sovdoc.model.adm.table.AdmAccessRule;
import ru.insoft.sovdoc.model.adm.table.AdmAccessRule_;
import ru.insoft.sovdoc.model.adm.table.AdmGroup;
import ru.insoft.sovdoc.model.adm.table.AdmUser;
import ru.insoft.sovdoc.model.complex.view.VComplexArchiveList;
import ru.insoft.sovdoc.model.desc.table.CoreParameter;
import ru.insoft.sovdoc.model.desc.table.CoreParameter_;
import ru.insoft.sovdoc.model.desc.table.DescDatatype;
import ru.insoft.sovdoc.model.desc.table.DescDatatype_;
import ru.insoft.sovdoc.model.desc.table.DescLanguage;
import ru.insoft.sovdoc.model.desc.table.DescLanguage_;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup_;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.table.DescriptorValueAttr;
import ru.insoft.sovdoc.model.desc.table.DescriptorValueAttr_;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue_;
import ru.insoft.sovdoc.model.desc.view.VDescAttrvalueWithCode;
import ru.insoft.sovdoc.model.desc.view.VDescAttrvalueWithCode_;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescGroup_;
import ru.insoft.sovdoc.model.desc.view.VDescSubsystem;
import ru.insoft.sovdoc.model.desc.view.VDescSubsystem_;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.model.desc.view.VDescValuePath;
import ru.insoft.sovdoc.model.desc.view.VDescValuePath_;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode_;
import ru.insoft.sovdoc.model.desc.view.VDescValue_;
import ru.insoft.sovdoc.ui.desc.MultilingualTable;
import ru.insoft.sovdoc.ui.desc.MultilingualView;

import com.google.common.collect.Lists;
import javax.faces.model.SelectItem;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss;

@RequestScoped
@Named
public class SystemEntity {

	@Inject
	EntityManager em;

	protected <T> SingularAttribute<? super T, ?> getColumnByName(String name, Class<T> clazz) {
		return em.getMetamodel().entity(clazz).getSingularAttribute(name);
	}

	public EntityManager getEntityManager() {
		return em;
	}

	public VDescSubsystem getSubsystem(Long number) {
		if (number == null) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescSubsystem> cq = cb.createQuery(VDescSubsystem.class);
		Root<VDescSubsystem> root = cq.from(VDescSubsystem.class);
		cq.select(root).where(cb.equal(root.get(VDescSubsystem_.subsystemNumber), number));
		return em.createQuery(cq).getSingleResult();
	}

	public List<DescDatatype> getDatatypes() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescDatatype> cq = cb.createQuery(DescDatatype.class);
		Root<DescDatatype> root = cq.from(DescDatatype.class);
		cq.select(root).orderBy(cb.asc(root.get(DescDatatype_.sortOrder)));
		return em.createQuery(cq).getResultList();
	}

	public List<VDescSubsystem> getSubsystemModel() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescSubsystem> cq = cb.createQuery(VDescSubsystem.class);
		cq.select(cq.from(VDescSubsystem.class));
		return (List<VDescSubsystem>) em.createQuery(cq).getResultList();
	}

	public List<VDescGroup> getDescGroups(Long subsystem) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescGroup> cq = cb.createQuery(VDescGroup.class);
		Root<VDescGroup> root = cq.from(VDescGroup.class);
		cq.select(root).orderBy(cb.asc(root.get(VDescGroup_.sortOrder)));
		if (subsystem != null) {
			cq.where(cb.equal(root.get(VDescGroup_.subsystemNumber), subsystem));
		}
		return em.createQuery(cq).getResultList();
	}

	public List<VDescGroup> getUserDescGroups() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescGroup> cq = cb.createQuery(VDescGroup.class);
		Root<VDescGroup> root = cq.from(VDescGroup.class);
		cq.select(root).orderBy(cb.asc(root.get(VDescGroup_.sortOrder)));
		cq.where(cb.equal(root.get(VDescGroup_.isSystem), false));
		return em.createQuery(cq).getResultList();
	}

	public DescriptorGroup getDescGroup(Long groupId) {
		if (groupId == null || groupId.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroup> cq = cb.createQuery(DescriptorGroup.class);
		Root<DescriptorGroup> root = cq.from(DescriptorGroup.class);
		cq.select(root).where(cb.equal(root.get(DescriptorGroup_.descriptorGroupId), groupId));
		return em.createQuery(cq).getSingleResult();
	}

	public DescriptorGroup getDescGroupByCode(String code) {
		if (code == null || code.isEmpty()) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroup> cq = cb.createQuery(DescriptorGroup.class);
		Root<DescriptorGroup> root = cq.from(DescriptorGroup.class);
		cq.select(root).where(cb.equal(root.get(DescriptorGroup_.groupCode), code));
		return em.createQuery(cq).getSingleResult();
	}

	public VDescGroup getImmDescGroup(Long groupId) {
		if (groupId == null || groupId.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescGroup> cq = cb.createQuery(VDescGroup.class);
		Root<VDescGroup> root = cq.from(VDescGroup.class);
		cq.select(root).where(cb.equal(root.get(VDescGroup_.descriptorGroupId), groupId));
		return em.createQuery(cq).getSingleResult();
	}

	public VDescGroup getImmGroupByValueId(Long valueId) {
		if (valueId == null || valueId.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<VDescValue> root = cq.from(VDescValue.class);
		cq.select(root.get(VDescValue_.descriptorGroupId));
		cq.where(cb.equal(root.get(VDescValue_.descriptorValueId), valueId));
		return getImmDescGroup(em.createQuery(cq).getSingleResult());
	}

	/*public VDescValueHierarch getValueHierarchById(Long id)
	 {
	 if (id == null || id.equals(0L))
	 return null;
		
	 CriteriaBuilder cb = em.getCriteriaBuilder();
	 CriteriaQuery<VDescValueHierarch> cq = cb.createQuery(VDescValueHierarch.class);
	 Root<VDescValueHierarch> root = cq.from(VDescValueHierarch.class);
	 cq.select(root).where(cb.equal(root.get(VDescValueHierarch_.descriptorValueId), id));
	 return em.createQuery(cq).getSingleResult();
	 }*/
	public VDescValuePath getValuePath(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescValuePath> cq = cb.createQuery(VDescValuePath.class);
		Root<VDescValuePath> root = cq.from(VDescValuePath.class);
		cq.select(root).where(cb.equal(root.get(VDescValuePath_.descriptorValueId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public Integer getValueFpRowNum(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		return ((BigDecimal) em.createNativeQuery(
				"select DESCRIPTOR_PACK.GET_FULL_PATH_ROWNUM(:p_value_id) from dual")
				.setParameter("p_value_id", id)
				.getSingleResult())
				.intValue();
	}

	public Integer getValueSpRowNum(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		return ((BigDecimal) em.createNativeQuery(
				"select DESCRIPTOR_PACK.GET_SHORT_PATH_ROWNUM(:p_value_id) from dual")
				.setParameter("p_value_id", id)
				.getSingleResult())
				.intValue();
	}

	public Integer getOrderRowNum(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		return ((BigDecimal) em.createNativeQuery(
				"select DESCRIPTOR_PACK.GET_ORDER_ROWNUM(:p_value_id) from dual")
				.setParameter("p_value_id", id)
				.getSingleResult())
				.intValue();
	}

	public VDescValue getImmDescValue(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescValue> cq = cb.createQuery(VDescValue.class);
		Root<VDescValue> root = cq.from(VDescValue.class);
		cq.select(root).where(cb.equal(root.get(VDescValue_.descriptorValueId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public DescriptorValue getDescValue(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		cq.select(root).where(cb.equal(root.get(DescriptorValue_.descriptorValueId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VDescValueWithCode getImmDescValueByCodes(String groupCode, String valueCode) {
		if (groupCode == null || groupCode.isEmpty() || valueCode == null || valueCode.isEmpty()) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescValueWithCode> cq = cb.createQuery(VDescValueWithCode.class);
		Root<VDescValueWithCode> root = cq.from(VDescValueWithCode.class);
		cq.select(root).where(cb.and(
				cb.equal(root.get(VDescValueWithCode_.groupCode), groupCode),
				cb.equal(root.get(VDescValueWithCode_.valueCode), valueCode)));
		return em.createQuery(cq).getSingleResult();
	}

	public DescriptorValue getDescValueByCodes(String groupCode, String valueCode) {
		if (groupCode == null || groupCode.isEmpty() || valueCode == null || valueCode.isEmpty()) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		Join<DescriptorValue, DescriptorGroup> join = root.join(DescriptorValue_.descriptorGroup);
		cq.select(root).where(cb.and(
				cb.equal(join.get(DescriptorGroup_.groupCode), groupCode),
				cb.equal(root.get(DescriptorValue_.valueCode), valueCode)));
		return em.createQuery(cq).getSingleResult();
	}

	public List<DescriptorValue> getChildDescValues(Long groupId, Long parentId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);

		List<Predicate> predicates = Lists.newArrayList();
		if (groupId != null) {
			predicates.add(cb.equal(root.get(DescriptorValue_.descriptorGroupId), groupId));
		}
		if (parentId != null) {
			predicates.add(cb.equal(root.get(DescriptorValue_.parentValueId), parentId));
		} else {
			predicates.add(cb.isNull(root.get(DescriptorValue_.parentValueId)));
		}
		cq.select(root).where(cb.and(predicates.toArray(new Predicate[0])));
		cq.orderBy(cb.asc(root.get(DescriptorValue_.sortOrder)));
		return em.createQuery(cq).getResultList();
	}

	public DescriptorValueAttr getDescValueAttr(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValueAttr> cq = cb.createQuery(DescriptorValueAttr.class);
		Root<DescriptorValueAttr> root = cq.from(DescriptorValueAttr.class);
		cq.select(root).where(cb.equal(root.get(DescriptorValueAttr_.descriptorValueAttrId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public String getSystemParameterValue(String code) {
		if (code == null || code.isEmpty()) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<String> cq = cb.createQuery(String.class);
		Root<CoreParameter> root = cq.from(CoreParameter.class);
		cq.select(root.get(CoreParameter_.parameterValue));
		cq.where(cb.equal(root.get(CoreParameter_.parameterCode), code));
		return em.createQuery(cq).getSingleResult();
	}

	/**
	 * Находждение дескрипторов, которые в качестве атрибута имеют дескриптор с
	 * кодом. Возвращаемый список отсортирован по порядковому номеру.
	 *
	 * @param code код дескриптора
	 * @return список элементов для combobox
	 */
	public List<SelectItem> getDescValuesByDescriptorAttribute(String code) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SelectItem> cq = cb.createQuery(SelectItem.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		Join<DescriptorValue, DescriptorValueAttr> vaJoin = root.join(DescriptorValue_.valueAttrList);
		Join<DescriptorValueAttr, DescriptorValue> avJoin = vaJoin.join(DescriptorValueAttr_.refDescriptorValue);
		cq.select(cb.construct(SelectItem.class,
				root.get(DescriptorValue_.descriptorValueId), root.get(DescriptorValue_.fullValue)))
				.where(cb.equal(avJoin.get(DescriptorValue_.valueCode), code))
				.orderBy(cb.asc(root.get(DescriptorValue_.sortOrder)));
		return em.createQuery(cq).getResultList();
	}

	/**
	 * Находждение дескрипторов, которые в качестве атрибута имеют дескриптор с
	 * кодомами. Возвращаемый список отсортирован по порядковому номеру.
	 *
	 * @param codes коды дескрипторов
	 * @return список элементов для combobox
	 */
	public List<SelectItem> getDescValuesByDescriptorAttribute(List<String> codes) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SelectItem> cq = cb.createQuery(SelectItem.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		Join<DescriptorValue, DescriptorValueAttr> vaJoin = root.join(DescriptorValue_.valueAttrList);
		Join<DescriptorValueAttr, DescriptorValue> avJoin = vaJoin.join(DescriptorValueAttr_.refDescriptorValue);
		cq.select(cb.construct(SelectItem.class,
				root.get(DescriptorValue_.descriptorValueId), root.get(DescriptorValue_.fullValue)))
				.where(avJoin.get(DescriptorValue_.valueCode).in(codes))
				.orderBy(cb.asc(root.get(DescriptorValue_.sortOrder)));
		return em.createQuery(cq).getResultList();
	}

	public <MT extends MultilingualTable> List<MT>
			getMultilingualData(Long rootId, Class<MT> clazz) {
		if (rootId == null || rootId.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<MT> cq = cb.createQuery(clazz);
		Root<MT> root = cq.from(clazz);
		cq.select(root).where(cb.equal(root.get(getColumnByName("rootId", clazz)), rootId));
		if (Lists.newArrayList(clazz.getInterfaces()).contains(MultilingualView.class)) {
			cq.orderBy(cb.asc(root.get(getColumnByName("sortOrder", clazz))));
		}
		return em.createQuery(cq).getResultList();
	}

	public List<DescLanguage> getLanguages() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescLanguage> cq = cb.createQuery(DescLanguage.class);
		Root<DescLanguage> root = cq.from(DescLanguage.class);
		cq.select(root).orderBy(cb.asc(root.get(DescLanguage_.sortOrder)));
		return em.createQuery(cq).getResultList();
	}

	public DescLanguage getLanguage(String code) {
		if (code == null) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescLanguage> cq = cb.createQuery(DescLanguage.class);
		Root<DescLanguage> root = cq.from(DescLanguage.class);
		cq.select(root).where(cb.equal(root.get(DescLanguage_.languageCode), code));
		return em.createQuery(cq).getSingleResult();
	}

	public List<VDescValueWithCode> getLinearValuesByCode(String code) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescValueWithCode> cq = cb.createQuery(VDescValueWithCode.class);
		Root<VDescValueWithCode> root = cq.from(VDescValueWithCode.class);
		cq.select(root).where(cb.equal(root.get(VDescValueWithCode_.groupCode), code));
		cq.orderBy(cb.asc(root.get(VDescValueWithCode_.sortOrder)));
		return em.createQuery(cq).getResultList();
	}

	public List<VDescValueWithCode> getLinearValuesByCodeForCardLink(String code, String descriptionValueCode) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		String descValueCode = null;
		String descValueCode1 = null;
		if (descriptionValueCode.equals("SERIES")) {
			descValueCode = "SERIES";
		}
		if (descriptionValueCode.equals("STORAGE_UNIT") || descriptionValueCode.equals("DOCUMENT")) {
			descValueCode = "STORAGE_UNIT";
			descValueCode1 = "DOCUMENT";
		}
		CriteriaQuery<VDescValueWithCode> cq = cb.createQuery(VDescValueWithCode.class);

		if (descValueCode != null && descValueCode1 == null) {

			Root<VDescValueWithCode> root = cq.from(VDescValueWithCode.class);
			cq.select(root).where(cb.and(
					cb.equal(root.get(VDescValueWithCode_.groupCode), code),
					cb.equal(root.get(VDescValueWithCode_.valueCode), descValueCode)));

			cq.orderBy(cb.asc(root.get(VDescValueWithCode_.sortOrder)));

		}
		if (descValueCode != null && descValueCode1 != null) {

			Root<VDescValueWithCode> root = cq.from(VDescValueWithCode.class);
			cq.select(root).where(cb.or(
					cb.and(cb.equal(root.get(VDescValueWithCode_.groupCode), code),
							cb.equal(root.get(VDescValueWithCode_.valueCode), descValueCode)),
					cb.and(cb.equal(root.get(VDescValueWithCode_.groupCode), code),
							cb.equal(root.get(VDescValueWithCode_.valueCode), descValueCode1))));

			cq.orderBy(cb.asc(root.get(VDescValueWithCode_.sortOrder)));

		}

		return em.createQuery(cq).getResultList();
	}

	public List<VDescAttrvalueWithCode> getAttrValuesByCode(Long valueId, String attrCode) {
		if (valueId == null || valueId.equals(0L) || attrCode == null || attrCode.isEmpty()) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescAttrvalueWithCode> cq = cb.createQuery(VDescAttrvalueWithCode.class);
		Root<VDescAttrvalueWithCode> root = cq.from(VDescAttrvalueWithCode.class);
		cq.select(root).where(cb.and(
				cb.equal(root.get(VDescAttrvalueWithCode_.descriptorValueId), valueId),
				cb.equal(root.get(VDescAttrvalueWithCode_.attrCode), attrCode)));
		return em.createQuery(cq).getResultList();
	}

	public VDescAttrvalueWithCode getAttrValueByCode(Long valueId, String attrCode) {
		List<VDescAttrvalueWithCode> list = getAttrValuesByCode(valueId, attrCode);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	public List<AdmGroup> getUserGroups() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmGroup> cq = cb.createQuery(AdmGroup.class);
		cq.select(cq.from(AdmGroup.class));
		return em.createQuery(cq).getResultList();
	}

	public List<AdmUser> getUsers() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUser> cq = cb.createQuery(AdmUser.class);
		cq.select(cq.from(AdmUser.class));
		return em.createQuery(cq).getResultList();
	}

	public List<VComplexArchiveList> getArchives() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexArchiveList> cq = cb.createQuery(VComplexArchiveList.class);
		Root<VComplexArchiveList> root = cq.from(VComplexArchiveList.class);
		cq.select(root);
		return em.createQuery(cq).getResultList();
	}

	public List<AdmAccessRule> getAccessRules(Long subsystem) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmAccessRule> cq = cb.createQuery(AdmAccessRule.class);
		Root<AdmAccessRule> root = cq.from(AdmAccessRule.class);
		cq.select(root);
		if (subsystem != null) {
			cq.where(cb.equal(root.get(AdmAccessRule_.subsystemNumber), subsystem));
		}
		return em.createQuery(cq).getResultList();
	}

	public List<DescriptorValue> getValuesIdByGroupId(Long groupId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		cq.select(root).where(cb.equal(root.get(DescriptorValue_.descriptorGroupId), groupId));
		return em.createQuery(cq).getResultList();
	}

	public VDescValueUnivclss getUnivClssValue(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		return em.find(VDescValueUnivclss.class, id);
	}
}
