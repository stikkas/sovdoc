package ru.insoft.sovdoc.system.ebook;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit_;
import ru.insoft.sovdoc.model.ebook.table.ArchEbook;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookSeries;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookSeries_;
import ru.insoft.sovdoc.model.ebook.table.ArchEbook_;
import ru.insoft.sovdoc.model.ebook.view.VEbook;
import ru.insoft.sovdoc.model.ebook.view.VEbook_;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.system.SystemEntity;

@RequestScoped
@Named("ebookQuery")
public class SystemQuery {

	@Inject
	EntityManager em;
	@Inject
	SystemEntity se;
	@Inject
	ru.insoft.sovdoc.system.complex.SystemQuery complexQuery;
	
	public ArchEbook queryEbookModel(Long id)
	{
		if (id == null || id.equals(0L))
			return null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchEbook> cq = cb.createQuery(ArchEbook.class);
		Root<ArchEbook> root = cq.from(ArchEbook.class);
		cq.select(root).where(cb.equal(root.get(ArchEbook_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public VEbook queryEbookImmModel(Long id)
	{
		if (id == null || id.equals(0L))
			return null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VEbook> cq = cb.createQuery(VEbook.class);
		Root<VEbook> root = cq.from(VEbook.class);
		cq.select(root).where(cb.equal(root.get(VEbook_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public int queryMaxRegNumber()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
		Root<UnivDataUnit> root = cq.from(UnivDataUnit.class);
		cq.select(cb.max(root.get(UnivDataUnit_.numberNumber)));
		cq.where(cb.equal(root.get(UnivDataUnit_.unitTypeId), 
				se.getImmDescValueByCodes("UNIV_UNIT_TYPE", "EBOOK").getDescriptorValueId()));
		try
		{
			Integer res = em.createQuery(cq).getSingleResult();
			return res == null ? 0 : res;
		}
		catch (NoResultException e)
		{
			return 0;
		}
	}
	
	public List<ArchEbookSeries> querySeriesList()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchEbookSeries> cq = cb.createQuery(ArchEbookSeries.class);
		Root<ArchEbookSeries> root = cq.from(ArchEbookSeries.class);
		cq.select(root);
		return em.createQuery(cq).getResultList();
	}
	
	public ArchEbookSeries querySeries(Long id)
	{
		if (id == null || id.equals(0L))
			return null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchEbookSeries> cq = cb.createQuery(ArchEbookSeries.class);
		Root<ArchEbookSeries> root = cq.from(ArchEbookSeries.class);
		cq.select(root).where(cb.equal(root.get(ArchEbookSeries_.seriesId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public ImagePath queryBookCover(Long unitId)
	{
		List<ImagePath> ipList = complexQuery.queryImagePathList(unitId, "BOOK_COVER");
		if (ipList.isEmpty())
			return null;
		else
			return ipList.get(0);
	}
}
