package ru.insoft.sovdoc.system;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import ru.insoft.sovdoc.model.web.table.WebImages;
import ru.insoft.sovdoc.model.web.table.WebImages_;
import ru.insoft.sovdoc.model.web.table.WebNews;
import ru.insoft.sovdoc.model.web.table.WebNews_;
import ru.insoft.sovdoc.model.web.table.WebPages;
import ru.insoft.sovdoc.model.web.table.WebPagesLocalized;
import ru.insoft.sovdoc.model.web.table.WebPagesLocalized_;
import ru.insoft.sovdoc.model.web.table.WebPages_;
import ru.insoft.sovdoc.model.web.view.VWebPageMultipleType;
import ru.insoft.sovdoc.model.web.view.VWebPageMultipleType_;
import ru.insoft.sovdoc.model.web.view.VWebPageMultipleValue;
import ru.insoft.sovdoc.model.web.view.VWebPageMultipleValue_;
import ru.insoft.sovdoc.model.web.view.VWebPageSingle;
import ru.insoft.sovdoc.model.web.view.VWebPageSingle_;

@RequestScoped
@Named
public class WebQuery {
	
	@Inject
	EntityManager em;
	
	public EntityManager getEntityManager()
	{
		return em;
	}
	
	public List<VWebPageSingle> getSingleWebPages()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VWebPageSingle> cq = cb.createQuery(VWebPageSingle.class);
		Root<VWebPageSingle> root = cq.from(VWebPageSingle.class);
		cq.select(root).orderBy(cb.asc(root.get(VWebPageSingle_.pageType)));
		return em.createQuery(cq).getResultList();
	}
	
	public List<VWebPageMultipleType> getMultipleWebPageTypes()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VWebPageMultipleType> cq = cb.createQuery(VWebPageMultipleType.class);
		Root<VWebPageMultipleType> root = cq.from(VWebPageMultipleType.class);
		cq.select(root).orderBy(cb.asc(root.get(VWebPageMultipleType_.pageType)));
		return em.createQuery(cq).getResultList();
	}
	
	public List<VWebPageMultipleValue> getMultipleWebPagesByType(Long pageTypeId)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VWebPageMultipleValue> cq = cb.createQuery(VWebPageMultipleValue.class);
		Root<VWebPageMultipleValue> root = cq.from(VWebPageMultipleValue.class);
		cq.select(root).orderBy(cb.asc(root.get(VWebPageMultipleValue_.sortOrder)));
		cq.where(cb.equal(root.get(VWebPageMultipleValue_.pageTypeId), pageTypeId));
		return em.createQuery(cq).getResultList();
	}
	
	public WebPages getWebPage(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebPages> cq = cb.createQuery(WebPages.class);
		Root<WebPages> root = cq.from(WebPages.class);
		cq.select(root).where(cb.equal(root.get(WebPages_.pageId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public VWebPageMultipleValue getPageInfo(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VWebPageMultipleValue> cq = cb.createQuery(VWebPageMultipleValue.class);
		Root<VWebPageMultipleValue> root = cq.from(VWebPageMultipleValue.class);
		cq.select(root).where(cb.equal(root.get(VWebPageMultipleValue_.pageId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public WebNews getWebNews(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebNews> cq = cb.createQuery(WebNews.class);
		Root<WebNews> root = cq.from(WebNews.class);
		cq.select(root).where(cb.equal(root.get(WebNews_.newsId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public WebPagesLocalized getPageLocale(Long id, String type, String language, Long contentTypeId)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebPagesLocalized> cq = cb.createQuery(WebPagesLocalized.class);
		Root<WebPagesLocalized> root = cq.from(WebPagesLocalized.class);
		
		Predicate predicate = null;
		if (type.equals("page"))
			predicate = cb.equal(root.get(WebPagesLocalized_.pageId), id);
		if (type.equals("news"))
			predicate = cb.equal(root.get(WebPagesLocalized_.newsId), id);
		cq.select(root).where(cb.and(cb.equal(root.get(WebPagesLocalized_.languageCode), language)), 
				cb.equal(root.get(WebPagesLocalized_.contentTypeId), contentTypeId), predicate);
		try
		{
			return em.createQuery(cq).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}
	
	public List<Long> getWebImageIds()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<WebImages> root = cq.from(WebImages.class);
		cq.select(root.get(WebImages_.imageId));
		return em.createQuery(cq).getResultList();
	}
	
	public WebImages getWebImage(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebImages> cq = cb.createQuery(WebImages.class);
		Root<WebImages> root = cq.from(WebImages.class);
		cq.select(root).where(cb.equal(root.get(WebImages_.imageId), id));
		return em.createQuery(cq).getSingleResult();
	}

}
