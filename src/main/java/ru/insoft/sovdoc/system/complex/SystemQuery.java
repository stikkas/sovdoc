package ru.insoft.sovdoc.system.complex;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import ru.insoft.sovdoc.model.complex.table.ArchDocument;
import ru.insoft.sovdoc.model.complex.table.ArchDocument_;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchFund_;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchSeries_;
import ru.insoft.sovdoc.model.complex.table.ArchStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchStorageUnit_;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit_;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval_;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor_;
import ru.insoft.sovdoc.model.complex.view.VComplexArchiveHierarchy;
import ru.insoft.sovdoc.model.complex.view.VComplexArchiveHierarchy_;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit_;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor_;
import ru.insoft.sovdoc.model.complex.view.VComplexDocument;
import ru.insoft.sovdoc.model.complex.view.VComplexDocument_;
import ru.insoft.sovdoc.model.complex.view.VComplexFund;
import ru.insoft.sovdoc.model.complex.view.VComplexFund_;
import ru.insoft.sovdoc.model.complex.view.VComplexSeries;
import ru.insoft.sovdoc.model.complex.view.VComplexSeries_;
import ru.insoft.sovdoc.model.complex.view.VComplexStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexStorageUnit_;
import ru.insoft.sovdoc.model.complex.view.VComplexUnitHierarchy;
import ru.insoft.sovdoc.model.complex.view.VComplexUnitHierarchy_;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue_;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.model.showfile.ImagePath_;

import com.google.common.collect.Lists;
import ru.insoft.sovdoc.model.complex.table.ApplicationToDocument;
import ru.insoft.sovdoc.model.complex.table.ApplicationToDocument_;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStorageUnit_;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStoryDescription_;
import ru.insoft.sovdoc.model.complex.table.ArchKinodoc;
import ru.insoft.sovdoc.model.complex.table.ArchPerformanceAnnotation;
import ru.insoft.sovdoc.model.complex.table.ArchPerformanceAnnotation_;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit_;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStorageUnit_;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStoryDescription_;
import ru.insoft.sovdoc.model.complex.table.ArchVideodoc;
import ru.insoft.sovdoc.model.complex.table.UnivCardLinks;
import ru.insoft.sovdoc.model.complex.table.UnivCardLinks_;
import ru.insoft.sovdoc.model.complex.view.VComplexCardLinks;
import ru.insoft.sovdoc.model.complex.view.VComplexCardLinks_;
import ru.insoft.sovdoc.model.complex.view.VComplexKinodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexKinodoc_;
import ru.insoft.sovdoc.model.complex.view.VComplexPhonodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexPhonodoc_;
import ru.insoft.sovdoc.model.complex.view.VComplexVideodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexVideodoc_;

@RequestScoped
@Named("complexQuery")
public class SystemQuery {

	@Inject
	EntityManager em;

	public UnivDataUnit queryModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UnivDataUnit> cq = cb.createQuery(UnivDataUnit.class);
		Root<UnivDataUnit> root = cq.from(UnivDataUnit.class);
		cq.select(root).where(cb.equal(root.get(UnivDataUnit_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexDataUnit queryImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexDataUnit> cq = cb.createQuery(VComplexDataUnit.class);
		Root<VComplexDataUnit> root = cq.from(VComplexDataUnit.class);
		cq.select(root).where(cb.equal(root.get(VComplexDataUnit_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public List<VComplexUnitHierarchy> queryHierarchy(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexUnitHierarchy> cq = cb.createQuery(VComplexUnitHierarchy.class);
		Root<VComplexUnitHierarchy> root = cq.from(VComplexUnitHierarchy.class);
		cq.select(root).where(cb.equal(root.get(VComplexUnitHierarchy_.univDataUnitId), id));
		cq.orderBy(cb.desc(root.get(VComplexUnitHierarchy_.parentLevel)));
		return em.createQuery(cq).getResultList();
	}

	public List<Long> queryArchiveHierarchy(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<VComplexArchiveHierarchy> root = cq.from(VComplexArchiveHierarchy.class);
		cq.select(root.get(VComplexArchiveHierarchy_.parentValueId));
		cq.where(cb.equal(root.get(VComplexArchiveHierarchy_.childValueId), id));
		cq.orderBy(cb.desc(root.get(VComplexArchiveHierarchy_.valueLevel)));
		return em.createQuery(cq).getResultList();
	}

	public ArchFund queryFundModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchFund> cq = cb.createQuery(ArchFund.class);
		Root<ArchFund> root = cq.from(ArchFund.class);
		cq.select(root).where(cb.equal(root.get(ArchFund_.dataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexFund queryFundImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexFund> cq = cb.createQuery(VComplexFund.class);
		Root<VComplexFund> root = cq.from(VComplexFund.class);
		cq.select(root).where(cb.equal(root.get(VComplexFund_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public UnivDateInterval queryDateInterval(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UnivDateInterval> cq = cb.createQuery(UnivDateInterval.class);
		Root<UnivDateInterval> root = cq.from(UnivDateInterval.class);
		cq.select(root).where(cb.equal(root.get(UnivDateInterval_.dateInetrvalId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public UnivDescriptor queryUnivDescriptor(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UnivDescriptor> cq = cb.createQuery(UnivDescriptor.class);
		Root<UnivDescriptor> root = cq.from(UnivDescriptor.class);
		cq.select(root).where(cb.equal(root.get(UnivDescriptor_.univDescriptorId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public DescriptorValue queryDescriptorValue(Long descriptorValueId) {
		if (descriptorValueId == null) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		cq.select(root).where(cb.equal(root.get(DescriptorValue_.descriptorValueId), descriptorValueId));
		return em.createQuery(cq).getSingleResult();
	}

	public DescriptorValue queryDescriptorFullValueReverse(String descriptorFullValue) {
		if (descriptorFullValue == null) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		cq.select(root).where(cb.equal(root.get(DescriptorValue_.fullValue), descriptorFullValue));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexDescriptor queryImmUnivDescriptor(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexDescriptor> cq = cb.createQuery(VComplexDescriptor.class);
		Root<VComplexDescriptor> root = cq.from(VComplexDescriptor.class);
		cq.select(root).where(cb.equal(root.get(VComplexDescriptor_.univDescriptorId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexDescriptor queryDescriptor1(String desc1, UnivDataUnit univDataUnit) {
		if (desc1 == null || desc1.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexDescriptor> cq = cb.createQuery(VComplexDescriptor.class);
		Root<VComplexDescriptor> root = cq.from(VComplexDescriptor.class);
		cq.select(root).where(cb.equal(root.get(VComplexDescriptor_.dataUnit), univDataUnit)).
				where(cb.equal(root.get(VComplexDescriptor_.descriptor1), desc1));

		return em.createQuery(cq).getSingleResult();
	}

	public ArchSeries querySeriesModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchSeries> cq = cb.createQuery(ArchSeries.class);
		Root<ArchSeries> root = cq.from(ArchSeries.class);
		cq.select(root).where(cb.equal(root.get(ArchSeries_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexSeries querySeriesImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexSeries> cq = cb.createQuery(VComplexSeries.class);
		Root<VComplexSeries> root = cq.from(VComplexSeries.class);
		cq.select(root).where(cb.equal(root.get(VComplexSeries_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public ArchStorageUnit queryStorageUnitModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchStorageUnit> cq = cb.createQuery(ArchStorageUnit.class);
		Root<ArchStorageUnit> root = cq.from(ArchStorageUnit.class);
		cq.select(root).where(cb.equal(root.get(ArchStorageUnit_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexStorageUnit queryStorageUnitImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexStorageUnit> cq = cb.createQuery(VComplexStorageUnit.class);
		Root<VComplexStorageUnit> root = cq.from(VComplexStorageUnit.class);
		cq.select(root).where(cb.equal(root.get(VComplexStorageUnit_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public ArchDocument queryDocumentModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchDocument> cq = cb.createQuery(ArchDocument.class);
		Root<ArchDocument> root = cq.from(ArchDocument.class);
		cq.select(root).where(cb.equal(root.get(ArchDocument_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public List<ApplicationToDocument> queryApplicationToDocumenttModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ApplicationToDocument> cq = cb.createQuery(ApplicationToDocument.class);
		Root<ApplicationToDocument> root = cq.from(ApplicationToDocument.class);
		cq.select(root).where(cb.equal(root.get(ApplicationToDocument_.univDataUnitId), id));
		cq.orderBy(cb.asc(root.get(ApplicationToDocument_.applNumber)));
		return em.createQuery(cq).getResultList();
	}

	public VComplexDocument queryDocumentImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexDocument> cq = cb.createQuery(VComplexDocument.class);
		Root<VComplexDocument> root = cq.from(VComplexDocument.class);
		cq.select(root).where(cb.equal(root.get(VComplexDocument_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public ImagePath queryImagePath(Long unitId, Long fileNum, String category) {
		if (unitId == null || unitId.equals(0L)) {
			return null;
		}

		List<ImagePath> ipList = queryImagePathList(unitId, fileNum, category);
		if (ipList.size() == 0) {
			return null;
		} else {
			return ipList.get(0);
		}
	}

	public List<ImagePath> queryImagePathList(Long unitId, String category) {
		if (unitId == null || unitId.equals(0L)) {
			return null;
		}

		return queryImagePathList(unitId, null, category);
	}

	public List<ImagePath> queryImagePathList(Long unitId) {
		if (unitId == null || unitId.equals(0L)) {
			return null;
		}

		return queryImagePathList(unitId, null, null);
	}

	protected List<ImagePath> queryImagePathList(Long unitId, Long fileNum, String category) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ImagePath> cq = cb.createQuery(ImagePath.class);
		Root<ImagePath> root = cq.from(ImagePath.class);
		List<Predicate> predicates = Lists.newArrayList();
		predicates.add(cb.equal(root.get(ImagePath_.dataUnitId), unitId));
		if (category != null) {
			Join<ImagePath, DescriptorValue> join = root.join(ImagePath_.imageCategory);
			predicates.add(cb.equal(join.get(DescriptorValue_.valueCode), category));
		}
		if (fileNum != null) {
			predicates.add(cb.equal(root.get(ImagePath_.sortOrder), fileNum));
		}
		cq.select(root).where(cb.and(predicates.toArray(new Predicate[0])));
		cq.orderBy(cb.asc(root.get(ImagePath_.sortOrder)));
		return em.createQuery(cq).getResultList();
	}

	public Long queryImagePathCount(Long unitId, DescriptorValue category) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ImagePath> root = cq.from(ImagePath.class);
		cq.select(cb.count(root)).where(cb.and(
				cb.equal(root.get(ImagePath_.dataUnitId), unitId),
				cb.equal(root.get(ImagePath_.imageCategory), category)));
		return em.createQuery(cq).getSingleResult();
	}

	public List<VComplexCardLinks> queryCardLinks(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexCardLinks> cq = cb.createQuery(VComplexCardLinks.class);
		Root<VComplexCardLinks> root = cq.from(VComplexCardLinks.class);
		cq.select(root).where(cb.equal(root.get(VComplexCardLinks_.unitIdOriginal), id));
		cq.orderBy(cb.asc(root.get(VComplexCardLinks_.unitFullName)));
		return em.createQuery(cq).getResultList();
	}

	public UnivCardLinks queryUnitIdLinkedCard(Long cardIdOriginal, Long cardIdLinked) {
		if (cardIdLinked == null || cardIdLinked.equals(0L)) {
			return null;
		}
		if (cardIdOriginal == null || cardIdOriginal.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UnivCardLinks> cq = cb.createQuery(UnivCardLinks.class);
		Root<UnivCardLinks> root = cq.from(UnivCardLinks.class);
		cq.select(root).where(cb.and(
				cb.equal(root.get(UnivCardLinks_.unitIdOriginal), cardIdOriginal),
				cb.equal(root.get(UnivCardLinks_.unitIdLinked), cardIdLinked)));
//  cq.orderBy(cb.asc(root.get(ApplicationToDocument_.applNumber)));
		return em.createQuery(cq).getSingleResult();
	}

	public ArchPhonodoc queryPhonodocModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		return em.find(ArchPhonodoc.class, id);
	}

	public ArchVideodoc queryVideodocModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		return em.find(ArchVideodoc.class, id);
	}

	public ArchKinodoc queryKinodocModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		return em.find(ArchKinodoc.class, id);
	}

	public VComplexPhonodoc queryPhonodocImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexPhonodoc> cq = cb.createQuery(VComplexPhonodoc.class);
		Root<VComplexPhonodoc> root = cq.from(VComplexPhonodoc.class);
		cq.select(root).where(cb.equal(root.get(VComplexPhonodoc_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexVideodoc queryVideodocImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexVideodoc> cq = cb.createQuery(VComplexVideodoc.class);
		Root<VComplexVideodoc> root = cq.from(VComplexVideodoc.class);
		cq.select(root).where(cb.equal(root.get(VComplexVideodoc_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public VComplexKinodoc queryKinodocImmModel(Long id) {
		if (id == null || id.equals(0L)) {
			return null;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexKinodoc> cq = cb.createQuery(VComplexKinodoc.class);
		Root<VComplexKinodoc> root = cq.from(VComplexKinodoc.class);
		cq.select(root).where(cb.equal(root.get(VComplexKinodoc_.univDataUnitId), id));
		return em.createQuery(cq).getSingleResult();
	}

	public ImagePath queryAudioUnitFile(Long storageUnitId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchPhonoStorageUnit> cq = cb.createQuery(ArchPhonoStorageUnit.class);
		Root<ArchPhonoStorageUnit> root = cq.from(ArchPhonoStorageUnit.class);
		cq.select(root).where(cb.equal(root.get(ArchPhonoStorageUnit_.phonoStorageUnitId), storageUnitId));
		return em.createQuery(cq).getSingleResult().getFilePathData();
	}

	public ImagePath queryAudioAnnotaFile(Long annotationId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchPerformanceAnnotation> cq = cb.createQuery(ArchPerformanceAnnotation.class);
		Root<ArchPerformanceAnnotation> root = cq.from(ArchPerformanceAnnotation.class);
		cq.select(root).where(cb.equal(root.get(ArchPerformanceAnnotation_.annotationId), annotationId));
		return em.createQuery(cq).getSingleResult().getFilePathData();
	}

	public ImagePath queryVideoUnitFile(Long storageUnitId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchVideoStorageUnit> cq = cb.createQuery(ArchVideoStorageUnit.class);
		Root<ArchVideoStorageUnit> root = cq.from(ArchVideoStorageUnit.class);
		cq.select(root).where(cb.equal(root.get(ArchVideoStorageUnit_.videoStorageUnitId), storageUnitId));
		return em.createQuery(cq).getSingleResult().getFilePathData();
	}

	public ImagePath queryKinoUnitFile(Long storageUnitId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchKinoStorageUnit> cq = cb.createQuery(ArchKinoStorageUnit.class);
		Root<ArchKinoStorageUnit> root = cq.from(ArchKinoStorageUnit.class);
		cq.select(root).where(cb.equal(root.get(ArchKinoStorageUnit_.kinoStorageUnitId), storageUnitId));
		return em.createQuery(cq).getSingleResult().getFilePathData();
	}

	public ImagePath queryVideoStoryDescFile(Long descriptionId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchVideoStoryDescription> cq = cb.createQuery(ArchVideoStoryDescription.class);
		Root<ArchVideoStoryDescription> root = cq.from(ArchVideoStoryDescription.class);
		cq.select(root).where(cb.equal(root.get(ArchVideoStoryDescription_.storyDescId), descriptionId));
		return em.createQuery(cq).getSingleResult().getFilePathData();
	}

	public ImagePath queryKinoStoryDescFile(Long descriptionId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ArchKinoStoryDescription> cq = cb.createQuery(ArchKinoStoryDescription.class);
		Root<ArchKinoStoryDescription> root = cq.from(ArchKinoStoryDescription.class);
		cq.select(root).where(cb.equal(root.get(ArchKinoStoryDescription_.storyDescId), descriptionId));
		return em.createQuery(cq).getSingleResult().getFilePathData();
	}

}
