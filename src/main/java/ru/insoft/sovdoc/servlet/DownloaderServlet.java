package ru.insoft.sovdoc.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/*
 * Поля для задания выгрузки: Поле архив (список архивов) номер фонда ( и рядом
 * поле литера) номер описи ( и рядом поле литера) 
 * Файл выгрузки должен содержать следующие поля № по порядку название дела дата нач
 * дата конч фонд с литерой опись с литерой дело с литерой Сортировка по номеру
 * дела Столбики в шапке таблицы (первая строка таблицы) надо также подписать
 */
public class DownloaderServlet extends HttpServlet {

	@Inject
	EntityManager em;
	private static final String queryString = "select UNIT_NAME, COMPLEX_PACK.GET_DATE_INTERVALS(u.UNIV_DATA_UNIT_ID),"
			+ " COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 0) ||"
			+ " TO_NUMBER(COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 1)) ||"
			+ " COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 2) fnumber,"
			+ " TO_NUMBER(COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 3)) ||  COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 4) snumber,"
			+ " TO_NUMBER(COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 5)) || COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 6) dnumber,"
			+ " asu.PAGE_COUNT, COMPLEX_PACK.GET_TYPED_CHILDREN_COUNT(asu.UNIV_DATA_UNIT_ID, 'DOCUMENT') doccount,"
			+ " (select count(*) from UNIV_IMAGE_PATH uip where uip.UNIV_DATA_UNIT_ID = u.UNIV_DATA_UNIT_ID and uip.IMAGE_CATEGORY_ID = DESCRIPTOR_PACK.GET_VALUE_ID_BY_CODE('IMAGE_CATEGORY', 'FULL_SIZE')) IMAGE_COUNT,"
			+ " ANNOTATION, (select  LISTAGG(vl.FULL_VALUE, ',') WITHIN GROUP (ORDER BY vl.FULL_VALUE)"
			+ " from UNIV_DATA_LANGUAGE l join DESCRIPTOR_VALUE vl ON l.DOC_LANGUAGE_ID = vl.DESCRIPTOR_VALUE_ID where u.UNIV_DATA_UNIT_ID = l.UNIV_DATA_UNIT_ID"
			+ " GROUP BY l.UNIV_DATA_UNIT_ID) lang"
			+ " from UNIV_DATA_UNIT u left outer join ARCH_STORAGE_UNIT asu ON u.UNIV_DATA_UNIT_ID = asu.UNIV_DATA_UNIT_ID"
			+ " where UNIT_TYPE_ID = 244 and RES_OWNER_ID = :archiveId and"
			+ " COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 1) = :fundNumber and"
			+ " COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 3) = :seriesNumber";
	private static final String fundPrefixNotNull = " and COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 0) = :fundPrefix";
	private static final String fundLiteraNotNull = " and COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 2) = :fundLitera";
	private static final String seriesLiteraNotNull = " and COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 4) = :seriesLitera";
	private static final String fundPrefixNull = " and COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 0) is null";
	private static final String fundLiteraNull = " and COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 2) is null";
	private static final String seriesLiteraNull = " and COMPLEX_PACK.extract_arch_code_part(ARCH_NUMBER_CODE, 4) is null";
	private static final String orderBy = " ORDER BY  COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 5) || COMPLEX_PACK.EXTRACT_ARCH_CODE_PART(ARCH_NUMBER_CODE, 6)";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String archive = request.getParameter("archive");
		String fund = request.getParameter("fund");
		String series = request.getParameter("series");

		String fundPrefix = "";
		String fundLitera = "";
		String seriesLitera = "";

		// getParameter - неправильно работает с кодировкой, поэтому буквенные значения ищем из сырой строки запроса
		String charsetName = Charset.defaultCharset().name();
		for (String param : request.getQueryString().split("&")) {
			String[] parsed = param.split("=");
			if (parsed.length == 2) {
				if (parsed[0].equals("fprefix")) {
					fundPrefix = URLDecoder.decode(parsed[1], charsetName);
				} else if (parsed[0].equals("flitera")) {
					fundLitera = URLDecoder.decode(parsed[1], charsetName);
				} else if (parsed[0].equals("slitera")) {
					seriesLitera = URLDecoder.decode(parsed[1], charsetName);
				}
			}
		}

		Long archiveId = Long.valueOf(archive);
		Integer fundNumber = Integer.valueOf(fund);
		Integer seriesNumber = Integer.valueOf(series);
		if (archiveId == null || fundNumber == null || seriesNumber == null) {
			response.sendError(500, "Недостаточно параметров");
		} else {
			String qs = queryString;
			if (!fundPrefix.isEmpty()) {
				qs += fundPrefixNotNull;
			} else {
				qs += fundPrefixNull;
			}
			if (!fundLitera.isEmpty()) {
				qs += fundLiteraNotNull;
			} else {
				qs += fundLiteraNull;
			}
			if (!seriesLitera.isEmpty()) {
				qs += seriesLiteraNotNull;
			} else {
				qs += seriesLiteraNull;
			}
			qs += orderBy;
			Query query = em.createNativeQuery(qs);
			query.setParameter("archiveId", archiveId);
			query.setParameter("fundNumber", fundNumber);
			query.setParameter("seriesNumber", seriesNumber);
			if (!fundPrefix.isEmpty()) {
				query.setParameter("fundPrefix", fundPrefix);
			}
			if (!fundLitera.isEmpty()) {
				query.setParameter("fundLitera", fundLitera);
			}
			if (!seriesLitera.isEmpty()) {
				query.setParameter("seriesLitera", seriesLitera);
			}

			Workbook wb = new HSSFWorkbook();
			Sheet sheet = wb.createSheet();
			createRow(sheet, 0, new String[]{"№", "Название дела", "Дата начала", "Дата окончания", "Фонд", "Опись",
				"Дело", "Количество листов", "Количество документов", "Количество графических образов", "Аннотация", "Язык документов"});

			int i = 1;
			for (Object[] data : (List<Object[]>) query.getResultList()) {
				try {
					createRow(sheet, i, data);
				} catch (SQLException ex) {
					Logger.getLogger(DownloaderServlet.class.getName()).log(Level.SEVERE, null, ex);
				}
				++i;
			}

			try (OutputStream out = response.getOutputStream()) {
				response.setHeader("Content-Disposition", "attachment; filename=\"download.xls\"");
				response.setContentType("application/vnd.ms-excel; name=\"download.xls\"");
				wb.write(out);
			}
		}
	}

	private void createCell(Row row, String value, int index) {
		Cell cell = row.createCell(index);
		cell.setCellValue(value);
	}

	private void createRow(Sheet sheet, int index, Object[] values) throws SQLException, IOException {
		Row row = sheet.createRow(index);
		createCell(row, String.valueOf(index), 0);
		for (int i = 0; i < values.length; ++i) {
			if (i == 1) {
				String dates = (String) values[i];
				if (dates != null) {
					String[] d = dates.split("-");
					createCell(row, d[0], i + 1);
					if (d.length > 1) {
						createCell(row, d[1], i + 2);
					}
				}
			} else if (i == 0) {
				createCell(row, (String) values[i], i + 1);
			} else if (i == 8) {
				Clob annotation = (Clob) values[i];
				if (annotation != null) {
					Reader reader = annotation.getCharacterStream();
					createCell(row, IOUtils.toString(reader), i + 2);
				}
			} else {
				if (values[i] instanceof BigDecimal) {
					createCell(row, ((BigDecimal) values[i]).toString(), i + 2);
				} else {
					createCell(row, (String) values[i], i + 2);
				}
			}
		}
	}

	private void createRow(Sheet sheet, int index, String[] values) {
		Row row = sheet.createRow(index);
		for (int i = 0; i < values.length; ++i) {
			createCell(row, values[i], i);
		}
	}
}
