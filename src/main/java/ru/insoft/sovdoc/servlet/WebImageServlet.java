package ru.insoft.sovdoc.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.insoft.sovdoc.model.web.table.WebImages;
import ru.insoft.sovdoc.model.web.table.WebImages_;

@SuppressWarnings("serial")
public class WebImageServlet extends HttpServlet {

	@Inject
	EntityManager em;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		WebImages image;
		response.setCharacterEncoding("UTF-8");
		Long id = Long.valueOf(request.getParameter("id"));
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<WebImages> cq = cb.createQuery(WebImages.class);
        Root<WebImages> root = cq.from(WebImages.class);
        cq.select(root).where(cb.equal(root.get(WebImages_.imageId), id));
        try
        {
        	image = em.createQuery(cq).getSingleResult();
        }
        catch (NoResultException e)
        {
        	response.setContentType("text/plain; charset=UTF-8");
        	response.getWriter().print("Изображение не найдено");
        	return;
        }
             
        ServletOutputStream out = response.getOutputStream();
        response.setContentType(image.getFormat().getValueCode());
        response.setContentLength(image.getImageData().length);
        out.write(image.getImageData());
        out.close();
	}
}
