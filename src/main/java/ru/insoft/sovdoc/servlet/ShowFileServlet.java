package ru.insoft.sovdoc.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.system.complex.SystemQuery;

public class ShowFileServlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8337820681425101557L;
	private static final int DEFAULT_BUFFER_SIZE = 10240;
	
	@Inject
	SystemQuery complexQuery;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String unitId = request.getParameter("unitId");
        String fileNum = request.getParameter("file_num");
        String category = request.getParameter("category");
        String fileName;
        String path;
        String format;

        ImagePath ip = complexQuery.queryImagePath(
        		Long.valueOf(unitId), fileNum == null ? null : Long.valueOf(fileNum), category);
        if (ip == null)
        	return;
       
        format = ip.getDescriptorValueId().getValueCode();
        fileName = ip.getFileName();
        path = ip.getFilePath();

        byte[] buf = new byte[DEFAULT_BUFFER_SIZE];
        File file = new File(path + File.separator + fileName);
        long length = file.length();
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
        ServletOutputStream out = response.getOutputStream();

        response.setCharacterEncoding("UTF-8");
        response.setContentType(format);
        
        //disable caching
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.

        response.setContentLength((int)length);
        while ((in != null) && ((length = in.read(buf)) != -1)) {
            out.write(buf, 0, (int)length);
        }
        in.close();
        out.close();
    }
}
