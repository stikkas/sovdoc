package ru.insoft.sovdoc;

import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

/**
 *
 * @author melnikov
 */
public class SovdocDialect extends Oracle10gDialect
{
    public SovdocDialect()
    {
        super();
        registerFunction("get_readable_arch_number",
                new StandardSQLFunction("COMPLEX_PACK.GET_READABLE_ARCH_NUMBER", StandardBasicTypes.STRING));
        registerFunction("get_date_intervals",
                new StandardSQLFunction("COMPLEX_PACK.GET_DATE_INTERVALS", StandardBasicTypes.STRING));
    }
}
