package ru.insoft.sovdoc.card.desc;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.YesNoBean;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroupAttr;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroupAttr_;
import ru.insoft.sovdoc.model.desc.view.VDescGroupAttr;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@RequestScoped
@Named("descAttrEmbdl")
public class DescAttrEmbeddedDataList extends EmbeddedDataList<VDescGroupAttr> {

	@Inject
	DescriptorGroupCard card;
	@Inject
	EntityManager em;
	@Inject
	YesNoBean yesNo;
	
	@Override
	public List<VDescGroupAttr> getWrappedData()
	{
		if (super.getWrappedData() == null && card.getImmModel() != null)
			setWrappedData(card.getImmModel().getAttrList());
		
		return super.getWrappedData();
	}
	
	public String attrMoveUp()
	{
		List <VDescGroupAttr> attrList = getWrappedData();
		int idx = getRowIndex();
		for (long i = 0; i < attrList.size(); i++)
		{
			VDescGroupAttr attr = attrList.get((int)i);
			Long correctOrder = null; 
			if (i == idx - 1)
				correctOrder = i + 2;
			if (i == idx && i > 0)
				correctOrder = i;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!attr.getSortOrder().equals(correctOrder))
			{
				DescriptorGroupAttr modifAttr = queryAttr(attr.getDescriptorGroupAttrId());
				modifAttr.setSortOrder(correctOrder);
				em.merge(modifAttr);
				em.flush();
			}
		}
		return MENU_PAGES.DESC_GROUP.getId() + "?faces-redirect=true&amp;groupId=" + 
			card.getModel().getDescriptorGroupId().toString() + "&amp;mode=edit&amp;selectedAttr=" +
			((Integer)(idx > 0 ? idx - 1 : 0)).toString();
	}
	
	public String attrMoveDown()
	{
		List <VDescGroupAttr> attrList = getWrappedData();
		int idx = getRowIndex();
		for (long i = 0; i < attrList.size(); i++)
		{
			VDescGroupAttr attr = attrList.get((int)i);
			Long correctOrder = null; 
			if (i == idx + 1)
				correctOrder = i;
			if (i == idx && i < attrList.size() - 1)
				correctOrder = i + 2;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!attr.getSortOrder().equals(correctOrder))
			{
				DescriptorGroupAttr modifAttr = queryAttr(attr.getDescriptorGroupAttrId());
				modifAttr.setSortOrder(correctOrder);
				em.merge(modifAttr);
				em.flush();
			}
		}
		return MENU_PAGES.DESC_GROUP.getId() + "?faces-redirect=true&amp;groupId=" + 
			card.getModel().getDescriptorGroupId().toString() + "&amp;mode=edit&amp;selectedAttr=" +
			((Integer)(idx < attrList.size() - 1 ? idx + 1 : attrList.size() - 1)).toString();
	}
	
	public void removeAttr(Long attrId)
	{
		DescriptorGroupAttr attr = queryAttr(attrId);
		Query q = em.createNativeQuery("begin DESCRIPTOR_PACK.ATTR_ORDER_DECREASE(:p_attr_id); end;");
		q.setParameter("p_attr_id", attrId);
		q.executeUpdate();
		em.remove(attr);
		try
		{
			em.flush();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null || msg.indexOf("FK_DSCGRPATR_DSCVALATR") == -1)
			{
				e.printStackTrace();
				return;
			}
			yesNo.setMessages(new String[]
					{"По данному атрибуту уже заданы значения",
					 "При удалении атрибута они будут также удалены",
					 "Продолжить?"
					});
			return;
		}
		em.refresh(card.getImmModel());
		setWrappedData(card.getImmModel().getAttrList());
	}
	
	private DescriptorGroupAttr queryAttr(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroupAttr> cq = cb.createQuery(DescriptorGroupAttr.class);
		Root<DescriptorGroupAttr> root = cq.from(DescriptorGroupAttr.class);
		cq.select(root).where(cb.equal(root.get(DescriptorGroupAttr_.descriptorGroupAttrId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public void forceRemoveAttr()
	{
		Map<String, String> reqMap = 
			FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long attrId = Long.valueOf(reqMap.get("argument"));
		
		Query q = em.createNativeQuery("begin DESCRIPTOR_PACK.FORCE_REMOVE_ATTR(:p_attr_id); end;");
		q.setParameter("p_attr_id", attrId);
		q.executeUpdate();
		
		em.refresh(card.getImmModel());
		setWrappedData(card.getImmModel().getAttrList());
	}
}
