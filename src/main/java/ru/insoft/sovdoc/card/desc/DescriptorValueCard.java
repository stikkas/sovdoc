package ru.insoft.sovdoc.card.desc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.desc.table.DescAttrInternational;
import ru.insoft.sovdoc.model.desc.table.DescValueInternational;
import ru.insoft.sovdoc.model.desc.table.DescriptorHistory;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.table.DescriptorValueAttr;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue_;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescGroupAttr;
import ru.insoft.sovdoc.model.desc.view.VDescHistory;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.model.desc.view.VDescValue_;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

import com.google.common.collect.Lists;
import ru.insoft.sovdoc.list.desc.UnivClssDataList;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataList;

@RequestScoped
@Named("descValueCard")
public class DescriptorValueCard {

	@Inject
	EntityManager em;
	@Inject
	SystemEntity se;
	@Inject
	ValueSelectDataListGeneral valueSelectDL;
        @Inject
        UnivClssDataList univClssDL;
	
	@Inject
	@RequestParam(value = "groupId")
	Long p_groupId;
	
	@Inject
	@RequestParam(value = "ord")
	Integer p_ord;
	
	@Inject
	@RequestParam(value = "valueId")
	Long p_valueId;
	
	@Inject
	@RequestParam(value = "parentId")
	Long p_parentId;
	
	@Inject
	@RequestParam(value = "argument")
	String p_argument;
	
	@Inject
	@RequestParam(value = "mode")
	String mode;
	
	private DescriptorValue model;
	private VDescValue immModel;
	private VDescGroup valueGroup; 
	
	private Long changedAttr;
	private Long changedValue;
	
	private DescriptorValue queryModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = cq.from(DescriptorValue.class);
		cq.select(root).where(cb.equal(root.get(DescriptorValue_.descriptorValueId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	private VDescValue queryImmModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescValue> cq = cb.createQuery(VDescValue.class);
		Root<VDescValue> root = cq.from(VDescValue.class);
		cq.select(root).where(cb.equal(root.get(VDescValue_.descriptorValueId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	@PostConstruct
	private void init()
	{
		initValueGroup();
		model = getModel();
	}
	
	private void initValueGroup()
	{
		if (p_groupId != null)
		{
			valueGroup = se.getImmDescGroup(p_groupId);
			return;
		}
		if (p_valueId != null)
		{
			valueGroup = se.getImmGroupByValueId(p_valueId);
			return;
		}
		if (p_parentId != null)
			valueGroup = se.getImmGroupByValueId(p_parentId);
	}

	public DescriptorValue getModel() 
	{
		if (model == null)
		{
			if (p_valueId == null)
				model = new DescriptorValue();
			else
				model = queryModel(p_valueId);
		}
		if (model.getDescriptorGroupId() == null && getValueGroup() != null)
		{
			model.setDescriptorGroupId(valueGroup.getDescriptorGroupId());
			model.setSortOrder(p_ord);
			model.setActual(true);
			model.setParentValueId(p_parentId);
			model.setValueAttrList(new ArrayList<DescriptorValueAttr>());
			model.setMultilangList(new ArrayList<DescValueInternational>());
		}
		return model;
	}

	public VDescValue getImmModel() 
	{
		if (immModel == null)
		{
			if (model.getDescriptorValueId() == null)
			{
				immModel = new VDescValue();
				if (getValueGroup() != null)
					immModel.setGroupName(valueGroup.getGroupName());
			}
			else
				immModel = queryImmModel(model.getDescriptorValueId());
		}
		return immModel;
	}
	
	public void refreshModel()
	{
		model    = null;
		immModel = null;
		model    = getModel();
		immModel = getImmModel();
	}

	public VDescGroup getValueGroup() 
	{
		if (valueGroup == null)
			initValueGroup();
		return valueGroup;
	}
	
	public boolean isEditing()
	{
		return mode != null && !mode.isEmpty();
	}
	
	public List<DescriptorValueAttr> getAttrValuesByAttrId(Long attrId)
	{		
		List<DescriptorValueAttr> res = Lists.newArrayList();
		for (DescriptorValueAttr attrVal : model.getValueAttrList())
			if (attrVal.getDescriptorGroupAttrId().equals(attrId))
			{
				if (changedValue != null && changedValue.equals(attrVal.getDescriptorValueAttrId()) ||
					changedAttr != null && changedAttr.equals(attrId) && attrVal.getDescriptorValueAttrId() == null)
					attrVal.setRefDescriptorValueId(getValueSelectDL().getSelectedValue());
				res.add(attrVal);
			}
		if (res.size() == 0)
		{
			DescriptorValueAttr attrVal = new DescriptorValueAttr();
			attrVal.setDescriptorGroupAttrId(attrId);
			attrVal.setDescriptorValue(model);
			if (changedAttr != null && changedAttr.equals(attrId))
				attrVal.setRefDescriptorValueId(getValueSelectDL().getSelectedValue());
			model.getValueAttrList().add(attrVal);
			res.add(attrVal);
		}
		return res;
	}
	
	public String saveValue()
	{
		if (!validate())
			return null;
		
		save();
		return MENU_PAGES.DESC_VALUE.getId() + "?faces-redirect=true&amp;valueId=" +
			model.getDescriptorValueId().toString() + "&amp;mode=edit&amp;saved=true";
	}
	
	protected boolean validate()
	{
		boolean res = true;
		if (model.getFullValue() == null && model.getShortValue() == null)
			res &= MessageUtils.ErrorMessage("Значение указателя должно быть заполнено");
		if (StringUtils.getByteLengthUTF8(model.getFullValue()) > 4000)
			res &= MessageUtils.ErrorMessage("Полное значение слишком длинное");
		if (StringUtils.getByteLengthUTF8(model.getShortValue()) > 250)
			res &= MessageUtils.ErrorMessage("Сокращённое значение слишком длинное");
		if (StringUtils.getByteLengthUTF8(model.getValueCode()) > 30)
			res &= MessageUtils.ErrorMessage("Код слишком длинный");
		for (VDescGroupAttr attr : valueGroup.getAttrList())
		{
			boolean empty = true;
			List<DescriptorValueAttr> attrValues = getAttrValuesByAttrId(attr.getDescriptorGroupAttrId());
			for (DescriptorValueAttr value : attrValues)
			{
				if (value.getAttrValue() != null || value.getRefDescriptorValueId() != null)
					empty = false;
				if (StringUtils.getByteLengthUTF8(value.getAttrValue()) > 4000)
					res &= MessageUtils.ErrorMessage("Атрибут '" + attr.getAttrName() +
							"': значение слишком длинное");
			}
			if (attr.isRequired() && empty)
				res &= MessageUtils.ErrorMessage("Значение '" + attr.getAttrName() +
						"' должно быть заполнено");
		}
		
		return res;
	}
	
	protected void save()
	{		
		valueGroup = getValueGroup();
		if (model.getFullValue() == null)
			model.setFullValue(model.getShortValue());
		if (model.getShortValue() == null && valueGroup.isShortValueSupported())
			model.setShortValue(StringUtils.truncateUTF8(model.getFullValue(), 250));
		if (model.getDescriptorValueId() == null)
		{
			em.persist(model);
			saveAttrValues();
			
			if (valueGroup.isHistorySupported())
			{
				DescriptorHistory history = new DescriptorHistory();
				history.setActualValueId(model.getDescriptorValueId());
				history.setOldValueId(model.getDescriptorValueId());
				history.setSortOrder(1);
				em.persist(history);
			}
		}
		else
		{
			saveAttrValues();
			em.merge(model);
		}
		
		String lang = se.getSystemParameterValue("LANGUAGE");
		boolean multiWritten = false;
		for (DescValueInternational multilangVal : model.getMultilangList())
			if (multilangVal.getLanguageCode().equals(lang))
			{
				multilangVal.setInternationalShortValue(model.getShortValue());
				multilangVal.setInternationalFullValue(model.getFullValue());
				em.merge(multilangVal);
				multiWritten = true;
				break;
			}
		if (!multiWritten)
		{
			DescValueInternational multilangVal = new DescValueInternational();
			multilangVal.setDescriptorValue(model);
			multilangVal.setInternationalFullValue(model.getFullValue());
			multilangVal.setInternationalShortValue(model.getShortValue());
			multilangVal.setLanguageCode(lang);
			em.persist(multilangVal);
		}			
		em.flush();
	}
	
	protected void saveAttrValues()
	{
		for (DescriptorValueAttr attrValue : model.getValueAttrList())
		{
			VDescGroupAttr attr = getAttrById(attrValue.getDescriptorGroupAttrId());
			if (attrValue.getDescriptorValueAttrId() == null)
			{
				if (attrValue.getAttrValue() != null || attrValue.getRefDescriptorValueId() != null)
				{
					em.persist(attrValue);
					if (attr.getDatatypeCode().equals("MULTILINGUAL"))
					{
						attrValue.setMultilangList(new ArrayList<DescAttrInternational>());
						saveAttrValueInternational(attrValue);
					}
				}
			}
			else
			{
				if (attrValue.getAttrValue() != null || attrValue.getRefDescriptorValueId() != null)
				{
					em.merge(attrValue);
					if (attr.getDatatypeCode().equals("MULTILINGUAL"))
						saveAttrValueInternational(attrValue);
				}
				else
					em.remove(attrValue);
			}
		}
	}
	
	protected void saveAttrValueInternational(DescriptorValueAttr attrValue)
	{
		String lang = se.getSystemParameterValue("LANGUAGE");
		for (DescAttrInternational multilangVal : attrValue.getMultilangList())
			if (multilangVal.getLanguageCode().equals(lang))
			{
				multilangVal.setAttrInternationalValue(attrValue.getAttrValue());
				em.merge(multilangVal);
				em.flush();
				return;
			}
		DescAttrInternational multilangVal = new DescAttrInternational();
		multilangVal.setDescriptorValueAttr(attrValue);
		multilangVal.setAttrInternationalValue(attrValue.getAttrValue());
		multilangVal.setLanguageCode(lang);
		em.persist(multilangVal);
		em.flush();
	}
	
	protected VDescGroupAttr getAttrById(Long id)
	{
		for (VDescGroupAttr attr : getValueGroup().getAttrList())
			if (attr.getDescriptorGroupAttrId().equals(id))
				return attr;
		return null;
	}
	
	public void addAttrValueToCollection(Long attrId)
	{
		for (DescriptorValueAttr attrVal : getAttrValuesByAttrId(attrId))
		{
			if (attrVal.getAttrValue() == null && attrVal.getRefDescriptorValueId() == null)
				return;
			if (attrVal.getDescriptorValueAttrId() == null)
				em.persist(attrVal);
			else
				em.merge(attrVal);
			em.flush();
		}
			
		DescriptorValueAttr attrVal = new DescriptorValueAttr();
		attrVal.setDescriptorGroupAttrId(attrId);
		attrVal.setDescriptorValue(model);
		model.getValueAttrList().add(attrVal);
		em.persist(attrVal);
		em.flush();
	}
	
	public void removeAttrValueFromCollection(VDescGroupAttr attr, DescriptorValueAttr value)
	{
		if (attr.isRequired() && getAttrValuesByAttrId(attr.getDescriptorGroupAttrId()).size() == 1)
		{
			MessageUtils.ErrorMessage("Невозможно удалить значение. Атрибут '" +
					attr.getAttrName() + "' обязательный");
		}
		
		model.getValueAttrList().remove(value);
		if (value.getDescriptorValueAttrId() != null)
		{
			em.remove(value);
			em.flush();
		}
	}
        
        public ValueSelectDataList getValueSelectDL()
        {
            if ("UNIV_CLASSIFIER".equals(getValueGroup().getGroupCode()))
                return univClssDL;
            else
                return valueSelectDL;
        }
	
	public void selectParent()
	{
            Long selectedValue = getValueSelectDL().getSelectedValue();
            if (selectedValue == null && model.getParentValueId() == null ||
		selectedValue != null && selectedValue.equals(model.getParentValueId()))
            {
                getValueSelectDL().setValueSelected(true);
                return;
            }
		
            if (selectedValue != null && model.getDescriptorValueId() != null)
            {
		@SuppressWarnings("unchecked")
		List<Long> childValues = (List<Long>)
			em.createNamedQuery("descriptors.hierarch")
			.setParameter("value_id", model.getDescriptorValueId())
			.getResultList();
		if (childValues.contains(BigDecimal.valueOf(selectedValue)))
		{
                    MessageUtils.ErrorMessage("Выбранное значение совпадает с одним из дочерних узлов");
                    return;
                }
            }
		
            if (model.getDescriptorValueId() != null)
            {
		Query q = em.createNativeQuery("begin DESCRIPTOR_PACK.VAL_ORDER_DECREASE(:p_value_id); end;");
		q.setParameter("p_value_id", model.getDescriptorValueId());
		q.executeUpdate();
            }
		
            model.setParentValueId(selectedValue);
            if (model.getParentValueId() == null)
		model.setSortOrder(getValueGroup().getValueCnt() + 1);
            else
            {
		VDescValue dv = se.getImmDescValue(model.getParentValueId());
		model.setSortOrder(dv.getChildrenCnt() + 1);
            }
            getValueSelectDL().setValueSelected(true);
	}
	
	public void selectAttrValue() 
	{
            Long selectedValue = getValueSelectDL().getSelectedValue();
            if (selectedValue == null)
            {
		MessageUtils.ErrorMessage("Значение должно быть выбрано");
		return;
            }
		
            if (p_argument != null && !p_argument.isEmpty())
            {
		if (p_argument.startsWith("val"))
                    changedValue = Long.valueOf(p_argument.substring(3));
		if (p_argument.startsWith("attr"))
                    changedAttr = Long.valueOf(p_argument.substring(4));
            }
            getValueSelectDL().setValueSelected(true);
	}
	
	public void selectOldValue()
	{
            Long selectedValue = getValueSelectDL().getSelectedValue();
            if (selectedValue == null)
            {
		MessageUtils.ErrorMessage("Значение должно быть выбрано");
		return;
            }
            for (VDescHistory history : getImmModel().getHistoryList())
		if (selectedValue.equals(history.getOldValueId()))
		{
                    MessageUtils.ErrorMessage("Значение уже есть в истории переименований");
                    return;
		}
		
            Query q = em.createNativeQuery(
			"begin DESCRIPTOR_PACK.ATTACH_HISTORY_VALUE(:p_value_id, :p_attached_value_id); end;");
            q.setParameter("p_value_id", model.getDescriptorValueId());
            q.setParameter("p_attached_value_id", selectedValue);
            q.executeUpdate();
		
            em.refresh(immModel);
		
            getValueSelectDL().setValueSelected(true);
	}
}
