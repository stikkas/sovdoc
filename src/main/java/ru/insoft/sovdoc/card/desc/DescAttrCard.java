package ru.insoft.sovdoc.card.desc;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroupAttr;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroupAttr_;
import ru.insoft.sovdoc.model.desc.view.VDescGroupAttr;
import ru.insoft.sovdoc.model.desc.view.VDescGroupAttr_;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;
import ru.insoft.sovdoc.ui.desc.ValueSearchParam;

@RequestScoped
@Named
public class DescAttrCard {

	@Inject
	EntityManager em;
	@Inject
	DescriptorGroupCard groupCard;
	@Inject
	DescAttrEmbeddedDataList attrDatalist;
	@Inject
	ValueSearchParam valueSearch;
	@Inject
	@RequestParam(value = "attr_mode")
	String attrMode;
	
	DescriptorGroupAttr model;
	VDescGroupAttr immModel;
	
	private Boolean validCard;
	
	private DescriptorGroupAttr queryModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroupAttr> cq = cb.createQuery(DescriptorGroupAttr.class);
		Root<DescriptorGroupAttr> root = cq.from(DescriptorGroupAttr.class);
		cq.select(root).where(cb.equal(root.get(DescriptorGroupAttr_.descriptorGroupAttrId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	private VDescGroupAttr queryImmModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescGroupAttr> cq = cb.createQuery(VDescGroupAttr.class);
		Root<VDescGroupAttr> root = cq.from(VDescGroupAttr.class);
		cq.select(root).where(cb.equal(root.get(VDescGroupAttr_.descriptorGroupAttrId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public DescriptorGroupAttr getModel()
	{
		Long attrId = null;
		if (model == null)
		{
			if (attrMode == null)
				return new DescriptorGroupAttr();
			
			if (attrDatalist.getRowIndex() != null && attrDatalist.getWrappedData() != null)
				attrId = attrDatalist.getWrappedData().
					get(attrDatalist.getRowIndex()).getDescriptorGroupAttrId();
			if (attrMode.equals("edit"))
				model = queryModel(attrId);
			else
				model = new DescriptorGroupAttr();
			if (attrMode.equals("add"))
			{
				model.setDescGroupId(groupCard.getModel().getDescriptorGroupId());
				model.setSortOrder((long)(attrDatalist.getWrappedData().size() + 1));
			}
			if (attrMode.equals("edit"))
				model = queryModel(attrId);
			if (attrMode.equals("descr"))
				model.setDatatype("DESCRIPTOR");
		}
		return model;
	}
	
	public VDescGroupAttr getImmModel()
	{
		model = getModel();
		if (immModel == null)
		{
			if (model.getDescriptorGroupAttrId() == null)
			{
				immModel = new VDescGroupAttr();
				if (model.getDescGroupId() != null)
					immModel.setDescriptorGroupId(model.getDescGroupId());
			}
			else
				immModel = queryImmModel(model.getDescriptorGroupAttrId());
		}
		return immModel;
	}
	
	public void emptyRefGroup()
	{
		model = getModel();
		model.setRefDescGroupId(null);
	}
	
	protected boolean validateAttr()
	{
		validCard = true;
		if (model.getAttrName() == null)
			validCard &= MessageUtils.ErrorMessage("Наименование атрибута должно быть заполнено");
		if (StringUtils.getByteLengthUTF8(model.getAttrName()) > 300)
			validCard &= MessageUtils.ErrorMessage("Наименование атрибута слишком длинное");
		
		if (model.getDatatype() == null)
			validCard &= MessageUtils.ErrorMessage("Тип данных должен быть выбран");
		else
			if (model.getDatatype().equals("DESCRIPTOR") && model.getRefDescGroupId() == null)
				validCard &= MessageUtils.ErrorMessage("Указатель должен быть выбран");
		
		if (model.getAttrCode() == null)
			validCard &= MessageUtils.ErrorMessage("Код атрибута должен быть заполнен");
		if (StringUtils.getByteLengthUTF8(model.getAttrCode()) > 30)
			validCard &= MessageUtils.ErrorMessage("Код атрибута слишком длинный");
		
		return validCard;
	}

	public String saveAttr()
	{
		model = getModel();
		if (!validateAttr())
			return null;
		
		if (model.getDescriptorGroupAttrId() == null)
			em.persist(model);
		else
			em.merge(model);
		em.flush();
		
		return MENU_PAGES.DESC_GROUP.getId() + "?faces-redirect=true&amp;groupId=" + 
			model.getDescGroupId().toString() + "&amp;mode=edit&amp;selectedAttr=" +
			((Long)(model.getSortOrder() - 1)).toString();
	}
	
	public void selectRefGroup()
	{
		model = getModel();
		model.setRefDescGroupId(valueSearch.getGroupId());
	}
}
