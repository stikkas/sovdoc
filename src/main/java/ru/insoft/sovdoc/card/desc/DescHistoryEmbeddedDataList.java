package ru.insoft.sovdoc.card.desc;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.sovdoc.model.desc.table.DescriptorHistory;
import ru.insoft.sovdoc.model.desc.table.DescriptorHistory_;
import ru.insoft.sovdoc.model.desc.view.VDescHistory;

@RequestScoped
@Named("descHistEmbdl")
public class DescHistoryEmbeddedDataList extends EmbeddedDataList<VDescHistory> {

	@Inject
	DescriptorValueCard card;
	@Inject
	EntityManager em;
	
	@Override
	public List<VDescHistory> getWrappedData()
	{
		if (super.getWrappedData() == null && card.getImmModel() != null)
			setWrappedData(card.getImmModel().getHistoryList());
		return super.getWrappedData();
	}
	
	public String getActualValue()
	{
		if (getWrappedData() == null)
			return null;
		for (VDescHistory hist : getWrappedData())
			if (hist.isActual())
				return hist.getFullValue();
		return null;
	}
	
	public void removeOldValue(VDescHistory history)
	{
		if (history.getOldValueId().equals(card.getModel().getDescriptorValueId()))
		{
			MessageUtils.ErrorMessage("Нельзя удалить из истории значения само значение");
			return;
		}
		
		Query q = em.createNativeQuery(
				"begin DESCRIPTOR_PACK.DETACH_HISTORY_VALUE(:p_value_id, :p_actual_value_id, :p_sort_order); end;");
		q.setParameter("p_value_id", 
				(history.getOldValueId().equals(history.getActualValueId()) ?
						card.getModel().getDescriptorValueId() : history.getOldValueId()));
		q.setParameter("p_actual_value_id", history.getActualValueId());
		q.setParameter("p_sort_order", history.getSortOrder());
		q.executeUpdate();
		
		em.clear();
		card.refreshModel();
		setWrappedData(card.getImmModel().getHistoryList());
		//card.getImmModel().getHistoryList().remove(history);
	}
	
	public void setValueActual()
	{
		VDescHistory history = getWrappedData().get(getRowIndex());
		if (history.isActual())
		{
			MessageUtils.ErrorMessage("Это значение уже актуально");
			return;
		}
		
		Query q = em.createNativeQuery(
				"begin DESCRIPTOR_PACK.SET_ACTUAL_VALUE(:p_new_actual_value_id, :p_old_actual_value_id); end;");
		q.setParameter("p_new_actual_value_id", history.getOldValueId());
		q.setParameter("p_old_actual_value_id", history.getActualValueId());
		q.executeUpdate();
		
		em.refresh(card.getImmModel());
	}
	
	public void moveUp()
	{
		for (int i = 0; i < getWrappedData().size() ; i++)
		{
			VDescHistory history = getWrappedData().get(i);
			Integer correctOrder = null;
			if (i == getRowIndex() - 1)
				correctOrder = i + 2;
			if (i == getRowIndex() && i > 0)
				correctOrder = i;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!history.getSortOrder().equals(correctOrder))
			{
				DescriptorHistory modifHistory = queryHistory(history.getDescriptorHistoryId());
				modifHistory.setSortOrder(correctOrder);
				em.merge(modifHistory);
			}
		}
		em.flush();
		em.refresh(card.getImmModel());
		setWrappedData(card.getImmModel().getHistoryList());
	}
	
	public void moveDown()
	{
		for (int i = 0; i < getWrappedData().size() ; i++)
		{
			VDescHistory history = getWrappedData().get(i);
			Integer correctOrder = null;
			if (i == getRowIndex() + 1)
				correctOrder = i;
			if (i == getRowIndex() && i < getRowCount() - 1)
				correctOrder = i + 2;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!history.getSortOrder().equals(correctOrder))
			{
				DescriptorHistory modifHistory = queryHistory(history.getDescriptorHistoryId());
				modifHistory.setSortOrder(correctOrder);
				em.merge(modifHistory);
			}
		}
		em.flush();
		em.refresh(card.getImmModel());
		setWrappedData(card.getImmModel().getHistoryList());
	}
	
	protected DescriptorHistory queryHistory(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorHistory> cq = cb.createQuery(DescriptorHistory.class);
		Root<DescriptorHistory> root = cq.from(DescriptorHistory.class);
		cq.select(root).where(cb.equal(root.get(DescriptorHistory_.descriptorHistoryId), id));
		return em.createQuery(cq).getSingleResult();
	}
}
