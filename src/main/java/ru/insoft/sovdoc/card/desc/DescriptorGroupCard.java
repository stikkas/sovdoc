package ru.insoft.sovdoc.card.desc;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup_;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescGroup_;
import ru.insoft.sovdoc.security.AccessService;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@RequestScoped
@Named("descGroupCard")
public class DescriptorGroupCard {

	@Inject
	EntityManager em;
	@Inject
	SystemEntity se;
	@Inject
	AccessService access;
	
	@Inject
	@RequestParam(value = "system")
	Boolean system;
	
	@Inject
	@RequestParam(value = "ss")
	Long subsystemNumber;
	
	@Inject
	@RequestParam(value = "ord")
	Long sortOrder;
	
	@Inject
	@RequestParam(value = "groupId")
	Long groupId;
	
	@Inject
	@RequestParam(value = "mode")
	String mode;
	
	private DescriptorGroup model;
	private VDescGroup immModel;
	
	private DescriptorGroup queryModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroup> cq = cb.createQuery(DescriptorGroup.class);
		Root<DescriptorGroup> root = cq.from(DescriptorGroup.class);
		cq.select(root).where(cb.equal(root.get(DescriptorGroup_.descriptorGroupId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	private VDescGroup queryImmModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VDescGroup> cq = cb.createQuery(VDescGroup.class);
		Root<VDescGroup> root = cq.from(VDescGroup.class);
		cq.select(root).where(cb.equal(root.get(VDescGroup_.descriptorGroupId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	public DescriptorGroup getModel()
	{
		if (model == null)
		{
			if (groupId == null)
			{
				model = new DescriptorGroup();
				model.setSubsystemNumber(subsystemNumber);
				if (system != null)
					model.setSystem(system);
				model.setSortOrder(sortOrder);
			}
			else
				model = queryModel(groupId);
		}
		return model;
	}
	
	public VDescGroup getImmModel()
	{
		model = getModel();
		if (immModel == null)
		{
			if (model.getDescriptorGroupId() == null)
			{
				immModel = new VDescGroup();
				if (model.getSubsystemNumber() != null)
					immModel.setSubsystemName(
							se.getSubsystem(model.getSubsystemNumber()).getSubsystemName());
			}
			else
				immModel = queryImmModel(model.getDescriptorGroupId());
		}
		return immModel;
	}
	
	public boolean isEditing()
	{
		return mode != null;
	}
	
	protected boolean validateGroup()
	{
		boolean res = true;
		if (model.getGroupName() == null)
			res &= MessageUtils.ErrorMessage("Наименование справочника должно быть заполнено");
		if (StringUtils.getByteLengthUTF8(model.getGroupName()) > 300)
			res &= MessageUtils.ErrorMessage("Наименование справочника слишком длинное");

		if (model.isSystem())
		{
			if (model.getGroupCode() == null)
				res &= MessageUtils.ErrorMessage("У системных справочников код должен быть заполнен");
			if (model.getSubsystemNumber() == null)
				res &= MessageUtils.ErrorMessage("Системный справочник должен относиться к одной из подсистем");
			if (!access.hasAccess("DESC_GROUP_SYS"))
				res &= MessageUtils.ErrorMessage("У Вас нет прав на создание системных справочников");
		}
		else
			if (model.getSubsystemNumber() != null)
				res &= MessageUtils.ErrorMessage("Пользовательский справочник не может относиться к подсистеме");
		if (StringUtils.getByteLengthUTF8(model.getGroupCode()) > 30)
			res &= MessageUtils.ErrorMessage("Код справочника слишком длинный");

		return res;
	}
	
	public String saveGroup()
	{
		model = getModel();
		if (!validateGroup())
			return null;
		
		if (model.getDescriptorGroupId() == null)
		{
			if (model.getSortOrder() == null)
			{
				if (model.getSubsystemNumber() == null)
					model.setSortOrder(Long.valueOf(se.getUserDescGroups().size() + 1));
				else
					model.setSortOrder(Long.valueOf(
							se.getSubsystem(model.getSubsystemNumber()).getGroupCnt() + 1));
			}
			
			em.persist(model);
			
			/*Query q = em.createNativeQuery("begin DESCRIPTOR_PACK.GRP_ORDER_INCREASE(:group_id); end;");
			q.setParameter("group_id", model.getDescriptorGroupId());
			q.executeUpdate();*/
		}
		else
		{
			Query q = em.createNativeQuery(
					"begin DESCRIPTOR_PACK.GRP_SET_PROPERTIES(:group_id, :new_hierarch, :new_short_values, :new_history); end;");
			q.setParameter("group_id", model.getDescriptorGroupId());
			q.setParameter("new_hierarch", model.isHierarchical());
			q.setParameter("new_short_values", model.isShortValueSupported());
			q.setParameter("new_history", model.isHistorySupported());
			q.executeUpdate();
			
			em.merge(model);
		}
		
		em.flush();
		
		return MENU_PAGES.DESC_GROUP.getId() + "?faces-redirect=true&amp;groupId=" + 
			model.getDescriptorGroupId().toString() + "&amp;mode=edit&amp;saved=true";
	}
}
