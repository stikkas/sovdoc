package ru.insoft.sovdoc.card.ebook;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.SimpleDataCollection;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookAuthor;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

@RequestScoped
@Named
public class AuthorCollection extends SimpleDataCollection<ArchEbookAuthor> {

  @Inject
  EbookCard card;
  @Inject
  EntityManager em;
  @Inject
  ValueSelectDataListGeneral valueSelectDL;

  @PostConstruct
  protected void init() {
    super.init();
  }

  public void setCard(EbookCard card) {
    this.card = card;
  }

  @Override
  protected String getCountParamName() {
    return "authorCount";
  }

  @Override
  protected List<ArchEbookAuthor> queryItems() {
    if (card != null && card.getEbookModel() != null) {
      return card.getEbookModel().getAuthors();
    }
    return null;
  }

  @Override
  protected ArchEbookAuthor copyItem(ArchEbookAuthor item) {
    ArchEbookAuthor newItem = new ArchEbookAuthor();
    newItem.setAuthorId(item.getAuthorId());
    newItem.setAuthor(item.getAuthor());
    return newItem;
  }

  @Override
  protected ArchEbookAuthor createEmptyItem() {
    ArchEbookAuthor item = new ArchEbookAuthor();
    item.setAuthor(new DescriptorValue());
    return item;
  }

  @Override
  protected boolean validateItem(ArchEbookAuthor item) {
    return true;
  }

  @Override
  protected boolean isEmpty(ArchEbookAuthor item) {
    return item.getAuthorId() == null;
  }

  @Override
  protected boolean areItemsEqual(ArchEbookAuthor item1, ArchEbookAuthor item2) {
    if (item1 == null) {
      return item2 == null;
    }
    return item1.getAuthorId().equals(item2.getAuthorId());
  }

  @Override
  protected void insertNewItemToDB(ArchEbookAuthor item) {
    item.setEbook(card.getEbookModel());
    item.getAuthor().setDescriptorValueId(item.getAuthorId());
    em.persist(item);
  }

  @Override
  protected void removeItemFromDB(ArchEbookAuthor item) {
//    if (item.getAuthorId() == null || item.getAuthorId().equals(0L)) {
//      return;
//    }
   item= em.merge(item);
//    CriteriaBuilder cb = em.getCriteriaBuilder();
//    CriteriaQuery<ArchEbookAuthor> cq = cb.createQuery(ArchEbookAuthor.class);
//    Root<ArchEbookAuthor> root = cq.from(ArchEbookAuthor.class);
//    cq.select(root).where(cb.equal(root.get(ArchEbookAuthor_.authorId), item.getAuthorId()));
//    return em.createQuery(cq).getSingleResult();
    em.remove(item);

  }

  public void selectAuthor() {
    if (valueSelectDL.getSelectedValue() == null) {
      MessageUtils.ErrorMessage("Автор должен быть выбран");
      return;
    }
    for (ArchEbookAuthor item : getItems()) {
      if (item.getAuthorId().equals(valueSelectDL.getSelectedValue())) {
        MessageUtils.ErrorMessage("Этот автор уже выбран");
        return;
      }
    }
    ArchEbookAuthor item = createEmptyItem();
    item.setAuthorId(valueSelectDL.getSelectedValue());
    if (item.getAuthorId() != null) {
      item.setAuthor(em.find(DescriptorValue.class, item.getAuthorId()));
    }
    addFilledItem(item);
    valueSelectDL.setValueSelected(true);
    valueSelectDL.setRowId(null);
  }
}
