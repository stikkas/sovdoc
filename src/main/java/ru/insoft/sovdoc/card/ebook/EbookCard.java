package ru.insoft.sovdoc.card.ebook;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.card.complex.AttachedFiles;
import ru.insoft.sovdoc.card.complex.ComplexCard;
import ru.insoft.sovdoc.card.complex.DescriptorEmbeddedDataList;
import ru.insoft.sovdoc.model.complex.table.UnivDataLanguage;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.ebook.table.ArchEbook;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookAuthor;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookSeries;
import ru.insoft.sovdoc.model.ebook.view.VEbook;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.system.ebook.SystemQuery;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@ViewScoped
@ManagedBean
public class EbookCard extends ComplexCard {

  private ArchEbook ebookModel;
  private VEbook ebookImmModel;
  private DescriptorEmbeddedDataList descriptorEmbDL;
  private UnivDataLanguage langModel;
  private ImagePath bookCover;

  @Inject
  SystemQuery ebookQuery;
  @Inject
  AuthorCollection authorCollection;
  @Inject
  AttachedFiles attachedFiles;

  @Inject
  @RequestParam(value = "argument")
  String argument;

  @Override
  public VComplexDataUnit getImmModel() {
    return null;
  }

  public ArchEbook getEbookModel() {
    if (ebookModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        ebookModel = new ArchEbook();
        ebookModel.setAuthors(new ArrayList<ArchEbookAuthor>());
      } else {
        ebookModel = ebookQuery.queryEbookModel(model.getUnivDataUnitId());
        journalHelper.copyEbookModel(ebookModel);
        if (action == null || !action.equals("save")) {
          em.detach(ebookModel);
        }
      }
    }
    return ebookModel;
  }

  public VEbook getEbookImmModel() {
    if (ebookImmModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        ebookImmModel = new VEbook();
      } else {
        ebookImmModel = ebookQuery.queryEbookImmModel(model.getUnivDataUnitId());
        journalHelper.copyEbookImmModel(ebookImmModel);
      }
    }
    return ebookImmModel;
  }

  public UnivDataLanguage getLangModel() {
   
    if (langModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        langModel = new UnivDataLanguage();
      } else {
        List<UnivDataLanguage> langList = model.getLanguages();
        if (langList.size() == 0) {
          langModel = new UnivDataLanguage();
        } else {
          langModel = langList.get(0);
          em.detach(langModel);
        }
      }
    }
    return langModel;
  }

  @Override
  protected Long getUnitTypeId() {
    return se.getImmDescValueByCodes("UNIV_UNIT_TYPE", "EBOOK").getDescriptorValueId();
  }

  @Override
  protected MENU_PAGES getMenuPage() {
    return MENU_PAGES.EBOOK;
  }

  public DescriptorEmbeddedDataList getDescriptorEmbDL() {
    if (descriptorEmbDL == null) {
      descriptorEmbDL = new DescriptorEmbeddedDataList() {
        @Override
        protected ComplexCard getCard() {
          return EbookCard.this;
        }

        @Override
        public List<VComplexDescriptor> getWrappedData() {
          if (super.getWrappedData() == null && getEbookImmModel() != null) {
            setWrappedData(ebookImmModel.getDescriptors());
          }
          return super.getWrappedData();
        }
      };
    }
    return descriptorEmbDL;
  }

  public void selectAuthor() {

  }

  public void updateSeriesInfo() {
    ArchEbookSeries series = ebookQuery.querySeries(ebookModel.getSeriesId());
    getEbookImmModel().setSeries(series);
  }

  @Override
  protected void saveDetails() {
    if (ebookModel.getExternalUrl() != null
            && !ebookModel.getExternalUrl().startsWith("http://")) {
      ebookModel.setExternalUrl("http://" + ebookModel.getExternalUrl());
    }

    boolean isNew = false;
    if (ebookModel.getUnivDataUnitId() == null) {
      ebookModel.setUnivDataUnitId(model.getUnivDataUnitId());
      em.persist(ebookModel);
      em.flush();

      isNew = true;
    } else {
      saveLangModel();
    }
    authorCollection.setCard(this);
    authorCollection.save();
    //em.flush();

//    langModel = em.merge(langModel);
    //ebookModel.setAuthors(authorCollection.getItems());
    ebookModel = em.merge(ebookModel);
    if( model.getLanguages().size()!=0 && model.getLanguages().get(0).getDocLanguageId()==null){
      model.getLanguages().remove(0);
    } 
    model = em.merge(model);
    em.flush();
    em.refresh(ebookModel);
    em.refresh(model);

    ebookImmModel = ebookQuery.queryEbookImmModel(model.getUnivDataUnitId());

    if (isNew) {
      journalHelper.recordFullUnivDataUnit(model, "INPUT", null, null);
    } else {

      em.refresh(ebookImmModel);
      journalHelper.recordEditEbook(model, ebookModel, ebookImmModel);
    }
  }

  @Override
  protected void saveDefaultLanguages() {
    if (langModel.getDocLanguageId() == null) {
      super.saveDefaultLanguages();
    } else {
      em.refresh(model);
      saveLangModel();
    }
  }

  protected void saveLangModel() {
    UnivDataLanguage lang = journalHelper.getCopyModel() != null && journalHelper.getCopyModel().getLanguages().size() > 0
            ? journalHelper.getCopyModel().getLanguages().get(0) : null;
    if (lang != null && !lang.getDocLanguageId().equals(langModel.getDocLanguageId())) {
      lang.setUnivDataUnit(model);
      lang = em.merge(lang);
      em.remove(lang);
    }
    if ((lang == null || !lang.getDocLanguageId().equals(langModel.getDocLanguageId()))
            && langModel.getDocLanguageId() != null) {
      if (langModel.getUnivDataUnit() == null) {
        langModel.setUnivDataUnit(model);
      }
      em.persist(langModel);
    }
  }

  @Override
  protected boolean validate() {
    boolean res = true;
    if (model.getUnitName() == null) {
      res &= MessageUtils.ErrorMessage("Название издания должно быть заполнено");
    }
    if (StringUtils.getByteLengthUTF8(model.getUnitName()) > 4000) {
      res &= MessageUtils.ErrorMessage("Название издания: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getUdc()) > 100) {
      res &= MessageUtils.ErrorMessage("УДК: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getBBC()) > 100) {
      res &= MessageUtils.ErrorMessage("ББК: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getIsbn()) > 100) {
      res &= MessageUtils.ErrorMessage("ISBN: слишком длинное значение");
    }
    if (ebookModel.getEditionTypeId() == null) {
      res &= MessageUtils.ErrorMessage("Вид издания должен быть указан");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getEditor()) > 200) {
      res &= MessageUtils.ErrorMessage("Редактор: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getEditorialBoard()) > 2500) {
      res &= MessageUtils.ErrorMessage("Редколлегия: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getCompilers()) > 2500) {
      res &= MessageUtils.ErrorMessage("Составители: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getVolume1Info()) > 100) {
      res &= MessageUtils.ErrorMessage("Том: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getVolume2Info()) > 100) {
      res &= MessageUtils.ErrorMessage("Книга: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getAtoptitleData()) > 2500) {
      res &= MessageUtils.ErrorMessage("Надзаголовочные данные: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getEditionNumber()) > 100) {
      res &= MessageUtils.ErrorMessage("Номер выпуска: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getEditionNote()) > 2500) {
      res &= MessageUtils.ErrorMessage("Сведения об издании: слишком длинное значение");
    }
    if (ebookModel.getPublishingPlace() == null) {
      res &= MessageUtils.ErrorMessage("Место издания должно быть заполнено");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getPublishingPlace()) > 2500) {
      res &= MessageUtils.ErrorMessage("Место издания: слишком длинное значение");
    }
    if (ebookModel.getPublisher() == null) {
      res &= MessageUtils.ErrorMessage("Название издательства должно быть заполнено");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getPublisher()) > 2500) {
      res &= MessageUtils.ErrorMessage("Название издательства: слишком длинное значение");
    }
    if (ebookModel.getPublishingYear() == null) {
      res &= MessageUtils.ErrorMessage("Год издания должен быть заполнен");
    }
    if (ebookModel.getEditionSize() == null) {
      res &= MessageUtils.ErrorMessage("Объём издания должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getEditionSize()) > 100) {
      res &= MessageUtils.ErrorMessage("Объём издания: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getNotes()) > 4000) {
      res &= MessageUtils.ErrorMessage("Примечание: слишком длинное значение");
    }
    if (StringUtils.getByteLengthUTF8(ebookModel.getExternalUrl()) > 500) {
      res &= MessageUtils.ErrorMessage("Ссылка на текст книги: слишком длинное значение");
    }

    if (!res) {
      em.detach(model);
      em.detach(ebookModel);
      em.detach(langModel);
    }

    return res;
  }

  @Override
  protected String fillArchNumberCode() {
    return null;
  }

  @Override
  protected String fillUnitName() {
    return model.getUnitName();
  }

  @Override
  protected Integer fillNumberNumber() {
    if (model.getNumberNumber() == null) {
      return ebookQuery.queryMaxRegNumber() + 1;
    } else {
      return model.getNumberNumber();
    }
  }

  @Override
  protected void recordAddDescriptor(Long univDescriptorId) {
    journalHelper.recordChangeUnivDescriptor(model, "ADD", univDescriptorId);
    em.detach(ebookImmModel);
    ebookImmModel = ebookQuery.queryEbookImmModel(model.getUnivDataUnitId());
  }

  public String confirmRemove() {
    return "Вы действительно хотите удалить \"" + getModel().getUnitName() + "\"";
  }

  public String removeDataUnit() {
    try {
      bookCover = getBookCover();

      journalHelper.recordFullUnivDataUnit(getModel(), "DELETE", null , null);

      model = em.merge(model);
      em.remove(model);
      em.flush();

      if (bookCover != null) {
        File dir = new File(bookCover.getFilePath());
        File file = new File(dir, bookCover.getFileName());
        file.delete();
        if (dir.isDirectory() && dir.list().length == 0) {
          dir.delete();
        }
      }

      return "/data/ebook/librarySearch.jsf?faces-redirect=true";
    } catch (Exception e) {
      String msg = ExceptionUtils.getConstraintViolationMessage(e);
      if (msg == null) {
        e.printStackTrace();
        return null;
      }
      if (msg.indexOf("FK_UNIDATUNT_UNIDATUNT") >= 0) {
        MessageUtils.ErrorMessage("Невозможно удалить ресурс, так как имеются вложенные данные");
      } else {
        MessageUtils.ErrorMessage("Невозможно удалить ресурс: обнаружены внешние ссылки");
      }
    }
    return null;
  }

  public ImagePath getBookCover() {
    if (bookCover == null && unitId != null) {
      bookCover = ebookQuery.queryBookCover(unitId);
    }

    return bookCover;
  }

  public String getBookCoverURL() {
    if (getBookCover() == null) {
      return null;
    }

    return attachedFiles.getFileURL(bookCover) + "?t=" + Calendar.getInstance().getTimeInMillis();
  }

  public void removeCover() {
    File f = new File(getBookCover().getFilePath(), getBookCover().getFileName());
    f.delete();
    bookCover = em.merge(bookCover);
    em.remove(bookCover);

    journalHelper.recordDetachFiles(unitId, "BOOK_COVER", bookCover);
    bookCover = null;
  }

}
