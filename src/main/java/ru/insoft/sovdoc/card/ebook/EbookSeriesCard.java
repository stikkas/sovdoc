package ru.insoft.sovdoc.card.ebook;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookSeries;
import ru.insoft.sovdoc.system.ebook.SystemQuery;

@RequestScoped
@Named
public class EbookSeriesCard {

	@Inject
	EntityManager em;
	@Inject
	EbookCard card;
	@Inject
	SystemQuery ebookQuery;
	
	@Inject
	@RequestParam(value = "seriesId")
	Long seriesId;
	
	private ArchEbookSeries model;
	private boolean complete;
	
	public ArchEbookSeries getModel()
	{
		if (model == null)
		{
			if (seriesId == null)
				model = new ArchEbookSeries();
			else
				model = ebookQuery.querySeries(seriesId);
		}
		return model;
	}
	
	public boolean isComplete()
	{
		return complete;
	}
	
	protected boolean validate()
	{
		boolean res = true;
		if (model.getSeriesName() == null)
			res &= MessageUtils.ErrorMessage("Название серии не может быть пустым");
		if (StringUtils.getByteLengthUTF8(model.getSeriesName()) > 300)
			res &= MessageUtils.ErrorMessage("Название серии слишком длинное");
		if (StringUtils.getByteLengthUTF8(model.getSeriesInfo()) > 2500)
			res &= MessageUtils.ErrorMessage("Сведения о серии: значение слишком длинное");
		return res;
	}
	
	public void save()
	{
		if (!validate())
			return;
		
		if (model.getSeriesId() == null)
			em.persist(model);
		else
			em.merge(model);
		em.flush();
		
		card.getEbookModel().setSeriesId(model.getSeriesId());
		card.getEbookImmModel().setSeries(model);
		complete = true;
	}
	
	public void remove()
	{
		try
		{
			em.remove(model);
			em.flush();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null)
			{
				e.printStackTrace();
				return;
			}
			if (msg.indexOf("FK_ARHEBKSER_ARHEBK") >= 0)
				MessageUtils.ErrorMessage("Невозможно удалить. По данной серии заведены книги");
			return;
		}
		
		card.getEbookModel().setSeriesId(null);
		card.getEbookImmModel().setSeries(null);
		complete = true;
	}
}
