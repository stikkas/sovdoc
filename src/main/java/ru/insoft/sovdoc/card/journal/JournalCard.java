package ru.insoft.sovdoc.card.journal;

import ru.insoft.sovdoc.card.NavigableCard;
import ru.insoft.sovdoc.model.correct.view.VUnivCorrectJournal;
import ru.insoft.sovdoc.model.correct.view.VUnivCorrectJournal_;

import javax.enterprise.context.*;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vasilev
 * Date: 11.04.13
 * Time: 13:46
 * To change this template use File | Settings | File Templates.
 */
@RequestScoped
@Named
public class JournalCard extends NavigableCard {
    private VUnivCorrectJournal card;
    private int indexOfSingle;

    public int getIndexOfSingle() {
        return indexOfSingle;
    }

    @Inject
    EntityManager em;

    public VUnivCorrectJournal getCard()
    {
        if (card == null && getCardIndex() != null)
        {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<VUnivCorrectJournal> cq = cb.createQuery(VUnivCorrectJournal.class);
            Root<VUnivCorrectJournal> root = cq.from(VUnivCorrectJournal.class);
            cq.select(root).where(cb.equal(root.get(VUnivCorrectJournal_.univCorrectJournalId), Long.valueOf(getIds().get(getCardIndex()))));
            card = em.createQuery(cq).getSingleResult();
        }
        return card;
    }

    public List<VUnivCorrectJournal> getAllCards() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<VUnivCorrectJournal> cq = cb.createQuery(VUnivCorrectJournal.class);
        Root<VUnivCorrectJournal> root = cq.from(VUnivCorrectJournal.class);
        cq.select(root)
                .where(cb.equal(root.get(VUnivCorrectJournal_.univDataUnitId), card.getUnivDataUnitId()));
        cq.orderBy(cb.asc(root.get(VUnivCorrectJournal_.operationDate)));
        List<VUnivCorrectJournal> allCards = em.createQuery(cq).getResultList();

        indexOfSingle = allCards.indexOf(card);
        return allCards;
    }
}

