package ru.insoft.sovdoc.card.web;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.web.table.WebNews;
import ru.insoft.sovdoc.system.WebQuery;

@RequestScoped
@Named
public class NewsCard {

	@Inject
	WebQuery webQuery;
	@Inject
	EntityManager em;
	
	@Inject
	@RequestParam(value = "newsId")
	Long newsId;
	
	private WebNews model;
	private boolean validCard;
	
	public Long getNewsId()
	{
		return newsId;
	}
	
	public WebNews getModel()
	{
		if (model == null)
		{
			if (newsId == null)
				model = new WebNews();
			else
				model = webQuery.getWebNews(newsId); 
		}
		return model;
	}
	
	public boolean isValidCard()
	{
		return validCard;
	}
	
	protected boolean validate()
	{
		validCard = true;
		if (model.getName() == null)
			validCard &= MessageUtils.ErrorMessage("Наименование новости должно быть заполнено");
		if (StringUtils.getByteLengthUTF8(model.getName()) > 300)
			validCard &= MessageUtils.ErrorMessage("Наименование новости слишком длинное");
		if (model.getNewsDate() == null)
			validCard &= MessageUtils.ErrorMessage("Дата новости должна быть заполнена");
		if (!validCard && newsId != null)
			em.detach(model);
		return validCard;
	}
	
	public void save()
	{
		if (!validate())
			return;
		
		if (newsId == null)
		{
			em.persist(model);
			em.flush();
			newsId = model.getNewsId();
		}
	}
}
