package ru.insoft.sovdoc.card.web;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.desc.view.VDescAttrvalueWithCode;
import ru.insoft.sovdoc.model.web.table.WebPages;
import ru.insoft.sovdoc.model.web.table.WebPagesLocalized;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.system.SystemSelectItem;
import ru.insoft.sovdoc.system.WebQuery;

@RequestScoped
@Named
public class ContentEdit {

	@Inject
	WebQuery webQuery;
	@Inject
	SystemSelectItem ssi;
	@Inject
	SystemEntity se;
	
	@Inject
	@RequestParam(value = "pageId")
	Long pageId;
	@Inject
	@RequestParam(value = "newsId")
	Long newsId;
	@Inject
	@RequestParam(value = "type")
	String type;
	@Inject
	@RequestParam(value = "pageTypeId")
	Long pageTypeId;
	@Inject
	@RequestParam(value = "ord")
	Integer ord;
	
	private String languageCode;
	private String contentName;
	private String fullContent;
	private Long contentTypeId;
	
	public String getBackRef()
	{
		String res = null;
		if (pageId != null || type != null && type.equals("page"))
		{
			res = "/adm/settings/web/staticWebPageList.jsf";
			if (pageId != null)
				res += "?pageId=" + pageId.toString();
		}
		if (newsId != null || type != null && type.equals("news"))
		{
			res = "/adm/settings/web/newsList.jsf";
			if (newsId != null)
				res += "?newsId=" + newsId.toString();
		}
		return res;
	}
	
	public String getPageTitle()
	{
		String res = null;
		if (pageId != null)
			res = "Страница \"" + webQuery.getPageInfo(pageId).getName() + "\"";
		if (newsId != null)
			res = "Новость \"" + webQuery.getWebNews(newsId).getName() + "\"";
		if (type != null)
		{
			if (type.equals("page"))
				res = "Новая страница";
			if (type.equals("news"))
				res = "Новая новость";
		}
		return res;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getContentName() {
		return contentName;
	}

	public void setContentName(String contentName) {
		this.contentName = contentName;
	}

	public String getFullContent() {
		return fullContent;
	}

	public void setFullContent(String fullContent) {
		this.fullContent = fullContent;
	}
	
	public Long getContentTypeId() {
		return contentTypeId;
	}

	public void setContentTypeId(Long contentTypeId) {
		this.contentTypeId = contentTypeId;
	}
	
	public List<SelectItem> getContentTypes()
	{
		if (pageId != null || type != null && type.equals("page"))
			return ssi.getWebContentTypes("PAGE");
		if (newsId != null || type != null && type.equals("news"))
			return ssi.getWebContentTypes("NEWS");
		return null;
	}
	
	public boolean isShowName()
	{
		if (contentTypeId == null)
			return true;
		
		VDescAttrvalueWithCode attrVal = se.getAttrValueByCode(contentTypeId, "SHOW_NAME");
		return Boolean.valueOf(attrVal.getAttrValue());
	}

	protected WebPagesLocalized queryLocale()
	{
		if (pageId != null)
			return webQuery.getPageLocale(pageId, "page", languageCode, contentTypeId);
		if (newsId != null)
			return webQuery.getPageLocale(newsId, "news", languageCode, contentTypeId);
		return null;
	}
	
	public void initContent()
	{
		WebPagesLocalized locale = queryLocale();
		if (locale != null)
		{
			contentName = locale.getName();
			fullContent = locale.getContent();
		}
	}
	
	protected boolean validate()
	{
		boolean res = true;
		if (StringUtils.getByteLengthUTF8(contentName) > 300)
			res &= MessageUtils.ErrorMessage("Наименование содержимого слишком длинное");
// эта валидация уже ненужна, т.к. может быть и пустым содержимое		
//        if (fullContent == null)
//			res &= MessageUtils.ErrorMessage("Содержимое должно быть заполнено");
		return res;
	}
	
	public String save()
	{
		String res = null;
		if (!validate())
			return null;
		
		EntityManager em = webQuery.getEntityManager();
		WebPagesLocalized locale = queryLocale();
		if (locale == null)
		{
			locale = new WebPagesLocalized();
			locale.setPageId(pageId);
			locale.setNewsId(newsId);
		}
		locale.setLanguageCode(languageCode);
		locale.setContentTypeId(contentTypeId);
		locale.setName(contentName);
		locale.setContent(fullContent);
		if (pageId == null && newsId == null && type.equals("page"))
		{
			WebPages page = new WebPages();
			page.setPageTypeId(pageTypeId);
			page.setSortOrder(ord);
			em.persist(page);
			em.flush();
			locale.setPageId(page.getPageId());
			res = "/adm/settings/web/pageContent?faces-redirect=true&amp;pageId=" + 
				page.getPageId().toString();
		}
		if (locale.getLocalizedPageId() == null)
			em.persist(locale);
		return res;
	}
	
	public void clear()
	{
		EntityManager em = webQuery.getEntityManager();
		WebPagesLocalized locale = queryLocale();
		if (locale != null)
			em.remove(locale);
		contentName = null;
		fullContent = null;
	}
}
