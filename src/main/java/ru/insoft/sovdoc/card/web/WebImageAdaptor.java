package ru.insoft.sovdoc.card.web;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import ru.insoft.sovdoc.model.web.table.WebImages;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.system.WebQuery;

@RequestScoped
@Named
public class WebImageAdaptor {

	@Inject
	SystemEntity se;
	@Inject
	WebQuery webQuery;
	
	public void fileUploadListener(FileUploadEvent event)
	{
		UploadedFile item = event.getUploadedFile();
		Long formatId = se.getImmDescValueByCodes("FILE_FORMAT", item.getContentType()).getDescriptorValueId();
		
		WebImages imageEntry = new WebImages();
		imageEntry.setFormatId(formatId);
		imageEntry.setImageData(item.getData());
		se.getEntityManager().persist(imageEntry);
	}
	
	public void removeImage()
	{
		Long imageId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("argument"));
		WebImages image = webQuery.getWebImage(imageId);
		se.getEntityManager().remove(image);
	}
}
