package ru.insoft.sovdoc.card.complex;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchPerformanceAnnotation;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.UnivDataLanguage;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexPhonodoc;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

/**
 *
 * @author melnikov
 */
@ViewScoped
@ManagedBean
public class PhonodocCard extends ComplexCard {

    @ManagedProperty("#{attachedFilesUpload}")
    AttachedFilesUpload afUpload;

    public AttachedFilesUpload getAfUpload() {
        return afUpload;
    }

    public void setAfUpload(AttachedFilesUpload afUpload) {
        this.afUpload = afUpload;
    }

    private Boolean editing;
    private ArchFund fundModel;
    private ArchSeries seriesModel;
    private ArchPhonodoc phonodocModel;
    private VComplexPhonodoc phonodocImmModel;
    private PhonoStorageUnitEmbDataList storageUnitEmbDL;
    private DescriptorEmbeddedDataList descriptorEmbDL;
    private UnivClssEmbeddedDataList univClssEmbDL;
    private PerformanceAnnoEmbDataList perfAnnoEmbDL;

    @Override
    public boolean isEditing() {
        if (editing == null) {
            editing = super.isEditing();
        }
        return editing;
    }

    public ArchFund getFundModel() {
        if (fundModel == null) {
            fundModel = getParentFund();
        }
        return fundModel;
    }

    public ArchSeries getSeriesModel() {
        if (seriesModel == null) {
            seriesModel = getParentSeries();
        }
        return seriesModel;
    }

    public ArchPhonodoc getPhonodocModel() {
        if (phonodocModel == null) {
            model = getModel();
            if (model.getUnivDataUnitId() == null) {
                phonodocModel = new ArchPhonodoc();
                phonodocModel.setRecordTypeId(
                        se.getDescValueByCodes("RECORD_TYPE", "DOCUMENTARY").getDescriptorValueId());
                phonodocModel.setStorageUnits(new ArrayList<ArchPhonoStorageUnit>());
            } else {
                phonodocModel = complexQuery.queryPhonodocModel(model.getUnivDataUnitId());
            }
        }
        return phonodocModel;
    }

    public VComplexPhonodoc getPhonodocImmModel() {
        if (phonodocImmModel == null) {
            model = getModel();
            if (model.getUnivDataUnitId() == null) {
                phonodocImmModel = new VComplexPhonodoc();
                phonodocImmModel.setStorageUnitCount(0);
                phonodocImmModel.setStorageUnits(new ArrayList<VComplexPhonoStorageUnit>());
                phonodocImmModel.setRecordTypeCode(
                        se.getDescValue(getPhonodocModel().getRecordTypeId()).getValueCode());
            } else {
                phonodocImmModel = complexQuery.queryPhonodocImmModel(model.getUnivDataUnitId());
            }
        }
        return phonodocImmModel;
    }

    @Override
    protected Long getUnitTypeId() {
        return se.getDescValueByCodes("UNIV_UNIT_TYPE", "PHONODOC").getDescriptorValueId();
    }

    @Override
    protected MENU_PAGES getMenuPage() {
        return MENU_PAGES.PHONODOC;
    }

    public void changeRecordType() {
        phonodocModel = getPhonodocModel();
        phonodocModel.setPhonodocName(null);
        phonodocModel.setAuthors(null);
        phonodocModel.setEventPlace(null);
        phonodocModel.setPerformerOrchestra(null);
        phonodocModel.setPerformerChoir(null);
        phonodocModel.setPerformers(null);
        phonodocModel.setRubrics(null);
        getPhonodocImmModel().setRecordTypeCode(
                se.getDescValue(phonodocModel.getRecordTypeId()).getValueCode());
    }

    public void changePhonodocType() {
        for (ArchPhonoStorageUnit storageUnit : getPhonodocModel().getStorageUnits()) {
            storageUnit.setStorageTypeId(null);
        }
    }

    public PhonoStorageUnitEmbDataList getStorageUnitEmbDL() {
        if (storageUnitEmbDL == null) {
            storageUnitEmbDL = new PhonoStorageUnitEmbDataList() {
                @Override
                public PhonodocCard getCard() {
                    return PhonodocCard.this;
                }
            };
        }
        return storageUnitEmbDL;
    }

    public String getPlaytimeStr(Integer playtime) {
        if (playtime == null) {
            return null;
        }
        return String.format("%1$d:%2$02d", playtime / 60, playtime % 60);
    }

    public String getTotalPlaytimeStr() {
        int res = 0;
        boolean show = false;
        for (VComplexPhonoStorageUnit storageUnit : getPhonodocImmModel().getStorageUnits()) {
            if (storageUnit.getPlaytime() != null) {
                res += storageUnit.getPlaytime();
                show = true;
            }
        }
        return getPlaytimeStr(show ? res : null);
    }

    public void postFileUpload() {
        if (!afUpload.getUploadedFiles().isEmpty()) {
            ImagePath file = afUpload.getUploadedFiles().get(0);
            if (afUpload.getCategory().getValueCode().equals("AUDIO_UNIT")) {
                getStorageUnitEmbDL().attachAudioFile(afUpload.getSubId(), file);
            }
            if (afUpload.getCategory().getValueCode().equals("AUDIO_ANNOTATION")) {
                getPerfAnnoEmbDL().attachAudioFile(afUpload.getSubId(), file);
            }
        }
        afUpload.finishUploading();
    }

    public UnivClssEmbeddedDataList getUnivClssEmbDL() {
        if (univClssEmbDL == null) {
            univClssEmbDL = new UnivClssEmbeddedDataList() {
                @Override
                public ComplexCard getCard() {
                    return PhonodocCard.this;
                }
            };
        }
        return univClssEmbDL;
    }

    public void selectUnivClssValue() {
        getUnivClssEmbDL().addClassifierValue();
    }

    @Override
    public DescriptorEmbeddedDataList getDescriptorEmbDL() {
        if (descriptorEmbDL == null) {
            descriptorEmbDL = new DescriptorEmbeddedDataList() {
                @Override
                protected ComplexCard getCard() {
                    return PhonodocCard.this;
                }
            };
        }
        return descriptorEmbDL;
    }

    public PerformanceAnnoEmbDataList getPerfAnnoEmbDL() {
        if (perfAnnoEmbDL == null) {
            perfAnnoEmbDL = new PerformanceAnnoEmbDataList() {
                @Override
                public PhonodocCard getCard() {
                    return PhonodocCard.this;
                }
            };
        }
        return perfAnnoEmbDL;
    }

    @Override
    protected boolean validate() {
        boolean res = true;
        if (phonodocModel.getRecordTypeId() == null) {
            res &= MessageUtils.ErrorMessage("Вид записанного материала должен быть указан");
        }
        if (immModel.getFundNum() == null && (immModel.getFundLetter() != null || immModel.getFundPrefix()!= null)) {
            res &= MessageUtils.ErrorMessage("Отсутствует номер фонда");
        }
        if (StringUtils.getByteLengthUTF8(immModel.getFundLetter()) > 250) {
            res &= MessageUtils.ErrorMessage("Литера фонда слишком длинная");
        }
        if (StringUtils.getByteLengthUTF8(immModel.getFundPrefix()) > 4) {
            res &= MessageUtils.ErrorMessage("Префикс фонда слишком длинный");
        }
        if (immModel.getSeriesNum() == null && immModel.getSeriesLetter() != null) {
            res &= MessageUtils.ErrorMessage("Литера описи не может быть без номера");
        }
        if (StringUtils.getByteLengthUTF8(immModel.getSeriesLetter()) > 250) {
            res &= MessageUtils.ErrorMessage("Литера описи слишком длинная");
        }
        if (model.getNumberNumber() == null) {
            res &= MessageUtils.ErrorMessage("Номер единицы учёта должен быть заполнен");
        }
        if (StringUtils.getByteLengthUTF8(model.getNumberText()) > 250) {
            res &= MessageUtils.ErrorMessage("Литера единицы учёта слишком длинная");
        }
        if (phonodocModel.getPhonodocTypeId() == null) {
            res &= MessageUtils.ErrorMessage("Вид фонодокумента должен быть указан");
        }
        if (phonodocImmModel.getRecordTypeCode() != null) {
            if (phonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
                if (phonodocModel.getPhonodocName() == null) {
                    res &= MessageUtils.ErrorMessage("Название документа должно быть заполнено");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getPhonodocName()) > 2500) {
                    res &= MessageUtils.ErrorMessage("Название документа слишком длинное");
                }
                if (phonodocModel.getAuthors() == null) {
                    res &= MessageUtils.ErrorMessage("Автор должен быть указан");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getAuthors()) > 250) {
                    res &= MessageUtils.ErrorMessage("Автор: слишком длинное значение");
                }
                if (phonodocModel.getEventPlace() == null) {
                    res &= MessageUtils.ErrorMessage("Место события должно быть указано");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getEventPlace()) > 250) {
                    res &= MessageUtils.ErrorMessage("Место события: слшком длинное значение");
                }
            }
            if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
                if (phonodocModel.getPhonodocName() == null) {
                    res &= MessageUtils.ErrorMessage("Название произведения должно быть заполнено");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getPhonodocName()) > 2500) {
                    res &= MessageUtils.ErrorMessage("Название произведения слишком длинное");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getPerformerOrchestra()) > 250) {
                    res &= MessageUtils.ErrorMessage("Исполнитель (оркестр): слишком длинное значение");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getPerformerChoir()) > 250) {
                    res &= MessageUtils.ErrorMessage("Исполнитель (хор): слишком длинное значение");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getPerformers()) > 250) {
                    res &= MessageUtils.ErrorMessage("Исполнители: слишком длинное значение");
                }
                if (phonodocModel.getAuthors() == null) {
                    res &= MessageUtils.ErrorMessage("Авторы должны быть указаны");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getAuthors()) > 250) {
                    res &= MessageUtils.ErrorMessage("Авторы: слишком длинное значение");
                }
                if (StringUtils.getByteLengthUTF8(phonodocModel.getRubrics()) > 250) {
                    res &= MessageUtils.ErrorMessage("Рубрика: слишком длинное значение");
                }
            }
        }
        res &= getStorageUnitEmbDL().validate();
        if (intervalModel.getBeginDate() == null) {
            res &= MessageUtils.ErrorMessage("Дата события должна быть заполнена");
        }
        if (phonodocModel.getRecordDate() == null) {
            res &= MessageUtils.ErrorMessage("Дата записи должна быть заполнена");
        }
        if (StringUtils.getByteLengthUTF8(phonodocModel.getKitInfo()) > 250) {
            res &= MessageUtils.ErrorMessage("Состав комплекта: значение слишком длинное");
        }
        if (StringUtils.getByteLengthUTF8(phonodocModel.getManufacturerName()) > 250) {
            res &= MessageUtils.ErrorMessage("Наименование изготовителя слишком длинное");
        }
        if (StringUtils.getByteLengthUTF8(phonodocModel.getNotes()) > 4000) {
            res &= MessageUtils.ErrorMessage("Примечание слишком длинное");
        }
        res &= getPerfAnnoEmbDL().validate();

        return res;
    }

    @Override
    protected String fillArchNumberCode() {
        StringBuilder sb = new StringBuilder();
        String text = immModel.getFundPrefix();
        if (text != null) {
            sb.append(text).append("\u0001");
        }
        Integer number = immModel.getFundNum();
        if (number != null) {
            sb.append(String.format("%1$08d", number));
        }
        sb.append("\u0001");
        text = immModel.getFundLetter();
        if (text != null) {
            sb.append(text);
        }
        sb.append("\u0001");
        number = immModel.getSeriesNum();
        if (number != null) {
            sb.append(String.format("%1$08d", number));
        }
        sb.append("\u0001");
        text = immModel.getSeriesLetter();
        if (text != null) {
            sb.append(text);
        }
        sb.append(String.format("\u0001%1$08d\u0001", model.getNumberNumber()));
        text = model.getNumberText();
        if (text != null) {
            sb.append(text);
        }
        return sb.toString();
    }

    @Override
    protected String fillUnitName() {
        return String.format("Фонодокумент %1$d%2$s. %3$s",
                model.getNumberNumber(),
                model.getNumberText() == null ? "" : model.getNumberText(),
                phonodocModel.getPhonodocName());
    }

    @Override
    protected void saveDefaultLanguages() {
    }

    protected class PhonoLanguageCollection extends LanguageCollection {

        @Override
        protected ComplexCard getCard() {
            return PhonodocCard.this;
        }

        @Override
        protected void init() {
            for (ArchPerformanceAnnotation annotation : perfAnnoEmbDL.getWrappedData()) {
                if (annotation.getLanguageId() != null) {
                    UnivDataLanguage language = new UnivDataLanguage();
                    language.setDocLanguageId(annotation.getLanguageId());
                    addFilledItem(language);
                }
            }
        }
    }

    @Override
    protected void saveDetails() {
        UnivDataUnit copyModel = null;
        ArchPhonodoc copyPhonodocModel = null;

        boolean isNew = false;
        if (phonodocModel.getUnivDataUnitId() == null) {
            phonodocModel.setUnivDataUnitId(model.getUnivDataUnitId());
            em.persist(phonodocModel);
            isNew = true;
        } else {
            copyModel = complexQuery.queryModel(model.getUnivDataUnitId());
            copyPhonodocModel = complexQuery.queryPhonodocModel(model.getUnivDataUnitId());
            journalHelper.setPhonodocCopyModels(copyPhonodocModel,
                    complexQuery.queryPhonodocImmModel(model.getUnivDataUnitId()));
            em.detach(phonodocImmModel);
            phonodocModel = em.merge(phonodocModel);
        }

        saveIntervalModel();
        new PhonoLanguageCollection().save();
        storageUnitEmbDL.save(copyPhonodocModel);
        univClssEmbDL.save(copyModel);
        perfAnnoEmbDL.save(copyPhonodocModel);
        em.flush();

        if (isNew) {
            journalHelper.recordFullUnivDataUnit(model, "INPUT", cardLinks, null);
        } else {
            immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
            phonodocImmModel = complexQuery.queryPhonodocImmModel(model.getUnivDataUnitId());
            journalHelper.recordEditPhonodoc(model, immModel, phonodocModel, phonodocImmModel);
        }
    }
}
