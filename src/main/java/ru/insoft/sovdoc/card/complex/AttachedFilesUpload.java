package ru.insoft.sovdoc.card.complex;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.service.correct.JournalHelper;
import ru.insoft.sovdoc.system.SystemEntity;

import com.google.common.io.Files;

@ViewScoped
@ManagedBean
public class AttachedFilesUpload implements Serializable {

	@Inject
	SystemEntity se;
	@Inject
	ru.insoft.sovdoc.system.complex.SystemQuery complexQuery;
	@Inject
	ru.insoft.sovdoc.system.ebook.SystemQuery ebookQuery;
	@Inject
	EntityManager em;
	@Inject
	JournalHelper journalHelper;

	private Long unitId;
	private DescriptorValue category;
	private Long lastSortOrder;
	private String rootPath;
	private Long subId;

	private List<ImagePath> uploadedFiles;
	private Map<String, Integer> unknownFormats;
	private boolean unhandledException;

	public List<ImagePath> getUploadedFiles() {
		return uploadedFiles;
	}

	public DescriptorValue getCategory() {
		return category;
	}

	public Long getSubId() {
		return subId;
	}

	public void startUploading() {
		Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		unitId = Long.valueOf(reqMap.get("unitId"));
		category = se.getDescValueByCodes("IMAGE_CATEGORY", reqMap.get("category"));
		if (reqMap.get("subId") != null) {
			subId = Long.valueOf(reqMap.get("subId"));
		} else {
			subId = null;
		}
		lastSortOrder = complexQuery.queryImagePathCount(unitId, category);

		rootPath = se.getSystemParameterValue("DOCUMENT_ROOT");
		String valueCode = category.getValueCode();
		switch (valueCode) {

			case "FULL_SIZE":
				rootPath += se.getSystemParameterValue("RELATIVE_PATH_FULLSIZE");
				rootPath += unitId.toString();
				break;
			case "PREVIEW":
				rootPath += se.getSystemParameterValue("RELATIVE_PATH_PREVIEW");
				rootPath += unitId.toString();
				break;
			case "BOOK_COVER":
				rootPath += se.getSystemParameterValue("RELATIVE_PATH_COVERS");
				break;
			case "AUDIO_UNIT":
			case "VIDEO_UNIT":
			case "KINO_UNIT":
				rootPath += se.getSystemParameterValue("RELATIVE_PATH_AUDIO_UNITS");
				break;
			case "AUDIO_ANNOTATION":
			case "VIDEO_STORY_DESCRIPTION":
			case "KINO_STORY_DESCRIPTION":
				rootPath += se.getSystemParameterValue("RELATIVE_PATH_AUDIO_ANNOTA");
				break;
		}

		uploadedFiles = new ArrayList<ImagePath>();
		unknownFormats = new HashMap<String, Integer>();
		unhandledException = false;
	}

	public void fileUploadListener(FileUploadEvent event) {
		UploadedFile item = event.getUploadedFile();
		DescriptorValue format = null;
		try {
			format = se.getDescValueByCodes("FILE_FORMAT", item.getFileExtension());
		} catch (NoResultException e) {
			Integer cnt = unknownFormats.get(item.getContentType());
			if (cnt == null) {
				unknownFormats.put(item.getContentType(), 1);
			} else {
				unknownFormats.put(item.getContentType(), cnt + 1);
			}
			return;
		}
		try {
			ImagePath ip = new ImagePath();
			ip.setDataUnitId(unitId);
			ip.setImageCategory(category);
			if (lastSortOrder > 0) {
				ImagePath oldIp = null;
				String valueCode = category.getValueCode();
				switch (valueCode) {
					case "BOOK_COVER":
						oldIp = ebookQuery.queryBookCover(unitId);
						break;
					case "AUDIO_UNIT":
						oldIp = complexQuery.queryAudioUnitFile(subId);
						break;
					case "AUDIO_ANNOTATION":
						oldIp = complexQuery.queryAudioAnnotaFile(subId);
						break;
					case "VIDEO_UNIT":
						oldIp = complexQuery.queryVideoUnitFile(subId);
						break;
					case "VIDEO_STORY_DESCRIPTION":
						oldIp = complexQuery.queryVideoStoryDescFile(subId);
						break;
					case "KINO_UNIT":
						oldIp = complexQuery.queryKinoUnitFile(subId);
						break;
					case "KINO_STORY_DESCRIPTION":
						oldIp = complexQuery.queryKinoStoryDescFile(subId);
						break;
				}
				if (oldIp != null) {
					ip.setSortOrder(oldIp.getSortOrder());
					File oldFile = new File(oldIp.getFilePath(), oldIp.getFileName());
					if (oldFile.exists()) {
						oldFile.delete();
					}
					em.remove(oldIp);
				}
			}

			if (ip.getSortOrder() == null) {
				if (category.getValueCode().equals("BOOK_COVER")) {
					ip.setSortOrder(1L);
				} else {
					ip.setSortOrder(++lastSortOrder);
				}
			}
			ip.setFileName(item.getName());
			ip.setCaption(item.getName().substring(0, item.getName().lastIndexOf('.')));
			ip.setFilePath(rootPath);
			ip.setDescriptorValueId(format);
			ip = em.merge(ip);
			ip.setFileName(ip.getImagePathId().toString() + "." + item.getFileExtension());
			ip = em.merge(ip);
			uploadedFiles.add(ip);

			File dir = new File(ip.getFilePath());
			if (!dir.isDirectory()) {
				dir.mkdirs();
			}
			Files.write(item.getData(), new File(dir, ip.getFileName()));
		} catch (Exception e) {
			unhandledException = true;
			throw new RuntimeException(e);
		}
	}

	public void finishUploading() {
		if (unhandledException) {
			MessageUtils.ErrorMessage("В процессе загрузки файлов произошла ошибка");
		} else {
			MessageUtils.InfoMessage(String.valueOf(uploadedFiles.size()) + " файлов загружено");
			for (String format : unknownFormats.keySet()) {
				MessageUtils.ErrorMessage("Неизвестный формат " + format
						+ ": " + unknownFormats.get(format) + " файла");
			}

			journalHelper.recordAttachFiles(unitId, category.getValueCode(), uploadedFiles, subId);
		}

		//Наивно полагаю, что это позволит освободить память
		unitId = null;
		category = null;
		subId = null;
		lastSortOrder = null;
		rootPath = null;
		uploadedFiles = null;
		unknownFormats = null;
	}

	public void uploadOnce(FileUploadEvent event) {
		fileUploadListener(event);
		finishUploading();
	}
}
