package ru.insoft.sovdoc.card.complex;

import java.util.List;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ApplicationToDocument;
import ru.insoft.sovdoc.model.complex.table.ArchDocument;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexDocument;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@ViewScoped
@ManagedBean
public class DocumentCard extends ComplexCard {

  private ArchFund fundModel;
  private ArchSeries seriesModel;
  private ArchStorageUnit storageUnitModel;
  private ArchDocument documentModel;
  protected List<ApplicationToDocument> applForDelete;
  private VComplexDocument documentImmModel;
  private DescriptorEmbeddedDataList descriptorEmbDL;
  private LanguageCollection langCollection;
  private List<ApplicationToDocument> applicationToDocumentModel;

  public ArchFund getFundModel() {
    if (fundModel == null) {
      fundModel = getParentFund();
    }
    
    return fundModel;
  }
  
  public ArchSeries getSeriesModel() {
    if (seriesModel == null) {
      seriesModel = getParentSeries();
    }
    
    return seriesModel;
  }
  
  public ArchStorageUnit getStorageUnitModel() {
    if (storageUnitModel == null) {
      storageUnitModel = getParentStorageUnit();
    }
    
    return storageUnitModel;
  }
  
  public ArchDocument getDocumentModel() {
    if (documentModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        documentModel = new ArchDocument();
      } else {
        documentModel = complexQuery.queryDocumentModel(model.getUnivDataUnitId());
        journalHelper.copyDocumentModel(documentModel);
        journalHelper.copyDocumentImmModel(getDocumentImmModel());
        if (action == null || !action.equals("save")) {
          em.detach(documentModel);
        }
      }
    }
    return documentModel;
  }

  public VComplexDocument getDocumentImmModel() {
    if (documentImmModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        documentImmModel = new VComplexDocument();
      } else {
        documentImmModel = complexQuery.queryDocumentImmModel(model.getUnivDataUnitId());
      }
    }
    return documentImmModel;
  }

  ApplicationToDocument applToDoc;

  public List<ApplicationToDocument> getApplicationToDocumentModel() {

    if (applicationToDocumentModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        applicationToDocumentModel = new ArrayList<ApplicationToDocument>();
      } else {
        applicationToDocumentModel = complexQuery.queryApplicationToDocumenttModel(model.getUnivDataUnitId());
        journalHelper.copyApplicationToDocumentModel(applicationToDocumentModel);
        if (action == null || !action.equals("save")) {
          em.detach(applicationToDocumentModel);
        }
      }
    }
    return applicationToDocumentModel;
  }

  public void deleteApplToDoc(ApplicationToDocument applToDoc) {

    if (applForDelete == null) {
      applForDelete = new ArrayList<ApplicationToDocument>();
    }
    applForDelete.add(applToDoc);
    applicationToDocumentModel.remove(applToDoc);
  }

  public void addApplToDoc() {
    ApplicationToDocument appl = new ApplicationToDocument();
    applicationToDocumentModel.add(appl);
  }

  public void setApplicationToDocumentModel(List<ApplicationToDocument> applicationToDocumentModel) {
    this.applicationToDocumentModel = applicationToDocumentModel;
  }

  @Override
  protected Long getUnitTypeId() {
    return se.getImmDescValueByCodes("UNIV_UNIT_TYPE", "PAPER_DOC").getDescriptorValueId();
  }

  @Override
  protected MENU_PAGES getMenuPage() {
    return MENU_PAGES.PAPER_DOC;
  }

  @Override
  public DescriptorEmbeddedDataList getDescriptorEmbDL() {
    if (descriptorEmbDL == null) {
      descriptorEmbDL = new DescriptorEmbeddedDataList() {
        @Override
        protected ComplexCard getCard() {
          return DocumentCard.this;
        }
      };
    }
    return descriptorEmbDL;
  }

  public LanguageCollection getLangCollection() {
    if (langCollection == null) {
      langCollection = new LanguageCollection() {
        @Override
        protected ComplexCard getCard() {
          return DocumentCard.this;
        }
      };
    }
    return langCollection;
  }

  public List<VComplexDescriptor> getRubrics() {
    List<VComplexDescriptor> rubrics = Lists.newArrayList();
    for (VComplexDescriptor desc : getDescriptorEmbDL().getWrappedData()) {
      if (desc.getGroupCode() != null && desc.getGroupCode().equals("UNIT_RUBRICATOR")) {
        rubrics.add(desc);
      }
    }
    return rubrics;
  }

  @Override
  protected void saveDetails() {
    boolean isNew = false;
    if (documentModel.getUnivDataUnitId() == null) {
      documentModel.setUnivDataUnitId(model.getUnivDataUnitId());
      em.persist(documentModel);

      isNew = true;
    }

    //удаление приложения
    if (applForDelete != null) {
      for (int i = 0; i < applForDelete.size(); i++) {
        if (applForDelete.get(i).getApplToDocId() != null) {
          em.remove(em.merge(applForDelete.get(i)));
        }

      }

      applForDelete.clear();
    }
//добавление приложения
    for (int i = 0; i < applicationToDocumentModel.size(); i++) {

      if (applicationToDocumentModel.get(i).getUnivDataUnitId() == null) {
        applicationToDocumentModel.get(i).setUnivDataUnitId(model.getUnivDataUnitId());

        if ((applicationToDocumentModel.get(i).getApplName() != null && !applicationToDocumentModel.get(i).getApplName().trim().equals(""))
                || (applicationToDocumentModel.get(i).getApplNumber() != null && !applicationToDocumentModel.get(i).getApplNumber().trim().equals(""))
                || (applicationToDocumentModel.get(i).getPages() != null && !applicationToDocumentModel.get(i).getPages().trim().equals(""))
                || (applicationToDocumentModel.get(i).getPagesQuantity() != null && !applicationToDocumentModel.get(i).getPagesQuantity().toString().trim().equals(""))) {
          em.persist(applicationToDocumentModel.get(i));
        } else {
          applicationToDocumentModel.remove(applicationToDocumentModel.get(i));
          i--;
        }
      }
    }

// обновление приложения
    for (int i = 0; i < applicationToDocumentModel.size(); i++) {
      if ((applicationToDocumentModel.get(i).getApplName() != null && !applicationToDocumentModel.get(i).getApplName().trim().equals(""))
              || (applicationToDocumentModel.get(i).getApplNumber() != null && !applicationToDocumentModel.get(i).getApplNumber().trim().equals(""))
              || (applicationToDocumentModel.get(i).getPages() != null && !applicationToDocumentModel.get(i).getPages().trim().equals(""))
              || (applicationToDocumentModel.get(i).getPagesQuantity() != null && !applicationToDocumentModel.get(i).getPagesQuantity().toString().trim().equals(""))) {
        ApplicationToDocument appl = em.merge(applicationToDocumentModel.get(i));
        em.flush();
        em.refresh(appl);
      } else {
        em.remove(em.merge(applicationToDocumentModel.get(i)));
        applicationToDocumentModel.remove(applicationToDocumentModel.get(i));
        i--;
      }
    }

    saveIntervalModel();
    langCollection.save();

    documentModel = em.merge(documentModel);
    model = em.merge(model);
    em.flush();

    em.refresh(documentModel);
    em.refresh(model);

    immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());

    documentImmModel = complexQuery.queryDocumentImmModel(model.getUnivDataUnitId());
    if (isNew) {
      journalHelper.recordFullUnivDataUnit(model, "INPUT", cardLinks, null);
    } else {
      journalHelper.recordEditDocument(model, immModel, documentModel, documentImmModel, applicationToDocumentModel, cardLinks);
    }
  }

  @Override
  protected void saveDefaultLanguages() {
    if (langCollection.getItemCount() == 0) {
      super.saveDefaultLanguages();
    }
  }

  @Override
  protected boolean validate() {
    boolean res = true;
    if (immModel.getFundNum() == null) {
      res &= MessageUtils.ErrorMessage("Номер фонда должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getFundLetter()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера фонда слишком длинная");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getFundPrefix()) > 4) {
      res &= MessageUtils.ErrorMessage("Префикс фонда слишком длинный");
    }
    if (immModel.getSeriesNum() == null) {
      res &= MessageUtils.ErrorMessage("Номер описи должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getSeriesLetter()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера описи слишком длинная");
    }
    if (immModel.getUnitNum() == null) {
      res &= MessageUtils.ErrorMessage("Номер дела должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getUnitLetter()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера дела слишком длинная");
    }
    if (StringUtils.getByteLengthUTF8(model.getNumberText()) > 250) {
      res &= MessageUtils.ErrorMessage("Номера листов: слишком длинное значение");
    }
    if (documentModel.getDocumentTypeId() == null) {
      res &= MessageUtils.ErrorMessage("Вид документа должен быть указан");
    }
    if (documentModel.getDocumentNumber() == null) {
      res &= MessageUtils.ErrorMessage("Номер документа должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(documentModel.getDocumentNumber()) > 150) {
      res &= MessageUtils.ErrorMessage("Номер документа слишком длинный");
    }
    if (documentModel.getDocumentName() == null) {
      res &= MessageUtils.ErrorMessage("Заголовок документа должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(documentModel.getDocumentName()) > 2500) {
      res &= MessageUtils.ErrorMessage("Заголовок документа слишком длинный");
    }
    res &= validateIntervalModel();
    if (StringUtils.getByteLengthUTF8(documentModel.getDatesNote()) > 50) {
      res &= MessageUtils.ErrorMessage("Комментарий к дате слишком длинный");
    }
    if (StringUtils.getByteLengthUTF8(documentModel.getNotes()) > 4000) {
      res &= MessageUtils.ErrorMessage("Примечание слишком длинное");
    }

    if (!res) {
      em.detach(model);
      em.detach(documentModel);
      em.detach(intervalModel);
    }

    return res;
  }

  @Override
  protected String fillArchNumberCode() {
      String fundPrefix = immModel.getFundPrefix();
      String fundLetter = immModel.getFundLetter();
      String seriesLetter = immModel.getSeriesLetter();
      String unitLetter = immModel.getUnitLetter();
    String res = String.format("%1$s\u0001%2$08d\u0001%3$s\u0001%4$08d\u0001%5$s\u0001%6$08d\u0001%7$s\u0001",
            fundPrefix == null ? "" : fundPrefix, immModel.getFundNum(), fundLetter == null ? "" : fundLetter,
            immModel.getSeriesNum(), seriesLetter == null ? "" : seriesLetter,
            immModel.getUnitNum(), unitLetter == null ? "" : unitLetter);
    if (immModel.getUnitVolume() != null) {
      res += String.format("%1$05d", immModel.getUnitVolume());
    }
    res += "\u0001";
    if (immModel.getUnitPart() != null) {
      res += String.format("%1$05d", immModel.getUnitPart());
    }
    res += "\u0001";
    if (model.getNumberNumber() != null) {
      res += String.format("%1$05d", model.getNumberNumber());
    }
    res += "\u0001";
    if (model.getNumberText() != null) {
      res += model.getNumberText();
    }
    return res;
  }

  @Override
  protected String fillUnitName() {
    return String.format("Документ %1$s. %2$s", documentModel.getDocumentNumber(),
            documentModel.getDocumentName());
  }
  
  }
