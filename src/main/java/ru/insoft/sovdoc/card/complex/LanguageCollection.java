package ru.insoft.sovdoc.card.complex;

import java.util.List;
import ru.insoft.commons.jsf.ui.datalist.SimpleDataCollection;
import ru.insoft.sovdoc.model.complex.table.UnivDataLanguage;

public abstract class LanguageCollection extends SimpleDataCollection<UnivDataLanguage> {

  protected abstract ComplexCard getCard();

  @Override
  protected String getCountParamName() {
    return "langCount";
  }

  @Override
  protected List<UnivDataLanguage> queryItems() {
    if (getCard().getModel() != null) {
      return getCard().getModel().getLanguages();
    }
    return null;
  }

  @Override
  protected UnivDataLanguage copyItem(UnivDataLanguage item) {
    UnivDataLanguage newItem = new UnivDataLanguage();
    newItem.setDocLanguageId(item.getDocLanguageId());
    return newItem;
  }

  @Override
  protected UnivDataLanguage createEmptyItem() {
    return new UnivDataLanguage();
  }

  @Override
  protected boolean validateItem(UnivDataLanguage item) {
    return true;
  }

  @Override
  protected boolean isEmpty(UnivDataLanguage item) {
    return item.getDocLanguageId() == null;
  }

  @Override
  protected boolean areItemsEqual(UnivDataLanguage item1, UnivDataLanguage item2) {
    if (item1 == null) {
      return item2 == null;
    }
    if (item1.getDocLanguageId() == null) {
      return item2.getDocLanguageId() == null;
    }
    return item1.getDocLanguageId().equals(item2.getDocLanguageId());
  }

  @Override
  protected void insertNewItemToDB(UnivDataLanguage item) {
    item.setUnivDataUnit(getCard().getModel());
    getCard().em.persist(item);
  }

  @Override
  protected void removeItemFromDB(UnivDataLanguage item) {
     getCard().em.remove(getCard().em.merge(item));
  
   
  }
}
