package ru.insoft.sovdoc.card.complex;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchVideodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexVideoStoryDescription;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
public abstract class VideoStoryDescriptionEmbDataList extends EmbeddedDataList<ArchVideoStoryDescription> {

	public abstract VideodocCard getCard();

	protected List<VComplexVideoStoryDescription> immutableData;

	@Override
	public List<ArchVideoStoryDescription> getWrappedData() {
		if (super.getWrappedData() == null && getCard().getVideodocModel() != null) {
			int size = 0;
			for (ArchVideoStorageUnit storageUnit : getCard().getVideodocModel().getStorageUnits()) {
				size += storageUnit.getStoryDescriptions().size();
			}
			List<ArchVideoStoryDescription> res = Lists.newArrayListWithExpectedSize(size);
			for (int i = 0; i < size; i++) {
				res.add(null);
			}
			for (ArchVideoStorageUnit storageUnit : getCard().getVideodocModel().getStorageUnits()) {
				for (ArchVideoStoryDescription description : storageUnit.getStoryDescriptions()) {
					res.set(description.getStoryNumber() - 1, description);
				}
			}
			setWrappedData(res);
		}
		return super.getWrappedData();
	}

	protected List<VComplexVideoStoryDescription> getImmutableData() {
		if (immutableData == null && getCard().getVideodocImmModel() != null) {
			immutableData = getCard().getVideodocImmModel().getStoryDescriptions();
		}
		return immutableData;
	}

	public List<VComplexVideoStoryDescription> getFilteredImmutableData() {
		Map<String, String> reqmap
				= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (Strings.isNullOrEmpty(reqmap.get("storageUnitIdx"))) {
			return getImmutableData();
		} else {
			return getFilteredImmutableData(Integer.valueOf(reqmap.get("storageUnitIdx")));
		}
	}

	public List<VComplexVideoStoryDescription> getFilteredImmutableData(Integer storageUnitIdx) {
		ArchVideoStorageUnit storageUnit
				= getCard().getStorageUnitEmbDL().getWrappedData().get(storageUnitIdx);
		List<VComplexVideoStoryDescription> filteredImmData = Lists.newArrayList();
		if (getImmutableData() == null) {
			normalizeInputData();
		}
		for (VComplexVideoStoryDescription description : immutableData) {
			if (storageUnit.equals(description.getStorageUnit())
					|| storageUnit.getVideoStorageUnitId() != null && description.getStorageUnit() != null
					&& storageUnit.getVideoStorageUnitId().equals(description.getStorageUnit().getVideoStorageUnitId())) {
				filteredImmData.add(description);
			}
		}
		return filteredImmData;
	}

	public void addStoryDescription() {
		ArchVideoStoryDescription annotation = new ArchVideoStoryDescription();
		setRowCount(getRowCount() + 1);
		getWrappedData().add(annotation);
	}

	private boolean canClose;

	public void closeWindow() {
		canClose = true;
		Map<String, String> reqmap
				= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (getCard().isEditing()) {
			if (!validate()) {
				canClose = false;
				return;
			}
			if (Strings.isNullOrEmpty(reqmap.get("storageUnitIdx"))) {
				normalizeInputData();
			}
		}
	}

	public boolean getCanClose() {
		return canClose;
	}

	protected void normalizeInputData() {
		for (ArchVideoStorageUnit storageUnit : getCard().getStorageUnitEmbDL().getWrappedData()) {
			storageUnit.setStoryDescriptions(new ArrayList<ArchVideoStoryDescription>());
		}
		List<VDescValueWithCode> langDescValues = getCard().se.getLinearValuesByCode("DOC_LANGUAGE");
		immutableData = Lists.newArrayList();
		for (int i = 0; i < getWrappedData().size(); i++) {
			ArchVideoStoryDescription item = getWrappedData().get(i);
			item.setStoryNumber(i + 1);
			if (item.getStorageUnit() != null) {
				item.getStorageUnit().getStoryDescriptions().add(item);
			}

			VComplexVideoStoryDescription immItem = new VComplexVideoStoryDescription();
			immItem.setStoryDescriptionId(item.getStoryDescId());
			immItem.setStoryNumber(item.getStoryNumber());
			immItem.setPart(item.getPart());
			immItem.setAnnotation(item.getAnnotation());
			for (VDescValueWithCode lang : langDescValues) {
				if (lang.getDescriptorValueId().equals(item.getLanguageId())) {
					immItem.setLanguage(lang.getFullValue());
					break;
				}
			}
			immItem.setPlaytime(item.getPlaytime());
			immItem.setFilePathData(item.getFilePathData());
			if (item.getStorageUnit() != null) {
				String suNumber = "";
				if (item.getStorageUnit().getNumberNumber() != null) {
					suNumber += item.getStorageUnit().getNumberNumber().toString();
				}
				if (item.getStorageUnit().getNumberText() != null) {
					suNumber += item.getStorageUnit().getNumberText();
				}
				immItem.setStorageUnitNumber(suNumber);
				immItem.setStorageUnit(item.getStorageUnit());
			}
			immutableData.add(immItem);
		}
	}

	public void saveStoryDescriptions() {
		if (validate()) {
			EntityManager em = getCard().em;
			int i = 0;
			for (ArchVideoStoryDescription description : getWrappedData()) {
				description.setStoryNumber(++i);
				if (description.getStoryDescId()== null) {
					em.persist(description);
				} else {
					em.merge(description);
				}
			}
			em.flush();
		}
	}

	protected boolean validate() {
		boolean res = true;
		for (ArchVideoStoryDescription description : getWrappedData()) {
			if (description.getStorageUnit() == null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: единица хранения должна быть указана");
			}
			if (description.getAnnotation() == null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: аннотация должна быть заполнена");
			}
			if (description.getLanguageId()== null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: язык должен быть выбран");
			}
			if (description.getPlaytime() == null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: время должно быть заполнено");
			}
			if (!res) {
				return false;
			}
		}
		return res;
	}

	protected void save(ArchVideodoc copyModel) {
		for (ArchVideoStoryDescription item1 : getWrappedData()) {
			if (item1.getStoryDescId() == null) {
				getCard().em.persist(item1);
			} else {
				for (ArchVideoStorageUnit copyStorageUnit : copyModel.getStorageUnits()) {
					for (ArchVideoStoryDescription item2 : copyStorageUnit.getStoryDescriptions()) {
						if (item2.getStoryDescId().equals(item1.getStoryDescId())) {
							getCard().em.detach(item2);
							getCard().em.merge(item1);
							break;
						}
					}
				}
			}
		}
		if (copyModel != null) {
			for (ArchVideoStorageUnit copyStorageUnit : copyModel.getStorageUnits()) {
				for (ArchVideoStoryDescription item1 : copyStorageUnit.getStoryDescriptions()) {
					boolean found = false;
					for (ArchVideoStoryDescription item2 : getWrappedData()) {
						if (item1.getStoryDescId().equals(item2.getStoryDescId())) {
							found = true;
							break;
						}
					}
					if (!found) {
						getCard().getStorageUnitEmbDL().removeImagePath(item1.getFilePathData());
						getCard().em.remove(item1);
					}
				}
			}
		}
	}

	public void moveUp() {
		ArchVideoStoryDescription item1 = getWrappedData().get(getRowIndex());
		ArchVideoStoryDescription item2 = getWrappedData().get(getRowIndex() - 1);
		getWrappedData().set(getRowIndex() - 1, item1);
		getWrappedData().set(getRowIndex(), item2);
	}

	public void moveDown() {
		ArchVideoStoryDescription item1 = getWrappedData().get(getRowIndex());
		ArchVideoStoryDescription item2 = getWrappedData().get(getRowIndex() + 1);
		getWrappedData().set(getRowIndex() + 1, item1);
		getWrappedData().set(getRowIndex(), item2);
	}

	public String getConfirmRemoveText(int index) {
		ArchVideoStoryDescription annotation = getWrappedData().get(index);
		String res = "";
		if (annotation.getFilePathData() != null) {
			res += "К данному описанию сюжета прикреплён файл.<br/>";
		}
		res += "Вы действительно хотите удалить это описание сюжета?";
		return res;
	}

	public void removeStoryDescription() {
		Map<String, String> reqmap
				= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer index = Integer.valueOf(reqmap.get("index"));
		setRowCount(getRowCount() - 1);
		ArchVideoStoryDescription annotation = getWrappedData().remove(index.intValue());
		Long id = annotation.getStoryDescId();
		if (id != null) {
			EntityManager em = getCard().em;
			ArchVideoStoryDescription found = em.find(ArchVideoStoryDescription.class, id);
			if (found != null) {
				found.getStorageUnit().getStoryDescriptions().remove(found);
				em.remove(found);
				em.flush();
			}
		}
	}

	public void onRemoveStorageUnit(ArchVideoStorageUnit storageUnit) {
		List<ArchVideoStoryDescription> toRemove = Lists.newArrayList();
		for (int i = 0; i < getWrappedData().size(); i++) {
			ArchVideoStoryDescription annotation = getWrappedData().get(i);
			if (storageUnit.equals(annotation.getStorageUnit())) {
				toRemove.add(annotation);
			} else {
				annotation.setStoryNumber(i - toRemove.size() + 1);
			}
		}
		getWrappedData().removeAll(toRemove);
	}

	public void attachVideoFile(Long storyDescId, ImagePath file) {
		for (ArchVideoStoryDescription description : getWrappedData()) {
			if (storyDescId.equals(description.getStoryDescId())) {
				description.setFilePathData(file);
				getCard().em.merge(description);
			}
		}
	}
}
