package ru.insoft.sovdoc.card.complex;

import com.google.common.collect.Lists;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchPerformanceAnnotation;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
public abstract class PhonoStorageUnitEmbDataList extends EmbeddedDataList<ArchPhonoStorageUnit>
{
    public abstract PhonodocCard getCard();
    
    protected List<SelectItem> cbValues;
    protected Integer removedItemIdx;

    public Integer getRemovedItemIdx() 
    {
        return removedItemIdx;
    }

    public void setRemovedItemIdx(Integer removedItemIdx) 
    {
        this.removedItemIdx = removedItemIdx;
    }
    
    @Override
    public List<ArchPhonoStorageUnit> getWrappedData()
    {
        if (super.getWrappedData() == null && getCard().getPhonodocModel() != null)
            setWrappedData(getCard().getPhonodocModel().getStorageUnits());

        return super.getWrappedData();
    }
    
    public void addStorageUnit()
    {
        ArchPhonoStorageUnit storageUnit = new ArchPhonoStorageUnit();
        storageUnit.setAnnotations(new ArrayList<ArchPerformanceAnnotation>());
        setRowCount(getRowCount() + 1);
        getWrappedData().add(storageUnit);
        getCard().getPhonodocImmModel().setStorageUnitCount(getRowCount());
    }
    
    protected boolean validate()
    {
        boolean res = true;
        if (getRowCount() == 0)
            res &= MessageUtils.ErrorMessage("Таблица с описанием единиц хранения должна быть заполнена");
        for (ArchPhonoStorageUnit storageUnit : getWrappedData())
        {
            if (storageUnit.getNumberNumber() == null)
                res &= MessageUtils.ErrorMessage("Номер единицы хранения должен быть заполнен");
            if (StringUtils.getByteLengthUTF8(storageUnit.getNumberText()) > 4)
                res &= MessageUtils.ErrorMessage("Литера единицы хранения слишком длинная");
            if (storageUnit.getPlaytime() == null)
                res &= MessageUtils.ErrorMessage("Время звучания должно быть заполнено");
            if (storageUnit.getStorageTypeId() == null)
                res &= MessageUtils.ErrorMessage("Тип носителя должен быть указан");
            for (ArchPhonoStorageUnit storageUnit2 : getWrappedData())
                if (storageUnit != storageUnit2 &&
                        getCard().journalHelper.safeEquals(storageUnit.getNumberNumber(), storageUnit2.getNumberNumber()) &&
                        getCard().journalHelper.safeEquals(storageUnit.getNumberText(), storageUnit2.getNumberText()))
                {
                    res &= MessageUtils.ErrorMessage("Номера единиц хранения дублируются");
                    break;
                }
            if (!res)
                return false;
        }
        
        return res;
    }
    
    protected void save(ArchPhonodoc copyModel)
    {
        for (ArchPhonoStorageUnit item1 : getWrappedData())
            if (item1.getPhonoStorageUnitId() == null)
            {
                item1.setPhonodoc(getCard().getPhonodocModel());
                ArchPhonoStorageUnit merged = getCard().em.merge(item1);
                for (ArchPerformanceAnnotation annotation : item1.getAnnotations())
                    annotation.setStorageUnit(merged);
            }
            else
                for (ArchPhonoStorageUnit item2 : copyModel.getStorageUnits())
                    if (item2.getPhonoStorageUnitId().equals(item1.getPhonoStorageUnitId()))
                    {
                        item2.getAnnotations().iterator();
                        getCard().em.detach(item2);
                        getCard().em.merge(item1);
                        break;
                    }
        if (copyModel != null)
            for (ArchPhonoStorageUnit item1 : copyModel.getStorageUnits())
            {
                boolean found = false;
                for (ArchPhonoStorageUnit item2 : getWrappedData())
                    if (item1.getPhonoStorageUnitId().equals(item2.getPhonoStorageUnitId()))
                    {
                        found = true;
                        break;
                    }
                if (!found)
                {
                    removeImagePath(item1.getFilePathData());
                    for (ArchPerformanceAnnotation annotation : item1.getAnnotations())
                    {
                        removeImagePath(annotation.getFilePathData());
                        getCard().em.remove(annotation);
                    }
                    getCard().em.remove(item1);
                }
            }
    }
    
    public void removeImagePath(ImagePath ip)
    {
        if (ip == null)
            return;
        
        File file = new File(ip.getFilePath(), ip.getFileName());
        if (file.exists())
            file.delete();
        getCard().em.remove(ip);
    }
    
    public String getConfirmRemoveText()
    {
        if (removedItemIdx == null)
            return null;
        
        ArchPhonoStorageUnit storageUnit = getWrappedData().get(removedItemIdx);
        String res = "";
        if (storageUnit.getFilePathData() != null || storageUnit.getAnnotations().size() > 0)
        {
            res = "К данной единице хранения ";
            if (storageUnit.getFilePathData() != null)
            {
                res += "прикреплён файл";
                if (storageUnit.getAnnotations().size() > 0)
                    res += " и ";                    
            }
            else
                res += "прикреплено ";
            if (storageUnit.getAnnotations().size() > 0)
                res += String.valueOf(storageUnit.getAnnotations().size()) + 
                        " аннотаций выступлений, которые будут удалены ";
            else
                res += ",<br/>который будет удалён ";
            res += "вместе с единицей хранения.<br/>";
        }
        res += "Вы действительно хотите удалить эту единицу хранения?";
        return res;
    }
    
    public void removeStorageUnit()
    {
        ArchPhonoStorageUnit storageUnit = getWrappedData().get(removedItemIdx);
        getCard().getPerfAnnoEmbDL().onRemoveStorageUnit(storageUnit);
        setRowCount(getRowCount() - 1);
        getWrappedData().remove(storageUnit);
        getCard().getPhonodocImmModel().setStorageUnitCount(getRowCount());
        removedItemIdx = null;
    }
    
    public void attachAudioFile(Long storageUnitId, ImagePath file)
    {
        for (ArchPhonoStorageUnit storageUnit : getWrappedData())
            if (storageUnitId.equals(storageUnit.getPhonoStorageUnitId()))
            {
                storageUnit.setFilePathData(file);
                getCard().em.merge(storageUnit);
            }
    }
    
    public List<SelectItem> getCbValues()
    {
        if (cbValues == null)
        {
            List<SelectItem> lsi = Lists.newArrayList();
            for (int i = 0; i < getWrappedData().size(); i++)
            {
                ArchPhonoStorageUnit storageUnit = getWrappedData().get(i);
                String suNumber = "";
                if (storageUnit.getNumberNumber() != null)
                    suNumber += storageUnit.getNumberNumber().toString();
                if (storageUnit.getNumberText() != null)
                    suNumber += storageUnit.getNumberText();
                lsi.add(new SelectItem(storageUnit, suNumber));
            }
            cbValues = SelectItemUtils.withEmpty(lsi);
        }
        return cbValues;
    }
    
    public void refreshCbValues()
    {
        cbValues = null;
    }
}
