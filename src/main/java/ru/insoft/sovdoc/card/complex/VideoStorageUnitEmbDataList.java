package ru.insoft.sovdoc.card.complex;

import com.google.common.collect.Lists;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchVideodoc;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
public abstract class VideoStorageUnitEmbDataList extends EmbeddedDataList<ArchVideoStorageUnit> {

	public abstract VideodocCard getCard();

	protected List<SelectItem> cbValues;
	protected Integer removedItemIdx;

	public Integer getRemovedItemIdx() {
		return removedItemIdx;
	}

	public void setRemovedItemIdx(Integer removedItemIdx) {
		this.removedItemIdx = removedItemIdx;
	}

	@Override
	public List<ArchVideoStorageUnit> getWrappedData() {
		if (super.getWrappedData() == null && getCard().getVideodocModel() != null) {
			setWrappedData(getCard().getVideodocModel().getStorageUnits());
		}

		return super.getWrappedData();
	}

	public void addStorageUnit() {
		ArchVideoStorageUnit storageUnit = new ArchVideoStorageUnit();
		storageUnit.setStoryDescriptions(new ArrayList<ArchVideoStoryDescription>());
		setRowCount(getRowCount() + 1);
		getWrappedData().add(storageUnit);
		getCard().getVideodocImmModel().setStorageUnitCount(getRowCount());
	}

	protected boolean validate() {
		boolean res = true;
		if (getRowCount() == 0) {
			res &= MessageUtils.ErrorMessage("Таблица с описанием единиц хранения должна быть заполнена");
		}
		for (ArchVideoStorageUnit storageUnit : getWrappedData()) {
			if (storageUnit.getNumberNumber() == null) {
				res &= MessageUtils.ErrorMessage("Номер единицы хранения должен быть заполнен");
			}
			if (StringUtils.getByteLengthUTF8(storageUnit.getNumberText()) > 4) {
				res &= MessageUtils.ErrorMessage("Литера единицы хранения слишком длинная");
			}
			if (storageUnit.getPlaytime() == null) {
				res &= MessageUtils.ErrorMessage("Время звучания должно быть заполнено");
			}
			if (storageUnit.getSoundId() == null) {
				res &= MessageUtils.ErrorMessage("Необходимо выбрать звук");
			}
			if (storageUnit.getColorId() == null) {
				res &= MessageUtils.ErrorMessage("Необходимо выбрать цветность");
			}
			for (ArchVideoStorageUnit storageUnit2 : getWrappedData()) {
				if (storageUnit != storageUnit2
						&& getCard().journalHelper.safeEquals(storageUnit.getNumberNumber(), storageUnit2.getNumberNumber())
						&& getCard().journalHelper.safeEquals(storageUnit.getNumberText(), storageUnit2.getNumberText())) {
					res &= MessageUtils.ErrorMessage("Номера единиц хранения дублируются");
					break;
				}
			}
			if (!res) {
				return false;
			}
		}

		return res;
	}

	protected void save(ArchVideodoc copyModel) {
		for (ArchVideoStorageUnit item1 : getWrappedData()) {
			if (item1.getVideoStorageUnitId() == null) {
				item1.setVideodoc(getCard().getVideodocModel());
				ArchVideoStorageUnit merged = getCard().em.merge(item1);
				for (ArchVideoStoryDescription description : item1.getStoryDescriptions()) {
					description.setStorageUnit(merged);
				}
			} else {
				for (ArchVideoStorageUnit item2 : copyModel.getStorageUnits()) {
					if (item2.getVideoStorageUnitId().equals(item1.getVideoStorageUnitId())) {
						item2.getStoryDescriptions().iterator();
						getCard().em.detach(item2);
						getCard().em.merge(item1);
						break;
					}
				}
			}
		}
		if (copyModel != null) {
			for (ArchVideoStorageUnit item1 : copyModel.getStorageUnits()) {
				boolean found = false;
				for (ArchVideoStorageUnit item2 : getWrappedData()) {
					if (item1.getVideoStorageUnitId().equals(item2.getVideoStorageUnitId())) {
						found = true;
						break;
					}
				}
				if (!found) {
					removeImagePath(item1.getFilePathData());
					for (ArchVideoStoryDescription description : item1.getStoryDescriptions()) {
						removeImagePath(description.getFilePathData());
						getCard().em.remove(description);
					}
					getCard().em.remove(item1);
				}
			}
		}
	}

	public void removeImagePath(ImagePath ip) {
		if (ip == null) {
			return;
		}

		File file = new File(ip.getFilePath(), ip.getFileName());
		if (file.exists()) {
			file.delete();
		}
		getCard().em.remove(ip);
	}

	public String getConfirmRemoveText() {
		if (removedItemIdx == null) {
			return null;
		}

		ArchVideoStorageUnit storageUnit = getWrappedData().get(removedItemIdx);
		String res = "";
		int descriptionsSize = storageUnit.getStoryDescriptions().size();
		if (storageUnit.getFilePathData() != null || descriptionsSize > 0) {
			res = "К данной единице хранения ";
			if (storageUnit.getFilePathData() != null) {
				res += "прикреплён файл";
				if (descriptionsSize > 0) {
					res += " и ";
				}
			} else {
				res += "прикреплено ";
			}
			if (descriptionsSize > 0) {
				res += String.valueOf(descriptionsSize)
						+ " описаний сюжетов, которые будут удалены ";
			} else {
				res += ",<br/>который будет удалён ";
			}
			res += "вместе с единицей хранения.<br/>";
		}
		res += "Вы действительно хотите удалить эту единицу хранения?";
		return res;
	}

	public void removeStorageUnit() {
		ArchVideoStorageUnit storageUnit = getWrappedData().get(removedItemIdx);
		getCard().getVideoStoryDescEmbDL().onRemoveStorageUnit(storageUnit);
		setRowCount(getRowCount() - 1);
		getWrappedData().remove(storageUnit);
		getCard().getVideodocImmModel().setStorageUnitCount(getRowCount());
		removedItemIdx = null;
	}

	public void attachVideoFile(Long storageUnitId, ImagePath file) {
		for (ArchVideoStorageUnit storageUnit : getWrappedData()) {
			if (storageUnitId.equals(storageUnit.getVideoStorageUnitId())) {
				storageUnit.setFilePathData(file);
				getCard().em.merge(storageUnit);
			}
		}
	}

	public List<SelectItem> getCbValues() {
		if (cbValues == null) {
			List<SelectItem> lsi = Lists.newArrayList();
			for (int i = 0; i < getWrappedData().size(); i++) {
				ArchVideoStorageUnit storageUnit = getWrappedData().get(i);
				String suNumber = "";
				if (storageUnit.getNumberNumber() != null) {
					suNumber += storageUnit.getNumberNumber().toString();
				}
				if (storageUnit.getNumberText() != null) {
					suNumber += storageUnit.getNumberText();
				}
				lsi.add(new SelectItem(storageUnit, suNumber));
			}
			cbValues = SelectItemUtils.withEmpty(lsi);
		}
		return cbValues;
	}

	public void refreshCbValues() {
		cbValues = null;
	}
}
