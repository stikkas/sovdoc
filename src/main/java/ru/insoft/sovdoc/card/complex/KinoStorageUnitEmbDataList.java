package ru.insoft.sovdoc.card.complex;

import com.google.common.collect.Lists;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchKinodoc;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
public abstract class KinoStorageUnitEmbDataList extends EmbeddedDataList<ArchKinoStorageUnit> {

	public abstract KinodocCard getCard();

	protected List<SelectItem> cbValues;
	protected Integer removedItemIdx;

	public Integer getRemovedItemIdx() {
		return removedItemIdx;
	}

	public void setRemovedItemIdx(Integer removedItemIdx) {
		this.removedItemIdx = removedItemIdx;
	}

	@Override
	public List<ArchKinoStorageUnit> getWrappedData() {
		if (super.getWrappedData() == null && getCard().getKinodocModel() != null) {
			setWrappedData(getCard().getKinodocModel().getStorageUnits());
		}

		return super.getWrappedData();
	}

	public void addStorageUnit() {
		ArchKinoStorageUnit storageUnit = new ArchKinoStorageUnit();
		storageUnit.setStoryDescriptions(new ArrayList<ArchKinoStoryDescription>());
		setRowCount(getRowCount() + 1);
		getWrappedData().add(storageUnit);
		getCard().getKinodocImmModel().setStorageUnitCount(getRowCount());
	}

	protected boolean validate() {
		boolean res = true;
		if (getRowCount() == 0) {
			res &= MessageUtils.ErrorMessage("Таблица с описанием единиц хранения должна быть заполнена");
		}
		for (ArchKinoStorageUnit storageUnit : getWrappedData()) {
			if (storageUnit.getNumberNumber() == null) {
				res &= MessageUtils.ErrorMessage("Номер единицы хранения должен быть заполнен");
			}
			if (StringUtils.getByteLengthUTF8(storageUnit.getNumberText()) > 4) {
				res &= MessageUtils.ErrorMessage("Литера единицы хранения слишком длинная");
			}
			if (storageUnit.getPlaytime() == null) {
				res &= MessageUtils.ErrorMessage("Время звучания должно быть заполнено");
			}
			if (storageUnit.getSoundId() == null) {
				res &= MessageUtils.ErrorMessage("Необходимо выбрать звук");
			}
			if (storageUnit.getColorId() == null) {
				res &= MessageUtils.ErrorMessage("Необходимо выбрать цветность");
			}
			for (ArchKinoStorageUnit storageUnit2 : getWrappedData()) {
				if (storageUnit != storageUnit2
						&& getCard().journalHelper.safeEquals(storageUnit.getNumberNumber(), storageUnit2.getNumberNumber())
						&& getCard().journalHelper.safeEquals(storageUnit.getNumberText(), storageUnit2.getNumberText())) {
					res &= MessageUtils.ErrorMessage("Номера единиц хранения дублируются");
					break;
				}
			}
			if (!res) {
				return false;
			}
		}

		return res;
	}

	protected void save(ArchKinodoc copyModel) {
		for (ArchKinoStorageUnit item1 : getWrappedData()) {
			if (item1.getKinoStorageUnitId() == null) {
				item1.setKinodoc(getCard().getKinodocModel());
				ArchKinoStorageUnit merged = getCard().em.merge(item1);
				for (ArchKinoStoryDescription description : item1.getStoryDescriptions()) {
					description.setStorageUnit(merged);
				}
			} else {
				for (ArchKinoStorageUnit item2 : copyModel.getStorageUnits()) {
					if (item2.getKinoStorageUnitId().equals(item1.getKinoStorageUnitId())) {
						item2.getStoryDescriptions().iterator();
						getCard().em.detach(item2);
						getCard().em.merge(item1);
						break;
					}
				}
			}
		}
		if (copyModel != null) {
			for (ArchKinoStorageUnit item1 : copyModel.getStorageUnits()) {
				boolean found = false;
				for (ArchKinoStorageUnit item2 : getWrappedData()) {
					if (item1.getKinoStorageUnitId().equals(item2.getKinoStorageUnitId())) {
						found = true;
						break;
					}
				}
				if (!found) {
					removeImagePath(item1.getFilePathData());
					for (ArchKinoStoryDescription description : item1.getStoryDescriptions()) {
						removeImagePath(description.getFilePathData());
						getCard().em.remove(description);
					}
					getCard().em.remove(item1);
				}
			}
		}
	}

	public void removeImagePath(ImagePath ip) {
		if (ip == null) {
			return;
		}

		File file = new File(ip.getFilePath(), ip.getFileName());
		if (file.exists()) {
			file.delete();
		}
		getCard().em.remove(ip);
	}

	public String getConfirmRemoveText() {
		if (removedItemIdx == null) {
			return null;
		}

		ArchKinoStorageUnit storageUnit = getWrappedData().get(removedItemIdx);
		String res = "";
		int descriptionsSize = storageUnit.getStoryDescriptions().size();
		if (storageUnit.getFilePathData() != null || descriptionsSize > 0) {
			res = "К данной единице хранения ";
			if (storageUnit.getFilePathData() != null) {
				res += "прикреплён файл";
				if (descriptionsSize > 0) {
					res += " и ";
				}
			} else {
				res += "прикреплено ";
			}
			if (descriptionsSize > 0) {
				res += String.valueOf(descriptionsSize)
						+ " описаний сюжетов, которые будут удалены ";
			} else {
				res += ",<br/>который будет удалён ";
			}
			res += "вместе с единицей хранения.<br/>";
		}
		res += "Вы действительно хотите удалить эту единицу хранения?";
		return res;
	}

	public void removeStorageUnit() {
		ArchKinoStorageUnit storageUnit = getWrappedData().get(removedItemIdx);
		getCard().getKinoStoryDescEmbDL().onRemoveStorageUnit(storageUnit);
		setRowCount(getRowCount() - 1);
		getWrappedData().remove(storageUnit);
		getCard().getKinodocImmModel().setStorageUnitCount(getRowCount());
		removedItemIdx = null;
	}

	public void attachKinoFile(Long storageUnitId, ImagePath file) {
		for (ArchKinoStorageUnit storageUnit : getWrappedData()) {
			if (storageUnitId.equals(storageUnit.getKinoStorageUnitId())) {
				storageUnit.setFilePathData(file);
				getCard().em.merge(storageUnit);
			}
		}
	}

	public List<SelectItem> getCbValues() {
		if (cbValues == null) {
			List<SelectItem> lsi = Lists.newArrayList();
			for (int i = 0; i < getWrappedData().size(); i++) {
				ArchKinoStorageUnit storageUnit = getWrappedData().get(i);
				String suNumber = "";
				if (storageUnit.getNumberNumber() != null) {
					suNumber += storageUnit.getNumberNumber().toString();
				}
				if (storageUnit.getNumberText() != null) {
					suNumber += storageUnit.getNumberText();
				}
				lsi.add(new SelectItem(storageUnit, suNumber));
			}
			cbValues = SelectItemUtils.withEmpty(lsi);
		}
		return cbValues;
	}

	public void refreshCbValues() {
		cbValues = null;
	}
}
