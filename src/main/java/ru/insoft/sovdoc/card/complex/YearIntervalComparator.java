package ru.insoft.sovdoc.card.complex;

import java.util.Comparator;

import ru.insoft.sovdoc.model.complex.view.VComplexIntervalYears;

public class YearIntervalComparator implements Comparator<VComplexIntervalYears> {

	@Override
	public int compare(VComplexIntervalYears o1, VComplexIntervalYears o2) 
	{
		if (o1.getBeginYear() == null)
		{
			if (o2.getBeginYear() != null)
				return 1;
		}
		else
		{
			if (o2.getBeginYear() == null || o1.getBeginYear() < o2.getBeginYear())
				return -1;
			if (o1.getBeginYear() > o2.getBeginYear())
				return 1;
		}
		
		if (o1.getEndYear() == null)
		{
			if (o2.getEndYear() != null)
				return 1;
		}
		else
		{
			if (o2.getEndYear() == null || o1.getEndYear() < o2.getEndYear())
				return -1;
			if (o1.getEndYear() > o2.getEndYear())
				return 1;
		}
		return 0;
	}
}
