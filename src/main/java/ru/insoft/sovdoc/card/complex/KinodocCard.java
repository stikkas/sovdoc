package ru.insoft.sovdoc.card.complex;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchKinodoc;
import ru.insoft.sovdoc.model.complex.table.UnivDataLanguage;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexKinoStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexKinodoc;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

/**
 *
 * @author melnikov
 */
@ViewScoped
@ManagedBean
public class KinodocCard extends ComplexCard {

	@ManagedProperty("#{attachedFilesUpload}")
	AttachedFilesUpload afUpload;

	public AttachedFilesUpload getAfUpload() {
		return afUpload;
	}

	public void setAfUpload(AttachedFilesUpload afUpload) {
		this.afUpload = afUpload;
	}

	private Boolean editing;
	private ArchFund fundModel;
	private ArchSeries seriesModel;
	private ArchKinodoc kinodocModel;

	private VComplexKinodoc kinodocImmModel;

	private KinoStorageUnitEmbDataList storageUnitEmbDL;

	private DescriptorEmbeddedDataList descriptorEmbDL;
	private UnivClssEmbeddedDataList univClssEmbDL;
	private KinoStoryDescriptionEmbDataList kinoStoryDescEmbDL;

	@Override
	public boolean isEditing() {
		if (editing == null) {
			editing = super.isEditing();
		}
		return editing;
	}

	public ArchFund getFundModel() {
		if (fundModel == null) {
			fundModel = getParentFund();
		}
		return fundModel;
	}

	public ArchSeries getSeriesModel() {
		if (seriesModel == null) {
			seriesModel = getParentSeries();
		}
		return seriesModel;
	}

	public ArchKinodoc getKinodocModel() {
		if (kinodocModel == null) {
			model = getModel();
			if (model.getUnivDataUnitId() == null) {
				kinodocModel = new ArchKinodoc();
				kinodocModel.setStorageUnits(new ArrayList<ArchKinoStorageUnit>());
			} else {
				kinodocModel = complexQuery.queryKinodocModel(model.getUnivDataUnitId());
			}
		}
		return kinodocModel;
	}

	public VComplexKinodoc getKinodocImmModel() {
		if (kinodocImmModel == null) {
			model = getModel();
			if (model.getUnivDataUnitId() == null) {
				kinodocImmModel = new VComplexKinodoc();
				kinodocImmModel.setStorageUnitCount(0);
				kinodocImmModel.setStorageUnits(new ArrayList<VComplexKinoStorageUnit>());
			} else {
				kinodocImmModel = complexQuery.queryKinodocImmModel(model.getUnivDataUnitId());
			}
		}
		return kinodocImmModel;
	}

	@Override
	protected Long getUnitTypeId() {
		return se.getDescValueByCodes("UNIV_UNIT_TYPE", "KINODOC").getDescriptorValueId();
	}

	@Override
	protected MENU_PAGES getMenuPage() {
		return MENU_PAGES.KINODOC;
	}

	public KinoStorageUnitEmbDataList getStorageUnitEmbDL() {
		if (storageUnitEmbDL == null) {
			storageUnitEmbDL = new KinoStorageUnitEmbDataList() {
				@Override
				public KinodocCard getCard() {
					return KinodocCard.this;
				}
			};
		}
		return storageUnitEmbDL;
	}

	public String getPlaytimeStr(Integer playtime) {
		if (playtime == null) {
			return null;
		}
		return String.format("%1$d:%2$02d", playtime / 60, playtime % 60);
	}

	public String getTotalPlaytimeStr() {
		int res = 0;
		boolean show = false;
		for (VComplexKinoStorageUnit storageUnit : getKinodocImmModel().getStorageUnits()) {
			if (storageUnit.getPlaytime() != null) {
				res += storageUnit.getPlaytime();
				show = true;
			}
		}
		return getPlaytimeStr(show ? res : null);
	}

	public void postFileUpload() {
		if (!afUpload.getUploadedFiles().isEmpty()) {
			ImagePath file = afUpload.getUploadedFiles().get(0);
			if (afUpload.getCategory().getValueCode().equals("KINO_UNIT")) {
				getStorageUnitEmbDL().attachKinoFile(afUpload.getSubId(), file);
			}
			if (afUpload.getCategory().getValueCode().equals("KINO_STORY_DESCRIPTION")) {
				getKinoStoryDescEmbDL().attachKinoFile(afUpload.getSubId(), file);
			}
		}
		afUpload.finishUploading();
	}

	public UnivClssEmbeddedDataList getUnivClssEmbDL() {
		if (univClssEmbDL == null) {
			univClssEmbDL = new UnivClssEmbeddedDataList() {
				@Override
				public ComplexCard getCard() {
					return KinodocCard.this;
				}
			};
		}
		return univClssEmbDL;
	}

	public void selectUnivClssValue() {
		getUnivClssEmbDL().addClassifierValue();
	}

	@Override
	public DescriptorEmbeddedDataList getDescriptorEmbDL() {
		if (descriptorEmbDL == null) {
			descriptorEmbDL = new DescriptorEmbeddedDataList() {
				@Override
				protected ComplexCard getCard() {
					return KinodocCard.this;
				}
			};
		}
		return descriptorEmbDL;
	}

	public KinoStoryDescriptionEmbDataList getKinoStoryDescEmbDL() {
		if (kinoStoryDescEmbDL == null) {
			kinoStoryDescEmbDL = new KinoStoryDescriptionEmbDataList() {
				@Override
				public KinodocCard getCard() {
					return KinodocCard.this;
				}
			};
		}
		return kinoStoryDescEmbDL;
	}

	@Override
	protected boolean validate() {
		boolean res = true;
		if (immModel.getFundNum() == null && (immModel.getFundLetter() != null || immModel.getFundPrefix() != null)) {
			res &= MessageUtils.ErrorMessage("Отсутствует номер фонда");
		}
		if (StringUtils.getByteLengthUTF8(immModel.getFundLetter()) > 250) {
			res &= MessageUtils.ErrorMessage("Литера фонда слишком длинная");
		}
		if (StringUtils.getByteLengthUTF8(immModel.getFundPrefix()) > 4) {
			res &= MessageUtils.ErrorMessage("Префикс фонда слишком длинный");
		}
		if (immModel.getSeriesNum() == null && immModel.getSeriesLetter() != null) {
			res &= MessageUtils.ErrorMessage("Литера описи не может быть без номера");
		}
		if (StringUtils.getByteLengthUTF8(immModel.getSeriesLetter()) > 250) {
			res &= MessageUtils.ErrorMessage("Литера описи слишком длинная");
		}
		if (model.getNumberNumber() == null) {
			res &= MessageUtils.ErrorMessage("Номер единицы учёта должен быть заполнен");
		}
		if (StringUtils.getByteLengthUTF8(model.getNumberText()) > 250) {
			res &= MessageUtils.ErrorMessage("Литера единицы учёта слишком длинная");
		}
		if (kinodocModel.getKinodocTypeId() == null) {
			res &= MessageUtils.ErrorMessage("Вид кинодокумента должен быть указан");
		}
		if (kinodocModel.getKinodocName() == null) {
			res &= MessageUtils.ErrorMessage("Название документа должно быть заполнено");
		}
		if (StringUtils.getByteLengthUTF8(kinodocModel.getKinodocName()) > 2500) {
			res &= MessageUtils.ErrorMessage("Название документа слишком длинное");
		}
		if (kinodocModel.getEventPlace() == null) {
			res &= MessageUtils.ErrorMessage("Место события должно быть указано");
		}
		if (StringUtils.getByteLengthUTF8(kinodocModel.getEventPlace()) > 250) {
			res &= MessageUtils.ErrorMessage("Место события слишком длинное");
		}
		if (kinodocModel.getFilmStudio() == null) {
			res &= MessageUtils.ErrorMessage("Киностудия должна быть указана");
		}
		if (StringUtils.getByteLengthUTF8(kinodocModel.getFilmStudio()) > 250) {
			res &= MessageUtils.ErrorMessage("Название киностудии слишком длинное");
		}
		if (kinodocModel.getDirector() == null) {
			res &= MessageUtils.ErrorMessage("Режиссер должен быть указан");
		}
		if (StringUtils.getByteLengthUTF8(kinodocModel.getDirector()) > 250) {
			res &= MessageUtils.ErrorMessage("Имя режиссера слишком длинное");
		}
		res &= getStorageUnitEmbDL().validate();
		if (intervalModel.getBeginDate() == null) {
			res &= MessageUtils.ErrorMessage("Дата события должна быть заполнена");
		}
		if (kinodocModel.getIssueDate()== null) {
			res &= MessageUtils.ErrorMessage("Дата выпуска должна быть заполнена");
		}
		if (StringUtils.getByteLengthUTF8(kinodocModel.getNotes()) > 4000) {
			res &= MessageUtils.ErrorMessage("Примечание слишком длинное");
		}
		res &= getKinoStoryDescEmbDL().validate();

		return res;
	}

	@Override
	protected String fillArchNumberCode() {
		StringBuilder sb = new StringBuilder();
		String text = immModel.getFundPrefix();
		if (text != null) {
			sb.append(text).append("\u0001");
		}
		Integer number = immModel.getFundNum();
		if (number != null) {
			sb.append(String.format("%1$08d", number));
		}
		sb.append("\u0001");
		text = immModel.getFundLetter();
		if (text != null) {
			sb.append(text);
		}
		sb.append("\u0001");
		number = immModel.getSeriesNum();
		if (number != null) {
			sb.append(String.format("%1$08d", number));
		}
		sb.append("\u0001");
		text = immModel.getSeriesLetter();
		if (text != null) {
			sb.append(text);
		}
		sb.append(String.format("\u0001%1$08d\u0001", model.getNumberNumber()));
		text = model.getNumberText();
		if (text != null) {
			sb.append(text);
		}
		return sb.toString();
	}

	@Override
	protected String fillUnitName() {
		return String.format("Кинодокумент %1$d%2$s. %3$s",
				model.getNumberNumber(),
				model.getNumberText() == null ? "" : model.getNumberText(),
				kinodocModel.getKinodocName());
	}

	@Override
	protected void saveDefaultLanguages() {
	}

	protected class PhonoLanguageCollection extends LanguageCollection {

		@Override
		protected ComplexCard getCard() {
			return KinodocCard.this;
		}

		@Override
		protected void init() {
			for (ArchKinoStoryDescription description : kinoStoryDescEmbDL.getWrappedData()) {
				if (description.getLanguageId() != null) {
					UnivDataLanguage language = new UnivDataLanguage();
					language.setDocLanguageId(description.getLanguageId());
					addFilledItem(language);
				}
			}
		}
	}

	@Override
	protected void saveDetails() {
		UnivDataUnit copyModel = null;
		ArchKinodoc copyKinodocModel = null;

		boolean isNew = false;
		if (kinodocModel.getUnivDataUnitId() == null) {
			kinodocModel.setUnivDataUnitId(model.getUnivDataUnitId());
			em.persist(kinodocModel);
			isNew = true;
		} else {
			copyModel = complexQuery.queryModel(model.getUnivDataUnitId());
			copyKinodocModel = complexQuery.queryKinodocModel(model.getUnivDataUnitId());
			journalHelper.setKinodocCopyModels(copyKinodocModel,
					complexQuery.queryKinodocImmModel(model.getUnivDataUnitId()));
			em.detach(kinodocImmModel);
			kinodocModel = em.merge(kinodocModel);
		}

		saveIntervalModel();
		new PhonoLanguageCollection().save();
		storageUnitEmbDL.save(copyKinodocModel);
		univClssEmbDL.save(copyModel);
		kinoStoryDescEmbDL.save(copyKinodocModel);
		em.flush();

		if (isNew) {
			journalHelper.recordFullUnivDataUnit(model, "INPUT", cardLinks, null);
		} else {
			immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
            kinodocImmModel = complexQuery.queryKinodocImmModel(model.getUnivDataUnitId());
            journalHelper.recordEditKinodoc(model, immModel, kinodocModel, kinodocImmModel);
		}
	}
}
