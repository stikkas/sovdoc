package ru.insoft.sovdoc.card.complex;

import com.google.common.base.Strings;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author melnikov
 */
@ManagedBean(name = "kinoStorageUnitConverter")
public class KinoStorageUnitConverter implements Converter
{
    @ManagedProperty("#{kinodocCard}")
    KinodocCard card;

    public KinodocCard getCard() 
    {
        return card;
    }

    public void setCard(KinodocCard card) 
    {
        this.card = card;
    }

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) 
    {
        if (Strings.isNullOrEmpty(string))
            return null;
        int idx = Integer.valueOf(string);
        return card.getStorageUnitEmbDL().getWrappedData().get(idx);
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) 
    {
        if (o == null)
            return "";
        return String.valueOf(card.getStorageUnitEmbDL().getWrappedData().indexOf(o));
    }    
}
