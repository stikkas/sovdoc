package ru.insoft.sovdoc.card.complex;

import java.util.List;

import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;

public abstract class DescriptorEmbeddedDataList extends EmbeddedDataList<VComplexDescriptor> {

  protected abstract ComplexCard getCard();

  @Override
  public List<VComplexDescriptor> getWrappedData() {
    if (super.getWrappedData() == null && getCard().getImmModel() != null) {
      setWrappedData(getCard().getImmModel().getDescriptors());
    }
    return super.getWrappedData();
  }

  public void remove(VComplexDescriptor desc) {
    getWrappedData().remove(desc);
    if (desc.getDescriptor1() != null) {
      UnivDescriptor univDescForDelete = getCard().complexQuery.queryUnivDescriptor(desc.getUnivDescriptorId());
      getCard().deleteDescriptor(univDescForDelete);
    }

  }

}
