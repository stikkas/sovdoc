package ru.insoft.sovdoc.card.complex;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchFundName;


public abstract class FundNameEmbeddedDataList extends EmbeddedDataList<ArchFundName> {

  protected abstract FundCard getCard();

	public FundNameEmbeddedDataList()
	{
    String p_nameCount = FacesContext.getCurrentInstance().getExternalContext()
            .getRequestParameterMap().get("nameCount");
		if (p_nameCount != null && !p_nameCount.isEmpty())
      setRowCount(Integer.valueOf(p_nameCount));
		else
		{
			if (getCard().getFundModel().getDataUnitId() == null)
        setRowCount(1);
			else
        setRowCount(getCard().getFundModel().getFundNames().size());
      }
    }

	public void increaseNameCount()
	{
    setRowCount(getRowCount() + 1);
    addEmptyNames();
  }

  @Override
	public List<ArchFundName> getWrappedData()
	{
		if (super.getWrappedData() == null && getCard().getFundModel() != null)
		{
      addEmptyNames();
			if (!"save".equals(getCard().action))
        detachContents(getCard().em);
      }
    return super.getWrappedData();
  }

	protected void addEmptyNames()
	{
    List<ArchFundName> names = null;
		if (super.getWrappedData() == null)
      names = getCard().getFundModel().getFundNames();
		else
      names = super.getWrappedData();
		if (getRowCount() > names.size())
			for (int i = 0; i < getRowCount(); i++)	
				if (i < names.size())
					names.get(i).setSortOrder(names.get(i).getSortOrder() + 
							getRowCount() - names.size());
				else
				{
          ArchFundName name = new ArchFundName();
          //name.setFund(getCard().getFundModel());
          name.setSortOrder(getRowCount() - i);
          names.add(0, name);
        }
    setWrappedData(names);
  }

	public ArchFundName getActualName()
	{
		if (getWrappedData() != null && getWrappedData().size() > 0)
      return getWrappedData().get(0);
		else
      return null;
    }

	public void moveUp()
	{
		if (getRowIndex() == null || getRowIndex().equals(0))
      return;

    ArchFundName name1 = getWrappedData().get(getRowIndex());
    getCard().em.detach(name1);
    name1.setSortOrder(getRowIndex());
    ArchFundName name2 = getWrappedData().get(getRowIndex() - 1);
    getCard().em.detach(name2);
    name2.setSortOrder(getRowIndex() + 1);
    getWrappedData().set(getRowIndex() - 1, name1);
    getWrappedData().set(getRowIndex(), name2);
  }

	public void moveDown()
	{
		if (getRowIndex() == null || getRowIndex() >= getRowCount() - 1)
      return;

    ArchFundName name1 = getWrappedData().get(getRowIndex());
    getCard().em.detach(name1);
    name1.setSortOrder(getRowIndex() + 2);
    ArchFundName name2 = getWrappedData().get(getRowIndex() + 1);
    getCard().em.detach(name2);
    name2.setSortOrder(getRowIndex() + 1);
    getWrappedData().set(getRowIndex() + 1, name1);
    getWrappedData().set(getRowIndex(), name2);
  }

	public void remove(ArchFundName name)
	{
		if (getWrappedData().size() == 1)
		{
      MessageUtils.ErrorMessage("Невозможно удалить последнее название фонда");
      return;
    }
    getWrappedData().remove(name);
    /*if (name.getFundNameId() != null)
     {
     getCard().journalHelper.copyModel(getCard().getModel());
     getCard().journalHelper.copyFundModel(getCard().getFundModel());
     getCard().journalHelper.copyFundImmModel(getCard().getFundImmModel());
			
     name = getCard().em.find(ArchFundName.class, name.getFundNameId());
     getCard().em.remove(name);
     getCard().em.flush();
     getCard().refreshFundModel();
			
     getCard().journalHelper.recordEditFund(getCard().getModel(), 
     getCard().getImmModel(), getCard().getFundModel(), 
     getCard().getFundImmModel());
     }*/
    setRowCount(getRowCount() - 1);
  }

	public boolean validate()
	{
		for (ArchFundName name : getWrappedData())
		{
      boolean res = true;
			if (name.getFundName() == null)
        res &= MessageUtils.ErrorMessage("Название фонда не может быть пустым");
			if (StringUtils.getByteLengthUTF8(name.getFundName()) > 4000)
        res &= MessageUtils.ErrorMessage("Название фонда слишком длинное");
			if (StringUtils.getByteLengthUTF8(name.getTextDate()) > 50)
        res &= MessageUtils.ErrorMessage("Даты названий слишком длинные");
			if (!res)
			{
        detachContents(getCard().em);
        return false;
      }
    }
    return true;
  }

  public void save() {
    List<ArchFundName> copyFundModel1 = null;
    List<ArchFundName> copyFundModel = null;

    if (getCard().journalHelper.getCopyFundModel() != null) {
      copyFundModel1 = getCard().journalHelper.getCopyFundModel().getFundNames();
    }

    if (copyFundModel1!=null && copyFundModel1.size() > 0) {
      copyFundModel = new ArrayList<ArchFundName>();
      for (int i = 0; i < copyFundModel1.size(); i++) {
        copyFundModel.add(copyFundModel1.get(i));
      }
    }
    for (int i = 0; i < getWrappedData().size(); i++) {
      ArchFundName name = getWrappedData().get(i);
      if (i < getRowCount()) {
        if (name.getFund() == null) {
          name.setFund(getCard().getFundModel());
        }
        if (name.getSortOrder() == null || !name.getSortOrder().equals(i + 1)) {
          name.setSortOrder(i + 1);
        }
        if (name.getFundNameId() == null) {
          getCard().em.persist(name);
        } else {
          getCard().em.merge(name);
        }
      }

      if (copyFundModel != null) {
        for (int j = 0; j < copyFundModel.size(); j++) {
          if (copyFundModel.get(j).getFundNameId().equals(name.getFundNameId())) {
            copyFundModel.remove(copyFundModel.get(j));
          }
        }
      }

    }
    if (copyFundModel != null && copyFundModel.size() > 0) {
      
      for (int j = 0; j < copyFundModel.size(); j++) {
        getCard().em.remove(getCard().em.merge(copyFundModel.get(j)));
      }
    }

  }
}
