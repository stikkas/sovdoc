package ru.insoft.sovdoc.card.complex;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchPerformanceAnnotation;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexPerformAnnotation;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
public abstract class PerformanceAnnoEmbDataList extends EmbeddedDataList<ArchPerformanceAnnotation> {

    public abstract PhonodocCard getCard();

    protected List<VComplexPerformAnnotation> immutableData;

    @Override
    public List<ArchPerformanceAnnotation> getWrappedData() {
        if (super.getWrappedData() == null && getCard().getPhonodocModel() != null) {
            int size = 0;
            for (ArchPhonoStorageUnit storageUnit : getCard().getPhonodocModel().getStorageUnits()) {
                size += storageUnit.getAnnotations().size();
            }
            List<ArchPerformanceAnnotation> res = Lists.newArrayListWithExpectedSize(size);
            for (int i = 0; i < size; i++) {
                res.add(null);
            }
            for (ArchPhonoStorageUnit storageUnit : getCard().getPhonodocModel().getStorageUnits()) {
                for (ArchPerformanceAnnotation annotation : storageUnit.getAnnotations()) {
                    res.set(annotation.getPerformanceNumber() - 1, annotation);
                }
            }
            setWrappedData(res);
        }
        return super.getWrappedData();
    }

    protected List<VComplexPerformAnnotation> getImmutableData() {
        if (immutableData == null && getCard().getPhonodocImmModel() != null) {
            immutableData = getCard().getPhonodocImmModel().getAnnotations();
        }
        return immutableData;
    }

    public List<VComplexPerformAnnotation> getFilteredImmutableData() {
        Map<String, String> reqmap
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (Strings.isNullOrEmpty(reqmap.get("storageUnitIdx"))) {
            return getImmutableData();
        } else {
            return getFilteredImmutableData(Integer.valueOf(reqmap.get("storageUnitIdx")));
        }
    }

    public List<VComplexPerformAnnotation> getFilteredImmutableData(Integer storageUnitIdx) {
        ArchPhonoStorageUnit storageUnit
                = getCard().getStorageUnitEmbDL().getWrappedData().get(storageUnitIdx);
        List<VComplexPerformAnnotation> filteredImmData = Lists.newArrayList();
        if (getImmutableData() == null) {
            normalizeInputData();
        }
        for (VComplexPerformAnnotation annotation : immutableData) {
            if (storageUnit.equals(annotation.getStorageUnit())
                    || storageUnit.getPhonoStorageUnitId() != null && annotation.getStorageUnit() != null
                    && storageUnit.getPhonoStorageUnitId().equals(annotation.getStorageUnit().getPhonoStorageUnitId())) {
                filteredImmData.add(annotation);
            }
        }
        return filteredImmData;
    }

    public void addPerformanceAnnotation() {
        ArchPerformanceAnnotation annotation = new ArchPerformanceAnnotation();
        setRowCount(getRowCount() + 1);
        getWrappedData().add(annotation);
    }
    private boolean canClose;
    public void closeWindow() {
        canClose = true;
        Map<String, String> reqmap
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (getCard().isEditing()) {
            if (!validate()) {
                canClose = false;
                return;
            }
            if (Strings.isNullOrEmpty(reqmap.get("storageUnitIdx"))) {
                normalizeInputData();
            }
        }
    }

    public boolean getCanClose() {
        return canClose;
    }

    protected void normalizeInputData() {
        for (ArchPhonoStorageUnit storageUnit : getCard().getStorageUnitEmbDL().getWrappedData()) {
            storageUnit.setAnnotations(new ArrayList<ArchPerformanceAnnotation>());
        }
        List<VDescValueWithCode> langDescValues = getCard().se.getLinearValuesByCode("DOC_LANGUAGE");
        immutableData = Lists.newArrayList();
        for (int i = 0; i < getWrappedData().size(); i++) {
            ArchPerformanceAnnotation item = getWrappedData().get(i);
            item.setPerformanceNumber(i + 1);
            if (item.getStorageUnit() != null) {
                item.getStorageUnit().getAnnotations().add(item);
            }

            VComplexPerformAnnotation immItem = new VComplexPerformAnnotation();
            immItem.setAnnotationId(item.getAnnotationId());
            immItem.setPerformanceNumber(item.getPerformanceNumber());
            immItem.setSideOrTrack(item.getSideOrTrack());
            immItem.setAnnotation(item.getAnnotation());
            immItem.setSpokesperson(item.getSpokesperson());
            for (VDescValueWithCode lang : langDescValues) {
                if (lang.getDescriptorValueId().equals(item.getLanguageId())) {
                    immItem.setRecordLanguage(lang.getFullValue());
                    break;
                }
            }
            immItem.setPlaytime(item.getPlaytime());
            immItem.setFilePathData(item.getFilePathData());
            if (item.getStorageUnit() != null) {
                String suNumber = "";
                if (item.getStorageUnit().getNumberNumber() != null) {
                    suNumber += item.getStorageUnit().getNumberNumber().toString();
                }
                if (item.getStorageUnit().getNumberText() != null) {
                    suNumber += item.getStorageUnit().getNumberText();
                }
                immItem.setStorageUnitNumber(suNumber);
                immItem.setStorageUnit(item.getStorageUnit());
            }
            immutableData.add(immItem);
        }
    }

    public void saveAnnotations() {
        if (validate()) {
            EntityManager em = getCard().em;
            int i = 0;
            for (ArchPerformanceAnnotation annotaion : getWrappedData()) {
                annotaion.setPerformanceNumber(++i);
                if (annotaion.getAnnotationId() == null) {
                    em.persist(annotaion);
                } else {
                    em.merge(annotaion);
                }
            }
            em.flush();
        }
    }

    protected boolean validate() {
        boolean res = true;
        for (ArchPerformanceAnnotation annotation : getWrappedData()) {
            if (annotation.getStorageUnit() == null) {
                res &= MessageUtils.ErrorMessage("Аннотации выступлений: единица хранения должна быть указана");
            }
            if (StringUtils.getByteLengthUTF8(annotation.getSideOrTrack()) > 2) {
                res &= MessageUtils.ErrorMessage("Аннотации выступлений, сторона/трек: слишком длинное значение");
            }
            if (annotation.getAnnotation() == null) {
                res &= MessageUtils.ErrorMessage("Аннотации выступлений: аннотация должна быть заполнена");
            }
            if (StringUtils.getByteLengthUTF8(annotation.getSpokesperson()) > 250) {
                res &= MessageUtils.ErrorMessage("Аннотации выступлений, докладчик: слишком длинное значение");
            }
            if (annotation.getPlaytime() == null) {
                res &= MessageUtils.ErrorMessage("Аннотации выступлений: время звучания должно быть заполнено");
            }
            if (!res) {
                return false;
            }
        }
        return res;
    }

    protected void save(ArchPhonodoc copyModel) {
        for (ArchPerformanceAnnotation item1 : getWrappedData()) {
            if (item1.getAnnotationId() == null) {
                getCard().em.persist(item1);
            } else {
                for (ArchPhonoStorageUnit copyStorageUnit : copyModel.getStorageUnits()) {
                    for (ArchPerformanceAnnotation item2 : copyStorageUnit.getAnnotations()) {
                        if (item2.getAnnotationId().equals(item1.getAnnotationId())) {
                            getCard().em.detach(item2);
                            getCard().em.merge(item1);
                            break;
                        }
                    }
                }
            }
        }
        if (copyModel != null) {
            for (ArchPhonoStorageUnit copyStorageUnit : copyModel.getStorageUnits()) {
                for (ArchPerformanceAnnotation item1 : copyStorageUnit.getAnnotations()) {
                    boolean found = false;
                    for (ArchPerformanceAnnotation item2 : getWrappedData()) {
                        if (item1.getAnnotationId().equals(item2.getAnnotationId())) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        getCard().getStorageUnitEmbDL().removeImagePath(item1.getFilePathData());
                        getCard().em.remove(item1);
                    }
                }
            }
        }
    }

    public void moveUp() {
        ArchPerformanceAnnotation item1 = getWrappedData().get(getRowIndex());
        ArchPerformanceAnnotation item2 = getWrappedData().get(getRowIndex() - 1);
        getWrappedData().set(getRowIndex() - 1, item1);
        getWrappedData().set(getRowIndex(), item2);
    }

    public void moveDown() {
        ArchPerformanceAnnotation item1 = getWrappedData().get(getRowIndex());
        ArchPerformanceAnnotation item2 = getWrappedData().get(getRowIndex() + 1);
        getWrappedData().set(getRowIndex() + 1, item1);
        getWrappedData().set(getRowIndex(), item2);
    }

    public String getConfirmRemoveText(int index) {
        ArchPerformanceAnnotation annotation = getWrappedData().get(index);
        String res = "";
        if (annotation.getFilePathData() != null) {
            res += "К данной аннотации выступления прикреплён файл.<br/>";
        }
        res += "Вы действительно хотите удалить эту аннотацию выступления?";
        return res;
    }

    public void removeAnnotation() {
        Map<String, String> reqmap
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        Integer index = Integer.valueOf(reqmap.get("index"));
        setRowCount(getRowCount() - 1);
        ArchPerformanceAnnotation annotation = getWrappedData().remove(index.intValue());
        Long id = annotation.getAnnotationId();
        if (id != null) {
            EntityManager em = getCard().em;
            ArchPerformanceAnnotation found = em.find(ArchPerformanceAnnotation.class, id);
            if (found != null) {
                found.getStorageUnit().getAnnotations().remove(found);
                em.remove(found);
                em.flush();
            }
        }
    }

    public void onRemoveStorageUnit(ArchPhonoStorageUnit storageUnit) {
        List<ArchPerformanceAnnotation> toRemove = Lists.newArrayList();
        for (int i = 0; i < getWrappedData().size(); i++) {
            ArchPerformanceAnnotation annotation = getWrappedData().get(i);
            if (storageUnit.equals(annotation.getStorageUnit())) {
                toRemove.add(annotation);
            } else {
                annotation.setPerformanceNumber(i - toRemove.size() + 1);
            }
        }
        getWrappedData().removeAll(toRemove);
    }

    public void attachAudioFile(Long annotationId, ImagePath file) {
        for (ArchPerformanceAnnotation annotation : getWrappedData()) {
            if (annotationId.equals(annotation.getAnnotationId())) {
                annotation.setFilePathData(file);
                getCard().em.merge(annotation);
            }
        }
    }
}
