package ru.insoft.sovdoc.card.complex;

import java.util.List;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.sovdoc.model.complex.table.UnivClassifierValue;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexUnivClassifier;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss;

/**
 *
 * @author melnikov
 */
public abstract class UnivClssEmbeddedDataList extends EmbeddedDataList<VComplexUnivClassifier>
{
    public abstract ComplexCard getCard();

    @Override
    public List<VComplexUnivClassifier> getWrappedData() 
    {
        if (super.getWrappedData() == null && getCard().getImmModel() != null)
            setWrappedData(getCard().getImmModel().getClassifierValues());
        
        return super.getWrappedData();
    }
    
    public void addClassifierValue()
    {
        Long valueId = getCard().univClssDL.getSelectedValue();
        if (valueId == null)
        {
            MessageUtils.ErrorMessage("Значение должно быть выбрано");
            return;
        }
        for (VComplexUnivClassifier val : getWrappedData())
            if (valueId.equals(val.getDescriptorValueId()))
            {
                MessageUtils.ErrorMessage("Данное значение уже прикреплено к карточке");
                return;
            }
        
        VDescValueUnivclss clssValue = getCard().se.getUnivClssValue(valueId);
        VComplexUnivClassifier item = new VComplexUnivClassifier();
        item.setDataUnit(getCard().getImmModel());
        item.setDescriptorValueId(valueId);
        item.setFullValue(clssValue.getFullValue());
        item.setValueIndex(clssValue.getValueIndex());
        getWrappedData().add(item);
        setRowCount(getRowCount() + 1);
        getCard().univClssDL.setValueSelected(true);
    }
    
    public void removeClassifierValue(VComplexUnivClassifier item)
    {
        setRowCount(getRowCount() - 1);
        getWrappedData().remove(item);
    }
    
    protected void save(UnivDataUnit copyModel)
    {
        boolean found;
        for (VComplexUnivClassifier item1 : getWrappedData())
        {
            found = false;
            if (copyModel != null)
                for (UnivClassifierValue item2 : copyModel.getClassifierValues())
                    if (item1.getDescriptorValueId().equals(item2.getDescriptorValueId()))
                    {
                        found = true;
                        break;
                    }
            if (!found)
            {
                UnivClassifierValue newItem = new UnivClassifierValue();
                newItem.setDataUnit(getCard().getModel());
                newItem.setDescriptorValueId(item1.getDescriptorValueId());
                getCard().em.persist(newItem);
            }
        }
        if (copyModel != null)
            for (UnivClassifierValue item1 : copyModel.getClassifierValues())
            {
                found = false;
                for (VComplexUnivClassifier item2 : getWrappedData())
                    if (item1.getDescriptorValueId().equals(item2.getDescriptorValueId()))
                    {
                        found = true;
                        break;
                    }
                if (!found)
                    getCard().em.remove(item1);
            }
    }
}
