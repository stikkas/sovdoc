package ru.insoft.sovdoc.card.complex;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchKinodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexKinoStoryDescription;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
public abstract class KinoStoryDescriptionEmbDataList extends EmbeddedDataList<ArchKinoStoryDescription> {

	public abstract KinodocCard getCard();

	protected List<VComplexKinoStoryDescription> immutableData;

	@Override
	public List<ArchKinoStoryDescription> getWrappedData() {
		if (super.getWrappedData() == null && getCard().getKinodocModel() != null) {
			int size = 0;
			for (ArchKinoStorageUnit storageUnit : getCard().getKinodocModel().getStorageUnits()) {
				size += storageUnit.getStoryDescriptions().size();
			}
			List<ArchKinoStoryDescription> res = Lists.newArrayListWithExpectedSize(size);
			for (int i = 0; i < size; i++) {
				res.add(null);
			}
			for (ArchKinoStorageUnit storageUnit : getCard().getKinodocModel().getStorageUnits()) {
				for (ArchKinoStoryDescription description : storageUnit.getStoryDescriptions()) {
					res.set(description.getStoryNumber() - 1, description);
				}
			}
			setWrappedData(res);
		}
		return super.getWrappedData();
	}

	protected List<VComplexKinoStoryDescription> getImmutableData() {
		if (immutableData == null && getCard().getKinodocImmModel() != null) {
			immutableData = getCard().getKinodocImmModel().getStoryDescriptions();
		}
		return immutableData;
	}

	public List<VComplexKinoStoryDescription> getFilteredImmutableData() {
		Map<String, String> reqmap
				= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (Strings.isNullOrEmpty(reqmap.get("storageUnitIdx"))) {
			return getImmutableData();
		} else {
			return getFilteredImmutableData(Integer.valueOf(reqmap.get("storageUnitIdx")));
		}
	}

	public List<VComplexKinoStoryDescription> getFilteredImmutableData(Integer storageUnitIdx) {
		ArchKinoStorageUnit storageUnit
				= getCard().getStorageUnitEmbDL().getWrappedData().get(storageUnitIdx);
		List<VComplexKinoStoryDescription> filteredImmData = Lists.newArrayList();
		if (getImmutableData() == null) {
			normalizeInputData();
		}
		for (VComplexKinoStoryDescription description : immutableData) {
			if (storageUnit.equals(description.getStorageUnit())
					|| storageUnit.getKinoStorageUnitId() != null && description.getStorageUnit() != null
					&& storageUnit.getKinoStorageUnitId().equals(description.getStorageUnit().getKinoStorageUnitId())) {
				filteredImmData.add(description);
			}
		}
		return filteredImmData;
	}

	public void addStoryDescription() {
		ArchKinoStoryDescription annotation = new ArchKinoStoryDescription();
		setRowCount(getRowCount() + 1);
		getWrappedData().add(annotation);
	}

	private boolean canClose;

	public void closeWindow() {
		canClose = true;
		Map<String, String> reqmap
				= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (getCard().isEditing()) {
			if (!validate()) {
				canClose = false;
				return;
			}
			if (Strings.isNullOrEmpty(reqmap.get("storageUnitIdx"))) {
				normalizeInputData();
			}
		}
	}

	public boolean getCanClose() {
		return canClose;
	}

	protected void normalizeInputData() {
		for (ArchKinoStorageUnit storageUnit : getCard().getStorageUnitEmbDL().getWrappedData()) {
			storageUnit.setStoryDescriptions(new ArrayList<ArchKinoStoryDescription>());
		}
		List<VDescValueWithCode> langDescValues = getCard().se.getLinearValuesByCode("DOC_LANGUAGE");
		immutableData = Lists.newArrayList();
		for (int i = 0; i < getWrappedData().size(); i++) {
			ArchKinoStoryDescription item = getWrappedData().get(i);
			item.setStoryNumber(i + 1);
			if (item.getStorageUnit() != null) {
				item.getStorageUnit().getStoryDescriptions().add(item);
			}

			VComplexKinoStoryDescription immItem = new VComplexKinoStoryDescription();
			immItem.setStoryDescriptionId(item.getStoryDescId());
			immItem.setStoryNumber(item.getStoryNumber());
			immItem.setPart(item.getPart());
			immItem.setAnnotation(item.getAnnotation());
			for (VDescValueWithCode lang : langDescValues) {
				if (lang.getDescriptorValueId().equals(item.getLanguageId())) {
					immItem.setLanguage(lang.getFullValue());
					break;
				}
			}
			immItem.setPlaytime(item.getPlaytime());
			immItem.setFilePathData(item.getFilePathData());
			if (item.getStorageUnit() != null) {
				String suNumber = "";
				if (item.getStorageUnit().getNumberNumber() != null) {
					suNumber += item.getStorageUnit().getNumberNumber().toString();
				}
				if (item.getStorageUnit().getNumberText() != null) {
					suNumber += item.getStorageUnit().getNumberText();
				}
				immItem.setStorageUnitNumber(suNumber);
				immItem.setStorageUnit(item.getStorageUnit());
			}
			immutableData.add(immItem);
		}
	}

	public void saveStoryDescriptions() {
		if (validate()) {
			EntityManager em = getCard().em;
			int i = 0;
			for (ArchKinoStoryDescription description : getWrappedData()) {
				description.setStoryNumber(++i);
				if (description.getStoryDescId()== null) {
					em.persist(description);
				} else {
					em.merge(description);
				}
			}
			em.flush();
		}
	}

	protected boolean validate() {
		boolean res = true;
		for (ArchKinoStoryDescription description : getWrappedData()) {
			if (description.getStorageUnit() == null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: единица хранения должна быть указана");
			}
			if (description.getAnnotation() == null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: аннотация должна быть заполнена");
			}
			if (description.getLanguageId()== null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: язык должен быть выбран");
			}
			if (description.getPlaytime() == null) {
				res &= MessageUtils.ErrorMessage("Описание сюжета: время должно быть заполнено");
			}
			if (!res) {
				return false;
			}
		}
		return res;
	}

	protected void save(ArchKinodoc copyModel) {
		for (ArchKinoStoryDescription item1 : getWrappedData()) {
			if (item1.getStoryDescId() == null) {
				getCard().em.persist(item1);
			} else {
				for (ArchKinoStorageUnit copyStorageUnit : copyModel.getStorageUnits()) {
					for (ArchKinoStoryDescription item2 : copyStorageUnit.getStoryDescriptions()) {
						if (item2.getStoryDescId().equals(item1.getStoryDescId())) {
							getCard().em.detach(item2);
							getCard().em.merge(item1);
							break;
						}
					}
				}
			}
		}
		if (copyModel != null) {
			for (ArchKinoStorageUnit copyStorageUnit : copyModel.getStorageUnits()) {
				for (ArchKinoStoryDescription item1 : copyStorageUnit.getStoryDescriptions()) {
					boolean found = false;
					for (ArchKinoStoryDescription item2 : getWrappedData()) {
						if (item1.getStoryDescId().equals(item2.getStoryDescId())) {
							found = true;
							break;
						}
					}
					if (!found) {
						getCard().getStorageUnitEmbDL().removeImagePath(item1.getFilePathData());
						getCard().em.remove(item1);
					}
				}
			}
		}
	}

	public void moveUp() {
		ArchKinoStoryDescription item1 = getWrappedData().get(getRowIndex());
		ArchKinoStoryDescription item2 = getWrappedData().get(getRowIndex() - 1);
		getWrappedData().set(getRowIndex() - 1, item1);
		getWrappedData().set(getRowIndex(), item2);
	}

	public void moveDown() {
		ArchKinoStoryDescription item1 = getWrappedData().get(getRowIndex());
		ArchKinoStoryDescription item2 = getWrappedData().get(getRowIndex() + 1);
		getWrappedData().set(getRowIndex() + 1, item1);
		getWrappedData().set(getRowIndex(), item2);
	}

	public String getConfirmRemoveText(int index) {
		ArchKinoStoryDescription annotation = getWrappedData().get(index);
		String res = "";
		if (annotation.getFilePathData() != null) {
			res += "К данному описанию сюжета прикреплён файл.<br/>";
		}
		res += "Вы действительно хотите удалить это описание сюжета?";
		return res;
	}

	public void removeStoryDescription() {
		Map<String, String> reqmap
				= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer index = Integer.valueOf(reqmap.get("index"));
		setRowCount(getRowCount() - 1);
		ArchKinoStoryDescription annotation = getWrappedData().remove(index.intValue());
		Long id = annotation.getStoryDescId();
		if (id != null) {
			EntityManager em = getCard().em;
			ArchKinoStoryDescription found = em.find(ArchKinoStoryDescription.class, id);
			if (found != null) {
				found.getStorageUnit().getStoryDescriptions().remove(found);
				em.remove(found);
				em.flush();
			}
		}
	}

	public void onRemoveStorageUnit(ArchKinoStorageUnit storageUnit) {
		List<ArchKinoStoryDescription> toRemove = Lists.newArrayList();
		for (int i = 0; i < getWrappedData().size(); i++) {
			ArchKinoStoryDescription annotation = getWrappedData().get(i);
			if (storageUnit.equals(annotation.getStorageUnit())) {
				toRemove.add(annotation);
			} else {
				annotation.setStoryNumber(i - toRemove.size() + 1);
			}
		}
		getWrappedData().removeAll(toRemove);
	}

	public void attachKinoFile(Long storyDescId, ImagePath file) {
		for (ArchKinoStoryDescription description : getWrappedData()) {
			if (storyDescId.equals(description.getStoryDescId())) {
				description.setFilePathData(file);
				getCard().em.merge(description);
			}
		}
	}
}
