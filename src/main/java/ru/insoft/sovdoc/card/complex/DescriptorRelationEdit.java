package ru.insoft.sovdoc.card.complex;

import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.service.correct.JournalHelper;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

@ViewScoped
@ManagedBean
public class DescriptorRelationEdit {

  @Inject
  ru.insoft.sovdoc.system.complex.SystemQuery complexQuery;
  @Inject
  ru.insoft.sovdoc.system.ebook.SystemQuery ebookQuery;
  @Inject
  SystemEntity se;
  @Inject
  EntityManager em;
  @Inject
  ValueSelectDataListGeneral valueSelectDL;
  @Inject
  JournalHelper journalHelper;

  @Inject
  @RequestParam(value = "descriptorRelationId")
  private String descriptorRelationId;

  private boolean isSaved;
  private UnivDescriptor model;
  Map<String, String> reqmap = null;
  Long univDescId = null;
  Long unitId;

  public boolean isSaved() {
    return isSaved;
  }

  public UnivDescriptor getModel() {
    reqmap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

    if (reqmap.containsKey("univDescId")) {
      univDescId = Long.valueOf(reqmap.get("univDescId"));
    }
    if (model != null) {
      if (!model.getUnivDescriptorId().equals(univDescId)) {
        model = null;
      }
    }

    if (model == null && univDescId != null) {
      model = complexQuery.queryUnivDescriptor(univDescId);
      em.detach(model);
    }

    return model;
  }

  public void selectRelatedValue() {
    reqmap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

    model = getModel();
    if (valueSelectDL.getSelectedValue() == null) {
      MessageUtils.ErrorMessage("Необходимо задать связанное значение");
      return;
    }
    model.setDescriptorValueId2(valueSelectDL.getSelectedValue());
    valueSelectDL.setValueSelected(true);
  }

  protected boolean validate() {
    boolean res = true;
    if (model.getDescriptorRelationId() == null) {
      res &= MessageUtils.ErrorMessage("Тип связи должен быть указан");
    }
    if (model.getDescriptorValueId2() == null) {
      res &= MessageUtils.ErrorMessage("Необходимо задать связанное значение");
    }
    return res;
  }

  public VComplexDescriptor save() {
    reqmap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

    //model = getModel();
    if (!validate()) {
      return null;
    }
    model = em.merge(model);

//   if (model.getDescriptorValueId1() == null) {
//      for (int i = 0; i < descriptors.size(); i++) {
//        if (descriptors.get(i).getDescriptor1().equals(reqmap.get("descriptor1"))) {
//          model.setDescriptorValueId1(
//                  complexQuery.queryDescriptorFullValueReverse(reqmap.get("descriptor1")).getDescriptorValueId());
//        }
//    for (int i = 0; i < descriptors.size(); i++) {
//      if (descriptors.get(i).getDescriptor1().equals(reqmap.get("descriptor1"))) {
//        descriptors.get(i).setDescriptor2(complexQuery.queryDescriptorValue(model.getDescriptorValueId2()).getFullValue());
//        descriptors.get(i).setDescRelation(complexQuery.queryDescriptorValue(model.getDescriptorRelationId()).getFullValue());
//      }
//    }
	String unitType = complexQuery.queryDescriptorValue(model.getDataUnit().getUnitTypeId()).getValueCode();
    copyDescContainer(unitType);
    VComplexDescriptor desc = new VComplexDescriptor();
    desc.setDescriptor1(reqmap.get("descriptor1"));
    DescriptorValue dv = complexQuery.queryDescriptorValue(model.getDescriptorValueId2());
    String fullValueWithSpravoschnik = dv.getFullValue() + " (" + dv.getDescriptorGroup().getGroupName() +")" ;
    desc.setDescriptor2(fullValueWithSpravoschnik);
    desc.setDescRelation(complexQuery.queryDescriptorValue(model.getDescriptorRelationId()).getFullValue());

    UnivDescriptor univDesc = complexQuery.queryUnivDescriptor(model.getUnivDescriptorId());
    univDesc.setDescriptorValueId2(model.getDescriptorValueId2());
    univDesc.setDescriptorRelationId(model.getDescriptorRelationId());
    em.persist(univDesc);
    em.flush();
    em.refresh(univDesc);
    em.merge(model);
    em.flush();
    journalHelper.recordChangeUnivDescriptor(model.getDataUnit(), "EDIT",
            model.getUnivDescriptorId());
    isSaved = true;
    return desc;
  }

  public VComplexDescriptor clearRelation() {
    reqmap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    model = em.merge(model);
   

    VComplexDescriptor desc = new VComplexDescriptor();

    String unitType = complexQuery.queryDescriptorValue(model.getDataUnit().getUnitTypeId()).getValueCode();
    copyDescContainer(unitType);

    desc.setDescriptor1(reqmap.get("descriptor1"));
    desc.setDescriptorRelationId(null);
    desc.setDescriptorValueId2(null);

    model.setDescriptorRelationId(null);
    model.setDescriptorValueId2(null);

    em.merge(model);
    em.flush();
    em.refresh(model);
    journalHelper.recordChangeUnivDescriptor(model.getDataUnit(), "EDIT",
            model.getUnivDescriptorId());
//    model = null;
    
    return desc;
  }

  protected void copyDescContainer(String unitType) {

    if (unitType.equals("EBOOK")) {
      journalHelper.copyEbookImmModel(ebookQuery.queryEbookImmModel(model.getUnivDataUnitId()));
    } else {
      journalHelper.copyImmModel(complexQuery.queryImmModel(model.getUnivDataUnitId()));
    }
  }
}
