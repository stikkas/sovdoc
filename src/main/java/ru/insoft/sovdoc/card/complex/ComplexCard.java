package ru.insoft.sovdoc.card.complex;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.sovdoc.list.desc.UnivClssDataList;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchStorageUnit;
import ru.insoft.sovdoc.model.complex.table.UnivCardLinks;
import ru.insoft.sovdoc.model.complex.table.UnivDataLanguage;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexCardLinks;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexIntervalYears;
import ru.insoft.sovdoc.model.complex.view.VComplexUnitHierarchy;
import ru.insoft.sovdoc.model.complex.view.VComplexUnivClassifier;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.view.VDescAttrvalueWithCode;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode;
import ru.insoft.sovdoc.security.SecurityServiceImpl;
import ru.insoft.sovdoc.service.correct.JournalHelper;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.system.SystemSelectItem;
import ru.insoft.sovdoc.system.complex.SystemQuery;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

public abstract class ComplexCard {

  @Inject
  SystemSelectItem systemSelectItem;

  @Inject
  protected EntityManager em;
  @Inject
  protected SystemEntity se;
  @Inject
  SystemQuery complexQuery;
  @Inject
  SecurityServiceImpl security;
  @Inject
  protected ValueSelectDataListGeneral valueSelectDL;
  @ManagedProperty("#{descriptorRelationEdit}")
  DescriptorRelationEdit relEdit;
  @Inject
  protected JournalHelper journalHelper;
  @Inject
  UnivClssDataList univClssDL;

  @Inject
  @RequestParam(value = "owner")
  protected Long owner;

  @Inject
  @RequestParam(value = "descLevel")
  protected Long descLevel;

  @Inject
  @RequestParam(value = "parentId")
  protected Long parentId;

  @Inject
  @RequestParam(value = "unitId")
  protected Long unitId;

  @Inject
  @RequestParam(value = "mode")
  protected String mode;

  @Inject
  @RequestParam(value = "action")
  protected String action;

  @Inject
  @RequestParam(value = "univDescId")
  private Long univDescId;

  @Inject
  @RequestParam(value = "univDescValueId")
  private Long univDescValueId;

  @Inject
  @RequestParam(value = "descriptor1")
  private String descriptor1;

  protected String row_fullValue;

  protected abstract Long getUnitTypeId();

  protected abstract MENU_PAGES getMenuPage();

  protected UnivDataUnit model;
  protected VComplexDataUnit immModel;

  protected List<VComplexCardLinks> cardLinksForDelete;
  protected List<VComplexCardLinks> cardLinks;

  protected List<VComplexUnitHierarchy> hierarchy;
  protected UnivDateInterval intervalModel;
  private List<UnivDescriptor> descriptorsToAddList;
  protected List<UnivDescriptor> descriptorsForRemoveList;

  public DescriptorRelationEdit getRelEdit() {
    return relEdit;
  }

  public void setRelEdit(DescriptorRelationEdit relEdit) {
    this.relEdit = relEdit;
  }

  protected DescriptorEmbeddedDataList getDescriptorEmbDL() {
    return null;
  }

  public UnivDataUnit getModel() {
    if (model == null) {
      if (unitId == null) {
        model = new UnivDataUnit();
        model.setResOwnerId(owner);
        model.setDescriptionLevelId(descLevel);
        model.setParentUnitId(parentId);
        if (model.getParentUnitId() != null) {
          hierarchy = getHierarchy();
          if (hierarchy.size() > 0) {
            model.setPortalSectionId(hierarchy.get(0).getPortalSectionId());
          }
        }
        model.setUnitTypeId(getUnitTypeId());
        model.setDateIntervals(new ArrayList<UnivDateInterval>());
        model.setLanguages(new ArrayList<UnivDataLanguage>());
      } else {
        model = complexQuery.queryModel(unitId);
        journalHelper.copyModel(model);
        if (action == null || !action.equals("save")) {
          for (UnivDataLanguage lang : model.getLanguages()) {
            em.detach(model);
          }
        } else {
//  journalHelper.copyModel(model);
        }
      }
    }
    return model;
  }

  public VComplexDataUnit getImmModel() {
    if (immModel == null) {
      if (unitId == null) {
        model = getModel();
        immModel = new VComplexDataUnit();
        immModel.setYears(new ArrayList<VComplexIntervalYears>());
        immModel.setDescriptors(new ArrayList<VComplexDescriptor>());
        immModel.setClassifierValues(new ArrayList<VComplexUnivClassifier>());
        if (model.getResOwnerId() != null) {
          immModel.setArchiveId(model.getResOwnerId());
          immModel.setArchiveName(se.getDescValue(model.getResOwnerId()).getFullValue());
        }
        if (model.getDescriptionLevelId() != null) {
          immModel.setDescriptionLevelId(model.getDescriptionLevelId());
          immModel.setDescriptionLevelCode(se.getDescValue(model.getDescriptionLevelId()).getValueCode());
          immModel.setDescriptionLevel(se.getDescValue(model.getDescriptionLevelId()).getFullValue());
        }
        if (model.getUnitTypeId() != null) {
          immModel.setUnitTypeId(model.getUnitTypeId());
          immModel.setUnitTypeCode(se.getDescValue(model.getUnitTypeId()).getValueCode());
        }
        if (model.getParentUnitId() != null) {
          immModel.setParentUnitId(model.getParentUnitId());
          hierarchy = getHierarchy();
          for (VComplexUnitHierarchy unit : hierarchy) {
            if (unit.getParentType().getValueCode().equals("FUND")) {
              immModel.setFundPrefix(unit.getParentNumberPrefix());
              immModel.setFundNum(unit.getParentNumberNumber());
              immModel.setFundLetter(unit.getParentNumberText());
            }
            if (unit.getParentType().getValueCode().equals("SERIES")) {
              immModel.setSeriesNum(unit.getParentNumberNumber());
              immModel.setSeriesLetter(unit.getParentNumberText());
            }
            if (unit.getParentType().getValueCode().equals("STORAGE_UNIT")) {
              immModel.setUnitNum(unit.getParentNumberNumber());
              immModel.setUnitLetter(unit.getParentNumberText());

              VComplexDataUnit strgUnit = complexQuery.queryImmModel(unit.getParentUnitId());
              immModel.setUnitVolume(strgUnit.getUnitVolume());
              immModel.setUnitPart(strgUnit.getUnitPart());
              immModel.setUnitInfo(strgUnit.getUnitInfo());
            }
          }
        }
      } else {
        immModel = complexQuery.queryImmModel(unitId);
        journalHelper.copyImmModel(immModel);
      }
    }
    return immModel;
  }

  public List<VComplexUnitHierarchy> getHierarchy() {
    if (hierarchy == null) {
      model = getModel();
      if (model.getParentUnitId() != null) {
        hierarchy = complexQuery.queryHierarchy(model.getParentUnitId());
      }
    }
    return hierarchy;
  }

  public UnivDateInterval getIntervalModel() {
    if (intervalModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        intervalModel = new UnivDateInterval();
      } else {
        List<UnivDateInterval> intervalList = model.getDateIntervals();
        if (intervalList.size() == 0) {
          intervalModel = new UnivDateInterval();
        } else {
          intervalModel = intervalList.get(0);
          if (action == null || !action.equals("save")) {
            em.detach(intervalModel);
          }
        }
      }
    }
    return intervalModel;
  }

  public ArchFund getParentFund() {
    if (unitId != null || parentId != null) {
      for (VComplexUnitHierarchy unit : getHierarchy()) {
        if (unit.getParentType().getValueCode().equals("FUND")) {
          return complexQuery.queryFundModel(unit.getParentUnitId());
        }
      }
    }

    return null;
  }

  public ArchSeries getParentSeries() {
    if (unitId != null || parentId != null) {
      for (VComplexUnitHierarchy unit : getHierarchy()) {
        if (unit.getParentType().getValueCode().equals("SERIES")) {
          return complexQuery.querySeriesModel(unit.getParentUnitId());
        }
      }
    }

    return null;
  }
  
    public ArchStorageUnit getParentStorageUnit() {
    if (unitId != null || parentId != null) {
      for (VComplexUnitHierarchy unit : getHierarchy()) {
        if (unit.getParentType().getValueCode().equals("STORAGE_UNIT")) {
          return complexQuery.queryStorageUnitModel(unit.getParentUnitId());
        }
      }
    }

    return null;
  }

  public List<VComplexCardLinks> getCardLinks() {
    if (cardLinks == null) {
      model = getModel();

      if (model.getUnivDataUnitId() == null) {
        cardLinks = new ArrayList<VComplexCardLinks>();
      } else {
        cardLinks = complexQuery.queryCardLinks(model.getUnivDataUnitId());
        journalHelper.copyCardLinksModel(cardLinks);

      }
    }
    return cardLinks;

  }

  public void deleteCardLink(VComplexCardLinks linkedCard) {

    if (cardLinksForDelete == null) {
      cardLinksForDelete = new ArrayList<VComplexCardLinks>();
    }
    cardLinksForDelete.add(linkedCard);
    cardLinks.remove(linkedCard);

  }

  public void addCardLink() {

    VComplexCardLinks cardLink = new VComplexCardLinks();

    Map<String, String> reqmap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

    cardLink.setUnitIdOriginal(model.getUnivDataUnitId());
    cardLink.setUnitIdLinked(Long.valueOf(reqmap.get("row_id")));
    cardLink.setUnitFullName(reqmap.get("row_archNumber").concat(" ").concat(reqmap.get("row_unitName")));
    cardLink.setUnitTypeCode(reqmap.get("row_unitTypeCode"));

    if (cardLink.getUnitIdOriginal()!= null && cardLink.getUnitIdLinked().equals(cardLink.getUnitIdOriginal())) {
      MessageUtils.ErrorMessage("Карточка не может иметь ссылку на саму себя");
      return;
    }
    for (int i = 0; i < cardLinks.size(); i++) {
      if (cardLinks.get(i).getUnitIdLinked().equals(cardLink.getUnitIdLinked())) {
        MessageUtils.ErrorMessage("Ссылка на выбранную карточку документального комплекса ранее уже была добавлена");
        return;
      }
    }

    cardLinks.add(cardLink);
    MessageUtils.InfoMessage("Ссылка успешно добавлена");

  }
  
  public List<SelectItem> getDescLevelsForCardLink() {
    List<SelectItem> listWithItems = SelectItemUtils.withEmpty(systemSelectItem.getDescValuesByCodeForCardLink("DESC_LEVEL", immModel.getDescriptionLevelCode()));
      return listWithItems;
    } 
  
  public boolean isEditing() {
    return mode != null;
  }

  public boolean isEditingPaperDoc() {
    if (mode != null) {
      return mode.equals("edit");
    } else {
      return false;
    }
  }

  protected abstract boolean validate();

  protected abstract String fillArchNumberCode();

  protected abstract String fillUnitName();

  protected Integer fillNumberNumber() {
    return model.getNumberNumber();
  }

  public String save() {
    if (!validate()) {
      return null;
    }

    try
    {
        em.detach(getImmModel());
        model.setArchNumberCode(fillArchNumberCode());
        model.setUnitName(fillUnitName());
        model.setNumberNumber(fillNumberNumber());
        model.setModUserId(security.getCurrentUser().getUserId());
        model.setLastUpdateDate(new Date());

        if (model.getUnivDataUnitId() == null) 
        {
            model.setAddUserId(model.getModUserId());
            model.setInsertDate(model.getLastUpdateDate());
            em.persist(model);
            em.flush();
            em.refresh(em.merge(model));
            if (cardLinks != null)
            {
                for (int i = 0; i < cardLinks.size(); i++)                
                    if (cardLinks.get(i).getIdLinkes() == null) 
                    {
                        cardLinks.get(i).setUnitIdOriginal(model.getUnivDataUnitId());
            //          applicationToDocumentModel.get(i).setUnivDataUnitId(model.getUnivDataUnitId());
                        UnivCardLinks univCardLink = new UnivCardLinks();
                        univCardLink.setUnitIdLinked(cardLinks.get(i).getUnitIdLinked());
                        univCardLink.setUnitIdOriginal(cardLinks.get(i).getUnitIdOriginal());

                        em.persist(univCardLink);
                    }
                
                if (cardLinksForDelete != null)
                    for (int i = 0; i < cardLinksForDelete.size(); i++)
                        if (cardLinksForDelete.get(i).getUnitIdOriginal() == null)
                            cardLinksForDelete.get(i).setUnitIdOriginal(model.getUnivDataUnitId());
            }     
            saveDefaultLanguages();
            saveDetails();
        } 
        else 
        {
            model = em.merge(model);
            if (cardLinksForDelete != null) 
            {
                for (int i = 0; i < cardLinksForDelete.size(); i++)                
                    if (cardLinksForDelete.get(i).getIdLinkes() != null) 
                    {
                        UnivCardLinks univ1;
                        univ1 = complexQuery.queryUnitIdLinkedCard(cardLinksForDelete.get(i).getUnitIdOriginal(), cardLinksForDelete.get(i).getUnitIdLinked());
                        em.remove(univ1);
                        em.flush();
                    }        
                cardLinksForDelete.clear();
            }

            if (cardLinks != null)             
                for (int i = 0; i < cardLinks.size(); i++)                
                    if (cardLinks.get(i).getIdLinkes() == null) 
                    {
            //          applicationToDocumentModel.get(i).setUnivDataUnitId(model.getUnivDataUnitId());
                        UnivCardLinks univCardLink = new UnivCardLinks();
                        univCardLink.setUnitIdLinked(cardLinks.get(i).getUnitIdLinked());
                        univCardLink.setUnitIdOriginal(cardLinks.get(i).getUnitIdOriginal());

                        em.persist(univCardLink);
                    }      

      //<editor-fold defaultstate="collapsed" desc="добавление и удаление дескрипторов(ключевые слова) в карточках док. комплексов">
//      if (descriptorsForRemoveList != null && descriptorsForRemoveList.size() != 0) {
//        for (int i = 0; i < descriptorsForRemoveList.size(); i++) {
//          if (descriptorsForRemoveList.get(i) != null) {
//            UnivDescriptor univDescForDelete = complexQuery.queryUnivDescriptor(descriptorsForRemoveList.get(i).getUnivDescriptorId());
//            em.remove(univDescForDelete);
//            em.flush();
//          }
//        }
//        journalHelper.recordChangeUnivDescriptor(model, "DELETE", descriptorsForRemoveList, null);
//
//      }
//
//      if (getDescriptorsToAddList() != null && getDescriptorsToAddList().size() != 0) {
//
//        for (int i = 0; i < getDescriptorsToAddList().size(); i++) {
//          if (getDescriptorsToAddList().get(i).getDescriptorValueId1() != null) {
//            em.persist(getDescriptorsToAddList().get(i));
//            em.flush();
//          }
//        }
//
//        recordAddDescriptor(getDescriptorsToAddList());
//      }
//</editor-fold>
            saveDetails();
            em.flush();

            if (model.getArchNumberCode() != null
                  && !model.getArchNumberCode().equals(journalHelper.getCopyModel().getArchNumberCode())) 
            {
                Query q = em.createNativeQuery("begin COMPLEX_PACK.UPDATE_ARCH_CODE("
                        + ":p_univ_data_unit_id, :p_desc_level_code, :p_arch_number_code); end;");
                q.setParameter("p_univ_data_unit_id", model.getUnivDataUnitId());
                q.setParameter("p_desc_level_code", immModel.getDescriptionLevelCode());
                q.setParameter("p_arch_number_code", model.getArchNumberCode());
                q.executeUpdate();
            }
            if (immModel != null
                  && !journalHelper.safeEquals(model.getPortalSectionId(), journalHelper.getCopyModel().getPortalSectionId())) 
            {
                Query q = em.createNativeQuery("begin COMPLEX_PACK.UPDATE_PORTAL_SECTION("
                        + ":p_univ_data_unit_id, :p_portal_section_id); end;");
                q.setParameter("p_univ_data_unit_id", model.getUnivDataUnitId());
                q.setParameter("p_portal_section_id",
                        (model.getPortalSectionId() == null ? "" : model.getPortalSectionId()));
                q.executeUpdate();
        
                Query t = em.createNativeQuery("begin COMPLEX_PACK.journalInsert_portal_section("
                        + ":p_univ_data_unit_id, :p_portal_section_id_old, :p_portal_section_id_new); end;");
                t.setParameter("p_univ_data_unit_id", model.getUnivDataUnitId());
                t.setParameter("p_portal_section_id_old",(journalHelper.getCopyModel().getPortalSectionId() == null ?
                                                  0 : journalHelper.getCopyModel().getPortalSectionId()));
                t.setParameter("p_portal_section_id_new",(model.getPortalSectionId() == null ? 0 : model.getPortalSectionId()));

                t.executeUpdate();
            }
        }
    }
    catch (Exception e) {
        em.getTransaction().setRollbackOnly();
        throw new RuntimeException(e);
    }
    Query q = em.createNativeQuery(
            "begin CTX_DDL.SYNC_INDEX('IDX_SEARCH'); CTX_DDL.SYNC_INDEX('ITXT_UNIV_UNIT_NAME'); CTX_DDL.SYNC_INDEX('ITXT_UNIV_ANNOTATION'); end;");
    q.executeUpdate();
    
    return getMenuPage()
            .getId() + "?faces-redirect=true&amp;unitId="
            + model.getUnivDataUnitId() + "&amp;mode=edit&amp;saved=true";
  }

  protected abstract void saveDetails();

  protected void saveDefaultLanguages() {
    for (VDescValueWithCode lang : se.getLinearValuesByCode("DOC_LANGUAGE")) {
      VDescAttrvalueWithCode attrVal = se.getAttrValueByCode(lang.getDescriptorValueId(), "DEFAULT");
      if (attrVal != null && attrVal.getAttrValue().equals("true")) {
        UnivDataLanguage defLang = new UnivDataLanguage();
        defLang.setUnivDataUnit(model);
        defLang.setDocLanguageId(lang.getDescriptorValueId());
        em.persist(defLang);
      }
    }
  }

  protected boolean validateIntervalModel() {
    boolean res = true;
    if (intervalModel.getBeginDate() == null && intervalModel.getEndDate() != null) {
      res &= MessageUtils.ErrorMessage("При указании дат необходимо заполнить начальную");
    }
    if (intervalModel.getBeginDate() != null && intervalModel.getEndDate() != null
            && intervalModel.getBeginDate().getTime() > intervalModel.getEndDate().getTime()) {
      res &= MessageUtils.ErrorMessage("Конечная дата не может быть меньше начальной");
    }
    return res;
  }

  protected void saveIntervalModel() {
    if (intervalModel.getBeginDate() != null && intervalModel.getEndDate() == null) {
      intervalModel.setEndDate(intervalModel.getBeginDate());
    }
    if (intervalModel.getDateInetrvalId() == null && intervalModel.getBeginDate() != null) {
      intervalModel.setDataUnit(model);
      em.persist(intervalModel);
    }
    if (intervalModel.getDateInetrvalId() != null && intervalModel.getBeginDate() != null) {
      em.find(UnivDateInterval.class, intervalModel.getDateInetrvalId());
      em.merge(intervalModel);
      em.flush();
    }
    if (intervalModel.getDateInetrvalId() != null && intervalModel.getBeginDate() == null) {
      em.remove(em.merge(intervalModel));
    }

  }

  //добавление дескриптора в карточку док.комплекса
  public void selectDescriptor() {
    if (valueSelectDL.getSelectedValue() == null) {
      MessageUtils.ErrorMessage("Значение должно быть выбрано");
      return;
    }

    VComplexDescriptor desc = new VComplexDescriptor();
    DescriptorValue dv = complexQuery.queryDescriptorValue(valueSelectDL.getSelectedValue());
    String fullValueWithSpravoschnik = dv.getFullValue() + " (" + dv.getDescriptorGroup().getGroupName() +")" ;
    desc.setDescriptor1(fullValueWithSpravoschnik);
    desc.setDescriptorValueId1(valueSelectDL.getSelectedValue());
    desc.setDataUnit(getImmModel());
    int i = 0;
    //immModel = em.merge(getImmModel());
    //em.refresh(immModel);
    List<VComplexDescriptor> list = getDescriptorEmbDL().getWrappedData();

    for (VComplexDescriptor desc1 : list) {
      if (desc1.getDescriptorValueId1().equals(desc.getDescriptorValueId1())) {
        i++;
      }
    }
    if (i == 0) {
      UnivDescriptor unitDesc = new UnivDescriptor();
      unitDesc.setDataUnit(getModel());
      unitDesc.setDescriptorValueId1(valueSelectDL.getSelectedValue());
      if (getDescriptorsToAddList() == null) {
        setDescriptorsToAddList(new ArrayList<UnivDescriptor>());
      }

      getDescriptorsToAddList().add(unitDesc);

      // добавил 11,11
      //em.merge(unitDesc);
      em.persist(unitDesc);
      em.flush();
      em.refresh(unitDesc);
      desc.setUnivDescriptorId(unitDesc.getUnivDescriptorId());
//      em.refresh(em.merge(model));
//      if (immModel != null)
//        em.refresh(em.merge(immModel));
      
      recordAddDescriptor(unitDesc.getUnivDescriptorId());

      desc.setUnivDescriptorId(unitDesc.getUnivDescriptorId());
      list.add(desc);
      valueSelectDL.setValueSelected(true);
    }
  }

  public void deleteDescriptor(UnivDescriptor descForDelete) {
    if (descriptorsForRemoveList == null) {
      descriptorsForRemoveList = new ArrayList<UnivDescriptor>();
    }

    descriptorsForRemoveList.add(descForDelete);

    // добавил 11,11
    journalHelper.recordChangeUnivDescriptor(model, "DELETE", descForDelete.getUnivDescriptorId());
    em.remove(descForDelete);

  }

  protected void recordAddDescriptor(Long univDescritptorId) {
    journalHelper.recordChangeUnivDescriptor(model, "ADD", univDescritptorId);
    em.detach(immModel);
    immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
  }

  public void selectRelatedValue() {
    relEdit.selectRelatedValue();
  }

  public void saveDescRelation() {
//    relEdit.save(getImmModel().getDescriptors());
    VComplexDescriptor desc = relEdit.save();
    if (desc == null) {
      return;
    }

    //VComplexDataUnit vDataUnit = em.find(VComplexDataUnit.class, model.getUnivDataUnitId());
    List<VComplexDescriptor> list = getDescriptorEmbDL().getWrappedData();
//    em.detach(immModel);
    for (int i = 0; i < list.size(); i++) {
      if (list.get(i).getDescriptor1().equals(desc.getDescriptor1())) {
        list.get(i).setDescriptor2(desc.getDescriptor2());
        list.get(i).setDescRelation(desc.getDescRelation());

      }
    }

//    if (immModel != null) {
//      immModel = complexQuery.queryImmModel(unitId);
//      em.refresh(em.merge(immModel));
//    }
//
//    em.refresh(em.merge(model));
  }

  public void clearDescRelation() {

    VComplexDescriptor desc = relEdit.clearRelation();

    //VComplexDataUnit vDataUnit = em.find(VComplexDataUnit.class, model.getUnivDataUnitId());
    List<VComplexDescriptor> list = getDescriptorEmbDL().getWrappedData();
//    em.detach(immModel);
    for (int i = 0; i < list.size(); i++) {
      if (list.get(i).getDescriptor1().equals(desc.getDescriptor1())) {
        if (list.get(i).getDescriptor2() == null) {
          return;
        }
        list.get(i).setDescriptor2(desc.getDescriptor2());
        list.get(i).setDescRelation(desc.getDescRelation());

      }
    }
//
//    em.refresh(em.merge(model));
////    immModel = complexQuery.queryImmModel(unitId);
////    em.refresh(em.merge(immModel));

  }

  public List<UnivDescriptor> getDescriptorsToAddList() {
    return descriptorsToAddList;
  }

  public void setDescriptorsToAddList(List<UnivDescriptor> descriptorsToAddList) {
    this.descriptorsToAddList = descriptorsToAddList;
  }
}
