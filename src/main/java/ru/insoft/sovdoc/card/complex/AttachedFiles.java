package ru.insoft.sovdoc.card.complex;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jpa.DbmsType;
import ru.insoft.commons.service.JPAUtils;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.service.correct.JournalHelper;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.system.complex.SystemQuery;

@RequestScoped
@Named
public class AttachedFiles {

	@Inject
	@RequestParam(value = "unitId")
	Long unitId;
	@Inject
	@RequestParam(value = "fileCaptionOld")
	String fileCaptionOld;
	@Inject
	@RequestParam(value = "fileId")
	Long fileId;
	@Inject
	SystemQuery systemQuery;
	@Inject
	SystemEntity se;
	@Inject
	EntityManager em;
	@Inject
	JournalHelper journalHelper;

	private final static Map<Character, Character> letters = new HashMap<Character, Character>() {
		{
			put('Р', 'R');
			put('Г', 'G');
			put('А', 'A');
			put('С', 'S');
			put('П', 'P');
			put('И', 'I');
			put('Ф', 'F');
			put('О', 'O');
			put('п', 'p');
			put('Д', 'D');
			put('Л', 'L');
			put('л', 'l');
		}
	};

	private Map<String, List<ImagePath>> filesMap;
	private int mapId = 0;
	private String serverNetAddress;
	private String documentRoot;

	public List<ImagePath> getFiles(String category) {
		if (filesMap == null) {
			filesMap = new HashMap<String, List<ImagePath>>();
		}
		if (filesMap.get(category) == null) {
			filesMap.put(category, systemQuery.queryImagePathList(unitId, category));
		}
		return filesMap.get(category);
	}

	public List<ImagePath> getMaps() {
		List<ImagePath> maps = new ArrayList<>();
		for (ImagePath p : getFiles("FULL_SIZE")) {
			if (isMap(p)) {
				maps.add(p);
			}
		}
		return maps;
	}

	public Integer getFilesCount(String category) {
		List<ImagePath> files = getFiles(category);
		if (files == null) {
			return 0;
		} else {
			return files.size();
		}
	}

	protected String getServerNetAddress() {
		if (serverNetAddress == null) {
			serverNetAddress = se.getSystemParameterValue("SERVER_NET_ADDRESS");
		}
		return serverNetAddress;
	}

	protected String getDocumentRoot() {
		if (documentRoot == null) {
			documentRoot = se.getSystemParameterValue("DOCUMENT_ROOT");
		}
		return documentRoot;
	}

	public boolean isMap(ImagePath ip) {
		return ip.getDescriptorValueId().getDescriptorValueId() == 420584;
	}

	public int getMapId(ImagePath ip) {
		if (isMap(ip)) {
			return mapId++;
		}
		return -1;
	}

	public String getMapURL(ImagePath ip) {
		return se.getSystemParameterValue("SERVER_NET_ADDRESS") + "Maps/" + rename(ip.getCaption()) + "/index.html";
	}

	private String rename(String name) {
		StringBuilder sb = new StringBuilder();
		for (Character c : name.toCharArray()) {
			Character letter = letters.get(c);
			if (letter == null) {
				sb.append(c);
			} else {
				sb.append(letter);
			}
		}
		return sb.toString();
	}

	public String getFileURL(ImagePath ip) {
		String filePath = ip.getFilePath().replaceFirst(
				getDocumentRoot(), getServerNetAddress());
		File f = new File(ip.getFilePath(), ip.getFileName());
		if (f.isFile()) {
			return filePath + "/" + ip.getFileName();
		} else {
			return "../../resources/images/404.jpg";
		}
	}

	public void fileMoveUp(Long fileNum, String category) {
		ImagePath ip1 = systemQuery.queryImagePath(unitId, fileNum, category);
		ImagePath ip2 = systemQuery.queryImagePath(unitId, fileNum - 1, category);
		ip1.setSortOrder(fileNum - 1L);
		ip2.setSortOrder(fileNum);
		List<ImagePath> ipList = filesMap.get(category);
		ipList.set(fileNum.intValue() - 2, ip1);
		ipList.set(fileNum.intValue() - 1, ip2);
		filesMap.put(category, ipList);
	}

	public void fileMoveDown(Long fileNum, String category) {
		ImagePath ip1 = systemQuery.queryImagePath(unitId, fileNum, category);
		ImagePath ip2 = systemQuery.queryImagePath(unitId, fileNum + 1, category);
		ip1.setSortOrder(fileNum + 1L);
		ip2.setSortOrder(fileNum);
		List<ImagePath> ipList = filesMap.get(category);
		ipList.set(fileNum.intValue(), ip1);
		ipList.set(fileNum.intValue() - 1, ip2);
		filesMap.put(category, ipList);
	}

	public void removeFile(String category) {
		ImagePath ip = em.find(ImagePath.class, fileId);
		File f = new File(ip.getFilePath(), ip.getFileName());

		f.delete();
		em.remove(ip);
		JPAUtils.callStoredProcedure(em, DbmsType.DBMS_TYPE_ORACLE,
				"COMPLEX_PACK.FILE_ORDER_DECREASE", ip.getDataUnitId(),
				ip.getImageCategory().getDescriptorValueId(), ip.getSortOrder());

		journalHelper.recordDetachFiles(unitId, category, ip);
	}

	public void changeFilesName(String fileCaptionNew, String category) {
		if (fileCaptionOld == null) {
			fileCaptionOld = "";
		}
		if (fileCaptionNew == null) {
			fileCaptionNew = "";
		}

		if (fileCaptionOld.trim().compareTo(fileCaptionNew.trim()) != 0) {
			if (fileCaptionOld.trim().equals("")) {
				fileCaptionOld = "&lt;&lt;пусто&gt;&gt;";
			}
			if (fileCaptionNew.trim().equals("")) {
				fileCaptionNew = "&lt;&lt;пусто&gt;&gt;";
			}
			journalHelper.recordChangeFiles(unitId, category, fileCaptionOld, fileCaptionNew);
		}
	}

}
