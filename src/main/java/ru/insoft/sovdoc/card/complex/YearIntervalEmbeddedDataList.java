package ru.insoft.sovdoc.card.complex;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.faces.context.FacesContext;
import com.google.common.collect.Lists;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval;
import ru.insoft.sovdoc.model.complex.view.VComplexIntervalYears;

public abstract class YearIntervalEmbeddedDataList extends EmbeddedDataList<VComplexIntervalYears> {

  protected abstract ComplexCard getCard();


  public YearIntervalEmbeddedDataList() {
    String p_yearCount = FacesContext.getCurrentInstance().getExternalContext()
            .getRequestParameterMap().get("yearCount");
    if (p_yearCount != null && !p_yearCount.isEmpty()) {
      setRowCount(Integer.valueOf(p_yearCount));
    } else {
      setRowCount(getCard().getImmModel().getYears().size());
    }
  }

  protected void addEmptyYears() {
    List<VComplexIntervalYears> years = null;
    if (super.getWrappedData() == null) {
      years = getCard().getImmModel().getYears();
      if (years.size() > getRowCount()) {
        years = years.subList(0, getRowCount());
      }
    } else {
      years = super.getWrappedData();
      Collections.sort(years, new YearIntervalComparator());
    }
    for (int i = years.size(); i < getRowCount(); i++) {
      VComplexIntervalYears year = new VComplexIntervalYears();
      year.setDataUnit(getCard().getImmModel());
      years.add(year);
    }
    setWrappedData(years);
  }

  @Override
  public List<VComplexIntervalYears> getWrappedData() {
    if (super.getWrappedData() == null && getCard().getImmModel() != null) {
      addEmptyYears();
      if (!"save".equals(getCard().action))
				detachContents(getCard().em);
    }
    return super.getWrappedData();
  }

  public void addInterval() {
    setRowCount(getRowCount() + 1);
    addEmptyYears();
  }

  public void remove(VComplexIntervalYears interval) {
    getWrappedData().remove(interval);
    /*if (interval.getDateIntervalId() != null)
     {
     UnivDateInterval udi = getCard().complexQuery.queryDateInterval(interval.getDateIntervalId());
     getCard().em.remove(udi);
     }*/
    setRowCount(getRowCount() - 1);
  }

  public boolean validate() {
    if (getWrappedData().size() == 0) {
      return MessageUtils.ErrorMessage("Крайние даты должны быть заполнены");
    }
    for (VComplexIntervalYears interval : getWrappedData()) {
      boolean res = true;
      if (interval.getBeginYear() == null) {
        res &= MessageUtils.ErrorMessage("Начальный год материалов не может быть пустым");
      }
      if (interval.getBeginYear() != null && interval.getEndYear() != null
              && interval.getBeginYear() > interval.getEndYear()) {
        res &= MessageUtils.ErrorMessage("Начальный год не может быть больше конечного");
      }
      if (!res) {
        return false;
      }
    }
    return true;
  }

  public void save() {
    List<UnivDateInterval> dateIntervals = Lists.newArrayList();
    Integer beginYear = null;
    Integer endYear = 0;

    Collections.sort(getWrappedData(), new YearIntervalComparator());
    for (VComplexIntervalYears interval : getWrappedData()) {
      if (interval.getBeginYear() > endYear + 1) {
        setNewInterval(beginYear, endYear, dateIntervals);
        beginYear = interval.getBeginYear();
      }
      if (interval.getEndYear() == null) {
        interval.setEndYear(interval.getBeginYear());
      }
      if (interval.getEndYear() > endYear) {
        endYear = interval.getEndYear();
      }
    }
    setNewInterval(beginYear, endYear, dateIntervals);

    for (int i = 0; i < Math.max(dateIntervals.size(), getCard().getModel().getDateIntervals().size()); i++) {
      if (i >= dateIntervals.size()) {
    //    getCard().em.find(null, i);

        getCard().em.remove(getCard().em.merge(getCard().getModel().getDateIntervals().get(i)));
        continue;
      }
      if (i >= getCard().getModel().getDateIntervals().size()) {
        getCard().em.persist(dateIntervals.get(i));
        continue;
      }
      UnivDateInterval interval = getCard().getModel().getDateIntervals().get(i);
      interval.setBeginDate(dateIntervals.get(i).getBeginDate());
      interval.setEndDate(dateIntervals.get(i).getEndDate());
      getCard().em.merge(interval);
    }
  }

  protected void setNewInterval(Integer beginYear, Integer endYear, List<UnivDateInterval> intervals) {
    if (beginYear != null) {
      Calendar calendar = Calendar.getInstance();
      UnivDateInterval newInterval = new UnivDateInterval();
      newInterval.setDataUnit(getCard().getModel());
      calendar.set(beginYear, Calendar.JANUARY, 1, 0, 0, 0);
      newInterval.setBeginDate(calendar.getTime());
      calendar.set(endYear, Calendar.DECEMBER, 31, 0, 0, 0);
      newInterval.setEndDate(calendar.getTime());
      intervals.add(newInterval);
    }
  }
}
