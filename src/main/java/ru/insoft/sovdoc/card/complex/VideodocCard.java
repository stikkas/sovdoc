package ru.insoft.sovdoc.card.complex;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchVideoStoryDescription;
import ru.insoft.sovdoc.model.complex.table.ArchVideodoc;
import ru.insoft.sovdoc.model.complex.table.UnivDataLanguage;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexVideoStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexVideodoc;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

/**
 *
 * @author melnikov
 */
@ViewScoped
@ManagedBean
public class VideodocCard extends ComplexCard {

	@ManagedProperty("#{attachedFilesUpload}")
	AttachedFilesUpload afUpload;

	public AttachedFilesUpload getAfUpload() {
		return afUpload;
	}

	public void setAfUpload(AttachedFilesUpload afUpload) {
		this.afUpload = afUpload;
	}

	private Boolean editing;
	private ArchFund fundModel;
	private ArchSeries seriesModel;
	private ArchVideodoc videodocModel;

	private VComplexVideodoc videodocImmModel;

	private VideoStorageUnitEmbDataList storageUnitEmbDL;

	private DescriptorEmbeddedDataList descriptorEmbDL;
	private UnivClssEmbeddedDataList univClssEmbDL;
	private VideoStoryDescriptionEmbDataList videoStoryDescEmbDL;

	@Override
	public boolean isEditing() {
		if (editing == null) {
			editing = super.isEditing();
		}
		return editing;
	}

	public ArchFund getFundModel() {
		if (fundModel == null) {
			fundModel = getParentFund();
		}
		return fundModel;
	}

	public ArchSeries getSeriesModel() {
		if (seriesModel == null) {
			seriesModel = getParentSeries();
		}
		return seriesModel;
	}

	public ArchVideodoc getVideodocModel() {
		if (videodocModel == null) {
			model = getModel();
			if (model.getUnivDataUnitId() == null) {
				videodocModel = new ArchVideodoc();
				videodocModel.setStorageUnits(new ArrayList<ArchVideoStorageUnit>());
			} else {
				videodocModel = complexQuery.queryVideodocModel(model.getUnivDataUnitId());
			}
		}
		return videodocModel;
	}

	public VComplexVideodoc getVideodocImmModel() {
		if (videodocImmModel == null) {
			model = getModel();
			if (model.getUnivDataUnitId() == null) {
				videodocImmModel = new VComplexVideodoc();
				videodocImmModel.setStorageUnitCount(0);
				videodocImmModel.setStorageUnits(new ArrayList<VComplexVideoStorageUnit>());
			} else {
				videodocImmModel = complexQuery.queryVideodocImmModel(model.getUnivDataUnitId());
			}
		}
		return videodocImmModel;
	}

	@Override
	protected Long getUnitTypeId() {
		return se.getDescValueByCodes("UNIV_UNIT_TYPE", "VIDEODOC").getDescriptorValueId();
	}

	@Override
	protected MENU_PAGES getMenuPage() {
		return MENU_PAGES.VIDEODOC;
	}

	public void changeVideodocType() {
		// пока закомментируем, потом либо удалим, либо используем
//		for (ArchVideoStorageUnit storageUnit : getVideodocModel().getStorageUnits()) {
//			storageUnit.setSystemRecordId(null);
//		}
	}

	public VideoStorageUnitEmbDataList getStorageUnitEmbDL() {
		if (storageUnitEmbDL == null) {
			storageUnitEmbDL = new VideoStorageUnitEmbDataList() {
				@Override
				public VideodocCard getCard() {
					return VideodocCard.this;
				}
			};
		}
		return storageUnitEmbDL;
	}

	public String getPlaytimeStr(Integer playtime) {
		if (playtime == null) {
			return null;
		}
		return String.format("%1$d:%2$02d", playtime / 60, playtime % 60);
	}

	public String getTotalPlaytimeStr() {
		int res = 0;
		boolean show = false;
		for (VComplexVideoStorageUnit storageUnit : getVideodocImmModel().getStorageUnits()) {
			if (storageUnit.getPlaytime() != null) {
				res += storageUnit.getPlaytime();
				show = true;
			}
		}
		return getPlaytimeStr(show ? res : null);
	}

	public void postFileUpload() {
		if (!afUpload.getUploadedFiles().isEmpty()) {
			ImagePath file = afUpload.getUploadedFiles().get(0);
			if (afUpload.getCategory().getValueCode().equals("VIDEO_UNIT")) {
				getStorageUnitEmbDL().attachVideoFile(afUpload.getSubId(), file);
			}
			if (afUpload.getCategory().getValueCode().equals("VIDEO_STORY_DESCRIPTION")) {
				getVideoStoryDescEmbDL().attachVideoFile(afUpload.getSubId(), file);
			}
		}
		afUpload.finishUploading();
	}

	public UnivClssEmbeddedDataList getUnivClssEmbDL() {
		if (univClssEmbDL == null) {
			univClssEmbDL = new UnivClssEmbeddedDataList() {
				@Override
				public ComplexCard getCard() {
					return VideodocCard.this;
				}
			};
		}
		return univClssEmbDL;
	}

	public void selectUnivClssValue() {
		getUnivClssEmbDL().addClassifierValue();
	}

	@Override
	public DescriptorEmbeddedDataList getDescriptorEmbDL() {
		if (descriptorEmbDL == null) {
			descriptorEmbDL = new DescriptorEmbeddedDataList() {
				@Override
				protected ComplexCard getCard() {
					return VideodocCard.this;
				}
			};
		}
		return descriptorEmbDL;
	}

	public VideoStoryDescriptionEmbDataList getVideoStoryDescEmbDL() {
		if (videoStoryDescEmbDL == null) {
			videoStoryDescEmbDL = new VideoStoryDescriptionEmbDataList() {
				@Override
				public VideodocCard getCard() {
					return VideodocCard.this;
				}
			};
		}
		return videoStoryDescEmbDL;
	}

	@Override
	protected boolean validate() {
		boolean res = true;
		if (immModel.getFundNum() == null && (immModel.getFundLetter() != null || immModel.getFundPrefix() != null)) {
			res &= MessageUtils.ErrorMessage("Отсутствует номер фонда");
		}
		if (StringUtils.getByteLengthUTF8(immModel.getFundLetter()) > 250) {
			res &= MessageUtils.ErrorMessage("Литера фонда слишком длинная");
		}
		if (StringUtils.getByteLengthUTF8(immModel.getFundPrefix()) > 4) {
			res &= MessageUtils.ErrorMessage("Префикс фонда слишком длинный");
		}
		if (immModel.getSeriesNum() == null && immModel.getSeriesLetter() != null) {
			res &= MessageUtils.ErrorMessage("Литера описи не может быть без номера");
		}
		if (StringUtils.getByteLengthUTF8(immModel.getSeriesLetter()) > 250) {
			res &= MessageUtils.ErrorMessage("Литера описи слишком длинная");
		}
		if (model.getNumberNumber() == null) {
			res &= MessageUtils.ErrorMessage("Номер единицы учёта должен быть заполнен");
		}
		if (StringUtils.getByteLengthUTF8(model.getNumberText()) > 250) {
			res &= MessageUtils.ErrorMessage("Литера единицы учёта слишком длинная");
		}
		if (videodocModel.getVideodocTypeId() == null) {
			res &= MessageUtils.ErrorMessage("Вид видеодокумента должен быть указан");
		}
		if (videodocModel.getVideodocName() == null) {
			res &= MessageUtils.ErrorMessage("Название документа должно быть заполнено");
		}
		if (StringUtils.getByteLengthUTF8(videodocModel.getVideodocName()) > 2500) {
			res &= MessageUtils.ErrorMessage("Название документа слишком длинное");
		}
		if (videodocModel.getEventPlace() == null) {
			res &= MessageUtils.ErrorMessage("Место события должно быть указано");
		}
		if (StringUtils.getByteLengthUTF8(videodocModel.getEventPlace()) > 250) {
			res &= MessageUtils.ErrorMessage("Место события слишком длинное");
		}
		if (videodocModel.getFilmStudio() == null) {
			res &= MessageUtils.ErrorMessage("Киностудия должна быть указана");
		}
		if (StringUtils.getByteLengthUTF8(videodocModel.getFilmStudio()) > 250) {
			res &= MessageUtils.ErrorMessage("Название киностудии слишком длинное");
		}
		if (videodocModel.getDirector() == null) {
			res &= MessageUtils.ErrorMessage("Режиссер должен быть указан");
		}
		if (StringUtils.getByteLengthUTF8(videodocModel.getDirector()) > 250) {
			res &= MessageUtils.ErrorMessage("Имя режиссера слишком длинное");
		}
		res &= getStorageUnitEmbDL().validate();
		if (intervalModel.getBeginDate() == null) {
			res &= MessageUtils.ErrorMessage("Дата события должна быть заполнена");
		}
		if (videodocModel.getIssueDate()== null) {
			res &= MessageUtils.ErrorMessage("Дата выпуска должна быть заполнена");
		}
		if (StringUtils.getByteLengthUTF8(videodocModel.getNotes()) > 4000) {
			res &= MessageUtils.ErrorMessage("Примечание слишком длинное");
		}
		res &= getVideoStoryDescEmbDL().validate();

		return res;
	}

	@Override
	protected String fillArchNumberCode() {
		StringBuilder sb = new StringBuilder();
		String text = immModel.getFundPrefix();
		if (text != null) {
			sb.append(text).append("\u0001");
		}
		Integer number = immModel.getFundNum();
		if (number != null) {
			sb.append(String.format("%1$08d", number));
		}
		sb.append("\u0001");
		text = immModel.getFundLetter();
		if (text != null) {
			sb.append(text);
		}
		sb.append("\u0001");
		number = immModel.getSeriesNum();
		if (number != null) {
			sb.append(String.format("%1$08d", number));
		}
		sb.append("\u0001");
		text = immModel.getSeriesLetter();
		if (text != null) {
			sb.append(text);
		}
		sb.append(String.format("\u0001%1$08d\u0001", model.getNumberNumber()));
		text = model.getNumberText();
		if (text != null) {
			sb.append(text);
		}
		return sb.toString();
	}

	@Override
	protected String fillUnitName() {
		return String.format("Видеодокумент %1$d%2$s. %3$s",
				model.getNumberNumber(),
				model.getNumberText() == null ? "" : model.getNumberText(),
				videodocModel.getVideodocName());
	}

	@Override
	protected void saveDefaultLanguages() {
	}

	protected class PhonoLanguageCollection extends LanguageCollection {

		@Override
		protected ComplexCard getCard() {
			return VideodocCard.this;
		}

		@Override
		protected void init() {
			for (ArchVideoStoryDescription description : videoStoryDescEmbDL.getWrappedData()) {
				if (description.getLanguageId() != null) {
					UnivDataLanguage language = new UnivDataLanguage();
					language.setDocLanguageId(description.getLanguageId());
					addFilledItem(language);
				}
			}
		}
	}

	@Override
	protected void saveDetails() {
		UnivDataUnit copyModel = null;
		ArchVideodoc copyVideodocModel = null;

		boolean isNew = false;
		if (videodocModel.getUnivDataUnitId() == null) {
			videodocModel.setUnivDataUnitId(model.getUnivDataUnitId());
			em.persist(videodocModel);
			isNew = true;
		} else {
			copyModel = complexQuery.queryModel(model.getUnivDataUnitId());
			copyVideodocModel = complexQuery.queryVideodocModel(model.getUnivDataUnitId());
			journalHelper.setVideodocCopyModels(copyVideodocModel,
					complexQuery.queryVideodocImmModel(model.getUnivDataUnitId()));
			em.detach(videodocImmModel);
			videodocModel = em.merge(videodocModel);
		}

		saveIntervalModel();
		new PhonoLanguageCollection().save();
		storageUnitEmbDL.save(copyVideodocModel);
		univClssEmbDL.save(copyModel);
		videoStoryDescEmbDL.save(copyVideodocModel);
		em.flush();

		if (isNew) {
			journalHelper.recordFullUnivDataUnit(model, "INPUT", cardLinks, null);
		} else {
			immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
            videodocImmModel = complexQuery.queryVideodocImmModel(model.getUnivDataUnitId());
            journalHelper.recordEditVideodoc(model, immModel, videodocModel, videodocImmModel);
		}
	}
}
