package ru.insoft.sovdoc.card.complex;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@RequestScoped
@Named
public class SectionCard extends ComplexCard {

	@Override
	protected Long getUnitTypeId() 
	{
		return se.getImmDescValueByCodes("UNIV_UNIT_TYPE", "SECTION").getDescriptorValueId();
	}

	@Override
	protected MENU_PAGES getMenuPage() 
	{
		return MENU_PAGES.SECTION;
	}

	@Override
	protected boolean validate() 
	{
		boolean res = true;
		if (model.getUnitName() == null)
			res &= MessageUtils.ErrorMessage("Наименование раздела должно быть заполнено");
		if (StringUtils.getByteLengthUTF8(model.getUnitName()) > 4000)
			res &= MessageUtils.ErrorMessage("Наименование раздела слишком длинное");
		return res;
	}

	@Override
	protected String fillArchNumberCode() 
	{
		return null;
	}

	@Override
	protected String fillUnitName() 
	{
		return model.getUnitName();
	}
	
	@Override
	protected void saveDetails()
	{
		
	}

}
