package ru.insoft.sovdoc.card.complex;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.system.complex.SystemQuery;

@RequestScoped
@Named
public class ShowImage {

	@Inject
	@RequestParam(value = "unitId")
	Long unitId;
	@Inject
	@RequestParam(value = "fileNum")
	Long fileNum;
	@Inject
	SystemQuery complexQuery;
	@Inject
	SystemEntity se;

	private VComplexDataUnit model;

	public VComplexDataUnit getModel() {
		if (model == null && unitId != null) {
			model = complexQuery.queryImmModel(unitId);
		}
		return model;
	}

	public String getImageURL() {
		try {
			ImagePath ip = complexQuery.queryImagePath(unitId, fileNum, "FULL_SIZE");
			String filePath = ip.getFilePath().replaceFirst(
					se.getSystemParameterValue("DOCUMENT_ROOT"),
					se.getSystemParameterValue("SERVER_NET_ADDRESS"));
			return filePath + "/" + ip.getFileName();
		} catch (Exception e) {
			return "../../resources/images/404.jpg";
		}
	}
}
