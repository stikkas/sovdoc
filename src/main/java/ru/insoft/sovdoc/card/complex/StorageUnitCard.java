package ru.insoft.sovdoc.card.complex;

import java.util.List;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexUnitHierarchy;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

import com.google.common.collect.Lists;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean
public class StorageUnitCard extends ComplexCard {
  
  private ArchFund fundModel;
  private ArchSeries seriesModel;
  private ArchStorageUnit storageUnitModel;
  private VComplexStorageUnit storageUnitImmModel;
  private DescriptorEmbeddedDataList descriptorEmbDL;
  private LanguageCollection langCollection;
  
  public ArchFund getFundModel() {
    if (fundModel == null) {
      fundModel = getParentFund();
    }
    
    return fundModel;
  }
  
  public ArchSeries getSeriesModel() {
    if (seriesModel == null) {
      seriesModel = getParentSeries();
    }
    
    return seriesModel;
  }
  
  public ArchStorageUnit getStorageUnitModel() {
    if (storageUnitModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        storageUnitModel = new ArchStorageUnit();
      } else {
        storageUnitModel = complexQuery.queryStorageUnitModel(model.getUnivDataUnitId());
        journalHelper.copyStorageUnitModel(storageUnitModel);
        if (action == null || !action.equals("save")) {
          em.detach(storageUnitModel);
        }
      }
    }
    return storageUnitModel;
  }
  
  public VComplexStorageUnit getStorageUnitImmModel() {
    if (storageUnitImmModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        storageUnitImmModel = new VComplexStorageUnit();
      } else {
        storageUnitImmModel = complexQuery.queryStorageUnitImmModel(model.getUnivDataUnitId());
      }
    }
    return storageUnitImmModel;
  }
  
  @Override
  protected Long getUnitTypeId() {
    return se.getImmDescValueByCodes("UNIV_UNIT_TYPE", "PAPER_FILE").getDescriptorValueId();
  }
  
  @Override
  protected MENU_PAGES getMenuPage() {
    return MENU_PAGES.PAPER_FILE;
  }
  
  public DescriptorEmbeddedDataList getDescriptorEmbDL() {
    if (descriptorEmbDL == null) {
      descriptorEmbDL = new DescriptorEmbeddedDataList() {
        @Override
        protected ComplexCard getCard() {
          return StorageUnitCard.this;
        }
      };
    }
    return descriptorEmbDL;
  }
  
  public LanguageCollection getLangCollection() {
    if (langCollection == null) {
      langCollection = new LanguageCollection() {
        @Override
        protected ComplexCard getCard() {
          return StorageUnitCard.this;
        }
      };
    }
    return langCollection;
  }
  
  public List<VComplexDescriptor> getRubrics() {
    descriptorEmbDL = getDescriptorEmbDL();
    List<VComplexDescriptor> rubrics = Lists.newArrayList();
    for (VComplexDescriptor desc : descriptorEmbDL.getWrappedData()) {
      if (desc.getGroupCode() != null && desc.getGroupCode().equals("UNIT_RUBRICATOR")) {
        rubrics.add(desc);
      }
    }
    return rubrics;
  }
  
  public List<VComplexUnitHierarchy> getSeriesStructures() {
    List<VComplexUnitHierarchy> structures = Lists.newArrayList();
    if (getHierarchy() != null) {
      for (VComplexUnitHierarchy unit : hierarchy) {
        if (unit.getParentType().getValueCode().equals("SERIES_STRUCTURE_LEVEL")) {
          structures.add(unit);
        }
      }
    }
    
    return structures;
  }
  
  @Override
  protected void saveDetails() {
    boolean isNew = false;
    if (storageUnitModel.getUnivDataUnitId() == null) {
      storageUnitModel.setUnivDataUnitId(model.getUnivDataUnitId());
      em.persist(storageUnitModel);
      isNew = true;
    }
    saveIntervalModel();
    langCollection.save();
//    em.flush();

    //model = getModel();
    //em.merge(model);
    //em.flush();
    storageUnitModel = em.merge(storageUnitModel);
    
    model = em.merge(model);
    em.flush();
    em.refresh(storageUnitModel);
    em.refresh(model);
    immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
    
    storageUnitImmModel = complexQuery.queryStorageUnitImmModel(model.getUnivDataUnitId());
    
    if (isNew) {
      journalHelper.recordFullUnivDataUnit(model, "INPUT", cardLinks, null);
    } else {
      journalHelper.recordEditStorageUnit(model, immModel, storageUnitModel, cardLinks);
    }
  }
  
  @Override
  protected void saveDefaultLanguages() {
    if (langCollection.getItemCount() == 0) {
      super.saveDefaultLanguages();
    }
  }
  
  @Override
  protected boolean validate() {
    boolean res = true;
    if (immModel.getFundNum() == null) {
      res &= MessageUtils.ErrorMessage("Номер фонда должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getFundPrefix()) > 4) {
      res &= MessageUtils.ErrorMessage("Префикс фонда слишком длинный");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getFundLetter()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера фонда слишком длинная");
    }
    if (immModel.getSeriesNum() == null) {
      res &= MessageUtils.ErrorMessage("Номер описи должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getSeriesLetter()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера описи слишком длинная");
    }
    if (model.getNumberNumber() == null) {
      res &= MessageUtils.ErrorMessage("Номер дела должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(model.getNumberText()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера дела слишком длинная");
    }
    if (storageUnitModel.getFileCaption() == null) {
      res &= MessageUtils.ErrorMessage("Заголовок дела должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(storageUnitModel.getFileCaption()) > 2500) {
      res &= MessageUtils.ErrorMessage("Заголовок дела слишком длинный");
    }
    res &= validateIntervalModel();
    if (StringUtils.getByteLengthUTF8(storageUnitModel.getDatesNote()) > 50) {
      res &= MessageUtils.ErrorMessage("Комментарий к дате слишком длинный");
    }
    if (StringUtils.getByteLengthUTF8(storageUnitModel.getWorkIndex()) > 250) {
      res &= MessageUtils.ErrorMessage("Делопроизводственный индекс слишком длинный");
    }
    if (StringUtils.getByteLengthUTF8(storageUnitModel.getNotes()) > 4000) {
      res &= MessageUtils.ErrorMessage("Примечание слишком длинное");
    }
    
    if (!res) {
      em.detach(model);
      em.detach(storageUnitModel);
      em.detach(intervalModel);
    }
    
    return res;
  }
  
  @Override
  protected String fillArchNumberCode() {
      String prefixFund = immModel.getFundPrefix();
      String letterFund = immModel.getFundLetter();
    String res = String.format("%1$s\u0001%2$08d\u0001%3$s\u0001%4$08d\u0001%5$s\u0001%6$08d\u0001%7$s\u0001",
            prefixFund == null ? "" : prefixFund, immModel.getFundNum(), letterFund == null ? "" : letterFund,
            immModel.getSeriesNum(),
            (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()),
            model.getNumberNumber(),
            (model.getNumberText() == null ? "" : model.getNumberText()));
    if (storageUnitModel.getVolumeNum() != null) {
      res += String.format("%1$05d", storageUnitModel.getVolumeNum());
    }
    res += "\u0001";
    if (storageUnitModel.getPartNum() != null) {
      res += String.format("%1$05d", storageUnitModel.getPartNum());
    }
    return res;
  }
  
  @Override
  protected String fillUnitName() {
    return String.format("Дело %1$d%2$s. %3$s", model.getNumberNumber(),
            (model.getNumberText() == null ? "" : model.getNumberText()),
            storageUnitModel.getFileCaption());
  }
  
}
