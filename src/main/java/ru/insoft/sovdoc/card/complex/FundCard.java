package ru.insoft.sovdoc.card.complex;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchFundName;
import ru.insoft.sovdoc.model.complex.view.VComplexFund;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@ViewScoped
@ManagedBean
public class FundCard extends ComplexCard {

    private ArchFund fundModel;
    private VComplexFund fundImmModel;
    private YearIntervalEmbeddedDataList yearIntervalEmbDL;
    private FundNameEmbeddedDataList fundNameEmbDL;
    private DescriptorEmbeddedDataList descriptorEmbDL;

    public ArchFund getFundModel() {
        if (fundModel == null) {
            model = getModel();
            if (model.getUnivDataUnitId() == null) {
                fundModel = new ArchFund();
                fundModel.setFundNames(new ArrayList<ArchFundName>());
            } else {
                fundModel = complexQuery.queryFundModel(model.getUnivDataUnitId());
                journalHelper.copyFundModel(fundModel);
                journalHelper.copyFundImmModel(getFundImmModel());
                if (action == null || !action.equals("save")) {
                    em.detach(fundModel);
                }
            }
        }
        return fundModel;
    }

    public void refreshFundModel() {
        fundModel = null;
        fundModel = getFundModel();
    }

    public VComplexFund getFundImmModel() {
        if (fundImmModel == null) {
            model = getModel();
            if (model.getUnivDataUnitId() == null) {
                fundImmModel = new VComplexFund();
            } else {
                fundImmModel = complexQuery.queryFundImmModel(model.getUnivDataUnitId());
            }
        }
        return fundImmModel;
    }

    @Override
    protected Long getUnitTypeId() {
        return se.getImmDescValueByCodes("UNIV_UNIT_TYPE", "FUND").getDescriptorValueId();
    }

    @Override
    protected MENU_PAGES getMenuPage() {
        return MENU_PAGES.FUND;
    }

    public FundNameEmbeddedDataList getFundNameEmbDL() {
        if (fundNameEmbDL == null) {
            fundNameEmbDL = new FundNameEmbeddedDataList() {
                @Override
                protected FundCard getCard() {
                    return FundCard.this;
                }
            };
        }
        return fundNameEmbDL;
    }

    public YearIntervalEmbeddedDataList getYearIntervalEmbDL() {
        if (yearIntervalEmbDL == null) {
            yearIntervalEmbDL = new YearIntervalEmbeddedDataList() {
                @Override
                protected ComplexCard getCard() {
                    return FundCard.this;
                }
            };
        }
        return yearIntervalEmbDL;
    }

    @Override
    public DescriptorEmbeddedDataList getDescriptorEmbDL() {
        if (descriptorEmbDL == null) {
            descriptorEmbDL = new DescriptorEmbeddedDataList() {
                @Override
                protected ComplexCard getCard() {
                    return FundCard.this;
                }
            };
        }
        return descriptorEmbDL;
    }

    @Override
    protected void saveDetails() {
        boolean isNew = false;
        if (fundModel.getDataUnitId() == null) {
            fundModel.setDataUnitId(model.getUnivDataUnitId());
            em.persist(fundModel);
            isNew = true;
        }
        fundNameEmbDL.save();
        yearIntervalEmbDL.save();
//    em.flush();

        fundModel = em.merge(fundModel);
        model = em.merge(model);
        em.flush();

        em.refresh(fundModel);
        em.refresh(model);

        immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());

        fundImmModel = complexQuery.queryFundImmModel(model.getUnivDataUnitId());

        if (isNew) {
            journalHelper.recordFullUnivDataUnit(model, "INPUT", null, null);
        } else {
            journalHelper.recordEditFund(model, immModel, fundModel, fundImmModel);
        }
    }

    @Override
    protected boolean validate() {
        boolean res = true;
        if (model.getNumberNumber() == null) {
            res &= MessageUtils.ErrorMessage("Номер фонда должен быть заполнен");
        }
        if (StringUtils.getByteLengthUTF8(model.getNumberText()) > 250) {
            res &= MessageUtils.ErrorMessage("Литера фонда слишком длинная");
        }
        if (StringUtils.getByteLengthUTF8(model.getNumberPrefix()) > 4) {
            res &= MessageUtils.ErrorMessage("Префикс фонда слишком длинный");
        }

        if (fundModel.getFundTypeId() == null) {
            res &= MessageUtils.ErrorMessage("Вид фонда должен быть указан");
        }
        if (StringUtils.getByteLengthUTF8(fundModel.getNotes()) > 4000) {
            res &= MessageUtils.ErrorMessage("Примечание слишком длинное");
        }
        res &= getFundNameEmbDL().validate();
        res &= getYearIntervalEmbDL().validate();

        if (!res) {
            em.detach(model);
            em.detach(fundModel);
        }

        return res;
    }

    @Override
    protected String fillArchNumberCode() {
        String res = String.format("%1$08d\u0001", model.getNumberNumber());
        String litera = model.getNumberText();
        String prefix = model.getNumberPrefix();
        if (litera != null) {
            res += litera;
        }
        if (prefix != null) {
            res = prefix + "\u0001" + res;
        }
        return res;
    }

    @Override
    protected String fillUnitName() {
        String prefix = model.getNumberPrefix();
        String litera = model.getNumberText();
        return "Фонд " + (prefix == null ? "" : prefix + "-") + model.getNumberNumber()
                + (litera == null ? "" : litera)
                + ". " + getFundNameEmbDL().getActualName().getFundName();
    }

}
