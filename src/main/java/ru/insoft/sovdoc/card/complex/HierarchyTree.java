package ru.insoft.sovdoc.card.complex;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;

import ru.insoft.commons.jsf.ui.CommonTree;
import ru.insoft.commons.jsf.ui.SimpleItemTreeNode;
import ru.insoft.sovdoc.model.complex.view.VComplexUnitHierarchy;
import ru.insoft.sovdoc.system.complex.SystemQuery;

@RequestScoped
@Named
public class HierarchyTree extends CommonTree<TreeNode> {

	@Inject
	EntityManager em;
	@Inject
	SystemQuery complexQuery;
	
	@Inject
	@RequestParam(value = "unitId")
	Long unitId;
	
	@Inject
	@RequestParam(value = "parentId")
	Long parentId;
	
	@Override
	protected void initRoots() 
	{
		roots = new TreeNodeImpl();
		List<VComplexUnitHierarchy> hierarchy = null;
		if (unitId != null || parentId != null)
			hierarchy = complexQuery.queryHierarchy(unitId == null ? parentId : unitId);
		else
			return;
		SimpleItemTreeNode child = null;
		SimpleItemTreeNode parent = null;
		for (int i = hierarchy.size() - 1; i >= 0; i--)
		{
			parent = new SimpleItemTreeNode(
					child == null, hierarchy.get(i).getParentUnitId(),
					hierarchy.get(i).getParentUnitName(), "unit");
			if (child != null)
				parent.addChild(child.getId().toString(), child);
			child = parent;
		}
		roots.addChild(parent.getId().toString(), parent);
	}

	@Override
	protected String getAdaptorId() 
	{
		return null;
	}

}
