package ru.insoft.sovdoc.card.complex;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.view.VComplexSeries;
import ru.insoft.sovdoc.system.SystemSelectItem;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@ViewScoped
@ManagedBean
public class SeriesCard extends ComplexCard {

  @Inject
  SystemSelectItem ssi;

  private ArchFund fundModel;
  private ArchSeries seriesModel;
  private VComplexSeries seriesImmModel;
  private YearIntervalEmbeddedDataList yearIntervalEmbDL;
  private DescriptorEmbeddedDataList descriptorEmbDL;

  public ArchFund getFundModel() {
    if (fundModel == null) {
      fundModel = getParentFund();
    }

    return fundModel;
  }

  public ArchSeries getSeriesModel() {
    if (seriesModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        seriesModel = new ArchSeries();
      } else {
        seriesModel = complexQuery.querySeriesModel(model.getUnivDataUnitId());
        journalHelper.copySeriesModel(seriesModel);
        journalHelper.copySeriesImmModel(getSeriesImmModel());
        if (action == null || !action.equals("save")) {
          em.detach(seriesModel);
        }
      }
    }
    return seriesModel;
  }

  public VComplexSeries getSeriesImmModel() {
    if (seriesImmModel == null) {
      model = getModel();
      if (model.getUnivDataUnitId() == null) {
        seriesImmModel = new VComplexSeries();
      } else {
        seriesImmModel = complexQuery.querySeriesImmModel(model.getUnivDataUnitId());
      }
    }
    return seriesImmModel;
  }

  @Override
  protected Long getUnitTypeId() {
    return se.getImmDescValueByCodes("UNIV_UNIT_TYPE", "SERIES").getDescriptorValueId();
  }

  @Override
  protected MENU_PAGES getMenuPage() {
    return MENU_PAGES.SERIES;
  }

  public YearIntervalEmbeddedDataList getYearIntervalEmbDL() {
    if (yearIntervalEmbDL == null) {
      yearIntervalEmbDL = new YearIntervalEmbeddedDataList() {
        @Override
        protected ComplexCard getCard() {
          return SeriesCard.this;
        }
      };
    }
    return yearIntervalEmbDL;
  }

  public DescriptorEmbeddedDataList getDescriptorEmbDL() {
    if (descriptorEmbDL == null) {
      descriptorEmbDL = new DescriptorEmbeddedDataList() {
        @Override
        protected ComplexCard getCard() {
          return SeriesCard.this;
        }
      };
    }
    return descriptorEmbDL;
  }

  @Override
  protected void saveDetails() {
    boolean isNew = false;
    if (seriesModel.getUnivDataUnitId() == null) {
      seriesModel.setUnivDataUnitId(model.getUnivDataUnitId());
      em.persist(seriesModel);
      isNew = true;
    }
    yearIntervalEmbDL.save();
//    em.flush();
  
    seriesModel = em.merge(seriesModel);
    model = em.merge(model);
   
    em.flush();
    em.refresh(seriesModel);
    em.refresh(model);
    
    immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
  
    seriesImmModel = complexQuery.querySeriesImmModel(model.getUnivDataUnitId());

    if (isNew) {
      journalHelper.recordFullUnivDataUnit(model, "INPUT", cardLinks , null);
    } else {
      journalHelper.recordEditSeries(model, immModel, seriesModel, seriesImmModel, cardLinks);
    }
  } 

  @Override
  protected boolean validate() {
    boolean res = true;
    if (immModel.getFundNum() == null) {
      res &= MessageUtils.ErrorMessage("Номер фонда должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getFundLetter()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера фонда слишком длинная");
    }
    if (StringUtils.getByteLengthUTF8(immModel.getFundPrefix()) > 4) {
      res &= MessageUtils.ErrorMessage("Префикс фонда слишком длинный");
    }
    if (model.getNumberNumber() == null) {
      res &= MessageUtils.ErrorMessage("Номер описи должен быть заполнен");
    }
    if (StringUtils.getByteLengthUTF8(model.getNumberText()) > 250) {
      res &= MessageUtils.ErrorMessage("Литера описи слишком длинная");
    }
    if (seriesModel.getDocumentKindId() == null) {
      res &= MessageUtils.ErrorMessage("Вид документов должен быть указан");
    }
    if (seriesModel.getSeriesName() == null) {
      res &= MessageUtils.ErrorMessage("Название описи должно быть задано");
    }
    if (StringUtils.getByteLengthUTF8(seriesModel.getSeriesName()) > 1000) {
      res &= MessageUtils.ErrorMessage("Название описи слишком длинное");
    }
    if (StringUtils.getByteLengthUTF8(seriesModel.getNotes()) > 4000) {
      res &= MessageUtils.ErrorMessage("Примечание слишком длинное");
    }
    res &= getYearIntervalEmbDL().validate();

    if (!res) {
      em.detach(model);
      em.detach(seriesModel);
    }

    return res;
  }

  @Override
  protected String fillArchNumberCode() {
      String prefixFund = immModel.getFundPrefix();
      String letterFund = immModel.getFundLetter();
    return String.format("%1$s\u0001%2$08d\u0001%3$s\u0001%4$08d\u0001%5$s",
            prefixFund == null ? "" : prefixFund, immModel.getFundNum(), letterFund == null ? "" : letterFund,
            model.getNumberNumber(), model.getNumberText() == null ? "" : model.getNumberText());
  }

  @Override
  protected String fillUnitName() {
    return String.format("Опись %1$d%2$s. %3$s", model.getNumberNumber(),
            (model.getNumberText() == null ? "" : model.getNumberText()),
            seriesModel.getSeriesName());
  }

}
