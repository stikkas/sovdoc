package ru.insoft.sovdoc.card.adm;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.list.adm.GroupDataList;
import ru.insoft.sovdoc.list.adm.GroupUsersDataList;
import ru.insoft.sovdoc.list.adm.UserSearchCriteria;
import ru.insoft.sovdoc.model.adm.table.AdmGroup;

@RequestScoped
@Named
public class GroupCard {

	@Inject
	GroupDataList groupDL;
	@Inject
	EntityManager em;
	@Inject
	UserSearchCriteria userSearch;
	@Inject
	GroupUsersDataList groupUsersDL;
	
	private Integer index;
	
	@Inject
	@RequestParam(value = "groupId")
	private Long groupId;
	private String groupName;
	private String groupNote;
	
	public Integer getIndex() {
		return index;
	}
	
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	public Long getGroupId() {
		return groupId;
	}
	
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getGroupNote() {
		return groupNote;
	}
	
	public void setGroupNote(String groupNote) {
		this.groupNote = groupNote;
	}
	
	public void setCurrentGroup()
	{
		index   = groupDL.getRowIndex();
		if (groupDL.getRowId() != null)
		{
			AdmGroup group = groupDL.getCurrentRow();
			if (group != null)
			{
				groupId = group.getGroupId();
				groupName = group.getGroupName();
				groupNote = group.getGroupNote();
				
				userSearch.getCriteria().setGroupId(groupId);
				groupUsersDL.reload();
			}
		}
	}
	
	public void saveGroup()
	{
		if (!validateGroup())
			return;
		AdmGroup group = null;
		if (groupId == null)
			group = new AdmGroup();
		else
		{
			groupDL.setRowId(groupId.toString());
			group = groupDL.getCurrentRow();
		}
		
		group.setGroupName(groupName);
		group.setGroupNote(groupNote);
		
		try
		{
			if (groupId == null)
				em.persist(group);
			else
				em.merge(group);
			em.flush();
			groupId = group.getGroupId();
			groupDL.resetPageData();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null || msg.indexOf("AK_ADM_GROUP_NAME") == -1)
				e.printStackTrace();
			else
				MessageUtils.ErrorMessage("Группа с таким наименованием уже существует");
		}
	}
	
	public void selectGroup()
	{
		if (index == null && groupId != null)
			groupDL.setRowIndexByKey(groupId.toString());
		else
		{
			if (index != null && index < groupDL.getWrappedData().size())
				groupDL.setRowIndex(index);
			else
				groupDL.setRowIndex(null);
		}
	}
	
	protected boolean validateGroup()
	{
		boolean res = true;
		if (groupName == null)
			res &= MessageUtils.ErrorMessage("Наименование группы не может быть пустым");
		if (StringUtils.getByteLengthUTF8(groupName) > 250)
			res &= MessageUtils.ErrorMessage("Наименование группы слишком длинное");
		if (StringUtils.getByteLengthUTF8(groupNote) > 1000)
			res &= MessageUtils.ErrorMessage("Описание группы слишком длинное");
		return res;
	}
}
