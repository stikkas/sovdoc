package ru.insoft.sovdoc.card.adm;

import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.solder.servlet.http.RequestParam;
import org.picketlink.idm.impl.api.PasswordCredential;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.constant.USER_STATUS_TYPE;
import ru.insoft.sovdoc.model.adm.table.AdmEmployee;
import ru.insoft.sovdoc.model.adm.table.AdmEmployee_;
import ru.insoft.sovdoc.model.adm.table.AdmUser;
import ru.insoft.sovdoc.model.adm.table.AdmUser_;
import ru.insoft.sovdoc.model.adm.view.VAdmEmployee;
import ru.insoft.sovdoc.model.adm.view.VAdmEmployee_;
import ru.insoft.sovdoc.model.adm.view.VAdmUser;
import ru.insoft.sovdoc.model.adm.view.VAdmUserGroup;
import ru.insoft.sovdoc.model.adm.view.VAdmUser_;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

@RequestScoped
@Named
public class UserCard {

	@Inject
	EntityManager em;
	@Inject
	ValueSelectDataListGeneral valueSelectDL;
	@Inject
	SystemEntity se;
	
	@Inject
	@RequestParam(value = "mode")
	private String mode;
	@Inject
	@RequestParam(value = "userId")
	private Long userId;
	
	private AdmUser model;
	private VAdmUser immModel;
	private String password1;
	private String password2;
	private AdmEmployee empModel;
	private VAdmEmployee empImmModel;
	
	public AdmUser getModel()
	{
		if (model == null)
		{
			if (userId == null)
				model = new AdmUser();
			else
				model = queryModel(userId);
		}
		return model;
	}
	
	public VAdmUser getImmModel()
	{
		if (immModel == null)
		{
			if (userId == null)
			{
				immModel = new VAdmUser();
				immModel.setUserStatus(USER_STATUS_TYPE.valueOf("TYPE_FALSE").getValue());
				immModel.setGroups(new ArrayList<VAdmUserGroup>());
			}
			else
				immModel = queryImmModel(userId);
		}
		return immModel;
	}
	
	public boolean isEditing()
	{
		return mode != null;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	
	public AdmEmployee getEmpModel()
	{
		if (empModel == null)
		{
			empImmModel = getEmpImmModel();
			if (empImmModel.getEmployeeId() == null)
			{
				empModel = new AdmEmployee();
				empModel.setUser(model);
			}
			else
				empModel = queryEmpModel(empImmModel.getEmployeeId());
		}
		return empModel;
	}
	
	public VAdmEmployee getEmpImmModel()
	{
		if (empImmModel == null)
		{
			model = getModel();
			if (model.getUserId() == null)
				empImmModel = new VAdmEmployee();
			else
				empImmModel = queryEmpImmModel(model.getUserId());
		}
		return empImmModel;
	}
	
	public void selectDepartment()
	{
		empModel = getEmpModel();
		empModel.setDepartmentId(valueSelectDL.getSelectedValue());
		valueSelectDL.setValueSelected(true);
	}
	
	public String save()
	{
		if (!validate())
			return null;
		model.setLogin(model.getLogin().toUpperCase());
		if (password1 != null)
			model.setPassword(PasswordCredential.md5AsHexString(password1).toUpperCase());
		String userTypeCode = se.getDescValue(model.getUserTypeId()).getValueCode();
		if (userTypeCode.equals("EMPLOYEE"))
			model.setDisplayedName(empModel.getLastName() + " " + 
					empModel.getFirstName().substring(0, 1) + "." +
					(empModel.getMiddleName() != null ? " " + empModel.getMiddleName().substring(0, 1) + "." : ""));
		
		try
		{
			if (model.getUserId() == null)
				em.persist(model);
			else
				em.merge(model);
			em.flush();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null || msg.indexOf("AK_ADM_LOGIN") == -1)
				e.printStackTrace();
			else
				MessageUtils.ErrorMessage("Пользователь с таким логином уже существует");
			return null;
		}
		
		if (userTypeCode.equals("EMPLOYEE"))
		{
			if (empModel.getEmployeeId() == null)
			{
				empModel.setUser(model);
				em.persist(empModel);
			}
			else
				em.merge(empModel);
		}
		em.flush();
		
		return MENU_PAGES.USER.getId() + "?faces-redirect=true&amp;userId=" +
			model.getUserId() + "&amp;mode=edit&amp;saved=true";
	}
	
	protected boolean validate()
	{
		boolean res = true;
		if (model.getLogin() == null)
			res &= MessageUtils.ErrorMessage("Логин пользователя должен быть заполнен");
		else
		{
			if (!model.getLogin().matches("^[A-Za-z0-9_-]+$"))
				res &= MessageUtils.ErrorMessage("Логин содержит недопустимые символы");
			if (StringUtils.getByteLengthUTF8(model.getLogin()) > 30)
				res &= MessageUtils.ErrorMessage("Логин пользователя слишком длинный");
		}
		if (model.getPassword() == null && password1 == null)
			res &= MessageUtils.ErrorMessage("Пароль не может быть пустым");
		if (password1 != null && !password1.equals(password2))
			res &= MessageUtils.ErrorMessage("Пароль и подтверждение не совпадают");
		
		if (model.getUserTypeId() == null)
			res &= MessageUtils.ErrorMessage("Тип пользователя должен быть указан");
		else
		{
			String userTypeCode = se.getDescValue(model.getUserTypeId()).getValueCode();
			if (userTypeCode.equals("TECH_USER") &&	model.getDisplayedName() == null)
				res &= MessageUtils.ErrorMessage("Имя пользователя должно быть заполнено");
			if (userTypeCode.equals("EMPLOYEE"))
			{
				if (empModel.getDepartmentId() == null)
					res &= MessageUtils.ErrorMessage("Структурное подразделение должно быть задано");
				if (empModel.getPositionId() == null)
					res &= MessageUtils.ErrorMessage("Должность должна быть указана");
				if (empModel.getLastName() == null)
					res &= MessageUtils.ErrorMessage("Фамилия должна быть указана");
				if (StringUtils.getByteLengthUTF8(empModel.getLastName()) > 150)
					res &= MessageUtils.ErrorMessage("Фамилия слишком длинная");
				if (empModel.getFirstName() == null)
					res &= MessageUtils.ErrorMessage("Имя должно быть указано");
				if (StringUtils.getByteLengthUTF8(empModel.getFirstName()) > 150)
					res &= MessageUtils.ErrorMessage("Имя слишком длинное");
				if (StringUtils.getByteLengthUTF8(empModel.getMiddleName()) > 150)
					res &= MessageUtils.ErrorMessage("Отчество слишком длинное");
			}
		}
		
		return res;
	}
	
	private AdmUser queryModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUser> cq = cb.createQuery(AdmUser.class);
		Root<AdmUser> root = cq.from(AdmUser.class);
		cq.select(root).where(cb.equal(root.get(AdmUser_.userId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	private VAdmUser queryImmModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VAdmUser> cq = cb.createQuery(VAdmUser.class);
		Root<VAdmUser> root = cq.from(VAdmUser.class);
		cq.select(root).where(cb.equal(root.get(VAdmUser_.userId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	private AdmEmployee queryEmpModel(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmEmployee> cq = cb.createQuery(AdmEmployee.class);
		Root<AdmEmployee> root = cq.from(AdmEmployee.class);
		cq.select(root).where(cb.equal(root.get(AdmEmployee_.employeeId), id));
		return em.createQuery(cq).getSingleResult();
	}
	
	private VAdmEmployee queryEmpImmModel(Long userId)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VAdmEmployee> cq = cb.createQuery(VAdmEmployee.class);
		Root<VAdmEmployee> root = cq.from(VAdmEmployee.class);
		cq.select(root).where(cb.equal(root.get(VAdmEmployee_.userId), userId));
		return em.createQuery(cq).getSingleResult();
	}
}
