package ru.insoft.sovdoc.card.adm;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.model.adm.table.AdmUserGroup;
import ru.insoft.sovdoc.model.adm.table.AdmUserGroup_;
import ru.insoft.sovdoc.model.adm.view.VAdmUserGroup;
import ru.insoft.sovdoc.ui.adm.GroupSelect;

@RequestScoped
@Named("userGroupEmbDL")
public class UserGroupEmbeddedDataList extends EmbeddedDataList<VAdmUserGroup> {

	@Inject
	UserCard card;
	@Inject
	EntityManager em;
	@Inject
	GroupSelect groupSelect;
	
	@Override
	public List<VAdmUserGroup> getWrappedData()
	{
		if (super.getWrappedData() == null && card.getImmModel() != null)
			setWrappedData(card.getImmModel().getGroups());
		
		return super.getWrappedData();
	}
	
	public void selectGroup()
	{
		if (groupSelect.getGroupId() == null)
			return;
		
		AdmUserGroup userGroup = new AdmUserGroup();
		userGroup.setGroupId(groupSelect.getGroupId());
		userGroup.setUserId(card.getModel().getUserId());
		em.persist(userGroup);
		try
		{
			em.flush();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null || msg.indexOf("PK_ADM_USER_GROUP") == -1)
				e.printStackTrace();
			else
				MessageUtils.ErrorMessage("Пользователь уже состоит в этой группе");
		}
	}
	
	public void removeGroup(VAdmUserGroup vUserGroup)
	{
		AdmUserGroup userGroup = queryUserGroup(
				vUserGroup.getUser().getUserId(), vUserGroup.getGroupId());
		em.remove(userGroup);
		em.flush();
		
		getWrappedData().remove(vUserGroup);
	}
	
	protected AdmUserGroup queryUserGroup(Long userId, Long groupId)
	{
		if (groupId == null || userId == null)
			return null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUserGroup> cq = cb.createQuery(AdmUserGroup.class);
		Root<AdmUserGroup> root = cq.from(AdmUserGroup.class);
		cq.select(root).where(cb.and(
				cb.equal(root.get(AdmUserGroup_.groupId), groupId),
				cb.equal(root.get(AdmUserGroup_.userId), userId)));
		return em.createQuery(cq).getSingleResult();
	}
}
