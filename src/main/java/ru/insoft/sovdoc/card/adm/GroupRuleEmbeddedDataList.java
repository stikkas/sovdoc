package ru.insoft.sovdoc.card.adm;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.list.adm.GroupDataList;
import ru.insoft.sovdoc.model.adm.table.AdmGroupRule;
import ru.insoft.sovdoc.model.adm.table.AdmGroupRule_;
import ru.insoft.sovdoc.model.adm.view.VAdmGroupRule;
import ru.insoft.sovdoc.ui.adm.RuleSelect;

@RequestScoped
@Named("groupRuleEmbDL")
public class GroupRuleEmbeddedDataList extends EmbeddedDataList<VAdmGroupRule> {

	@Inject
	GroupDataList groupDL;
	@Inject
	GroupCard card;
	@Inject
	RuleSelect ruleSelect;
	@Inject
	EntityManager em;
	
	@Override
	public List<VAdmGroupRule> getWrappedData()
	{
		if (super.getWrappedData() == null && card.getGroupId() != null)
		{
			groupDL.setRowId(card.getGroupId().toString());
			setWrappedData(groupDL.getCurrentRow().getGroupRules());
		}
		
		return super.getWrappedData();
	}
	
	public void selectRule()
	{
		if (ruleSelect.getAccessRuleId() == null)
			return;
		
		groupDL.setRowId(card.getGroupId().toString());
		
		AdmGroupRule groupRule = new AdmGroupRule();
		groupRule.setGroupId(card.getGroupId());
		groupRule.setAccessRuleId(ruleSelect.getAccessRuleId());
		em.persist(groupRule);
		try
		{
			em.flush();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null || msg.indexOf("PK_ADM_GROUP_RULE") == -1)
				e.printStackTrace();
			else
				MessageUtils.ErrorMessage("Группа уже содержит это право доступа");
		}
	}
	
	public void removeValue(VAdmGroupRule vGroupRule)
	{
		AdmGroupRule groupRule = queryGroupRule(
				vGroupRule.getGroup().getGroupId(), vGroupRule.getAccessRuleId());
		em.remove(groupRule);
		em.flush();
		
		getWrappedData().remove(vGroupRule);
	}
	
	protected AdmGroupRule queryGroupRule(Long groupId, Long ruleId)
	{
		if (groupId == null || ruleId == null)
			return null;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmGroupRule> cq = cb.createQuery(AdmGroupRule.class);
		Root<AdmGroupRule> root = cq.from(AdmGroupRule.class);
		cq.select(root).where(cb.and(
				cb.equal(root.get(AdmGroupRule_.groupId), groupId),
				cb.equal(root.get(AdmGroupRule_.accessRuleId), ruleId)));
		return em.createQuery(cq).getSingleResult();
	}
}
