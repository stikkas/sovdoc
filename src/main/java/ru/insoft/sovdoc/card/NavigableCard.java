package ru.insoft.sovdoc.card;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: vasilev
 * Date: 27.05.13
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
public abstract class NavigableCard {

    private List<String> ids;
    private Integer cardIndex;

    @PostConstruct
    private void init()
    {
        String page = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("page");
        if (page != null)
        {
            ids = stringToList(page);
            cardIndex = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("index"));
        }
    }

    private List<String> stringToList(String str)
    {
        return Arrays.asList(str.substring(1, str.length() - 1).split(", "));
    }

    public String getSerializedIds() {
        return (ids == null ? null : ids.toString());
    }

    public void setSerializedIds(String ids) {
        this.ids = stringToList(ids);
    }

    public List<String> getIds()
    {
        return ids;
    }
    
    public Integer getIdsCount()
    {
    	if (ids != null)
    		return ids.size();
    	else
    		return 0;
    }

    public Integer getCardIndex() {
        return cardIndex;
    }

    public void setCardIndex(Integer cardIndex)
    {
        this.cardIndex = cardIndex;
    }

    public String getPageLabel()
    {
        return String.format("Страница %1$s из %2$s", getPageNum(), getTotalPages());
    }

    public Integer getPageNum() {
        return Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pageNum"));
    }

    public Integer getTotalPages() {
        return Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("totalPages"));
    }

    public Integer firstIndex()
    {
        return 0;
    }

    public Integer prevIndex()
    {
        if (cardIndex > 0)
            return (cardIndex - 1);
        return cardIndex;
    }

    public Integer nextIndex()
    {
        if (cardIndex < ids.size() - 1)
            return (cardIndex + 1);
        return cardIndex;
    }

    public Integer lastIndex()
    {
        return (ids.size() - 1);
    }
}
