package ru.insoft.sovdoc.ui.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import ru.insoft.commons.jsf.ui.panel.PanelMenuAbstract;
import ru.insoft.commons.jsf.ui.panel.PanelMenuItem;

import com.google.common.collect.ImmutableList;

@SuppressWarnings("serial")
@RequestScoped
@Named
public class WebSettingsTabMenu extends PanelMenuAbstract {

	List<PanelMenuItem> items;
	
	@SuppressWarnings("unused")
	@PostConstruct
	private void init()
	{
		PanelMenuItem pages = PanelMenuItem.build("pages")
			.setLabel("Статические страницы сайта")
			.setOutcome("/adm/settings/web/staticWebPageList.jsf")
			.setRendered(true)
			.setTitle("Статические страницы сайта");
		PanelMenuItem news = PanelMenuItem.build("news")
			.setLabel("Новости портала")
			.setOutcome("/adm/settings/web/newsList.jsf")
			.setRendered(true)
			.setTitle("Новости портала");
		
		items = ImmutableList.<PanelMenuItem>builder().add(pages, news).build();
	}
	
	@Override
	public List<PanelMenuItem> getItems() 
	{
		return items;
	}
}
