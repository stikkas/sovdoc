package ru.insoft.sovdoc.ui.core;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("rmUtils")
public class RichMenuUtils extends ru.insoft.commons.jsf.ui.RichMenuUtils<MENU_PAGES> {

}
