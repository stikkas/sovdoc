package ru.insoft.sovdoc.ui.core;

import javax.inject.Named;

import ru.insoft.commons.model.usertypes.ENUM_TYPE;

@Named("MENU_PAGES")
public enum MENU_PAGES implements ENUM_TYPE<String> {

	LOGIN_PAGE("/login.jsf"),
	MAIN_PAGE("/mainPage.jsf"),
	//Data resources
	COMPLEX_TREE("/data/complex/complexTree.jsf"),
	SECTION("/data/complex/section.jsf"),
	FUND("/data/complex/fund.jsf"),
	SERIES("/data/complex/series.jsf"),
	PAPER_FILE("/data/complex/paperFile.jsf"),
	PAPER_DOC("/data/complex/paperDoc.jsf"),
	PHONODOC("/data/complex/phonodoc.jsf"),
	KINODOC("/data/complex/kinodoc.jsf"),
	VIDEODOC("/data/complex/videodoc.jsf"),
	LIBRARY_SEARCH("/data/ebook/librarySearch.jsf"),
	EBOOK("/data/ebook/ebook.jsf"),
	JOURNAL("/data/journal/journalOfChange.jsf"),
	//Administration
	DESCRIPTORS("/adm/descriptors/descriptors.jsf"),
	DESC_GROUP("/adm/descriptors/descGroup.jsf"),
	DESC_VALUE("/adm/descriptors/descValue.jsf"),
	USERLIST("/adm/accessCtrl/userlist.jsf"),
	USER("/adm/accessCtrl/user.jsf"),
	GROUPS("/adm/accessCtrl/groups.jsf"),
	STATIC_WEB_PAGES("/adm/settings/web/staticWebPageList.jsf"),
	WEB_NEWS("/adm/settings/web/newsList.jsf"),
	PAGE_CONTENT("/adm/settings/web/pageContent.jsf");

	String page;

	private MENU_PAGES(String page) {
		this.page = page;
	}

	@Override
	public String getId() {
		return page;
	}
}
