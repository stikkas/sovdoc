package ru.insoft.sovdoc.ui.desc;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.datalist.CommonSearchCriteria;
import ru.insoft.sovdoc.constant.VALUE_DISPLAY_TYPE;
import ru.insoft.sovdoc.list.desc.UnivClssDataList;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.model.desc.view.VDescValuePath;
import ru.insoft.sovdoc.system.SystemEntity;


@RequestScoped
@Named
public class ValueSearchParam implements CommonSearchCriteria {

	@Inject
	@RequestParam(value = "p_groupId")
	private Long groupId;
	private VDescGroup group;
	@Inject
	@RequestParam(value = "actual")
	private String p_actual;
	@Inject
	@RequestParam(value = "lastPath")
	private String lastPath;
	@Inject
	@RequestParam(value = "lastNumber")
	private Integer lastNumber;
	@Inject
	@RequestParam(value = "valueId")
	private Long valueId;
	@Inject
	@RequestParam(value = "displayType")
	String p_displayType;
	
	@Inject
	EntityManager em;
	@Inject
	SystemEntity se;
        @Inject
        ValueSelectDataListGeneral valueSelectDL;
        @Inject
        UnivClssDataList univClssDL;
        
        private ValueSearchParamGeneral vspImpl;
	
	@PostConstruct
        private void init()
        {
            if (groupId != null || valueId != null)                
            {
                if (groupId != null)
                    group = __getGroup();
                else
                {
                    group   = se.getImmGroupByValueId(valueId);
                    groupId = group.getDescriptorGroupId();
                }
                
                if ("UNIV_CLASSIFIER".equals(group.getGroupCode()))
                {
                    vspImpl = new ValueSearchParamUnivClss();
                    vspImpl.dataList = univClssDL;
                }
            }
            if (vspImpl == null)
            {
                vspImpl = new ValueSearchParamGeneral();
                vspImpl.dataList = valueSelectDL;
            }
            
            vspImpl.groupId       = groupId;
            vspImpl.group         = group;
            vspImpl.p_actual      = p_actual;
            vspImpl.lastPath      = lastPath;
            vspImpl.lastNumber    = lastNumber;
            vspImpl.valueId       = valueId;
            vspImpl.p_displayType = p_displayType;
            vspImpl.em            = em;
            vspImpl.se            = se;
            
            vspImpl.init();
        }

	public Long getGroupId() 
        {
            return groupId;
	}

	public void setGroupId(Long groupId) 
        {
            this.groupId = groupId;
            if (groupId == null && vspImpl.groupId == null || groupId != null && groupId.equals(vspImpl.groupId))
                return;
            init();
	}
        
        private VDescGroup __getGroup()
        {
            if (group == null && groupId != null)
		group = se.getImmDescGroup(groupId);
            return group;
        }

	public VDescGroup getGroup() 
	{
            return vspImpl.getGroup();
	}

	public void setGroup(VDescGroup group) 
        {
            vspImpl.setGroup(group);
	}

	public Boolean getActual() 
        {
            return vspImpl.getActual();
	}

	public void setActual(Boolean actual) 
        {
            vspImpl.setActual(actual);
	}

	public VALUE_DISPLAY_TYPE getDisplayedType() 
        {
            return vspImpl.getDisplayedType();
	}

	public void setDisplayedType(VALUE_DISPLAY_TYPE displayedType) 
        {
            vspImpl.setDisplayedType(displayedType);
	}

	public String getSearchString() 
        {
            return vspImpl.getSearchString();
	}

	public void setSearchString(String searchString) 
        {
            vspImpl.setSearchString(searchString);
	}
	
	public Long getParentId() 
        {
            return vspImpl.getParentId();
	}

	public void setParentId(Long parentId) 
        {
            vspImpl.setParentId(parentId);
	}

	public String getLastPath() 
        {
            return vspImpl.getLastPath();
	}

	public void setLastPath(String lastPath) 
        {
            vspImpl.setLastPath(lastPath);
	}
	
	public Integer getLastNumber() 
        {
            return vspImpl.getLastNumber();
	}

	public void setLastNumber(Integer lastNumber) 
        {
            vspImpl.setLastNumber(lastNumber);
	}

	public Long getValueId() 
        {
            return vspImpl.getValueId();
	}

	public void setValueId(Long valueId) 
        {
            vspImpl.setValueId(valueId);
	}
	
	public void selectGroup()
	{
            vspImpl.selectGroup();
	}

	@Override
	public boolean validateCriteria()
	{
            return vspImpl.validateCriteria();
	}

	public List<VDescValue> searchValues(int limit)
	{
            return vspImpl.searchValues(limit);
	}
	
	public List<VDescValuePath> searchValuesByString(Integer pageNum, Integer pageSize)
	{
            return vspImpl.searchValuesByString(pageNum, pageSize);
	}
	
	public Integer getRowCountByString()
	{
            return vspImpl.getRowCountByString();
	}
	
	public Long searchNext()
	{
            return vspImpl.searchNext();
	}
        
        public void setDataListId(String rowId)
        {
            vspImpl.setDataListId(rowId);
        }
}
