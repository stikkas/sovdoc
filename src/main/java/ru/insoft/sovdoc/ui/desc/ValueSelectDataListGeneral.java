package ru.insoft.sovdoc.ui.desc;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import ru.insoft.commons.jsf.ui.datalist.CommonDataList;
import ru.insoft.commons.jsf.ui.datalist.CommonSearchCriteria;
import ru.insoft.commons.jsf.ui.datalist.Paginator;
import ru.insoft.sovdoc.model.desc.view.VDescValuePath;
import ru.insoft.sovdoc.system.SystemEntity;

@RequestScoped
@Named("descValueSelectDL")
public class ValueSelectDataListGeneral extends CommonDataList<VDescValuePath> 
    implements ValueSelectDataList
{

	@Inject
	ValueSearchParam valueSearch;
	@Inject
	Paginator paginator;
	@Inject
	EntityManager em;
	@Inject
	SystemEntity se;
	
	private boolean valueSelected;

        @Override
	public boolean isValueSelected() {
		return valueSelected;
	}

        @Override
	public void setValueSelected(boolean valueSelected) {
		this.valueSelected = valueSelected;
	}

	@Override
	public Paginator getPaginator() 
	{
		return paginator;
	}

	@Override
	public CommonSearchCriteria getSearchCriteria() 
	{
		return valueSearch;
	}

	@Override
	protected EntityManager getEntityManager() 
	{
		return em;
	}

	@Override
	public Integer queryRowCount() 
	{
		return valueSearch.getRowCountByString();
	}

	@Override
	public List<VDescValuePath> getWrappedData(Integer pageNum,
			Integer pageSize) 
	{
		if (wrappedData == null && isShowResult())
			wrappedData = valueSearch.searchValuesByString(pageNum, pageSize);
		return wrappedData;
	}
	
	@Override
	public String getDataLabel()
	{
		return null;
	}
	
        @Override
	public Long getSelectedValue()
	{
		if (getRowId() == null)
			return null;
		else
			return Long.valueOf(getRowId());
	}
}
