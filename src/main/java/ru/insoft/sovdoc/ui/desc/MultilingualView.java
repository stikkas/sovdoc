package ru.insoft.sovdoc.ui.desc;

public interface MultilingualView extends MultilingualTable {

	public Long getId();
	void setId(Long id);
	
	public String getValue();
	void setValue(String value);
	
	public String getLanguageName();
	void setLanguageName(String languageName);
	
	public Integer getSortOrder();
	void setSortOrder(Integer sortOrder);
}
