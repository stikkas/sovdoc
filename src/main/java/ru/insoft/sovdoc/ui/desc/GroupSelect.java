package ru.insoft.sovdoc.ui.desc;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescSubsystem;
import ru.insoft.sovdoc.system.SystemEntity;

@RequestScoped
@Named
public class GroupSelect {

	@Inject
	SystemEntity se;
	
	private Long subsystemNumber;
	private Long descGroupId;

	public Long getSubsystemNumber() {
		return subsystemNumber;
	}

	public void setSubsystemNumber(Long subsystemNumber) {
		this.subsystemNumber = subsystemNumber;
	}
	
	public Long getDescGroupId() {
		return descGroupId;
	}

	public void setDescGroupId(Long descGroupId) {
		this.descGroupId = descGroupId;
	}
	
	public List<SelectItem> getSubsystems()
	{
		List<SelectItem> siList = new ArrayList<SelectItem>();
		siList.add(new SelectItem(-1, ""));
		for (VDescSubsystem ss : se.getSubsystemModel())
			siList.add(new SelectItem(ss.getSubsystemNumber(), ss.getSubsystemName()));
		return siList;
	}

	public List<SelectItem> getDescGroups()
	{
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (VDescGroup grp : se.getDescGroups(subsystemNumber))
			siList.add(new SelectItem(grp.getDescriptorGroupId(), grp.getGroupName()));
		return SelectItemUtils.withEmpty(siList);
	}
}
