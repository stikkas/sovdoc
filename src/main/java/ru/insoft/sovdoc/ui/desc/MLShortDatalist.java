package ru.insoft.sovdoc.ui.desc;

import java.util.List;

import javax.persistence.EntityManager;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.desc.table.DescValueInternational;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.view.VDescMlShort;
import ru.insoft.sovdoc.system.SystemEntity;

public class MLShortDatalist extends EmbeddedDataList<VDescMlShort> implements
		MultilingualDataList {
	
	private SystemEntity se;
	private Long rootId;
	
	public MLShortDatalist(Long rootId)
	{
		this.rootId = rootId;
	}
	
	@Override
	public void initSystemEntity(SystemEntity se)
	{
		this.se = se;
	}
	
	@Override
	public List<VDescMlShort> getWrappedData() 
	{
		if (super.getWrappedData() == null)
			setWrappedData(se.getMultilingualData(rootId, VDescMlShort.class));
		return super.getWrappedData();
	}
	
	@Override
	public MultilingualView getCurrentRow() 
	{
		if (getWrappedData() != null && getRowIndex() != null)
			return getWrappedData().get(getRowIndex());
		return null;
	}

	@Override
	public List<DescValueInternational> getModifiableData() 
	{
		return se.getMultilingualData(rootId, DescValueInternational.class);
	}

	@Override
	public boolean validateValue(String value) 
	{
		if (value != null && StringUtils.getByteLengthUTF8(value) > 500)
			return MessageUtils.ErrorMessage("Значение слишком длинное");
		return true;
	}

	@Override
	public void merge(MultilingualTable obj, String value) 
	{
		DescValueInternational model = (DescValueInternational)obj;
		if (model.getInternationalShortValue().equals(value))
			return;
		EntityManager em = se.getEntityManager();
		model.setInternationalShortValue(value);
		em.merge(model);
		if (model.getLanguageCode().equals(se.getSystemParameterValue("LANGUAGE")))
		{
			DescriptorValue dv = se.getDescValue(rootId);
			dv.setShortValue(StringUtils.truncateUTF8(value, 250));
			em.merge(dv);
		}
		em.flush();
		em.clear();
	}

	@Override
	public void persist(String language, String value) 
	{
		EntityManager em = se.getEntityManager();
		DescValueInternational model = new DescValueInternational();
		model.setDescriptorValue(se.getDescValue(rootId));
		model.setInternationalFullValue(value);
		model.setInternationalShortValue(value);
		model.setLanguageCode(language);
		em.persist(model);
		em.flush();
	}

	@Override
	public void emptyWrappedData() 
	{
		setWrappedData(null);
	}

}
