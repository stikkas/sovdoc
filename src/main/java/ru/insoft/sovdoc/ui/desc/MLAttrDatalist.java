package ru.insoft.sovdoc.ui.desc;

import java.util.List;

import javax.persistence.EntityManager;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.desc.table.DescAttrInternational;
import ru.insoft.sovdoc.model.desc.table.DescriptorValueAttr;
import ru.insoft.sovdoc.model.desc.view.VDescMlAttr;
import ru.insoft.sovdoc.system.SystemEntity;

public class MLAttrDatalist extends EmbeddedDataList<VDescMlAttr> implements
		MultilingualDataList {
	
	private SystemEntity se;
	private Long rootId;
	
	public MLAttrDatalist(Long rootId)
	{
		this.rootId = rootId;
	}

	@Override
	public void initSystemEntity(SystemEntity se) 
	{
		this.se = se;
	}

	@Override
	public List<VDescMlAttr> getWrappedData() 
	{
		if (super.getWrappedData() == null)
			setWrappedData(se.getMultilingualData(rootId, VDescMlAttr.class));
		return super.getWrappedData();
	}

	@Override
	public MultilingualView getCurrentRow() 
	{
		if (getWrappedData() != null && getRowIndex() != null)
			return getWrappedData().get(getRowIndex());
		return null;
	}

	@Override
	public List<DescAttrInternational> getModifiableData() 
	{
		return se.getMultilingualData(rootId, DescAttrInternational.class);
	}

	@Override
	public boolean validateValue(String value) 
	{
		if (value != null && StringUtils.getByteLengthUTF8(value) > 4000)
			return MessageUtils.ErrorMessage("Значение слишком длинное");
		return true;
	}

	@Override
	public void merge(MultilingualTable obj, String value) 
	{
		DescAttrInternational model = (DescAttrInternational)obj;
		if (model.getAttrInternationalValue().equals(value))
			return;
		EntityManager em = se.getEntityManager();
		model.setAttrInternationalValue(value);
		em.merge(model);
		if (model.getLanguageCode().equals(se.getSystemParameterValue("LANGUAGE")))
		{
			DescriptorValueAttr dva = se.getDescValueAttr(rootId);
			dva.setAttrValue(value);
			em.merge(dva);
		}
		em.flush();
		em.clear();
	}

	@Override
	public void persist(String language, String value) 
	{
		EntityManager em = se.getEntityManager();
		DescAttrInternational model = new DescAttrInternational();
		model.setDescriptorValueAttr(se.getDescValueAttr(rootId));
		model.setAttrInternationalValue(value);
		model.setLanguageCode(language);
		em.persist(model);
		if (language.equals(se.getSystemParameterValue("LANGUAGE")))
		{
			DescriptorValueAttr dva = se.getDescValueAttr(rootId);
			dva.setAttrValue(value);
			em.merge(dva);
		}
		em.flush();
	}

	@Override
	public void emptyWrappedData() 
	{
		setWrappedData(null);
	}

}
