package ru.insoft.sovdoc.ui.desc;

import java.util.List;
import ru.insoft.commons.jsf.ui.datalist.IndexedDataList;
import ru.insoft.sovdoc.system.SystemEntity;

public interface MultilingualDataList extends IndexedDataList {

	public void initSystemEntity(SystemEntity se);
	public List<? extends MultilingualView> getWrappedData();
	public MultilingualView getCurrentRow();
	public List<? extends MultilingualTable> getModifiableData();
	public boolean validateValue(String value);
	public void merge(MultilingualTable obj, String value);
	public void persist(String language, String value);
	public void emptyWrappedData();
}
