package ru.insoft.sovdoc.ui.desc;

/**
 *
 * @author melnikov
 */
public interface ValueSelectDataList 
{
    public Long getSelectedValue();
    public boolean isValueSelected();
    public void setValueSelected(boolean valueSelected);
}
