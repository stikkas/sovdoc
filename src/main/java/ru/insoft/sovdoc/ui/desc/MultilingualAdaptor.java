package ru.insoft.sovdoc.ui.desc;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.sovdoc.constant.MULTILINGUAL_VALUE_TYPE;
import ru.insoft.sovdoc.system.SystemEntity;

import com.google.common.collect.ImmutableMap;

@RequestScoped
@Named("multiAdaptor")
public class MultilingualAdaptor {

	@Inject
	@RequestParam(value = "multiType")
	private String p_multiType;
	private MULTILINGUAL_VALUE_TYPE multiType;
	
	private Map<MULTILINGUAL_VALUE_TYPE, Class<? extends MultilingualDataList>> classMap = 
		ImmutableMap.<MULTILINGUAL_VALUE_TYPE, Class<? extends MultilingualDataList>>builder()
		.put(MULTILINGUAL_VALUE_TYPE.TYPE_FULL, MLFullDatalist.class)
		.put(MULTILINGUAL_VALUE_TYPE.TYPE_SHORT, MLShortDatalist.class)
		.put(MULTILINGUAL_VALUE_TYPE.TYPE_ATTR, MLAttrDatalist.class)
		.build();
	
	@Inject
	@RequestParam(value = "rootId")
	private Long rootId;
	
	@Inject
	SystemEntity se;
	@Inject
	EntityManager em;
	
	private MultilingualDataList datalist;
	
	private String value;
	private String language;
	
	@PostConstruct
	protected void init() throws SecurityException, NoSuchMethodException, 
		IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException
	{
		if (p_multiType != null)
		{
			multiType = MULTILINGUAL_VALUE_TYPE.valueOf(p_multiType);
			Constructor<? extends MultilingualDataList> c = 
				classMap.get(multiType).getConstructor(new Class[]{Long.class});
			datalist = (MultilingualDataList)c.newInstance(rootId);
			datalist.initSystemEntity(se);
		}
	}
	
	public MultilingualDataList getDatalist()
	{
		return datalist;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLanguage() 
	{
		if (language == null)
			language = se.getSystemParameterValue("LANGUAGE");
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void saveValue()
	{
		if (!datalist.validateValue(value))
			return;
		List<? extends MultilingualTable> mTableList = datalist.getModifiableData();
		for (MultilingualTable mTable : mTableList)
			if (mTable.getLanguageCode().equals(language))
			{
				if (value == null || value.isEmpty())
				{
					if (language.equals(se.getSystemParameterValue("LANGUAGE")))
					{
						MessageUtils.ErrorMessage("Невозможно удалить значение на основном языке системы");
						return;
					}
					em.remove(mTable);
					em.flush();
				}
				else
					datalist.merge(mTable, value);
				
				datalist.emptyWrappedData();
				return;
			}
		datalist.persist(language, value);
		datalist.emptyWrappedData();
	}
	
	public Integer getModifValueIndex()
	{
		for (int i = 0; i < datalist.getWrappedData().size(); i++)
			if (datalist.getWrappedData().get(i).getLanguageCode().equals(language))
				return i;
		return 0;
	}
	
	public void setCurrentValue()
	{
		MultilingualView mView = datalist.getCurrentRow();
		value = mView.getValue();
		language = mView.getLanguageCode();
	}
	
	public void removeValue(String language)
	{
		if (language.equals(se.getSystemParameterValue("LANGUAGE")))
		{
			MessageUtils.ErrorMessage("Невозможно удалить значение на основном языке системы");
			return;
		}
		List<? extends MultilingualTable> mTableList = datalist.getModifiableData();
		for (MultilingualTable mTable : mTableList)
			if (mTable.getLanguageCode().equals(language))
			{
				em.remove(mTable);
				em.flush();
				datalist.emptyWrappedData();
				return;
			}
	}
}
