package ru.insoft.sovdoc.ui.desc;

import com.google.common.collect.Lists;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.CommonDataList;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.constant.VALUE_DISPLAY_TYPE;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.model.desc.view.VDescValuePath;
import ru.insoft.sovdoc.model.desc.view.VDescValuePath_;
import ru.insoft.sovdoc.model.desc.view.VDescValue_;
import ru.insoft.sovdoc.system.SystemEntity;

/**
 *
 * @author melnikov
 */
public class ValueSearchParamGeneral 
{
    protected Long groupId;
    protected VDescGroup group;
    protected String p_actual;
    protected boolean actual = true;
    protected VALUE_DISPLAY_TYPE displayedType;
    protected String searchString;
    protected Long parentId;
    protected String lastPath;
    protected Integer lastNumber;
    protected Long valueId;
    protected String p_displayType;
    
    protected EntityManager em;
    protected SystemEntity se;
    protected CommonDataList dataList;
    
    protected void init()
    {
	if (p_actual != null && p_actual.equals("false") ||
            group != null && !group.isHistorySupported())
            actual = false;
	if (p_displayType != null && p_displayType.equals("SHORT"))
            displayedType = VALUE_DISPLAY_TYPE.SHORT;
	else
            displayedType = VALUE_DISPLAY_TYPE.FULL;
	if (lastNumber == null)
            lastNumber = 0;
    }

    public VDescGroup getGroup() 
    {
	if (group == null && groupId != null)
            group = se.getImmDescGroup(groupId);
	return group;
    }

    public void setGroup(VDescGroup group) 
    {
	this.group = group;
    }

    public Boolean getActual() 
    {
	return actual;
    }

    public void setActual(Boolean actual) 
    {
	this.actual = actual;
    }

    public VALUE_DISPLAY_TYPE getDisplayedType() 
    {
	return displayedType;
    }

    public void setDisplayedType(VALUE_DISPLAY_TYPE displayedType) 
    {
    	this.displayedType = displayedType;
    }

    public String getSearchString() 
    {
	return searchString;
    }

    public void setSearchString(String searchString) 
    {
	if (searchString == null || searchString.trim().equals("")) 
            this.searchString = null;
        else 
            this.searchString = searchString;  
    }
	
    public Long getParentId() 
    {
	return parentId;
    }

    public void setParentId(Long parentId) 
    {
    	this.parentId = parentId;
    }

    public String getLastPath() 
    {
	return lastPath;
    }

    public void setLastPath(String lastPath) 
    {
	this.lastPath = lastPath;
    }
	
    public Integer getLastNumber() 
    {
	return lastNumber;
    }

    public void setLastNumber(Integer lastNumber) 
    {
	this.lastNumber = lastNumber;
    }

    public Long getValueId() 
    {
	return valueId;
    }

    public void setValueId(Long valueId) 
    {
	this.valueId = valueId;
    }
	
    public void selectGroup()
    {
	group = se.getImmDescGroup(groupId);
    }

    public boolean validateCriteria()
    {
	boolean res = true;
	if (groupId == null)
            res &= MessageUtils.ErrorMessage("Группа указателей должна быть выбрана");
			
	return res;
    }

    public List<VDescValue> searchValues(int limit)
    {
	if (groupId == null && parentId == null)
            return null;
		
	CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<VDescValue> cq = cb.createQuery(VDescValue.class);
	Root<VDescValue> root = cq.from(VDescValue.class);
		
	List<Predicate> predicates = Lists.newArrayList();
	if (groupId != null)
            predicates.add(cb.equal(root.get(VDescValue_.descriptorGroupId), groupId));
	if (actual)
            predicates.add(cb.equal(root.get(VDescValue_.isActual), true));
	if (parentId != null)
            predicates.add(cb.equal(root.get(VDescValue_.parentValueId), parentId));
	else
            predicates.add(cb.isNull(root.get(VDescValue_.parentValueId)));
	Predicate p = cb.and(predicates.toArray(new Predicate[0]));
		
        if (getGroup().isAlphabeticSort())
	{
            if (displayedType.equals(VALUE_DISPLAY_TYPE.SHORT))
		cq.orderBy(cb.asc(root.get(VDescValue_.shortValue)));
            else
		cq.orderBy(cb.asc(root.get(VDescValue_.fullValue)));
	}
	else
            cq.orderBy(cb.asc(root.get(VDescValue_.sortOrder)));
		
	cq.select(root).where(p);                
	return em.createQuery(cq).setMaxResults(limit).getResultList();
    }
	
    protected Predicate getPredicateByCriteria(CriteriaBuilder cb, Root<VDescValuePath> root)
    {
	List<Predicate> predicates = Lists.newArrayList();
	if (groupId != null)
            predicates.add(cb.equal(root.get(VDescValuePath_.descriptorGroupId), groupId));
	if (actual)
            predicates.add(cb.equal(root.get(VDescValuePath_.isActual), true));
	if (searchString != null)
	{
            if (displayedType.equals(VALUE_DISPLAY_TYPE.SHORT))
		predicates.add(cb.greaterThan(cb.function("contains", Integer.class, 
				root.get(VDescValuePath_.shortValue),
				cb.literal(StringUtils.textRequest(searchString))), 0));
            if (displayedType.equals(VALUE_DISPLAY_TYPE.FULL))
		predicates.add(cb.greaterThan(cb.function("contains", Integer.class, 
				root.get(VDescValuePath_.fullValue),
				cb.literal(StringUtils.textRequest(searchString))), 0));
	}
	return cb.and(predicates.toArray(new Predicate[0]));
    }
	
    public List<VDescValuePath> searchValuesByString(Integer pageNum, Integer pageSize)
    {
	CriteriaBuilder cb = em.getCriteriaBuilder();
	CriteriaQuery<VDescValuePath> cq = cb.createQuery(VDescValuePath.class);
	Root<VDescValuePath> root = cq.from(VDescValuePath.class);
	cq.select(root).where(getPredicateByCriteria(cb, root));
		
	if (group.isAlphabeticSort())
	{
            Path<String> rnColumn = null;
            if (displayedType.equals(VALUE_DISPLAY_TYPE.SHORT))
		rnColumn = root.get(VDescValuePath_.shortPath);
            if (displayedType.equals(VALUE_DISPLAY_TYPE.FULL))
		rnColumn = root.get(VDescValuePath_.fullPath);
            cq.orderBy(cb.asc(cb.function("DESCRIPTOR_PACK.SORT_BY_PATH", String.class, 
			cb.upper(rnColumn), cb.literal(lastPath == null ? "" : lastPath.toUpperCase()))), 
			cb.asc(cb.upper(rnColumn)));
        }
	else
            cq.orderBy(cb.asc(cb.function("DESCRIPTOR_PACK.SORT_BY_ROWNUM", Integer.class, 
			cb.function("DESCRIPTOR_PACK.GET_ORDER_ROWNUM", Integer.class, 
					root.get(VDescValuePath_.descriptorValueId)),
					cb.literal(lastNumber))),
					cb.asc(cb.function("DESCRIPTOR_PACK.GET_ORDER_ROWNUM", 
							Integer.class, root.get(VDescValuePath_.descriptorValueId))));
        if (pageNum != null && pageSize != null)
            return em.createQuery(cq).setFirstResult((pageNum - 1) * pageSize)
		.setMaxResults(pageSize).getResultList();
        else
            return em.createQuery(cq).getResultList();
    }
	
    public Integer getRowCountByString()
    {
	CriteriaBuilder cb = em.getCriteriaBuilder();
	CriteriaQuery<Long> cq = cb.createQuery(Long.class);
	Root<VDescValuePath> root = cq.from(VDescValuePath.class);
	cq.select(cb.count(root)).where(getPredicateByCriteria(cb, root));
	return em.createQuery(cq).getSingleResult().intValue();
    }
	
    public Long searchNext()
    {
	if (searchString == null || groupId == null)
            return null;
	List<VDescValuePath> searchResults = searchValuesByString(1, 1);
	if (searchResults == null || searchResults.size() == 0)
            return null;
	if (group.isAlphabeticSort())
	{
            if (displayedType.equals(VALUE_DISPLAY_TYPE.SHORT))
		setLastPath(searchResults.get(0).getShortPath());
            if (displayedType.equals(VALUE_DISPLAY_TYPE.FULL))
		setLastPath(searchResults.get(0).getFullPath());
	}
	else
            setLastNumber(se.getOrderRowNum(searchResults.get(0).getDescriptorValueId()));
	return searchResults.get(0).getDescriptorValueId();
    }
    
    public void setDataListId(String rowId)
    {
        dataList.setRowId(rowId);
    }
}
