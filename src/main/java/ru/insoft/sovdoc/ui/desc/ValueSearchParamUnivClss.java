package ru.insoft.sovdoc.ui.desc;

import com.google.common.collect.Lists;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss_;

/**
 *
 * @author melnikov
 */
public class ValueSearchParamUnivClss extends ValueSearchParamGeneral
{
    @Override
    public List<VDescValue> searchValues(int limit)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<VDescValue> cq = cb.createQuery(VDescValue.class);
        Root<VDescValueUnivclss> root = cq.from(VDescValueUnivclss.class);
        
        List<Predicate> predicates = Lists.newArrayList();
        if (actual)
            predicates.add(cb.equal(root.get(VDescValueUnivclss_.isActual), true));
        if (parentId != null)
            predicates.add(cb.equal(root.get(VDescValueUnivclss_.parentValueId), parentId));
        else
            predicates.add(cb.isNull(root.get(VDescValueUnivclss_.parentValueId)));
        Predicate p = cb.and(predicates.toArray(new Predicate[0]));
        
        cq.orderBy(cb.asc(root.get(VDescValueUnivclss_.valueIndex)));
        cq.multiselect(root).where(p);
        return em.createQuery(cq).setMaxResults(limit).getResultList();
    }    
}
