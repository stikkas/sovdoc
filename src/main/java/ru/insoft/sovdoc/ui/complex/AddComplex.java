package ru.insoft.sovdoc.ui.complex;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexUnitHierarchy;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.view.VDescAttrvalueWithCode;
import ru.insoft.sovdoc.model.desc.view.VDescValueWithCode;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.system.SystemSelectItem;
import ru.insoft.sovdoc.system.complex.SystemQuery;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

import com.google.common.collect.Lists;

@RequestScoped
@Named
public class AddComplex {

	@Inject
	SystemEntity se;
	@Inject
	SystemSelectItem ssi;
	@Inject
	SystemQuery complexQuery;
	
	@Inject
	@RequestParam(value = "parentId")
	Long parentId;
	
	@Inject
	@RequestParam(value = "parentType")
	String parentType;
	
	@Inject
	@RequestParam(value = "descLevel")
	Long descLevelId;
	
	private String unitType;
	private List<VComplexUnitHierarchy> hierarchy;
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getDescLevelId() {
		return descLevelId;
	}

	public void setDescLevelId(Long descLevelId) {
		this.descLevelId = descLevelId;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	
	public String getArchName()
	{
		if (parentId == null || parentType == null)
			return null;
		
		if (parentType.equals("archive"))
			return se.getDescValue(parentId).getFullValue();
		else
		{
			Long archiveId = complexQuery.queryImmModel(parentId).getArchiveId();
			return se.getDescValue(archiveId).getFullValue();
		}
	}
	
	public List<VComplexUnitHierarchy> getHierarchy()
	{
		if (hierarchy == null && parentType != null && !parentType.equals("archive"))
		{
			hierarchy = complexQuery.queryHierarchy(parentId);
		}
		return hierarchy;
	}

	public boolean hasDescriptionLevel(String levelCode)
	{
		if (getHierarchy() != null)
			for (VComplexUnitHierarchy level : hierarchy)
				if (level.getParentType().getValueCode().equals(levelCode))
					return true;
		return false;
	}
	
	public String getUnitNamesByLevel(String levelCode)
	{
		String res = null;
		if (getHierarchy() != null)
			for (VComplexUnitHierarchy level : hierarchy)
				if (level.getParentType().getValueCode().equals(levelCode))
				{
					if (res == null)
						res = level.getParentUnitName();
					else
						res += "/" + level.getParentUnitName();
				}
		return res;
	}
	
	public List<SelectItem> getDescLevelsSI()
	{
		if (parentType == null)
			return null;
		
		if (parentType.equals("archive"))
			return ssi.getDescValuesByCode("DESC_LEVEL");
		else
		{
			UnivDataUnit unit = complexQuery.queryModel(parentId);
			return ssi.getDescAttrValuesByList(
					se.getAttrValuesByCode(unit.getDescriptionLevelId(), "CHILD_DESC_LEVELS"));
		}
	}
	
	public List<SelectItem> getUnitTypesSI()
	{
		if (descLevelId == null)
		{
			List<SelectItem> descLevelsSI = getDescLevelsSI();
			if (descLevelsSI != null && descLevelsSI.size() > 0)
				descLevelId = (Long)(getDescLevelsSI().get(0).getValue());
			else
				return null;
		}
		
		List<SelectItem> siList = Lists.newArrayList();
		for (VDescAttrvalueWithCode unitType : se.getAttrValuesByCode(descLevelId, "UNIT_TYPES"))
		{
			DescriptorValue val = se.getDescValue(unitType.getRefDescriptorValueId());
			siList.add(new SelectItem(val.getValueCode(), val.getFullValue()));
		}
		return siList;
	}
	
	public String redirectNewCard()
	{
		if (parentType == null || unitType == null)
			return null;
		
		Long owner = null;
		Long parentId = null;
		if (parentType.equals("archive"))
			owner = this.parentId;
		else
		{
			owner = complexQuery.queryModel(this.parentId).getResOwnerId();
			parentId = this.parentId;
		}
			
		String res = getPageName(unitType) + "?faces-redirect=true&amp;mode=add";
		if (owner != null)
			res += "&amp;owner=" + owner.toString();
		if (parentId != null)
			res += "&amp;parentId=" + parentId.toString();
		if (descLevelId != null)
			res += "&amp;descLevel=" + descLevelId.toString();
		
		return res;
	}
	
	public String getPageName(String unitType)
	{
		return MENU_PAGES.valueOf(unitType).getId();
	}
}
