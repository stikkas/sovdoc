package ru.insoft.sovdoc.ui.complex;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.panel.PanelMenuAbstract;
import ru.insoft.commons.jsf.ui.panel.PanelMenuItem;
import ru.insoft.sovdoc.list.complex.ComplexTree;

import com.google.common.collect.ImmutableList;

@SuppressWarnings("serial")
@RequestScoped
@Named
public class ComplexTabMenu extends PanelMenuAbstract {

	List<PanelMenuItem> items;
	
	@Inject
	@RequestParam(value = "unitId")
	Long unitId;
	
	@PostConstruct
	private void init()
	{
		PanelMenuItem tree = PanelMenuItem.build("tree")
			.setLabel("Структура документальных комплексов")
			.setOutcome("/data/complex/complexTree.jsf")
			.setRendered(true)
			.setTitle("Структура документальных комплексов");		
		PanelMenuItem search = PanelMenuItem.build("search")
			.setLabel("Поиск по документальным комплексам")
			.setOutcome("/data/complex/complexSearch.jsf")
			.setRendered(true)
			.setTitle("Поиск по документальным комплексам");
		
		if (unitId != null)
		{
			tree.addParam("unitId", unitId.toString());
			search.addParam("unitId", unitId.toString());
		}
		
		items = ImmutableList.<PanelMenuItem>builder().add(tree, search).build();
	}
	
	@Override
	public List<PanelMenuItem> getItems() 
	{
		return items;
	}

}
