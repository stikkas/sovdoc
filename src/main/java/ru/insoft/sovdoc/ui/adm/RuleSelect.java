package ru.insoft.sovdoc.ui.adm;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import ru.insoft.commons.jsf.ui.SelectItemUtils;
import ru.insoft.sovdoc.model.adm.table.AdmAccessRule;
import ru.insoft.sovdoc.model.desc.view.VDescSubsystem;
import ru.insoft.sovdoc.system.SystemEntity;

@RequestScoped
@Named
public class RuleSelect {

	@Inject
	SystemEntity se;
	
	private Long subsystemNumber;
	private Long accessRuleId;

	public Long getSubsystemNumber() {
		return subsystemNumber;
	}

	public void setSubsystemNumber(Long subsystemNumber) {
		this.subsystemNumber = subsystemNumber;
	}
	
	public Long getAccessRuleId() {
		return accessRuleId;
	}

	public void setAccessRuleId(Long descGroupId) {
		this.accessRuleId = descGroupId;
	}
	
	public List<SelectItem> getSubsystems()
	{
		List<SelectItem> siList = new ArrayList<SelectItem>();
		siList.add(new SelectItem(-1, ""));
		for (VDescSubsystem ss : se.getSubsystemModel())
			siList.add(new SelectItem(ss.getSubsystemNumber(), ss.getSubsystemName()));
		return siList;
	}

	public List<SelectItem> getAccessRules()
	{
		List<SelectItem> siList = new ArrayList<SelectItem>();
		for (AdmAccessRule rule : se.getAccessRules(subsystemNumber))
			siList.add(new SelectItem(rule.getAccessRuleId(), rule.getRuleName()));
		return SelectItemUtils.withEmpty(siList);
	}
}
