package ru.insoft.sovdoc.ui.adm;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("userGroupSelect")
public class GroupSelect {

	private Long groupId;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
}
