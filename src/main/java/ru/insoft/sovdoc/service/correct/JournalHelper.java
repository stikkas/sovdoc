package ru.insoft.sovdoc.service.correct;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import ru.insoft.commons.utils.StringUtils;

import ru.insoft.sovdoc.model.HasDescriptors;
import ru.insoft.sovdoc.model.complex.table.ApplicationToDocument;
import ru.insoft.sovdoc.model.complex.table.ArchDocument;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchFundName;
import ru.insoft.sovdoc.model.complex.table.ArchKinodoc;
import ru.insoft.sovdoc.model.complex.table.ArchPerformanceAnnotation;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchVideodoc;
import ru.insoft.sovdoc.model.complex.table.UnivDataLanguage;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval;
import ru.insoft.sovdoc.model.complex.view.VComplexCardLinks;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.complex.view.VComplexDocument;
import ru.insoft.sovdoc.model.complex.view.VComplexFund;
import ru.insoft.sovdoc.model.complex.view.VComplexKinoStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexKinoStoryDescription;
import ru.insoft.sovdoc.model.complex.view.VComplexKinodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexPerformAnnotation;
import ru.insoft.sovdoc.model.complex.view.VComplexPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexPhonodoc;
import ru.insoft.sovdoc.model.complex.view.VComplexSeries;
import ru.insoft.sovdoc.model.complex.view.VComplexUnivClassifier;
import ru.insoft.sovdoc.model.complex.view.VComplexVideoStorageUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexVideoStoryDescription;
import ru.insoft.sovdoc.model.complex.view.VComplexVideodoc;
import ru.insoft.sovdoc.model.correct.table.UnivCorrectJournal;
import ru.insoft.sovdoc.model.desc.view.VDescAttrvalueWithCode;
import ru.insoft.sovdoc.model.ebook.table.ArchEbook;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookAuthor;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookSeries;
import ru.insoft.sovdoc.model.ebook.view.VEbook;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.security.SecurityServiceImpl;
import ru.insoft.sovdoc.system.SystemEntity;

public class JournalHelper implements Serializable {

	@Inject
	SecurityServiceImpl security;
	@Inject
	SystemEntity se;
	@Inject
	EntityManager em;
	@Inject
	ru.insoft.sovdoc.system.complex.SystemQuery complexQuery;
	@Inject
	ru.insoft.sovdoc.system.ebook.SystemQuery ebookQuery;

	private UnivDataUnit copyModel;
	private VComplexDataUnit copyImmModel;
	private ArchFund copyFundModel;
	private VComplexFund copyFundImmModel;
	private ArchSeries copySeriesModel;
	private VComplexSeries copySeriesImmModel;
	private ArchStorageUnit copyStorageUnitModel;
	private ArchDocument copyDocumentModel;
	private ApplicationToDocument copyApplicationToDocumentModel;
	private List<ApplicationToDocument> copyapplicationToDocumentModelList;
	private VComplexCardLinks copycardLink;
	private List<VComplexCardLinks> copycardLinksList;
	private VComplexDocument copyDocumentImmModel;
	private ArchEbook copyEbookModel;
	private VEbook copyEbookImmModel;
	private ArchPhonodoc copyPhonodocModel;
	private ArchVideodoc copyVideodocModel;
	private ArchKinodoc copyKinodocModel;
	private VComplexPhonodoc copyPhonodocImmModel;
	private VComplexVideodoc copyVideodocImmModel;
	private VComplexKinodoc copyKinodocImmModel;

	public UnivDataUnit getCopyModel() {
		return copyModel;
	}

	public ArchFund getCopyFundModel() {
		return copyFundModel;
	}

	public void copyModel(UnivDataUnit model) {
		copyModel = new UnivDataUnit();
		copyModel.setPortalSectionId(model.getPortalSectionId());
		copyModel.setUnitName(model.getUnitName());
		copyModel.setNumberNumber(model.getNumberNumber());
		copyModel.setNumberText(model.getNumberText());
		copyModel.setArchNumberCode(model.getArchNumberCode());
		copyModel.setAnnotation(model.getAnnotation());
		copyModel.setAccessLimited(model.isAccessLimited());

		List<UnivDateInterval> intervals = new ArrayList<UnivDateInterval>();
		for (UnivDateInterval interval : model.getDateIntervals()) {
			UnivDateInterval newInterval = new UnivDateInterval();
			newInterval.setBeginDate(interval.getBeginDate());
			newInterval.setEndDate(interval.getEndDate());
			intervals.add(newInterval);
		}
		copyModel.setDateIntervals(intervals);

		List<UnivDataLanguage> languages = new ArrayList<UnivDataLanguage>();
		for (UnivDataLanguage language : model.getLanguages()) {
			UnivDataLanguage newLanguage = new UnivDataLanguage();
			newLanguage.setDocLanguageId(language.getDocLanguageId());
			languages.add(newLanguage);
		}
		copyModel.setLanguages(languages);
	}

	public void copyImmModel(VComplexDataUnit immModel) {
		if (immModel == null) {
			return;
		}

		copyImmModel = new VComplexDataUnit();
		copyImmModel.setFundNum(immModel.getFundNum());
		copyImmModel.setFundLetter(immModel.getFundLetter());
		copyImmModel.setSeriesNum(immModel.getSeriesNum());
		copyImmModel.setSeriesLetter(immModel.getSeriesLetter());
		copyImmModel.setUnitInfo(immModel.getUnitInfo());
		copyImmModel.setStartPage(immModel.getStartPage());
		copyImmModel.setPages(immModel.getPages());
		copyImmModel.setPortalSection(immModel.getPortalSection());
		copyImmModel.setDateIntervals(immModel.getDateIntervals());

		List<VComplexDescriptor> descriptors = new ArrayList<VComplexDescriptor>();
		for (VComplexDescriptor descriptor : immModel.getDescriptors()) {
			em.detach(descriptor);
			VComplexDescriptor newDescriptor = new VComplexDescriptor();
			newDescriptor.setUnivDescriptorId(descriptor.getUnivDescriptorId());
			newDescriptor.setDescriptor1(descriptor.getDescriptor1());
			newDescriptor.setDescriptorRelationId(descriptor.getDescriptorRelationId());
			newDescriptor.setDescRelation(descriptor.getDescRelation());
			newDescriptor.setDescriptor2(descriptor.getDescriptor2());
			descriptors.add(newDescriptor);
		}
		copyImmModel.setDescriptors(descriptors);

		List<VComplexUnivClassifier> clssValues = new ArrayList<VComplexUnivClassifier>();
		for (VComplexUnivClassifier clssVal : immModel.getClassifierValues()) {
			em.detach(clssVal);
			VComplexUnivClassifier newValue = new VComplexUnivClassifier();
			newValue.setDescriptorValueId(clssVal.getDescriptorValueId());
			newValue.setFullValue(clssVal.getFullValue());
			newValue.setValueIndex(clssVal.getValueIndex());
			clssValues.add(newValue);
		}
		copyImmModel.setClassifierValues(clssValues);
	}

	public void copyFundModel(ArchFund fundModel) {
		copyFundModel = new ArchFund();
		copyFundModel.setFundTypeId(fundModel.getFundTypeId());
		copyFundModel.setSeriesCount(fundModel.getSeriesCount());
		copyFundModel.setStorageUnitCount(fundModel.getStorageUnitCount());
		copyFundModel.setNotes(fundModel.getNotes());
		copyFundModel.setHistoricalNote(fundModel.getHistoricalNote());

		List<ArchFundName> names = new ArrayList<ArchFundName>();
		for (ArchFundName name : fundModel.getFundNames()) {
			ArchFundName newName = new ArchFundName();
			newName.setFundNameId(name.getFundNameId());
			newName.setFundName(name.getFundName());
			newName.setSortOrder(name.getSortOrder());
			newName.setTextDate(name.getTextDate());
			em.detach(newName);
			names.add(newName);
		}
		copyFundModel.setFundNames(names);
	}

	public void copyFundImmModel(VComplexFund fundImmModel) {
		copyFundImmModel = new VComplexFund();
		copyFundImmModel.setFundType(fundImmModel.getFundType());
	}

	public void copySeriesModel(ArchSeries seriesModel) {
		copySeriesModel = new ArchSeries();
		copySeriesModel.setDocumentKindId(seriesModel.getDocumentKindId());
		copySeriesModel.setSeriesName(seriesModel.getSeriesName());
		copySeriesModel.setStorageUnitCount(seriesModel.getStorageUnitCount());
		copySeriesModel.setReproductionTypeId(seriesModel.getReproductionTypeId());
		copySeriesModel.setNotes(seriesModel.getNotes());
	}

	public void copySeriesImmModel(VComplexSeries seriesImmModel) {
		copySeriesImmModel = new VComplexSeries();
		copySeriesImmModel.setDocumentKind(seriesImmModel.getDocumentKind());
		copySeriesImmModel.setReproductionType(seriesImmModel.getReproductionType());
	}

	public void copyStorageUnitModel(ArchStorageUnit storageUnitModel) {
		copyStorageUnitModel = new ArchStorageUnit();
		copyStorageUnitModel.setVolumeNum(storageUnitModel.getVolumeNum());
		copyStorageUnitModel.setPartNum(storageUnitModel.getPartNum());
		copyStorageUnitModel.setFileCaption(storageUnitModel.getFileCaption());
		copyStorageUnitModel.setPageCount(storageUnitModel.getPageCount());
		copyStorageUnitModel.setDatesNote(storageUnitModel.getDatesNote());
		copyStorageUnitModel.setWorkIndex(storageUnitModel.getWorkIndex());
		copyStorageUnitModel.setNotes(storageUnitModel.getNotes());
	}

	public void copyDocumentModel(ArchDocument documentModel) {
		copyDocumentModel = new ArchDocument();
		copyDocumentModel.setDocumentTypeId(documentModel.getDocumentTypeId());
		copyDocumentModel.setDocumentNumber(documentModel.getDocumentNumber());
		copyDocumentModel.setDocumentName(documentModel.getDocumentName());
		copyDocumentModel.setPageCount(documentModel.getPageCount());
		copyDocumentModel.setDatesNote(documentModel.getDatesNote());
		copyDocumentModel.setReproductionTypeId(documentModel.getReproductionTypeId());
		copyDocumentModel.setBaseMaterialId(documentModel.getBaseMaterialId());
		copyDocumentModel.setAuthenticityTypeId(documentModel.getAuthenticityTypeId());
		copyDocumentModel.setNotes(documentModel.getNotes());

	}

	public void copyDocumentImmModel(VComplexDocument documentImmModel) {
		copyDocumentImmModel = new VComplexDocument();
		copyDocumentImmModel.setSingleDocumentType(documentImmModel.getSingleDocumentType());
		copyDocumentImmModel.setBaseMaterial(documentImmModel.getBaseMaterial());
		copyDocumentImmModel.setReproductionType(documentImmModel.getReproductionType());
		copyDocumentImmModel.setAuthenticityType(documentImmModel.getAuthenticityType());
	}

	public void copyApplicationToDocumentModel(List<ApplicationToDocument> applicationToDocumentModelList) {
		copyapplicationToDocumentModelList = new ArrayList<ApplicationToDocument>();

		for (int i = 0; i < applicationToDocumentModelList.size(); i++) {
			copyApplicationToDocumentModel = new ApplicationToDocument();
			copyApplicationToDocumentModel.setApplToDocId(applicationToDocumentModelList.get(i).getApplToDocId());
			copyApplicationToDocumentModel.setApplNumber(applicationToDocumentModelList.get(i).getApplNumber());
			copyApplicationToDocumentModel.setApplName(applicationToDocumentModelList.get(i).getApplName());
			copyApplicationToDocumentModel.setPages(applicationToDocumentModelList.get(i).getPages());
			copyApplicationToDocumentModel.setPagesQuantity(applicationToDocumentModelList.get(i).getPagesQuantity());
			copyApplicationToDocumentModel.setUnivDataUnitId(applicationToDocumentModelList.get(i).getUnivDataUnitId());
			copyapplicationToDocumentModelList.add(copyApplicationToDocumentModel);
		}
	}

	public void copyCardLinksModel(List<VComplexCardLinks> cardLinksList) {
		copycardLinksList = new ArrayList<VComplexCardLinks>();

		for (int i = 0; i < cardLinksList.size(); i++) {
			copycardLink = new VComplexCardLinks();
			copycardLink.setUnitFullName(cardLinksList.get(i).getUnitFullName());
			copycardLink.setUnitTypeCode(cardLinksList.get(i).getUnitTypeCode());
			copycardLink.setUnitIdLinked(cardLinksList.get(i).getUnitIdLinked());
			copycardLink.setUnitIdOriginal(cardLinksList.get(i).getUnitIdOriginal());

			copycardLinksList.add(copycardLink);
		}
	}

	public void copyEbookModel(ArchEbook ebookModel) {
		copyEbookModel = new ArchEbook();
		copyEbookModel.setEditionTypeId(ebookModel.getEditionTypeId());
		copyEbookModel.setUdc(ebookModel.getUdc());
		copyEbookModel.setBBC(ebookModel.getBBC());
		copyEbookModel.setIsbn(ebookModel.getIsbn());
		copyEbookModel.setEditor(ebookModel.getEditor());
		copyEbookModel.setEditorialBoard(ebookModel.getEditorialBoard());
		copyEbookModel.setCompilers(ebookModel.getCompilers());
		copyEbookModel.setVolume1Info(ebookModel.getVolume1Info());
		copyEbookModel.setVolume2Info(ebookModel.getVolume2Info());
		copyEbookModel.setAtoptitleData(ebookModel.getAtoptitleData());
		copyEbookModel.setSeriesId(ebookModel.getSeriesId());
		copyEbookModel.setEditionNumber(ebookModel.getEditionNumber());
		copyEbookModel.setEditionNote(ebookModel.getEditionNote());
		copyEbookModel.setPublishingPlace(ebookModel.getPublishingPlace());
		copyEbookModel.setPublisher(ebookModel.getPublisher());
		copyEbookModel.setPublishingYear(ebookModel.getPublishingYear());
		copyEbookModel.setEditionSize(ebookModel.getEditionSize());
		copyEbookModel.setNotes(ebookModel.getNotes());
		copyEbookModel.setExternalUrl(ebookModel.getExternalUrl());

		List<ArchEbookAuthor> authors = new ArrayList<ArchEbookAuthor>();
		for (ArchEbookAuthor author : ebookModel.getAuthors()) {
			ArchEbookAuthor newAuthor = new ArchEbookAuthor();
			newAuthor.setAuthorId(author.getAuthorId());
			authors.add(newAuthor);
		}
		copyEbookModel.setAuthors(authors);
	}

	public void copyEbookImmModel(VEbook ebookImmModel) {
		copyEbookImmModel = new VEbook();
		copyEbookImmModel.setEditionType(ebookImmModel.getEditionType());
		copyEbookImmModel.setLanguageName(ebookImmModel.getLanguageName());
		copyEbookImmModel.setAuthors(ebookImmModel.getAuthors());

		if (ebookImmModel.getSeries() != null) {
			ArchEbookSeries series = new ArchEbookSeries();
			series.setSeriesName(ebookImmModel.getSeries().getSeriesName());
			series.setSeriesInfo(ebookImmModel.getSeries().getSeriesInfo());
			copyEbookImmModel.setSeries(series);
		}

		List<VComplexDescriptor> descriptors = new ArrayList<VComplexDescriptor>();
		for (VComplexDescriptor descriptor : ebookImmModel.getDescriptors()) {
			em.detach(descriptor);
			VComplexDescriptor newDescriptor = new VComplexDescriptor();
			newDescriptor.setUnivDescriptorId(descriptor.getUnivDescriptorId());
			newDescriptor.setDescriptor1(descriptor.getDescriptor1());
			newDescriptor.setDescriptorRelationId(descriptor.getDescriptorRelationId());
			newDescriptor.setDescRelation(descriptor.getDescRelation());
			newDescriptor.setDescriptor2(descriptor.getDescriptor2());
			descriptors.add(newDescriptor);
		}
		copyEbookImmModel.setDescriptors(descriptors);
	}

	public void setPhonodocCopyModels(ArchPhonodoc phonodocModel, VComplexPhonodoc phonodocImmModel) {
		phonodocModel.getStorageUnits().iterator();
		em.detach(phonodocModel);
		copyPhonodocModel = phonodocModel;

		for (VComplexPhonoStorageUnit storageUnit : phonodocImmModel.getStorageUnits()) {
			em.detach(storageUnit);
		}
		for (VComplexPerformAnnotation annotation : phonodocImmModel.getAnnotations()) {
			em.detach(annotation);
		}
		em.detach(phonodocImmModel);
		copyPhonodocImmModel = phonodocImmModel;
	}

	public void setVideodocCopyModels(ArchVideodoc videodocModel, VComplexVideodoc videodocImmModel) {
		videodocModel.getStorageUnits().iterator();
		em.detach(videodocModel);
		copyVideodocModel = videodocModel;

		for (VComplexVideoStorageUnit storageUnit : videodocImmModel.getStorageUnits()) {
			em.detach(storageUnit);
		}
		for (VComplexVideoStoryDescription description : videodocImmModel.getStoryDescriptions()) {
			em.detach(description);
		}
		em.detach(videodocImmModel);
		copyVideodocImmModel = videodocImmModel;
	}

	public void setKinodocCopyModels(ArchKinodoc kinodocModel, VComplexKinodoc kinodocImmModel) {
		kinodocModel.getStorageUnits().iterator();
		em.detach(kinodocModel);
		copyKinodocModel = kinodocModel;

		for (VComplexKinoStorageUnit storageUnit : kinodocImmModel.getStorageUnits()) {
			em.detach(storageUnit);
		}
		for (VComplexKinoStoryDescription description : kinodocImmModel.getStoryDescriptions()) {
			em.detach(description);
		}
		em.detach(kinodocImmModel);
		copyKinodocImmModel = kinodocImmModel;
	}

	protected void recordUnivDataUnit(UnivDataUnit model, String operation, Long changedEntityTypeId, String dataBlock) {
		if (operation.equals("EDIT") && dataBlock.equals(formatHeader("Редактируемые значения"))) {
			return;
		}

		UnivCorrectJournal journalEntry = new UnivCorrectJournal();
		journalEntry.setUserId(security.getCurrentUser().getUserId());
		journalEntry.setArchiveId(model.getResOwnerId());
		journalEntry.setUnivDataUnitId(model.getUnivDataUnitId());
		journalEntry.setOperationTypeId(se.getImmDescValueByCodes("OPERATION_TYPE", operation).getDescriptorValueId());
		journalEntry.setChangedEntityTypeId(changedEntityTypeId);
		journalEntry.setOperationDate(new Date());
		journalEntry.setDataBlock(dataBlock);
		em.persist(journalEntry);
	}

	protected String formatHeader(String text) {
		return String.format("<b><u><font color=\"#B72C36\">%1$s</font></u></b><br/>", text);
	}

	protected String formatLabel(String text) {
		return String.format("<b>%1$s</b><br/>", text);
	}

	protected String formatCrossedLabel(String text) {
		return String.format("<s><b>%1$s</b></s><br/>", text);
	}

	protected String formatValue(String text) {
		return String.format("&nbsp;&nbsp;&nbsp;%1$s<br/>", text);
	}

	protected String formatCrossedValue(String text) {
		return String.format("&nbsp;&nbsp;&nbsp;<s>%1$s</s><br/>", text);
	}

	protected String empty() {
		return "&lt;&lt;пусто&gt;&gt;";
	}

	public void recordFullUnivDataUnit(UnivDataUnit model, String operation, List<VComplexCardLinks> cardLinksList, List<ApplicationToDocument> applToDocList) {
		VDescAttrvalueWithCode changedEntity = se.getAttrValueByCode(model.getUnitTypeId(), "CHANGED_ENTITY_TYPE");
		if (changedEntity == null) {
			return;
		}

		String dataBlock = "";
		if (operation.equals("INPUT") || operation.equals("LOAD")) {
			dataBlock = formatHeader("Исходные значения");
		}
		if (operation.equals("DELETE")) {
			dataBlock = formatHeader("Значения до удаления");
		}

		String entityType = se.getDescValue(changedEntity.getRefDescriptorValueId()).getValueCode();
		if (entityType.equals("FUND")) {
			dataBlock += getFullFundDataBlock(model);
		}
		if (entityType.equals("SERIES")) {
			dataBlock += getFullSeriesDataBlock(model, cardLinksList);
		}
		if (entityType.equals("FILE")) {
			dataBlock += getFullStorageUnitDataBlock(model, cardLinksList);
		}
		if (entityType.equals("DOCUMENT")) {
			dataBlock += getFullDocumentDataBlock(model, cardLinksList, applToDocList);
		}
		if (entityType.equals("EBOOK")) {
			dataBlock += getFullEbookDataBlock(model);
		}
		if (entityType.equals("PHONODOC")) {
			dataBlock += getFullPhonodocDataBlock(model);
		}

		recordUnivDataUnit(model, operation, changedEntity.getRefDescriptorValueId(), dataBlock);
	}

	protected String getFullFundDataBlock(UnivDataUnit model) {
		VComplexDataUnit immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
		ArchFund fundModel = complexQuery.queryFundModel(model.getUnivDataUnitId());
		VComplexFund fundImmModel = complexQuery.queryFundImmModel(model.getUnivDataUnitId());

		String res = formatLabel("Фонд");
		res += formatValue(immModel.getFundNum().toString()
				+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		if (model.getPortalSectionId() != null) {
			res += formatLabel("Тематический раздел портала");
			res += formatValue(immModel.getPortalSection());
		}
		if (fundModel.getFundTypeId() != null) {
			res += formatLabel("Вид фонда");
			res += formatValue(fundImmModel.getFundType());
		}
		res += formatLabel("Названия фонда");
		for (ArchFundName name : fundModel.getFundNames()) {
			res += formatValue(name.getFundName()
					+ (name.getTextDate() == null ? "" : " (" + name.getTextDate() + ")"));
		}
		res += formatLabel("Количество описей");
		res += formatValue(String.valueOf(fundImmModel.getSeriesCount()));
		res += formatLabel("Количество ед.хр.");
		res += formatValue(String.valueOf(fundImmModel.getStorageUnitCount()));
		res += formatLabel("Крайние даты");
		res += formatValue(immModel.getDateIntervals());
		if (model.getAnnotation() != null) {
			res += formatLabel("Аннотация");
			res += formatValue(model.getAnnotation());
		}
		if (fundModel.getNotes() != null) {
			res += formatLabel("Примечание");
			res += formatValue(fundModel.getNotes());
		}
		if (fundModel.getHistoricalNote() != null) {
			res += formatLabel("Историческая справка");
			res += formatValue(fundModel.getHistoricalNote());
		}
		res += getUnivDescriptorsData(immModel);

		return res;
	}

	protected String getFullSeriesDataBlock(UnivDataUnit model, List<VComplexCardLinks> cardLinksList) {
		VComplexDataUnit immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
		ArchSeries seriesModel = complexQuery.querySeriesModel(model.getUnivDataUnitId());
		VComplexSeries seriesImmModel = complexQuery.querySeriesImmModel(model.getUnivDataUnitId());

		String res = formatLabel("Фонд");
		res += formatValue(immModel.getFundNum().toString()
				+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		res += formatLabel("Опись");
		res += formatValue(immModel.getSeriesNum().toString()
				+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		if (model.getPortalSectionId() != null) {
			res += formatLabel("Тематический раздел портала");
			res += formatValue(immModel.getPortalSection());
		}
		if (seriesModel.getDocumentKindId() != null) {
			res += formatLabel("Вид документов");
			res += formatValue(seriesImmModel.getDocumentKind());
		}
		res += formatLabel("Название описи");
		res += formatValue(seriesModel.getSeriesName());
		res += formatLabel("Количество единиц хранения");
		res += formatValue(seriesModel.getStorageUnitCount().toString());
		res += formatLabel("Крайние даты");
		res += formatValue(immModel.getDateIntervals());
		if (model.getAnnotation() != null) {
			res += formatLabel("Аннотация");
			res += formatValue(model.getAnnotation());
		}
		if (seriesModel.getReproductionTypeId() != null) {
			res += formatLabel("Способ воспроизведения");
			res += formatValue(seriesImmModel.getReproductionType());
		}
		if (seriesModel.getNotes() != null) {
			res += formatLabel("Примечание");
			res += formatValue(seriesModel.getNotes());
		}
		res += getUnivDescriptorsData(immModel);

		if (cardLinksList != null && cardLinksList.size() > 0) {
			res += formatLabel("Ссылки");

			for (VComplexCardLinks cardLink : cardLinksList) {
				res += formatValue(cardLink.getUnitFullName());
			}

		}
		return res;
	}

	protected String getFullStorageUnitDataBlock(UnivDataUnit model, List<VComplexCardLinks> cardLinksList) {
		VComplexDataUnit immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
		ArchStorageUnit storageUnitModel = complexQuery.queryStorageUnitModel(model.getUnivDataUnitId());

		String res = formatLabel("Фонд");
		res += formatValue(immModel.getFundNum().toString()
				+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		res += formatLabel("Опись");
		res += formatValue(immModel.getSeriesNum().toString()
				+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		res += formatLabel("Дело");
		res += formatValue(immModel.getUnitInfo());
		if (model.getPortalSectionId() != null) {
			res += formatLabel("Тематический раздел портала");
			res += formatValue(immModel.getPortalSection());
		}
		res += formatLabel("Заголовок дела");
		res += formatValue(storageUnitModel.getFileCaption());
		if (storageUnitModel.getPageCount() != null) {
			res += formatLabel("Количество листов");
			res += formatValue(storageUnitModel.getPageCount().toString());
		}
		if (immModel.getDateIntervals() != null) {
			res += formatLabel("Крайние даты");
			res += formatValue(immModel.getDateIntervals());
		}
		if (storageUnitModel.getDatesNote() != null) {
			res += formatLabel("Даты в своб.формате");
			res += formatValue(storageUnitModel.getDatesNote());
		}
		if (model.getAnnotation() != null) {
			res += formatLabel("Аннотация");
			res += formatValue(model.getAnnotation());
		}
		if (model.getLanguages().size() > 0) {
			res += formatLabel("Язык документов");
			String langs = "";
			for (UnivDataLanguage lang : model.getLanguages()) {
				if (!langs.isEmpty()) {
					langs += ", ";
				}
				langs += se.getDescValue(lang.getDocLanguageId()).getFullValue();
			}
			res += formatValue(langs);
		}
		if (storageUnitModel.getWorkIndex() != null) {
			res += formatLabel("Делопроизводственный индекс");
			res += formatValue(storageUnitModel.getWorkIndex());
		}
		if (storageUnitModel.getNotes() != null) {
			res += formatLabel("Примечание");
			res += formatValue(storageUnitModel.getNotes());
		}
		if (model.isAccessLimited()) {
			res += formatLabel("Имеются ограничения к использованию");
		}
		res += getUnivDescriptorsData(immModel);
		res += getAttachedFilesData(model.getUnivDataUnitId());

		if (cardLinksList != null && cardLinksList.size() > 0) {
			res += formatLabel("Ссылки");

			for (VComplexCardLinks cardLink : cardLinksList) {
				res += formatValue(cardLink.getUnitFullName());
			}

		}
		return res;
	}

	protected String getFullDocumentDataBlock(UnivDataUnit model, List<VComplexCardLinks> cardLinksList, List<ApplicationToDocument> applToDocList) {
		VComplexDataUnit immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
		ArchDocument documentModel = complexQuery.queryDocumentModel(model.getUnivDataUnitId());
		VComplexDocument documentImmModel = complexQuery.queryDocumentImmModel(model.getUnivDataUnitId());

		String res = formatLabel("Фонд");
		res += formatValue(immModel.getFundNum().toString()
				+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		res += formatLabel("Опись");
		res += formatValue(immModel.getSeriesNum().toString()
				+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		res += formatLabel("Дело");
		res += formatValue(immModel.getUnitInfo());
		if (immModel.getStartPage() != null) {
			res += formatLabel("Номер начального листа");
			res += formatValue(immModel.getStartPage().toString());
		}
		if (immModel.getPages() != null) {
			res += formatLabel("Листы");
			res += formatValue(immModel.getPages());
		}
		if (model.getPortalSectionId() != null) {
			res += formatLabel("Тематический раздел портала");
			res += formatValue(immModel.getPortalSection());
		}
		if (documentModel.getDocumentTypeId() != null) {
			res += formatLabel("Вид документа");
			res += formatValue(documentImmModel.getSingleDocumentType());
		}
		res += formatLabel("Номер документа");
		res += formatValue(documentModel.getDocumentNumber());
		res += formatLabel("Заголовок документа");
		res += formatValue(documentModel.getDocumentName());
		if (documentModel.getPageCount() != null) {
			res += formatLabel("Количество листов");
			res += formatValue(documentModel.getPageCount().toString());
		}
		if (immModel.getDateIntervals() != null) {
			res += formatLabel("Крайние даты");
			res += formatValue(immModel.getDateIntervals());
		}
		if (documentModel.getDatesNote() != null) {
			res += formatLabel("Даты в своб.формате");
			res += formatValue(documentModel.getDatesNote());
		}
//<editor-fold defaultstate="collapsed" desc="приложения к документу">
		if (applToDocList != null && applToDocList.size() > 0) {
			res += formatLabel("Приложения");

			for (ApplicationToDocument appl : applToDocList) {
				String datablockForApp = "";

				if (appl.getApplNumber() != null) {
					datablockForApp += appl.getApplNumber().concat(". ");
				}
				if (appl.getApplName() != null) {
					datablockForApp += appl.getApplName().concat(" ");
				}
				if (appl.getPages() != null || appl.getPagesQuantity() != null) {
					datablockForApp += "(";
				}
				if (appl.getPages() != null) {
					datablockForApp += "лл. ".concat(appl.getPages());
				}
				if (appl.getPagesQuantity() != null) {
					if (appl.getPages() != null) {
						datablockForApp += ", ";
					}
					datablockForApp += "количество ".concat(appl.getPagesQuantity().toString());
				}
				if (appl.getPages() != null || appl.getPagesQuantity() != null) {
					datablockForApp += ")";
				}
				res += formatValue(datablockForApp);
			}

		}
//</editor-fold>    

		if (model.getAnnotation() != null) {
			res += formatLabel("Аннотация");
			res += formatValue(model.getAnnotation());
		}
		if (documentModel.getBaseMaterialId() != null) {
			res += formatLabel("Основа (материалы)");
			res += formatValue(documentImmModel.getBaseMaterial());
		}
		if (documentModel.getReproductionTypeId() != null) {
			res += formatLabel("Способ воспроизведения");
			res += formatValue(documentImmModel.getReproductionType());
		}
		if (model.getLanguages().size() > 0) {
			res += formatLabel("Язык документов");
			String langs = "";
			for (UnivDataLanguage lang : model.getLanguages()) {
				if (!langs.isEmpty()) {
					langs += ", ";
				}
				langs += se.getDescValue(lang.getDocLanguageId()).getFullValue();
			}
			res += formatValue(langs);
		}
		if (documentModel.getAuthenticityTypeId() != null) {
			res += formatLabel("Подлинность");
			res += formatValue(documentImmModel.getAuthenticityType());
		}
		if (documentModel.getNotes() != null) {
			res += formatLabel("Примечание");
			res += formatValue(documentModel.getNotes());
		}
		if (model.isAccessLimited()) {
			res += formatLabel("Имеются ограничения к использованию");
		}
		res += getUnivDescriptorsData(immModel);
		res += getAttachedFilesData(model.getUnivDataUnitId());

		if (cardLinksList != null && cardLinksList.size() > 0) {
			res += formatLabel("Ссылки");

			for (VComplexCardLinks cardLink : cardLinksList) {
				res += formatValue(cardLink.getUnitFullName());
			}

		}
		return res;
	}

	protected String getFullEbookDataBlock(UnivDataUnit model) {
		ArchEbook ebookModel = ebookQuery.queryEbookModel(model.getUnivDataUnitId());
		VEbook ebookImmModel = ebookQuery.queryEbookImmModel(model.getUnivDataUnitId());

		String res = formatLabel("Номер в реестре");
		res += formatValue(model.getNumberNumber().toString());
		if (ebookModel.getUdc() != null) {
			res += formatLabel("УДК");
			res += formatValue(ebookModel.getUdc());
		}
		if (ebookModel.getBBC() != null) {
			res += formatLabel("ББК");
			res += formatValue(ebookModel.getBBC());
		}
		if (ebookModel.getIsbn() != null) {
			res += formatLabel("ISBN");
			res += formatValue(ebookModel.getIsbn());
		}
		res += formatLabel("Вид издания");
		res += formatValue(ebookImmModel.getEditionType());
		if (model.getLanguages().size() > 0) {
			res += formatLabel("Язык документов");
			res += formatValue(ebookImmModel.getLanguageName());
		}
		if (ebookModel.getAuthors().size() > 0) {
			res += formatLabel("Автор");
			res += formatValue(ebookImmModel.getAuthors());
		}
		if (ebookModel.getEditor() != null) {
			res += formatLabel("Редактор");
			res += formatValue(ebookModel.getEditor());
		}
		if (ebookModel.getEditorialBoard() != null) {
			res += formatLabel("Редколлегия");
			res += formatValue(ebookModel.getEditorialBoard());
		}
		if (ebookModel.getCompilers() != null) {
			res += formatLabel("Составители");
			res += formatValue(ebookModel.getCompilers());
		}
		res += formatLabel("Название издания");
		res += formatValue(model.getUnitName());
		if (ebookModel.getVolume1Info() != null) {
			res += formatLabel("Том");
			res += formatValue(ebookModel.getVolume1Info());
		}
		if (ebookModel.getVolume2Info() != null) {
			res += formatLabel("Книга");
			res += formatValue(ebookModel.getVolume2Info());
		}
		if (ebookModel.getAtoptitleData() != null) {
			res += formatLabel("Надзаголовочные данные");
			res += formatValue(ebookModel.getAtoptitleData());
		}
		if (ebookModel.getSeriesId() != null) {
			res += formatLabel("Название серии");
			res += formatValue(ebookImmModel.getSeries().getSeriesName());
			if (ebookImmModel.getSeries().getSeriesInfo() != null) {
				res += formatLabel("Другие сведения о серии");
				res += formatValue(ebookImmModel.getSeries().getSeriesInfo());
			}
		}
		if (ebookModel.getEditionNumber() != null) {
			res += formatLabel("Номер выпуска");
			res += formatValue(ebookModel.getEditionNumber());
		}
		if (ebookModel.getEditionNote() != null) {
			res += formatLabel("Сведения об издании");
			res += formatValue(ebookModel.getEditionNote());
		}
		res += formatLabel("Место издания");
		res += formatValue(ebookModel.getPublishingPlace());
		res += formatLabel("Название издательства");
		res += formatValue(ebookModel.getPublisher());
		res += formatLabel("Год издания");
		res += formatValue(ebookModel.getPublishingYear().toString());
		res += formatLabel("Объём издания");
		res += formatValue(ebookModel.getEditionSize());
		if (model.getAnnotation() != null) {
			res += formatLabel("Аннотация");
			res += formatValue(model.getAnnotation());
		}
		if (ebookModel.getNotes() != null) {
			res += formatLabel("Примечание");
			res += formatValue(ebookModel.getNotes());
		}
		if (ebookModel.getExternalUrl() != null) {
			res += formatLabel("Ссылка на текст книги");
			res += formatValue(ebookModel.getExternalUrl());
		}
		res += getUnivDescriptorsData(ebookImmModel);

		return res;
	}

	protected String getFullPhonodocDataBlock(UnivDataUnit model) {
		VComplexDataUnit immModel = complexQuery.queryImmModel(model.getUnivDataUnitId());
		ArchPhonodoc phonodocModel = complexQuery.queryPhonodocModel(model.getUnivDataUnitId());
		VComplexPhonodoc phonodocImmModel = complexQuery.queryPhonodocImmModel(model.getUnivDataUnitId());

		String res = "";
		if (immModel.getFundNum() != null) {
			res += formatLabel("Фонд");
			res += formatValue(immModel.getFundNum().toString()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (immModel.getSeriesNum() != null) {
			res += formatLabel("Опись");
			res += formatValue(immModel.getSeriesNum().toString()
					+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		}
		res += formatLabel("Единица учёта");
		res += formatValue(model.getNumberNumber().toString()
				+ (model.getNumberText() == null ? "" : model.getNumberText()));
		res += formatLabel("Вид записанного материала");
		res += formatValue(phonodocImmModel.getRecordType());
		if (model.getPortalSectionId() != null) {
			res += formatLabel("Тематический раздел портала");
			res += formatValue(immModel.getPortalSection());
		}
		res += formatLabel("Вид фонодокумента");
		res += formatValue(phonodocImmModel.getPhonodocType());
		if (phonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
			res += formatLabel("Название документа");
			res += formatValue(phonodocModel.getPhonodocName());
			res += formatLabel("Автор");
			res += formatValue(phonodocModel.getAuthors());
			res += formatLabel("Место события");
			res += formatValue(phonodocModel.getEventPlace());
		}
		if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
			res += formatLabel("Название произведения");
			res += formatValue(phonodocModel.getPhonodocName());
			if (phonodocModel.getPerformerOrchestra() != null) {
				res += formatLabel("Исполнитель (оркестр)");
				res += formatValue(phonodocModel.getPerformerOrchestra());
			}
			if (phonodocModel.getPerformerChoir() != null) {
				res += formatLabel("Исполнитель (хор)");
				res += formatValue(phonodocModel.getPerformerChoir());
			}
			if (phonodocModel.getPerformers() != null) {
				res += formatLabel("Исполнители");
				res += formatValue(phonodocModel.getPerformers());
			}
			res += formatLabel("Авторы");
			res += formatValue(phonodocModel.getAuthors());
			if (phonodocModel.getRubrics() != null) {
				res += formatLabel("Рубрика");
				res += formatValue(phonodocModel.getRubrics());
			}
		}
		res += formatLabel("Количество единиц хранения");
		res += formatValue(phonodocImmModel.getStorageUnitCount().toString());
		res += formatLabel("Единицы хранения");
		Integer totalPlaytime = 0;
		for (VComplexPhonoStorageUnit storageUnit : phonodocImmModel.getStorageUnits()) {
			String suInfo = storageUnit.getNumberNumber().toString();
			if (storageUnit.getNumberText() != null) {
				suInfo += storageUnit.getNumberText();
			}
			res += formatValue(suInfo + " " + getPhonoStorageUnitData(storageUnit));

			totalPlaytime += storageUnit.getPlaytime();
		}
		res += formatLabel("Дата события");
		res += formatValue(phonodocImmModel.getEventDateStr());
		res += formatLabel("Дата записи");
		res += formatValue(phonodocImmModel.getRecordDateStr());
		if (phonodocModel.getRewriteDate() != null) {
			res += formatLabel("Дата перезаписи");
			res += formatValue(phonodocImmModel.getRewriteDateStr());
		}
		res += formatLabel("Время звучания");
		res += formatValue(String.format("%1$d:%2$02d", totalPlaytime / 60, totalPlaytime % 60));
		if (phonodocModel.getKitInfo() != null) {
			res += formatLabel("Состав комплекта");
			res += formatValue(phonodocModel.getKitInfo());
		}
		if (phonodocModel.getManufacturerName() != null) {
			res += formatLabel("Наименование изготовителя");
			res += formatValue(phonodocModel.getManufacturerName());
		}
		if (model.getAnnotation() != null) {
			res += formatLabel("Аннотация (реферат)");
			res += formatValue(model.getAnnotation());
		}
		if (phonodocModel.getNotes() != null) {
			res += formatLabel("Примечание");
			res += formatValue(phonodocModel.getNotes());
		}
		res += getUnivClassifierValues(immModel);
		res += getUnivDescriptorsData(immModel);
		if (!phonodocImmModel.getAnnotations().isEmpty()) {
			res += formatLabel("Аннотации выступлений");
			for (VComplexPerformAnnotation annotation : phonodocImmModel.getAnnotations()) {
				res += formatValue(annotation.getPerformanceNumber().toString() + ") "
						+ getPerformanceAnnotationData(annotation));
				res += formatValue("&nbsp;&nbsp;&nbsp;" + annotation.getAnnotation());
			}
		}

		return res;
	}

	protected String getUnivClassifierValues(VComplexDataUnit immModel) {
		if (immModel.getClassifierValues().isEmpty()) {
			return "";
		}

		String res = formatLabel("Тематический классификатор");
		for (VComplexUnivClassifier clssValue : immModel.getClassifierValues()) {
			res += formatValue(String.format("%1$s %2$s",
					clssValue.getValueIndex(), clssValue.getFullValue()));
		}
		return res;
	}

	protected String getUnivDescriptorsData(HasDescriptors immModel) {
		if (immModel.getDescriptors().size() == 0) {
			return "";
		}

		String res = formatLabel("Ключевые слова");
		for (VComplexDescriptor desc : immModel.getDescriptors()) {
			res += formatValue(desc.getDescriptor1()
					+ (desc.getDescriptorRelationId() == null ? ""
					: " &gt;&gt; " + desc.getDescRelation() + " &gt;&gt; " + desc.getDescriptor2()));
		}
		return res;
	}

	protected String getAttachedFilesData(Long unitId) {
		List<ImagePath> fullsizeList = complexQuery.queryImagePathList(unitId, "FULL_SIZE");
		List<ImagePath> previewList = complexQuery.queryImagePathList(unitId, "PREVIEW");

		String res = "";
		if (fullsizeList.size() > 0) {
			res += formatLabel("Полноразмерные файлы");
			for (ImagePath fs : fullsizeList) {
				res += formatValue(fs.getCaption());
			}
		}
		if (previewList.size() > 0) {
			res += formatLabel("Сжатые файлы");
			for (ImagePath preview : previewList) {
				res += formatValue(preview.getCaption());
			}
		}
		return res;
	}

	protected String getPhonoStorageUnitData(VComplexPhonoStorageUnit storageUnit) {
		String suInfo = String.format("(время - %1$d:%2$02d", storageUnit.getPlaytime() / 60,
				storageUnit.getPlaytime() % 60);
		suInfo += ", " + storageUnit.getStorageType().toLowerCase();
		if (storageUnit.getChannelCount() != null) {
			suInfo += ", " + storageUnit.getChannelCount().toLowerCase();
		}
		if (storageUnit.getSpeed() != null) {
			suInfo += ", скорость - " + storageUnit.getSpeed().toString();
		}
		if (storageUnit.getSoundQuality() != null) {
			suInfo += ", качество - " + storageUnit.getSoundQuality().toLowerCase();
		}
		if (storageUnit.getFilePathData() != null) {
			suInfo += ", файл - '" + storageUnit.getFilePathData().getCaption() + "'";
		}
		suInfo += ")";
		return suInfo;
	}

	protected String getPerformanceAnnotationData(VComplexPerformAnnotation annotation) {
		String annInfo = "ед.хр. - " + annotation.getStorageUnitNumber();
		if (annotation.getSideOrTrack() != null) {
			annInfo += ", сторона/трек - " + annotation.getSideOrTrack();
		}
		if (annotation.getSpokesperson() != null) {
			annInfo += ", докладчик - " + annotation.getSpokesperson();
		}
		if (annotation.getRecordLanguage() != null) {
			annInfo += ", язык - " + annotation.getRecordLanguage().toLowerCase();
		}
		annInfo += String.format(", время - %1$d:%2$02d",
				annotation.getPlaytime() / 60, annotation.getPlaytime() % 60);
		if (annotation.getFilePathData() != null) {
			annInfo += ", файл - '" + annotation.getFilePathData().getCaption() + "'";
		}
		return annInfo;
	}

	protected String getVideoStoryDescriptionData(VComplexVideoStoryDescription annotation) {
		String annInfo = "ед.хр. - " + annotation.getStorageUnitNumber();
		if (annotation.getPart() != null) {
			annInfo += ", часть - " + annotation.getPart();
		}
		if (annotation.getLanguage() != null) {
			annInfo += ", язык - " + annotation.getLanguage().toLowerCase();
		}
		annInfo += String.format(", время - %1$d:%2$02d",
				annotation.getPlaytime() / 60, annotation.getPlaytime() % 60);
		if (annotation.getFilePathData() != null) {
			annInfo += ", файл - '" + annotation.getFilePathData().getCaption() + "'";
		}
		return annInfo;
	}

	protected String getKinoStoryDescriptionData(VComplexKinoStoryDescription annotation) {
		String annInfo = "ед.хр. - " + annotation.getStorageUnitNumber();
		if (annotation.getPart() != null) {
			annInfo += ", часть - " + annotation.getPart();
		}
		if (annotation.getLanguage() != null) {
			annInfo += ", язык - " + annotation.getLanguage().toLowerCase();
		}
		annInfo += String.format(", время - %1$d:%2$02d",
				annotation.getPlaytime() / 60, annotation.getPlaytime() % 60);
		if (annotation.getFilePathData() != null) {
			annInfo += ", файл - '" + annotation.getFilePathData().getCaption() + "'";
		}
		return annInfo;
	}

	public boolean safeEquals(Object o1, Object o2) {
		if (o1 == null) {
			return o2 == null;
		}
		return o1.equals(o2);
	}

	public void recordEditFund(UnivDataUnit model, VComplexDataUnit immModel,
			ArchFund fundModel, VComplexFund fundImmModel) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(immModel.getFundNum(), copyImmModel.getFundNum())
				|| !safeEquals(immModel.getFundLetter(), copyImmModel.getFundLetter())) {
			dataBlock += formatLabel("Фонд");
			dataBlock += formatCrossedValue(copyImmModel.getFundNum()
					+ (copyImmModel.getFundLetter() == null ? "" : copyImmModel.getFundLetter()));
			dataBlock += formatValue(immModel.getFundNum()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (!safeEquals(model.getPortalSectionId(), copyModel.getPortalSectionId())) {
			dataBlock += formatLabel("Тематический раздел портала");
			dataBlock += formatCrossedValue(copyModel.getPortalSectionId() == null
					? empty() : copyImmModel.getPortalSection());
			dataBlock += formatValue(model.getPortalSectionId() == null
					? empty() : immModel.getPortalSection());
		}
		if (!safeEquals(fundModel.getFundTypeId(), copyFundModel.getFundTypeId())) {
			dataBlock += formatLabel("Вид фонда");
			dataBlock += formatCrossedValue(copyFundModel.getFundTypeId() == null
					? empty() : copyFundImmModel.getFundType());
			dataBlock += formatValue(fundModel.getFundTypeId() == null
					? empty() : fundImmModel.getFundType());
		}

		boolean sizeOfFundNamesIsNull = false;
		boolean sizeOfCopyFundNamesIsNull = false;

		if (fundModel.getFundNames().size() == 0) {
			sizeOfFundNamesIsNull = true;
		}
		if (copyFundModel.getFundNames().size() == 0) {
			sizeOfCopyFundNamesIsNull = true;
		}
		if (fundModel.getFundNames().size() >= 0) {
			List<ArchFundName> fundNameListForDelete = new ArrayList<ArchFundName>();
			List<ArchFundName> copyFundNameListForDelete = new ArrayList<ArchFundName>();
			List<ArchFundName> fundNameListUpdated = new ArrayList<ArchFundName>();
			List<ArchFundName> copyFundNameListUpdated = new ArrayList<ArchFundName>();

			for (int i = 0; i < copyFundModel.getFundNames().size(); i++) {
				for (int j = 0; j < fundModel.getFundNames().size(); j++) {
					ArchFundName copyFundName = copyFundModel.getFundNames().get(i);
					ArchFundName fundName = fundModel.getFundNames().get(j);

					if (safeEquals(copyFundName.getFundNameId(), fundName.getFundNameId())
							&& safeEquals(copyFundName.getFundName(), fundName.getFundName())
							&& safeEquals(copyFundName.getTextDate(), fundName.getTextDate())) {
						fundNameListForDelete.add(fundName);
						copyFundNameListForDelete.add(copyFundName);

					} else if (safeEquals(copyFundName.getFundNameId(), fundName.getFundNameId())
							&& (!safeEquals(copyFundName.getFundName(), fundName.getFundName())
							|| !safeEquals(copyFundName.getTextDate(), fundName.getTextDate()))) {
						fundNameListUpdated.add(fundName);
						copyFundNameListUpdated.add(copyFundName);
						fundNameListForDelete.add(fundName);
						copyFundNameListForDelete.add(copyFundName);

					}
				}
			}

			if (copyFundNameListForDelete.size() > 0) {
				for (ArchFundName commonFundName : copyFundNameListForDelete) {
					copyFundModel.getFundNames().remove(commonFundName);
				}

			}
			if (fundNameListForDelete.size() > 0) {
				for (ArchFundName commonFundName : fundNameListForDelete) {
					fundModel.getFundNames().remove(commonFundName);
				}

			}

			if (copyFundModel.getFundNames().size() > 0 || fundModel.getFundNames().size() > 0 || fundNameListUpdated.size() > 0 || copyFundNameListUpdated.size() > 0) {
				dataBlock += formatLabel("Названия фонда");

				if (fundNameListUpdated.size() > 0 && copyFundNameListUpdated.size() > 0) {
					for (int i = 0; i < copyFundNameListUpdated.size(); i++) {
						String datablockForFundNameCrossed = "";
						String datablockForFundNameNotCrossed = "";
						for (int j = 0; j < fundNameListUpdated.size(); j++) {
							ArchFundName copyFundName = copyFundNameListUpdated.get(i);
							ArchFundName fundName = fundNameListUpdated.get(j);
							if (safeEquals(copyFundName.getFundNameId(), fundName.getFundNameId())) {
								if (copyFundName.getFundName() != null) {
									datablockForFundNameCrossed += String.format("&nbsp;&nbsp;&nbsp;<s>%1$s</s>", copyFundName.getFundName());
								}
								if (copyFundName.getTextDate() != null) {
									datablockForFundNameCrossed += String.format("<s>%1$s</s>", " (" + copyFundName.getTextDate() + ")");
								}
								if (fundName.getFundName() != null) {
									datablockForFundNameNotCrossed += String.format("%1$s", " —> " + fundName.getFundName());
								}
								if (fundName.getTextDate() != null) {
									datablockForFundNameNotCrossed += String.format("%1$s", " (" + fundName.getTextDate() + ")");
								}
								datablockForFundNameNotCrossed += String.format("<br/>", datablockForFundNameNotCrossed);
							}
						}
						dataBlock += (datablockForFundNameCrossed + datablockForFundNameNotCrossed);
					}
				}

				if (copyFundModel.getFundNames().size() > 0) {
					for (ArchFundName copyFund : copyFundModel.getFundNames()) {
						String datablockForFundName = "";
						if (copyFund.getFundName() != null) {
							datablockForFundName += (copyFund.getFundName());
						}
						if (copyFund.getTextDate() != null) {
							datablockForFundName += " (" + copyFund.getTextDate() + ")";
						}
						dataBlock += formatCrossedValue(datablockForFundName);

					}
				} else if (sizeOfCopyFundNamesIsNull) {
					dataBlock += formatCrossedValue(empty());
				}
				if (fundModel.getFundNames().size() > 0) {
					for (ArchFundName copyFund : fundModel.getFundNames()) {
						String datablockForFundName = "";
						if (copyFund.getFundName() != null) {
							datablockForFundName += (copyFund.getFundName());
						}
						if (copyFund.getTextDate() != null) {
							datablockForFundName += " (" + copyFund.getTextDate() + ")";
						}
						dataBlock += formatValue(datablockForFundName);

					}
				} else if (sizeOfFundNamesIsNull) {
					dataBlock += formatValue(empty());
				}
			}
			fundNameListForDelete.clear();
			copyFundNameListForDelete.clear();
			fundNameListUpdated.clear();
			copyFundNameListUpdated.clear();

		}
		if (copyFundModel.getSeriesCount() != fundModel.getSeriesCount()) {
			dataBlock += formatLabel("Количество описей");
			dataBlock += formatCrossedValue(String.valueOf(copyFundModel.getSeriesCount()));
			dataBlock += formatValue(String.valueOf(fundModel.getSeriesCount()));
		}
		if (copyFundModel.getStorageUnitCount() != fundModel.getStorageUnitCount()) {
			dataBlock += formatLabel("Количество ед.хр.");
			dataBlock += formatCrossedValue(String.valueOf(copyFundModel.getStorageUnitCount()));
			dataBlock += formatValue(String.valueOf(fundModel.getStorageUnitCount()));
		}
		if (!safeEquals(copyImmModel.getDateIntervals(), immModel.getDateIntervals())) {
			dataBlock += formatLabel("Крайние даты");
			dataBlock += formatCrossedValue(String.valueOf(copyImmModel.getDateIntervals()));
			dataBlock += formatValue(String.valueOf(immModel.getDateIntervals()));
		}
		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null ? empty()
					: model.getAnnotation());
		}
		if (!safeEquals(copyFundModel.getNotes(), fundModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copyFundModel.getNotes() == null
					? empty() : copyFundModel.getNotes());
			dataBlock += formatValue(fundModel.getNotes() == null ? empty()
					: fundModel.getNotes());
		}
		if (!safeEquals(copyFundModel.getHistoricalNote(), fundModel.getHistoricalNote())) {
			dataBlock += formatLabel("Историческая справка");
			dataBlock += formatCrossedValue(copyFundModel.getHistoricalNote() == null
					? empty() : copyFundModel.getHistoricalNote());
			dataBlock += formatValue(fundModel.getHistoricalNote() == null ? empty()
					: fundModel.getHistoricalNote());
		}

		recordUnivDataUnit(model, "EDIT",
				se.getImmDescValueByCodes("CHANGED_ENTITY_TYPE", "FUND").getDescriptorValueId(),
				dataBlock);
	}

	public void recordEditSeries(UnivDataUnit model, VComplexDataUnit immModel,
			ArchSeries seriesModel, VComplexSeries seriesImmModel, List<VComplexCardLinks> cardLinksList) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(copyImmModel.getFundPrefix(), immModel.getFundPrefix())
				|| !safeEquals(copyImmModel.getFundNum(), immModel.getFundNum())
				|| !safeEquals(copyImmModel.getFundLetter(), immModel.getFundLetter())) {
			dataBlock += formatLabel("Фонд");
			dataBlock += formatCrossedValue((copyImmModel.getFundPrefix() == null ? "" : copyImmModel.getFundPrefix() + "-")
					+ copyImmModel.getFundNum().toString()
					+ (copyImmModel.getFundLetter() == null ? "" : copyImmModel.getFundLetter()));
			dataBlock += formatValue((immModel.getFundPrefix() == null ? "" : immModel.getFundPrefix() + "-")
					+ immModel.getFundNum().toString()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (!safeEquals(copyImmModel.getSeriesNum(), immModel.getSeriesNum())
				|| !safeEquals(copyImmModel.getSeriesLetter(), immModel.getSeriesLetter())) {
			dataBlock += formatLabel("Опись");
			dataBlock += formatCrossedValue(copyImmModel.getSeriesNum().toString()
					+ (copyImmModel.getSeriesLetter() == null ? "" : copyImmModel.getSeriesLetter()));
			dataBlock += formatValue(immModel.getSeriesNum().toString()
					+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		}
		if (!safeEquals(model.getPortalSectionId(), copyModel.getPortalSectionId())) {
			dataBlock += formatLabel("Тематический раздел портала");
			dataBlock += formatCrossedValue(copyModel.getPortalSectionId() == null
					? empty() : copyImmModel.getPortalSection());
			dataBlock += formatValue(model.getPortalSectionId() == null
					? empty() : immModel.getPortalSection());
		}
		if (!safeEquals(copySeriesModel.getDocumentKindId(), seriesModel.getDocumentKindId())) {
			dataBlock += formatLabel("Вид документов");
			dataBlock += formatCrossedValue(copySeriesImmModel.getDocumentKind());
			dataBlock += formatValue(seriesImmModel.getDocumentKind());
		}
		if (!safeEquals(copySeriesModel.getSeriesName(), seriesModel.getSeriesName())) {
			dataBlock += formatLabel("Название описи");
			dataBlock += formatCrossedValue(copySeriesModel.getSeriesName());
			dataBlock += formatValue(seriesModel.getSeriesName());
		}
		if (!safeEquals(copySeriesModel.getStorageUnitCount(), seriesModel.getStorageUnitCount())) {
			dataBlock += formatLabel("Количество ед.хр.");
			dataBlock += formatCrossedValue(copySeriesModel.getStorageUnitCount().toString());
			dataBlock += formatValue(seriesModel.getStorageUnitCount().toString());
		}
		if (!safeEquals(copyImmModel.getDateIntervals(), immModel.getDateIntervals())) {
			dataBlock += formatLabel("Крайние даты");
			dataBlock += formatCrossedValue(copyImmModel.getDateIntervals());
			dataBlock += formatValue(immModel.getDateIntervals());
		}

		boolean sizeOfCardLinksIsNull = false;
		if (cardLinksList.size() == 0) {
			sizeOfCardLinksIsNull = true;
		}
		boolean copySizeOfCardLinksIsNull = false;
		if (copycardLinksList.size() == 0) {
			copySizeOfCardLinksIsNull = true;
		}

		if (cardLinksList.size() >= 0) {
			List<VComplexCardLinks> copycardLinksListForDelete = new ArrayList<VComplexCardLinks>();
			List<VComplexCardLinks> cardLinksListForDelete = new ArrayList<VComplexCardLinks>();

			for (int i = 0; i < copycardLinksList.size(); i++) {
				for (int j = 0; j < cardLinksList.size(); j++) {
					VComplexCardLinks copyCardLink = copycardLinksList.get(i);
					VComplexCardLinks cardLink = cardLinksList.get(j);

					if (safeEquals(copyCardLink.getUnitFullName(), cardLink.getUnitFullName())) {
						copycardLinksListForDelete.add(copyCardLink);
						cardLinksListForDelete.add(cardLink);
					}
				}
			}

			if (copycardLinksListForDelete.size() > 0) {
				for (VComplexCardLinks copyCardLink : copycardLinksListForDelete) {
					copycardLinksList.remove(copyCardLink);
				}

			}
			if (cardLinksListForDelete.size() > 0) {
				for (VComplexCardLinks cardLink : cardLinksListForDelete) {
					cardLinksList.remove(cardLink);
				}

			}

			if (copycardLinksList.size() > 0 || cardLinksList.size() > 0) {
				dataBlock += formatLabel("Ссылки");

				if (copycardLinksList.size() > 0) {
					for (VComplexCardLinks copyCardLink : copycardLinksList) {
						dataBlock += formatCrossedValue(copyCardLink.getUnitFullName());
					}
				} else if (copySizeOfCardLinksIsNull) {
					dataBlock += formatCrossedValue(empty());
				}

				if (cardLinksList.size() > 0) {
					for (VComplexCardLinks cardLink : cardLinksList) {
						dataBlock += formatValue(cardLink.getUnitFullName());
					}
				} else if (sizeOfCardLinksIsNull) {
					dataBlock += formatValue(empty());
				}
			}
			cardLinksListForDelete.clear();
			copycardLinksListForDelete.clear();
		}

		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null
					? empty() : model.getAnnotation());
		}
		if (!safeEquals(copySeriesModel.getReproductionTypeId(), seriesModel.getReproductionTypeId())) {
			dataBlock += formatLabel("Способ воспроизведения");
			dataBlock += formatCrossedValue(copySeriesModel.getReproductionTypeId() == null
					? empty() : copySeriesImmModel.getReproductionType());
			dataBlock += formatValue(seriesModel.getReproductionTypeId() == null
					? empty() : seriesImmModel.getReproductionType());
		}
		if (!safeEquals(copySeriesModel.getNotes(), seriesModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copySeriesModel.getNotes() == null
					? empty() : copySeriesModel.getNotes());
			dataBlock += formatValue(seriesModel.getNotes() == null
					? empty() : seriesModel.getNotes());
		}

		recordUnivDataUnit(model, "EDIT",
				se.getImmDescValueByCodes("CHANGED_ENTITY_TYPE", "SERIES").getDescriptorValueId(),
				dataBlock);
	}

	public void recordEditStorageUnit(UnivDataUnit model, VComplexDataUnit immModel, ArchStorageUnit storageUnitModel,
			List<VComplexCardLinks> cardLinksList) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(copyImmModel.getFundNum(), immModel.getFundNum())
				|| !safeEquals(copyImmModel.getFundLetter(), immModel.getFundLetter())) {
			dataBlock += formatLabel("Фонд");
			dataBlock += formatCrossedValue(copyImmModel.getFundNum().toString()
					+ (copyImmModel.getFundLetter() == null ? "" : copyImmModel.getFundLetter()));
			dataBlock += formatValue(immModel.getFundNum().toString()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (!safeEquals(copyImmModel.getSeriesNum(), immModel.getSeriesNum())
				|| !safeEquals(copyImmModel.getSeriesLetter(), immModel.getSeriesLetter())) {
			dataBlock += formatLabel("Опись");
			dataBlock += formatCrossedValue(copyImmModel.getSeriesNum().toString()
					+ (copyImmModel.getSeriesLetter() == null ? "" : copyImmModel.getSeriesLetter()));
			dataBlock += formatValue(immModel.getSeriesNum().toString()
					+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		}
		if (!safeEquals(copyImmModel.getUnitInfo(), immModel.getUnitInfo())) {
			dataBlock += formatLabel("Дело");
			dataBlock += formatCrossedValue(copyImmModel.getUnitInfo());
			dataBlock += formatValue(immModel.getUnitInfo());
		}
		if (!safeEquals(model.getPortalSectionId(), copyModel.getPortalSectionId())) {
			dataBlock += formatLabel("Тематический раздел портала");
			dataBlock += formatCrossedValue(copyModel.getPortalSectionId() == null
					? empty() : copyImmModel.getPortalSection());
			dataBlock += formatValue(model.getPortalSectionId() == null
					? empty() : immModel.getPortalSection());
		}
		if (!safeEquals(copyStorageUnitModel.getFileCaption(), storageUnitModel.getFileCaption())) {
			dataBlock += formatLabel("Заголовок дела");
			dataBlock += formatCrossedValue(copyStorageUnitModel.getFileCaption());
			dataBlock += formatValue(storageUnitModel.getFileCaption());
		}
		if (!safeEquals(copyStorageUnitModel.getPageCount(), storageUnitModel.getPageCount())) {
			dataBlock += formatLabel("Количество листов");
			dataBlock += formatCrossedValue(copyStorageUnitModel.getPageCount() == null
					? empty() : copyStorageUnitModel.getPageCount().toString());
			dataBlock += formatValue(storageUnitModel.getPageCount() == null
					? empty() : storageUnitModel.getPageCount().toString());
		}
		if (!safeEquals(copyImmModel.getDateIntervals(), immModel.getDateIntervals())) {
			dataBlock += formatLabel("Крайние даты");
			dataBlock += formatCrossedValue(copyImmModel.getDateIntervals() == null
					? empty() : copyImmModel.getDateIntervals());
			dataBlock += formatValue(immModel.getDateIntervals() == null
					? empty() : immModel.getDateIntervals());
		}
		if (!safeEquals(copyStorageUnitModel.getDatesNote(), storageUnitModel.getDatesNote())) {
			dataBlock += formatLabel("Даты в своб.формате");
			dataBlock += formatCrossedValue(copyStorageUnitModel.getDatesNote() == null
					? empty() : copyStorageUnitModel.getDatesNote());
			dataBlock += formatValue(storageUnitModel.getDatesNote() == null
					? empty() : storageUnitModel.getDatesNote());
		}

		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null
					? empty() : model.getAnnotation());
		}

		boolean langChanged = false;
		if (copyModel.getLanguages().size() != model.getLanguages().size()) {
			langChanged = true;
		} else {
			for (int i = 0; i < copyModel.getLanguages().size(); i++) {
				UnivDataLanguage lang1 = copyModel.getLanguages().get(i);
				UnivDataLanguage lang2 = model.getLanguages().get(i);
				if (!safeEquals(lang1.getDocLanguageId(), lang2.getDocLanguageId())) {
					langChanged = true;
					break;
				}
			}
		}
		if (langChanged) {
			dataBlock += formatLabel("Язык документов");
			String langs = "";
			for (UnivDataLanguage lang : copyModel.getLanguages()) {
				if (!langs.isEmpty()) {
					langs += ", ";
				}
				langs += se.getDescValue(lang.getDocLanguageId()).getFullValue();
			}
			dataBlock += formatCrossedValue(langs);
			langs = "";
			for (UnivDataLanguage lang : model.getLanguages()) {
				if (!langs.isEmpty()) {
					langs += ", ";
				}
				langs += se.getDescValue(lang.getDocLanguageId()).getFullValue();
			}
			dataBlock += formatValue(langs);
		}

		if (!safeEquals(copyStorageUnitModel.getWorkIndex(), storageUnitModel.getWorkIndex())) {
			dataBlock += formatLabel("Делопроизводственный индекс");
			dataBlock += formatCrossedValue(copyStorageUnitModel.getWorkIndex() == null
					? empty() : copyStorageUnitModel.getWorkIndex());
			dataBlock += formatValue(storageUnitModel.getWorkIndex() == null
					? empty() : storageUnitModel.getWorkIndex());
		}

		if (!safeEquals(copyStorageUnitModel.getNotes(), storageUnitModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copyStorageUnitModel.getNotes() == null
					? empty() : copyStorageUnitModel.getNotes());
			dataBlock += formatValue(storageUnitModel.getNotes() == null
					? empty() : storageUnitModel.getNotes());
		}
		if (copyModel.isAccessLimited() != model.isAccessLimited()) {
			if (copyModel.isAccessLimited()) {
				dataBlock += formatCrossedLabel("Имеются ограничения к использованию");
			} else {
				dataBlock += formatLabel("Имеются ограничения к использованию");
			}
		}
		boolean sizeOfCardLinksIsNull = false;
		if (cardLinksList.size() == 0) {
			sizeOfCardLinksIsNull = true;
		}
		boolean copySizeOfCardLinksIsNull = false;
		if (copycardLinksList.size() == 0) {
			copySizeOfCardLinksIsNull = true;
		}

		if (cardLinksList.size() >= 0) {
			List<VComplexCardLinks> copycardLinksListForDelete = new ArrayList<VComplexCardLinks>();
			List<VComplexCardLinks> cardLinksListForDelete = new ArrayList<VComplexCardLinks>();

			for (int i = 0; i < copycardLinksList.size(); i++) {
				for (int j = 0; j < cardLinksList.size(); j++) {
					VComplexCardLinks copyCardLink = copycardLinksList.get(i);
					VComplexCardLinks cardLink = cardLinksList.get(j);

					if (safeEquals(copyCardLink.getUnitFullName(), cardLink.getUnitFullName())) {
						copycardLinksListForDelete.add(copyCardLink);
						cardLinksListForDelete.add(cardLink);
					}
				}
			}

			if (copycardLinksListForDelete.size() > 0) {
				for (VComplexCardLinks copyCardLink : copycardLinksListForDelete) {
					copycardLinksList.remove(copyCardLink);
				}

			}
			if (cardLinksListForDelete.size() > 0) {
				for (VComplexCardLinks cardLink : cardLinksListForDelete) {
					cardLinksList.remove(cardLink);
				}

			}

			if (copycardLinksList.size() > 0 || cardLinksList.size() > 0) {
				dataBlock += formatLabel("Ссылки");

				if (copycardLinksList.size() > 0) {
					for (VComplexCardLinks copyCardLink : copycardLinksList) {
						dataBlock += formatCrossedValue(copyCardLink.getUnitFullName());
					}
				} else if (copySizeOfCardLinksIsNull) {
					dataBlock += formatCrossedValue(empty());
				}

				if (cardLinksList.size() > 0) {
					for (VComplexCardLinks cardLink : cardLinksList) {
						dataBlock += formatValue(cardLink.getUnitFullName());
					}
				} else if (sizeOfCardLinksIsNull) {
					dataBlock += formatValue(empty());
				}
			}
			cardLinksListForDelete.clear();
			copycardLinksListForDelete.clear();
		}

		recordUnivDataUnit(model, "EDIT",
				se.getImmDescValueByCodes("CHANGED_ENTITY_TYPE", "FILE").getDescriptorValueId(),
				dataBlock);
	}

	public void recordEditDocument(UnivDataUnit model, VComplexDataUnit immModel,
			ArchDocument documentModel, VComplexDocument documentImmModel, List<ApplicationToDocument> applicationToDocumentModelList, List<VComplexCardLinks> cardLinksList) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(copyImmModel.getFundNum(), immModel.getFundNum())
				|| !safeEquals(copyImmModel.getFundLetter(), immModel.getFundLetter())) {
			dataBlock += formatLabel("Фонд");
			dataBlock += formatCrossedValue(copyImmModel.getFundNum().toString()
					+ (copyImmModel.getFundLetter() == null ? "" : copyImmModel.getFundLetter()));
			dataBlock += formatValue(immModel.getFundNum().toString()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (!safeEquals(copyImmModel.getSeriesNum(), immModel.getSeriesNum())
				|| !safeEquals(copyImmModel.getSeriesLetter(), immModel.getSeriesLetter())) {
			dataBlock += formatLabel("Опись");
			dataBlock += formatCrossedValue(copyImmModel.getSeriesNum().toString()
					+ (copyImmModel.getSeriesLetter() == null ? "" : copyImmModel.getSeriesLetter()));
			dataBlock += formatValue(immModel.getSeriesNum().toString()
					+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		}
		if (!safeEquals(copyImmModel.getUnitInfo(), immModel.getUnitInfo())) {
			dataBlock += formatLabel("Дело");
			dataBlock += formatCrossedValue(copyImmModel.getUnitInfo());
			dataBlock += formatValue(immModel.getUnitInfo());
		}
		if (!safeEquals(copyImmModel.getStartPage(), immModel.getStartPage())) {
			dataBlock += formatLabel("Номер начального листа");
			dataBlock += formatCrossedValue(copyImmModel.getStartPage() == null
					? empty() : copyImmModel.getStartPage().toString());
			dataBlock += formatValue(immModel.getStartPage() == null
					? empty() : immModel.getStartPage().toString());
		}
		if (!safeEquals(copyImmModel.getPages(), immModel.getPages())) {
			dataBlock += formatLabel("Листы");
			dataBlock += formatCrossedValue(copyImmModel.getPages() == null
					? empty() : copyImmModel.getPages());
			dataBlock += formatValue(immModel.getPages() == null
					? empty() : immModel.getPages());
		}
		if (!safeEquals(model.getPortalSectionId(), copyModel.getPortalSectionId())) {
			dataBlock += formatLabel("Тематический раздел портала");
			dataBlock += formatCrossedValue(copyModel.getPortalSectionId() == null
					? empty() : copyImmModel.getPortalSection());
			dataBlock += formatValue(model.getPortalSectionId() == null
					? empty() : immModel.getPortalSection());
		}
		if (!safeEquals(copyDocumentModel.getDocumentTypeId(), documentModel.getDocumentTypeId())) {
			dataBlock += formatLabel("Вид документа");
			dataBlock += formatCrossedValue(copyDocumentModel.getDocumentTypeId() == null
					? empty() : copyDocumentImmModel.getSingleDocumentType());
			dataBlock += formatValue(documentModel.getDocumentTypeId() == null
					? empty() : documentImmModel.getSingleDocumentType());
		}
		if (!safeEquals(copyDocumentModel.getDocumentNumber(), documentModel.getDocumentNumber())) {
			dataBlock += formatLabel("Номер документа");
			dataBlock += formatCrossedValue(copyDocumentModel.getDocumentNumber());
			dataBlock += formatValue(documentModel.getDocumentNumber());
		}
		if (!safeEquals(copyDocumentModel.getDocumentName(), documentModel.getDocumentName())) {
			dataBlock += formatLabel("Заголовок документа");
			dataBlock += formatCrossedValue(copyDocumentModel.getDocumentName());
			dataBlock += formatValue(documentModel.getDocumentName());
		}
		if (!safeEquals(copyDocumentModel.getPageCount(), documentModel.getPageCount())) {
			dataBlock += formatLabel("Количество листов");
			dataBlock += formatCrossedValue(copyDocumentModel.getPageCount() == null
					? empty() : copyDocumentModel.getPageCount().toString());
			dataBlock += formatValue(documentModel.getPageCount() == null
					? empty() : documentModel.getPageCount().toString());
		}
		if (!safeEquals(copyImmModel.getDateIntervals(), immModel.getDateIntervals())) {
			dataBlock += formatLabel("Крайние даты");
			dataBlock += formatCrossedValue(copyImmModel.getDateIntervals() == null
					? empty() : copyImmModel.getDateIntervals());
			dataBlock += formatValue(immModel.getDateIntervals() == null
					? empty() : immModel.getDateIntervals());
		}
		if (!safeEquals(copyDocumentModel.getDatesNote(), documentModel.getDatesNote())) {
			dataBlock += formatLabel("Даты в своб.формате");
			dataBlock += formatCrossedValue(copyDocumentModel.getDatesNote() == null
					? empty() : copyDocumentModel.getDatesNote());
			dataBlock += formatValue(documentModel.getDatesNote() == null
					? empty() : documentModel.getDatesNote());
		}

//<editor-fold defaultstate="collapsed" desc="приложения к документу">
		boolean sizeOfApplIsNull = false;
		boolean sizeOfCopyApplIsNull = false;

		if (applicationToDocumentModelList.size() == 0) {
			sizeOfApplIsNull = true;
		}
		if (copyapplicationToDocumentModelList.size() == 0) {
			sizeOfCopyApplIsNull = true;
		}
		if (applicationToDocumentModelList.size() >= 0) {

			List<ApplicationToDocument> applListForDelete = new ArrayList<ApplicationToDocument>();
			List<ApplicationToDocument> copyApplListForDelete = new ArrayList<ApplicationToDocument>();
			List<ApplicationToDocument> applListUpdated = new ArrayList<ApplicationToDocument>();
			List<ApplicationToDocument> copyapplListUpdated = new ArrayList<ApplicationToDocument>();

			for (int i = 0; i < copyapplicationToDocumentModelList.size(); i++) {
				for (int j = 0; j < applicationToDocumentModelList.size(); j++) {
					ApplicationToDocument copyAppl = copyapplicationToDocumentModelList.get(i);
					ApplicationToDocument appl = applicationToDocumentModelList.get(j);

					if (safeEquals(copyAppl.getApplToDocId(), appl.getApplToDocId())
							&& safeEquals(copyAppl.getApplNumber(), appl.getApplNumber())
							&& safeEquals(copyAppl.getApplName(), appl.getApplName())
							&& safeEquals(copyAppl.getPages(), appl.getPages())
							&& safeEquals(copyAppl.getPagesQuantity(), appl.getPagesQuantity())) {
						applListForDelete.add(appl);
						copyApplListForDelete.add(copyAppl);

					} else if (safeEquals(copyAppl.getApplToDocId(), appl.getApplToDocId())
							&& (!safeEquals(copyAppl.getApplNumber(), appl.getApplNumber())
							|| !safeEquals(copyAppl.getApplName(), appl.getApplName())
							|| !safeEquals(copyAppl.getPages(), appl.getPages())
							|| !safeEquals(copyAppl.getPagesQuantity(), appl.getPagesQuantity()))) {
						applListUpdated.add(appl);
						copyapplListUpdated.add(copyAppl);
						applListForDelete.add(appl);
						copyApplListForDelete.add(copyAppl);

					}
				}
			}

			if (copyApplListForDelete.size() > 0) {
				for (ApplicationToDocument commonAppl : copyApplListForDelete) {
					copyapplicationToDocumentModelList.remove(commonAppl);
				}

			}
			if (applicationToDocumentModelList.size() > 0) {
				for (ApplicationToDocument commonAppl : applListForDelete) {
					applicationToDocumentModelList.remove(commonAppl);
				}

			}

			if (copyapplicationToDocumentModelList.size() > 0 || applicationToDocumentModelList.size() > 0 || applListUpdated.size() > 0 || copyapplListUpdated.size() > 0) {
				dataBlock += formatLabel("Приложения");

				if (applListUpdated.size() > 0 && copyapplListUpdated.size() > 0) {
					for (int i = 0; i < copyapplListUpdated.size(); i++) {
						String datablockForApplCrossed = "";
						String datablockForApplNotCrossed = "";
						for (int j = 0; j < applListUpdated.size(); j++) {
							ApplicationToDocument copyAppl = copyapplListUpdated.get(i);
							ApplicationToDocument appl = applListUpdated.get(j);
							if (safeEquals(copyAppl.getApplToDocId(), appl.getApplToDocId())) {
								datablockForApplCrossed += String.format("&nbsp;&nbsp;&nbsp;<s>%1$s</s>", datablockForApplCrossed);
								if (copyAppl.getApplNumber() != null) {
									datablockForApplCrossed += String.format("<s>%1$s</s>", copyAppl.getApplNumber().concat(". "));
								}
								if (copyAppl.getApplName() != null) {
									datablockForApplCrossed += String.format("<s>%1$s</s>", copyAppl.getApplName().concat(" "));
								}
								if (copyAppl.getPages() != null || copyAppl.getPagesQuantity() != null) {
									datablockForApplCrossed += String.format("<s>%1$s</s>", "(");
								}
								if (copyAppl.getPages() != null) {
									datablockForApplCrossed += String.format("<s>%1$s</s>", "лл. ".concat(copyAppl.getPages()));
								}
								if (copyAppl.getPagesQuantity() != null) {
									if (copyAppl.getPages() != null) {
										datablockForApplCrossed += String.format("<s>%1$s</s>", ", ");
									}
									datablockForApplCrossed += String.format("<s>%1$s</s>", "количество ".concat(copyAppl.getPagesQuantity().toString()));
								}
								if (copyAppl.getPages() != null || copyAppl.getPagesQuantity() != null) {
									datablockForApplCrossed += String.format("<s>%1$s</s>", ")");
								}

								if (appl.getApplToDocId() != null) {
									datablockForApplNotCrossed += String.format("%1$s", " —> ");
								}

								if (appl.getApplNumber() != null) {
									datablockForApplNotCrossed += String.format("%1$s", appl.getApplNumber().concat(". "));
								}
								if (appl.getApplName() != null) {
									datablockForApplNotCrossed += String.format("%1$s", appl.getApplName().concat(" "));
								}
								if (appl.getPages() != null || appl.getPagesQuantity() != null) {
									datablockForApplNotCrossed += String.format("%1$s", "(");
								}
								if (appl.getPages() != null) {
									datablockForApplNotCrossed += String.format("%1$s", "лл. ".concat(appl.getPages()));
								}
								if (appl.getPagesQuantity() != null) {
									if (appl.getPages() != null) {
										datablockForApplNotCrossed += String.format("%1$s", ", ");
									}
									datablockForApplNotCrossed += String.format("%1$s", "количество ".concat(appl.getPagesQuantity().toString()));
								}
								if (appl.getPages() != null || appl.getPagesQuantity() != null) {
									datablockForApplNotCrossed += String.format("%1$s", ")");
								}
								datablockForApplNotCrossed += String.format("<br/>", datablockForApplNotCrossed);
							}
						}
						dataBlock += (datablockForApplCrossed + datablockForApplNotCrossed);
					}
				}

				if (copyapplicationToDocumentModelList != null && copyapplicationToDocumentModelList.size() > 0) {
					for (ApplicationToDocument copyAppl : copyapplicationToDocumentModelList) {
						String datablockForApplCrossed = "";
						datablockForApplCrossed += String.format("&nbsp;&nbsp;&nbsp;<s>%1$s</s>", datablockForApplCrossed);
						if (copyAppl.getApplNumber() != null) {
							datablockForApplCrossed += String.format("<s>%1$s</s>", copyAppl.getApplNumber().concat(". "));
						}
						if (copyAppl.getApplName() != null) {
							datablockForApplCrossed += String.format("<s>%1$s</s>", copyAppl.getApplName().concat(" "));
						}
						if (copyAppl.getPages() != null || copyAppl.getPagesQuantity() != null) {
							datablockForApplCrossed += String.format("<s>%1$s</s>", "(");
						}
						if (copyAppl.getPages() != null) {
							datablockForApplCrossed += String.format("<s>%1$s</s>", "лл. ".concat(copyAppl.getPages()));
						}
						if (copyAppl.getPagesQuantity() != null) {
							if (copyAppl.getPages() != null) {
								datablockForApplCrossed += String.format("<s>%1$s</s>", ", ");
							}
							datablockForApplCrossed += String.format("<s>%1$s</s>", "количество ".concat(copyAppl.getPagesQuantity().toString()));
						}
						if (copyAppl.getPages() != null || copyAppl.getPagesQuantity() != null) {
							datablockForApplCrossed += String.format("<s>%1$s</s>", ")");
						}
						datablockForApplCrossed += String.format("<br/>", datablockForApplCrossed);
						dataBlock += datablockForApplCrossed;

					}
				} else if (sizeOfCopyApplIsNull) {
					dataBlock += formatCrossedValue(empty());
				}
				if (applicationToDocumentModelList != null && applicationToDocumentModelList.size() > 0) {
					for (ApplicationToDocument appl : applicationToDocumentModelList) {
						String datablockForApplNotCrossed = "";
						datablockForApplNotCrossed += String.format("&nbsp;&nbsp;&nbsp;%1$s", datablockForApplNotCrossed);
						if (appl.getApplNumber() != null) {
							datablockForApplNotCrossed += String.format("%1$s", appl.getApplNumber().concat(". "));
						}
						if (appl.getApplName() != null) {
							datablockForApplNotCrossed += String.format("%1$s", appl.getApplName().concat(" "));
						}
						if (appl.getPages() != null || appl.getPagesQuantity() != null) {
							datablockForApplNotCrossed += String.format("%1$s", "(");
						}
						if (appl.getPages() != null) {
							datablockForApplNotCrossed += String.format("%1$s", "лл. ".concat(appl.getPages()));
						}
						if (appl.getPagesQuantity() != null) {
							if (appl.getPages() != null) {
								datablockForApplNotCrossed += String.format("%1$s", ", ");
							}
							datablockForApplNotCrossed += String.format("%1$s", "количество ".concat(appl.getPagesQuantity().toString()));
						}
						if (appl.getPages() != null || appl.getPagesQuantity() != null) {
							datablockForApplNotCrossed += String.format("%1$s", ")");
						}
						datablockForApplNotCrossed += String.format("<br/>", datablockForApplNotCrossed);

						dataBlock += datablockForApplNotCrossed;

					}
				} else if (sizeOfApplIsNull) {
					dataBlock += formatValue(empty());
				}

			}
			applListForDelete.clear();
			copyApplListForDelete.clear();
			applListUpdated.clear();
			copyapplListUpdated.clear();

		}
//</editor-fold>

		boolean sizeOfCardLinksIsNull = false;
		if (cardLinksList.size() == 0) {
			sizeOfCardLinksIsNull = true;
		}
		boolean copySizeOfCardLinksIsNull = false;
		if (copycardLinksList.size() == 0) {
			copySizeOfCardLinksIsNull = true;
		}

		if (cardLinksList.size() >= 0) {
			List<VComplexCardLinks> copycardLinksListForDelete = new ArrayList<VComplexCardLinks>();
			List<VComplexCardLinks> cardLinksListForDelete = new ArrayList<VComplexCardLinks>();

			for (int i = 0; i < copycardLinksList.size(); i++) {
				for (int j = 0; j < cardLinksList.size(); j++) {
					VComplexCardLinks copyCardLink = copycardLinksList.get(i);
					VComplexCardLinks cardLink = cardLinksList.get(j);

					if (safeEquals(copyCardLink.getUnitFullName(), cardLink.getUnitFullName())) {
						copycardLinksListForDelete.add(copyCardLink);
						cardLinksListForDelete.add(cardLink);
					}
				}
			}

			if (copycardLinksListForDelete.size() > 0) {
				for (VComplexCardLinks copyCardLink : copycardLinksListForDelete) {
					copycardLinksList.remove(copyCardLink);
				}

			}
			if (cardLinksListForDelete.size() > 0) {
				for (VComplexCardLinks cardLink : cardLinksListForDelete) {
					cardLinksList.remove(cardLink);
				}

			}

			if (copycardLinksList.size() > 0 || cardLinksList.size() > 0) {
				dataBlock += formatLabel("Ссылки");

				if (copycardLinksList.size() > 0) {
					for (VComplexCardLinks copyCardLink : copycardLinksList) {
						dataBlock += formatCrossedValue(copyCardLink.getUnitFullName());
					}
				} else if (copySizeOfCardLinksIsNull) {
					dataBlock += formatCrossedValue(empty());
				}

				if (cardLinksList.size() > 0) {
					for (VComplexCardLinks cardLink : cardLinksList) {
						dataBlock += formatValue(cardLink.getUnitFullName());
					}
				} else if (sizeOfCardLinksIsNull) {
					dataBlock += formatValue(empty());
				}
			}
			cardLinksListForDelete.clear();
			copycardLinksListForDelete.clear();
		}

		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null
					? empty() : model.getAnnotation());
		}
		if (!safeEquals(copyDocumentModel.getBaseMaterialId(), documentModel.getBaseMaterialId())) {
			dataBlock += formatLabel("Основа (материалы)");
			dataBlock += formatCrossedValue(copyDocumentModel.getBaseMaterialId() == null
					? empty() : copyDocumentImmModel.getBaseMaterial());
			dataBlock += formatValue(documentModel.getBaseMaterialId() == null
					? empty() : documentImmModel.getBaseMaterial());
		}
		if (!safeEquals(copyDocumentModel.getReproductionTypeId(), documentModel.getReproductionTypeId())) {
			dataBlock += formatLabel("Способ воспроизведения");
			dataBlock += formatCrossedValue(copyDocumentModel.getReproductionTypeId() == null
					? empty() : copyDocumentImmModel.getReproductionType());
			dataBlock += formatValue(documentModel.getReproductionTypeId() == null
					? empty() : documentImmModel.getReproductionType());
		}

		boolean langChanged = false;
		if (copyModel.getLanguages().size() != model.getLanguages().size()) {
			langChanged = true;
		} else {
			for (int i = 0; i < copyModel.getLanguages().size(); i++) {
				UnivDataLanguage lang1 = copyModel.getLanguages().get(i);
				UnivDataLanguage lang2 = model.getLanguages().get(i);
				if (!safeEquals(lang1.getDocLanguageId(), lang2.getDocLanguageId())) {
					langChanged = true;
					break;
				}
			}
		}
		if (langChanged) {
			dataBlock += formatLabel("Язык документов");
			String langs = "";
			for (UnivDataLanguage lang : copyModel.getLanguages()) {
				if (!langs.isEmpty()) {
					langs += ", ";
				}
				langs += se.getDescValue(lang.getDocLanguageId()).getFullValue();
			}
			dataBlock += formatCrossedValue(langs);
			langs = "";
			for (UnivDataLanguage lang : model.getLanguages()) {
				if (!langs.isEmpty()) {
					langs += ", ";
				}
				langs += se.getDescValue(lang.getDocLanguageId()).getFullValue();
			}
			dataBlock += formatValue(langs);
		}

		if (!safeEquals(copyDocumentModel.getAuthenticityTypeId(), documentModel.getAuthenticityTypeId())) {
			dataBlock += formatLabel("Подлинность");
			dataBlock += formatCrossedValue(copyDocumentModel.getAuthenticityTypeId() == null
					? empty() : copyDocumentImmModel.getAuthenticityType());
			dataBlock += formatValue(documentModel.getAuthenticityTypeId() == null
					? empty() : documentImmModel.getAuthenticityType());
		}
		if (!safeEquals(copyDocumentModel.getNotes(), documentModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copyDocumentModel.getNotes() == null
					? empty() : copyDocumentModel.getNotes());
			dataBlock += formatValue(documentModel.getNotes() == null
					? empty() : documentModel.getNotes());
		}
		if (copyModel.isAccessLimited() != model.isAccessLimited()) {
			if (copyModel.isAccessLimited()) {
				dataBlock += formatCrossedLabel("Имеются ограничения к использованию");
			} else {
				dataBlock += formatLabel("Имеются ограничения к использованию");
			}
		}

		recordUnivDataUnit(model, "EDIT",
				se.getImmDescValueByCodes("CHANGED_ENTITY_TYPE", "DOCUMENT").getDescriptorValueId(),
				dataBlock);
	}

	public void recordEditEbook(UnivDataUnit model, ArchEbook ebookModel, VEbook ebookImmModel) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(copyEbookModel.getUdc(), ebookModel.getUdc())) {
			dataBlock += formatLabel("УДК");
			dataBlock += formatCrossedValue(copyEbookModel.getUdc() == null
					? empty() : copyEbookModel.getUdc());
			dataBlock += formatValue(ebookModel.getUdc() == null
					? empty() : ebookModel.getUdc());
		}
		if (!safeEquals(copyEbookModel.getBBC(), ebookModel.getBBC())) {
			dataBlock += formatLabel("ББК");
			dataBlock += formatCrossedValue(copyEbookModel.getBBC() == null
					? empty() : copyEbookModel.getBBC());
			dataBlock += formatValue(ebookModel.getBBC() == null
					? empty() : ebookModel.getBBC());
		}
		if (!safeEquals(copyEbookModel.getIsbn(), ebookModel.getIsbn())) {
			dataBlock += formatLabel("ISBN");
			dataBlock += formatCrossedValue(copyEbookModel.getIsbn() == null
					? empty() : copyEbookModel.getIsbn());
			dataBlock += formatValue(ebookModel.getIsbn() == null
					? empty() : ebookModel.getIsbn());
		}
		if (!safeEquals(copyEbookModel.getEditionTypeId(), ebookModel.getEditionTypeId())) {
			dataBlock += formatLabel("Вид издания");
			dataBlock += formatCrossedValue(copyEbookImmModel.getEditionType());
			dataBlock += formatValue(ebookImmModel.getEditionType());
		}
		if (!safeEquals(copyEbookImmModel.getLanguageName(), ebookImmModel.getLanguageName())) {
			dataBlock += formatLabel("Язык");
			dataBlock += formatCrossedValue(copyModel.getLanguages().size() == 0
					? empty() : copyEbookImmModel.getLanguageName());
			dataBlock += formatValue(model.getLanguages() == null || model.getLanguages().size() == 0
					? empty() : ebookImmModel.getLanguageName());
		}
		if (!safeEquals(copyEbookImmModel.getAuthors(), ebookImmModel.getAuthors())) {
			dataBlock += formatLabel("Автор");
			dataBlock += formatCrossedValue(copyEbookModel.getAuthors().size() == 0
					? empty() : copyEbookImmModel.getAuthors());
			dataBlock += formatValue(ebookModel.getAuthors().size() == 0
					? empty() : ebookImmModel.getAuthors());
		}
		if (!safeEquals(copyEbookModel.getEditor(), ebookModel.getEditor())) {
			dataBlock += formatLabel("Редактор");
			dataBlock += formatCrossedValue(copyEbookModel.getEditor() == null
					? empty() : copyEbookModel.getEditor());
			dataBlock += formatValue(ebookModel.getEditor() == null
					? empty() : ebookModel.getEditor());
		}
		if (!safeEquals(copyEbookModel.getEditorialBoard(), ebookModel.getEditorialBoard())) {
			dataBlock += formatLabel("Редколлегия");
			dataBlock += formatCrossedValue(copyEbookModel.getEditorialBoard() == null
					? empty() : copyEbookModel.getEditorialBoard());
			dataBlock += formatValue(ebookModel.getEditorialBoard() == null
					? empty() : ebookModel.getEditorialBoard());
		}
		if (!safeEquals(copyEbookModel.getCompilers(), ebookModel.getCompilers())) {
			dataBlock += formatLabel("Составители");
			dataBlock += formatCrossedValue(copyEbookModel.getCompilers() == null
					? empty() : copyEbookModel.getCompilers());
			dataBlock += formatValue(ebookModel.getCompilers() == null
					? empty() : ebookModel.getCompilers());
		}
		if (!safeEquals(copyModel.getUnitName(), model.getUnitName())) {
			dataBlock += formatLabel("Название издания");
			dataBlock += formatCrossedValue(copyModel.getUnitName() == null
					? empty() : copyModel.getUnitName());
			dataBlock += formatValue(model.getUnitName() == null
					? empty() : model.getUnitName());
		}
		if (!safeEquals(copyEbookModel.getVolume1Info(), ebookModel.getVolume1Info())) {
			dataBlock += formatLabel("Том");
			dataBlock += formatCrossedValue(copyEbookModel.getVolume1Info() == null
					? empty() : copyEbookModel.getVolume1Info());
			dataBlock += formatValue(ebookModel.getVolume1Info() == null
					? empty() : ebookModel.getVolume1Info());
		}
		if (!safeEquals(copyEbookModel.getVolume2Info(), ebookModel.getVolume2Info())) {
			dataBlock += formatLabel("Книга");
			dataBlock += formatCrossedValue(copyEbookModel.getVolume2Info() == null
					? empty() : copyEbookModel.getVolume2Info());
			dataBlock += formatValue(ebookModel.getVolume2Info() == null
					? empty() : ebookModel.getVolume2Info());
		}
		if (!safeEquals(copyEbookModel.getAtoptitleData(), ebookModel.getAtoptitleData())) {
			dataBlock += formatLabel("Надзаголовочные данные");
			dataBlock += formatCrossedValue(copyEbookModel.getAtoptitleData() == null
					? empty() : copyEbookModel.getAtoptitleData());
			dataBlock += formatValue(ebookModel.getAtoptitleData() == null
					? empty() : ebookModel.getAtoptitleData());
		}

		String copySeriesName = copyEbookImmModel.getSeries() == null ? null : copyEbookImmModel.getSeries().getSeriesName();
		String seriesName = ebookImmModel.getSeries() == null ? null : ebookImmModel.getSeries().getSeriesName();
		if (!safeEquals(copySeriesName, seriesName)) {
			dataBlock += formatLabel("Название серии");
			dataBlock += formatCrossedValue(copySeriesName == null ? empty() : copySeriesName);
			dataBlock += formatValue(seriesName == null ? empty() : seriesName);
		}
		String copySeriesInfo = copyEbookImmModel.getSeries() == null ? null : copyEbookImmModel.getSeries().getSeriesInfo();
		String seriesInfo = ebookImmModel.getSeries() == null ? null : ebookImmModel.getSeries().getSeriesInfo();
		if (!safeEquals(copySeriesInfo, seriesInfo)) {
			dataBlock += formatLabel("Другие сведения о серии");
			dataBlock += formatCrossedValue(copySeriesInfo == null ? empty() : copySeriesInfo);
			dataBlock += formatValue(seriesInfo == null ? empty() : seriesInfo);
		}

		if (!safeEquals(copyEbookModel.getEditionNumber(), ebookModel.getEditionNumber())) {
			dataBlock += formatLabel("Номер выпуска");
			dataBlock += formatCrossedValue(copyEbookModel.getEditionNumber() == null
					? empty() : copyEbookModel.getEditionNumber());
			dataBlock += formatValue(ebookModel.getEditionNumber() == null
					? empty() : ebookModel.getEditionNumber());
		}
		if (!safeEquals(copyEbookModel.getEditionNote(), ebookModel.getEditionNote())) {
			dataBlock += formatLabel("Сведения об издании");
			dataBlock += formatCrossedValue(copyEbookModel.getEditionNote() == null
					? empty() : copyEbookModel.getEditionNote());
			dataBlock += formatValue(ebookModel.getEditionNote() == null
					? empty() : ebookModel.getEditionNote());
		}
		if (!safeEquals(copyEbookModel.getPublishingPlace(), ebookModel.getPublishingPlace())) {
			dataBlock += formatLabel("Место издания");
			dataBlock += formatCrossedValue(copyEbookModel.getPublishingPlace() == null
					? empty() : copyEbookModel.getPublishingPlace());
			dataBlock += formatValue(ebookModel.getPublishingPlace() == null
					? empty() : ebookModel.getPublishingPlace());
		}
		if (!safeEquals(copyEbookModel.getPublisher(), ebookModel.getPublisher())) {
			dataBlock += formatLabel("Название издательства");
			dataBlock += formatCrossedValue(copyEbookModel.getPublisher() == null
					? empty() : copyEbookModel.getPublisher());
			dataBlock += formatValue(ebookModel.getPublisher() == null
					? empty() : ebookModel.getPublisher());
		}
		if (!safeEquals(copyEbookModel.getPublishingYear(), ebookModel.getPublishingYear())) {
			dataBlock += formatLabel("Год издания");
			dataBlock += formatCrossedValue(copyEbookModel.getPublishingYear() == null
					? empty() : copyEbookModel.getPublishingYear().toString());
			dataBlock += formatValue(ebookModel.getPublishingYear() == null
					? empty() : ebookModel.getPublishingYear().toString());
		}
		if (!safeEquals(copyEbookModel.getEditionSize(), ebookModel.getEditionSize())) {
			dataBlock += formatLabel("Объём издания");
			dataBlock += formatCrossedValue(copyEbookModel.getEditionSize() == null
					? empty() : copyEbookModel.getEditionSize());
			dataBlock += formatValue(ebookModel.getEditionSize() == null
					? empty() : ebookModel.getEditionSize());
		}
		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null
					? empty() : model.getAnnotation());
		}
		if (!safeEquals(copyEbookModel.getNotes(), ebookModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copyEbookModel.getNotes() == null
					? empty() : copyEbookModel.getNotes());
			dataBlock += formatValue(ebookModel.getNotes() == null
					? empty() : ebookModel.getNotes());
		}
		if (!safeEquals(copyEbookModel.getExternalUrl(), ebookModel.getExternalUrl())) {
			dataBlock += formatLabel("Ссылка на текст книги");
			dataBlock += formatCrossedValue(copyEbookModel.getExternalUrl() == null
					? empty() : copyEbookModel.getExternalUrl());
			dataBlock += formatValue(ebookModel.getExternalUrl() == null
					? empty() : ebookModel.getExternalUrl());
		}

		recordUnivDataUnit(model, "EDIT",
				se.getImmDescValueByCodes("CHANGED_ENTITY_TYPE", "EBOOK").getDescriptorValueId(),
				dataBlock);
	}

	public void recordEditPhonodoc(UnivDataUnit model, VComplexDataUnit immModel,
			ArchPhonodoc phonodocModel, VComplexPhonodoc phonodocImmModel) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(copyImmModel.getFundNum(), immModel.getFundNum())
				|| !safeEquals(copyImmModel.getFundLetter(), immModel.getFundLetter())) {
			dataBlock += formatLabel("Фонд");
			dataBlock += formatCrossedValue(copyImmModel.getFundNum() == null ? empty()
					: copyImmModel.getFundNum().toString()
					+ (copyImmModel.getFundLetter() == null ? "" : copyImmModel.getFundLetter()));
			dataBlock += formatValue(immModel.getFundNum() == null ? empty()
					: immModel.getFundNum().toString()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (!safeEquals(copyImmModel.getSeriesNum(), immModel.getSeriesNum())
				|| !safeEquals(copyImmModel.getSeriesLetter(), immModel.getSeriesLetter())) {
			dataBlock += formatLabel("Опись");
			dataBlock += formatCrossedValue(copyImmModel.getSeriesNum() == null ? empty()
					: copyImmModel.getSeriesNum().toString()
					+ (copyImmModel.getSeriesLetter() == null ? "" : copyImmModel.getSeriesLetter()));
			dataBlock += formatValue(immModel.getSeriesNum() == null ? empty()
					: immModel.getSeriesNum().toString()
					+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		}
		if (!safeEquals(copyModel.getNumberNumber(), model.getNumberNumber())
				|| !safeEquals(copyModel.getNumberText(), model.getNumberText())) {
			dataBlock += formatLabel("Единица учёта");
			dataBlock += formatCrossedValue(copyModel.getNumberNumber().toString()
					+ (copyModel.getNumberText() == null ? "" : copyModel.getNumberText()));
			dataBlock += formatValue(model.getNumberNumber().toString()
					+ (model.getNumberText() == null ? "" : model.getNumberText()));
		}
		if (!safeEquals(copyPhonodocModel.getRecordTypeId(), phonodocModel.getRecordTypeId())) {
			dataBlock += formatLabel("Вид записанного материала");
			dataBlock += formatCrossedValue(copyPhonodocImmModel.getRecordType());
			dataBlock += formatValue(phonodocImmModel.getRecordType());
		}
		if (!safeEquals(copyModel.getPortalSectionId(), model.getPortalSectionId())) {
			dataBlock += formatLabel("Тематический раздел портала");
			dataBlock += formatCrossedValue(copyModel.getPortalSectionId() == null
					? empty() : copyImmModel.getPortalSection());
			dataBlock += formatValue(model.getPortalSectionId() == null
					? empty() : immModel.getPortalSection());
		}
		if (!safeEquals(copyPhonodocModel.getPhonodocTypeId(), phonodocModel.getPhonodocTypeId())) {
			dataBlock += formatLabel("Вид фонодокумента");
			dataBlock += formatCrossedValue(copyPhonodocImmModel.getPhonodocType());
			dataBlock += formatValue(phonodocImmModel.getPhonodocType());
		}
		if (copyPhonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")
				|| phonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
			if (!safeEquals(copyPhonodocModel.getRecordTypeId(), phonodocModel.getRecordTypeId())
					|| !safeEquals(copyPhonodocModel.getPhonodocName(), phonodocModel.getPhonodocName())) {
				dataBlock += formatLabel("Название документа");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getPhonodocName());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
					dataBlock += formatValue(phonodocModel.getPhonodocName());
				}
			}
			if (!safeEquals(copyPhonodocModel.getRecordTypeId(), phonodocModel.getRecordTypeId())
					|| !safeEquals(copyPhonodocModel.getAuthors(), phonodocModel.getAuthors())) {
				dataBlock += formatLabel("Автор");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getAuthors());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
					dataBlock += formatValue(phonodocModel.getAuthors());
				}
			}
			if (!safeEquals(copyPhonodocModel.getEventPlace(), phonodocModel.getEventPlace())) {
				dataBlock += formatLabel("Место события");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getEventPlace());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("DOCUMENTARY")) {
					dataBlock += formatValue(phonodocModel.getEventPlace());
				}
			}
		}
		if (copyPhonodocImmModel.getRecordTypeCode().equals("ARTISTIC")
				|| phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
			if (!safeEquals(copyPhonodocModel.getRecordTypeId(), phonodocModel.getRecordTypeId())
					|| !safeEquals(copyPhonodocModel.getPhonodocName(), phonodocModel.getPhonodocName())) {
				dataBlock += formatLabel("Название произведения");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getPhonodocName());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatValue(phonodocModel.getPhonodocName());
				}
			}
			if (!safeEquals(copyPhonodocModel.getPerformerOrchestra(), phonodocModel.getPerformerOrchestra())) {
				dataBlock += formatLabel("Исполнитель (оркестр)");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getPerformerOrchestra() == null
							? empty() : copyPhonodocModel.getPerformerOrchestra());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatValue(phonodocModel.getPerformerOrchestra() == null
							? empty() : phonodocModel.getPerformerOrchestra());
				}
			}
			if (!safeEquals(copyPhonodocModel.getPerformerChoir(), phonodocModel.getPerformerChoir())) {
				dataBlock += formatLabel("Исполнитель (хор)");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getPerformerChoir() == null
							? empty() : copyPhonodocModel.getPerformerChoir());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatValue(phonodocModel.getPerformerChoir() == null
							? empty() : phonodocModel.getPerformerChoir());
				}
			}
			if (!safeEquals(copyPhonodocModel.getPerformers(), phonodocModel.getPerformers())) {
				dataBlock += formatLabel("Исполнители");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getPerformers() == null
							? empty() : copyPhonodocModel.getPerformers());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatValue(phonodocModel.getPerformers() == null
							? empty() : phonodocModel.getPerformers());
				}
			}
			if (!safeEquals(copyPhonodocModel.getRecordTypeId(), phonodocModel.getRecordTypeId())
					|| !safeEquals(copyPhonodocModel.getAuthors(), phonodocModel.getAuthors())) {
				dataBlock += formatLabel("Авторы");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getAuthors());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatValue(phonodocModel.getAuthors());
				}
			}
			if (!safeEquals(copyPhonodocModel.getRubrics(), phonodocModel.getRubrics())) {
				dataBlock += formatLabel("Рубрика");
				if (copyPhonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatCrossedValue(copyPhonodocModel.getRubrics() == null
							? empty() : copyPhonodocModel.getRubrics());
				}
				if (phonodocImmModel.getRecordTypeCode().equals("ARTISTIC")) {
					dataBlock += formatValue(phonodocModel.getRubrics() == null
							? empty() : phonodocModel.getRubrics());
				}
			}
		}
		if (!safeEquals(copyPhonodocImmModel.getStorageUnitCount(), phonodocImmModel.getStorageUnitCount())) {
			dataBlock += formatLabel("Количество единиц хранения");
			dataBlock += formatCrossedValue(copyPhonodocImmModel.getStorageUnitCount().toString());
			dataBlock += formatValue(phonodocImmModel.getStorageUnitCount().toString());
		}

		boolean found;
		String suBlock = "";
		Integer copyTotalPlaytime = 0, totalPlaytime = 0;
		for (VComplexPhonoStorageUnit copyStorageUnit : copyPhonodocImmModel.getStorageUnits()) {
			found = false;
			for (VComplexPhonoStorageUnit storageUnit : phonodocImmModel.getStorageUnits()) {
				if (safeEquals(copyStorageUnit.getNumberNumber(), storageUnit.getNumberNumber())
						&& safeEquals(copyStorageUnit.getNumberText(), storageUnit.getNumberText())) {
					List<String> changes = Lists.newArrayList();
					if (!safeEquals(copyStorageUnit.getPlaytime(), storageUnit.getPlaytime())) {
						changes.add(String.format("время - <s>%1$d:%2$02d</s> —> %3$d:%4$02d",
								copyStorageUnit.getPlaytime() / 60, copyStorageUnit.getPlaytime() % 60,
								storageUnit.getPlaytime() / 60, storageUnit.getPlaytime() % 60));
					}
					if (!safeEquals(copyStorageUnit.getStorageType(), storageUnit.getStorageType())) {
						changes.add(String.format("<s>%1$s</s> —> %2$s",
								copyStorageUnit.getStorageType(), storageUnit.getStorageType()));
					}
					if (!safeEquals(copyStorageUnit.getChannelCount(), storageUnit.getChannelCount())) {
						changes.add(String.format("<s>%1$s</s> —> %2$s",
								(copyStorageUnit.getChannelCount() == null ? empty() : copyStorageUnit.getChannelCount()),
								(storageUnit.getChannelCount() == null ? empty() : storageUnit.getChannelCount())));
					}
					if (!safeEquals(copyStorageUnit.getSpeed(), storageUnit.getSpeed())) {
						changes.add(String.format("скорость - <s>%1$s</s> —> %2$s",
								(copyStorageUnit.getSpeed() == null ? empty() : copyStorageUnit.getSpeed().toString()),
								(storageUnit.getSpeed() == null ? empty() : storageUnit.getSpeed().toString())));
					}
					if (!safeEquals(copyStorageUnit.getSoundQuality(), storageUnit.getSoundQuality())) {
						changes.add(String.format("качество - <s>%1$s</s> —> %2$s",
								(copyStorageUnit.getSoundQuality() == null ? empty() : copyStorageUnit.getSoundQuality()),
								(storageUnit.getSoundQuality() == null ? empty() : storageUnit.getSoundQuality())));
					}
					if (!changes.isEmpty()) {
						suBlock += formatValue(storageUnit.getNumberNumber().toString()
								+ (storageUnit.getNumberText() == null ? "" : storageUnit.getNumberText())
								+ " (" + StringUtils.uniteString(changes, ", ") + ")");
					}
					found = true;
					break;
				}
			}
			if (!found) {
				suBlock += formatCrossedValue(copyStorageUnit.getNumberNumber().toString()
						+ (copyStorageUnit.getNumberText() == null ? "" : copyStorageUnit.getNumberText())
						+ " " + getPhonoStorageUnitData(copyStorageUnit));
			}
			copyTotalPlaytime += copyStorageUnit.getPlaytime();
		}
		for (VComplexPhonoStorageUnit storageUnit : phonodocImmModel.getStorageUnits()) {
			found = false;
			for (VComplexPhonoStorageUnit copyStorageUnit : copyPhonodocImmModel.getStorageUnits()) {
				if (safeEquals(copyStorageUnit.getNumberNumber(), storageUnit.getNumberNumber())
						&& safeEquals(copyStorageUnit.getNumberText(), storageUnit.getNumberText())) {
					found = true;
					break;
				}
			}
			if (!found) {
				suBlock += formatValue(storageUnit.getNumberNumber().toString()
						+ (storageUnit.getNumberText() == null ? "" : storageUnit.getNumberText())
						+ " " + getPhonoStorageUnitData(storageUnit));
			}
			totalPlaytime += storageUnit.getPlaytime();
		}
		if (!suBlock.isEmpty()) {
			dataBlock += formatLabel("Единицы хранения");
			dataBlock += suBlock;
		}

		if (!safeEquals(copyPhonodocImmModel.getEventDateStr(), phonodocImmModel.getEventDateStr())) {
			dataBlock += formatLabel("Дата события");
			dataBlock += formatCrossedValue(copyPhonodocImmModel.getEventDateStr());
			dataBlock += formatValue(phonodocImmModel.getEventDateStr());
		}
		if (!safeEquals(copyPhonodocModel.getRecordDate(), phonodocModel.getRecordDate())) {
			dataBlock += formatLabel("Дата записи");
			dataBlock += formatCrossedValue(copyPhonodocImmModel.getRecordDateStr());
			dataBlock += formatValue(phonodocImmModel.getRecordDateStr());
		}
		if (!safeEquals(copyPhonodocModel.getRewriteDate(), phonodocModel.getRewriteDate())) {
			dataBlock += formatLabel("Дата перезаписи");
			dataBlock += formatCrossedValue(copyPhonodocModel.getRewriteDate() == null
					? empty() : copyPhonodocImmModel.getRewriteDateStr());
			dataBlock += formatValue(phonodocModel.getRewriteDate() == null
					? empty() : phonodocImmModel.getRewriteDateStr());
		}
		if (!safeEquals(copyTotalPlaytime, totalPlaytime)) {
			dataBlock += formatLabel("Время звучания");
			dataBlock += formatCrossedValue(String.format("%1$d:%2$02d",
					copyTotalPlaytime / 60, copyTotalPlaytime % 60));
			dataBlock += formatValue(String.format("%1$d:%2$02d", totalPlaytime / 60, totalPlaytime % 60));
		}
		if (!safeEquals(copyPhonodocModel.getKitInfo(), phonodocModel.getKitInfo())) {
			dataBlock += formatLabel("Состав комплекта");
			dataBlock += formatCrossedValue(copyPhonodocModel.getKitInfo() == null
					? empty() : copyPhonodocModel.getKitInfo());
			dataBlock += formatValue(phonodocModel.getKitInfo() == null
					? empty() : phonodocModel.getKitInfo());
		}
		if (!safeEquals(copyPhonodocModel.getManufacturerName(), phonodocModel.getManufacturerName())) {
			dataBlock += formatLabel("Наименование изготовителя");
			dataBlock += formatCrossedValue(copyPhonodocModel.getManufacturerName() == null
					? empty() : copyPhonodocModel.getManufacturerName());
			dataBlock += formatValue(phonodocModel.getManufacturerName() == null
					? empty() : phonodocModel.getManufacturerName());
		}
		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация (реферат)");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null
					? empty() : model.getAnnotation());
		}
		if (!safeEquals(copyPhonodocModel.getNotes(), phonodocModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copyPhonodocModel.getNotes() == null
					? empty() : copyPhonodocModel.getNotes());
			dataBlock += formatValue(phonodocModel.getNotes() == null
					? empty() : phonodocModel.getNotes());
		}
		dataBlock += getUnivClassifierChanges(
				copyImmModel.getClassifierValues(), immModel.getClassifierValues());

		String annBlock = "";
		for (VComplexPerformAnnotation copyAnnotation : copyPhonodocImmModel.getAnnotations()) {
			found = false;
			for (VComplexPerformAnnotation annotation : phonodocImmModel.getAnnotations()) {
				if (safeEquals(copyAnnotation.getPerformanceNumber(), annotation.getPerformanceNumber())) {
					List<String> changes = Lists.newArrayList();
					if (!safeEquals(copyAnnotation.getStorageUnitNumber(), annotation.getStorageUnitNumber())) {
						changes.add(String.format("ед.хр. - <s>%1$s</s> —> %2$s",
								copyAnnotation.getStorageUnitNumber(), annotation.getStorageUnitNumber()));
					}
					if (!safeEquals(copyAnnotation.getSideOrTrack(), annotation.getSideOrTrack())) {
						changes.add(String.format("сторона/трек - <s>%1$s</s> —> %2$s",
								(copyAnnotation.getSideOrTrack() == null ? empty() : copyAnnotation.getSideOrTrack()),
								(annotation.getSideOrTrack() == null ? empty() : annotation.getSideOrTrack())));
					}
					if (!safeEquals(copyAnnotation.getSpokesperson(), annotation.getSpokesperson())) {
						changes.add(String.format("докладчик - <s>%1$s</s> —> %2$s",
								(copyAnnotation.getSpokesperson() == null ? empty() : copyAnnotation.getSpokesperson()),
								(annotation.getSpokesperson() == null ? empty() : annotation.getSpokesperson())));
					}
					if (!safeEquals(copyAnnotation.getRecordLanguage(), annotation.getRecordLanguage())) {
						changes.add(String.format("язык - <s>%1$s</s> —> %2$s",
								(copyAnnotation.getRecordLanguage() == null ? empty() : copyAnnotation.getRecordLanguage()),
								(annotation.getRecordLanguage() == null ? empty() : annotation.getRecordLanguage())));
					}
					if (!safeEquals(copyAnnotation.getPlaytime(), annotation.getPlaytime())) {
						changes.add(String.format("время - <s>%1$d:%2$02d</s> —> %3$d:%4$02d",
								copyAnnotation.getPlaytime() / 60, copyAnnotation.getPlaytime() % 60,
								annotation.getPlaytime() / 60, annotation.getPlaytime() % 60));
					}
					if (!changes.isEmpty()) {
						annBlock += formatValue(annotation.getPerformanceNumber().toString() + ") "
								+ StringUtils.uniteString(changes, ", "));
					}

					if (!safeEquals(copyAnnotation.getAnnotation(), annotation.getAnnotation())) {
						annBlock += formatValue((changes.isEmpty()
								? String.format("%1$s) ", annotation.getPerformanceNumber()) : "&nbsp;&nbsp;&nbsp;")
								+ String.format("<s>%1$s</s> —> %2$s", copyAnnotation.getAnnotation(),
										annotation.getAnnotation()));
					}
					found = true;
					break;
				}
			}
			if (!found) {
				annBlock += formatCrossedValue(String.format("%1$d) %2$s",
						copyAnnotation.getPerformanceNumber(), getPerformanceAnnotationData(copyAnnotation)));
				annBlock += "&nbsp;&nbsp;&nbsp;" + formatCrossedValue(copyAnnotation.getAnnotation());
			}
		}
		for (VComplexPerformAnnotation annotation : phonodocImmModel.getAnnotations()) {
			found = false;
			for (VComplexPerformAnnotation copyAnnotation : copyPhonodocImmModel.getAnnotations()) {
				if (safeEquals(copyAnnotation.getPerformanceNumber(), annotation.getPerformanceNumber())) {
					found = true;
					break;
				}
			}
			if (!found) {
				annBlock += formatValue(String.format("%1$d) %2$s",
						annotation.getPerformanceNumber(), getPerformanceAnnotationData(annotation)));
				annBlock += formatValue("&nbsp;&nbsp;&nbsp;" + annotation.getAnnotation());
			}
		}
		if (!annBlock.isEmpty()) {
			dataBlock += formatLabel("Аннотации выступлений") + annBlock;
		}

		recordUnivDataUnit(model, "EDIT",
				se.getDescValueByCodes("CHANGED_ENTITY_TYPE", "PHONODOC").getDescriptorValueId(),
				dataBlock);
	}

	protected String getUnivClassifierChanges(List<VComplexUnivClassifier> copyUnivClassifierValues,
			List<VComplexUnivClassifier> univClassifierValues) {
		boolean found;
		String dataBlock = "";
		for (VComplexUnivClassifier copyUnivClassifier : copyUnivClassifierValues) {
			found = false;
			for (VComplexUnivClassifier univClassifier : univClassifierValues) {
				if (safeEquals(copyUnivClassifier.getDescriptorValueId(), univClassifier.getDescriptorValueId())) {
					found = true;
					break;
				}
			}
			if (!found) {
				dataBlock += formatCrossedValue(String.format("%1$s %2$s",
						copyUnivClassifier.getValueIndex(), copyUnivClassifier.getFullValue()));
			}
		}
		for (VComplexUnivClassifier univClassifier : univClassifierValues) {
			found = false;
			for (VComplexUnivClassifier copyUnivClassifier : copyUnivClassifierValues) {
				if (safeEquals(copyUnivClassifier.getDescriptorValueId(), univClassifier.getDescriptorValueId())) {
					found = true;
					break;
				}
			}
			if (!found) {
				dataBlock += formatValue(String.format("%1$s %2$s",
						univClassifier.getValueIndex(), univClassifier.getFullValue()));
			}
		}
		if (!dataBlock.isEmpty()) {
			dataBlock = formatLabel("Тематический классификатор") + dataBlock;
		}
		return dataBlock;
	}

	public void recordChangeUnivDescriptor(UnivDataUnit model, String operation, Long univDescriptorId) {
		VDescAttrvalueWithCode changedEntity = se.getAttrValueByCode(model.getUnitTypeId(), "CHANGED_ENTITY_TYPE");
		if (changedEntity == null) {
			return;
		}

		String dataBlock = formatHeader("Редактируемые значения");
		dataBlock += formatLabel("Ключевые слова");
		if (operation.equals("EDIT") || operation.equals("DELETE")) {
			HasDescriptors descContainer;
			if (copyImmModel != null) {
				descContainer = complexQuery.queryImmModel(model.getUnivDataUnitId());
			} else {
				descContainer = ebookQuery.queryEbookImmModel(model.getUnivDataUnitId());
			}
			for (VComplexDescriptor copyDesc : descContainer.getDescriptors()) {
				if (copyDesc.getUnivDescriptorId().equals(univDescriptorId)) {
					dataBlock += formatCrossedValue(copyDesc.getDescriptor1()
							+ (copyDesc.getDescriptorRelationId() == null ? "" : " &gt;&gt; "
							+ copyDesc.getDescRelation() + " &gt;&gt; "
							+ copyDesc.getDescriptor2()));
					break;
				}
			}
		}
		if (operation.equals("ADD") || operation.equals("EDIT")) {
			VComplexDescriptor desc = complexQuery.queryImmUnivDescriptor(univDescriptorId);
			dataBlock += formatValue(desc.getDescriptor1()
					+ (desc.getDescriptorRelationId() == null ? "" : " &gt;&gt; "
					+ desc.getDescRelation() + " &gt;&gt; " + desc.getDescriptor2()));
		}

		recordUnivDataUnit(model, "EDIT", changedEntity.getRefDescriptorValueId(), dataBlock);
	}
	// Моя реализация - если сохранять не сразу после добавления дескриптора, а после нажатия вверху кнопки сохранить
//    public void recordChangeUnivDescriptor(UnivDataUnit model, String operation, List<UnivDescriptor> descriptorsList, Long univDescriptorId) {
//    VDescAttrvalueWithCode changedEntity = se.getAttrValueByCode(model.getUnitTypeId(), "CHANGED_ENTITY_TYPE");
//    if (changedEntity == null) {
//      return;
//    }
//
//    String dataBlock = formatHeader("Редактируемые значения");
//    dataBlock += formatLabel("Ключевые слова");
//    if (operation.equals("EDIT") || operation.equals("DELETE")) {
//      HasDescriptors descContainer;
//      if (copyImmModel != null) {
//        descContainer = copyImmModel;
//      } else {
//        descContainer = copyEbookImmModel;
//      }
//
//      if (descriptorsList != null) {
//        for (UnivDescriptor descriptorForDelete : descriptorsList) {
//          for (VComplexDescriptor copyDesc : descContainer.getDescriptors()) {
//            if (copyDesc.getUnivDescriptorId().equals(descriptorForDelete.getUnivDescriptorId())) {
//              dataBlock += formatCrossedValue(copyDesc.getDescriptor1()
//                      + (copyDesc.getDescriptorRelationId() == null ? "" : " &gt;&gt; "
//                              + copyDesc.getDescRelation() + " &gt;&gt; "
//                              + copyDesc.getDescriptor2()));
//              break;
//            }
//          }
//        }
//      }
//      if (univDescriptorId != null) {
//        for (VComplexDescriptor copyDesc : descContainer.getDescriptors()) {
//          if (copyDesc.getUnivDescriptorId().equals(univDescriptorId)) {
//            dataBlock += formatCrossedValue(copyDesc.getDescriptor1()
//                    + (copyDesc.getDescriptorRelationId() == null ? "" : " &gt;&gt; "
//                            + copyDesc.getDescRelation() + " &gt;&gt; "
//                            + copyDesc.getDescriptor2()));
//            break;
//          }
//        }
//      }
//
//    }
//    if (operation.equals("ADD") || operation.equals("EDIT")) {
//      if (descriptorsList != null) {
//        for (UnivDescriptor descriptorForAdd : descriptorsList) {
//          VComplexDescriptor desc = complexQuery.queryImmUnivDescriptor(descriptorForAdd.getUnivDescriptorId());
//          dataBlock += formatValue(desc.getDescriptor1()
//                  + (desc.getDescriptorRelationId() == null ? "" : " &gt;&gt; "
//                          + desc.getDescRelation() + " &gt;&gt; " + desc.getDescriptor2()));
//        }
//      }
//      if (univDescriptorId != null) {
//        VComplexDescriptor desc = complexQuery.queryImmUnivDescriptor(univDescriptorId);
//        dataBlock += formatValue(desc.getDescriptor1()
//                + (desc.getDescriptorRelationId() == null ? "" : " &gt;&gt; "
//                        + desc.getDescRelation() + " &gt;&gt; " + desc.getDescriptor2()));
//
//      }
//    }
//    recordUnivDataUnit(model, "EDIT", changedEntity.getRefDescriptorValueId(), dataBlock);
//  }

	public void recordAttachFiles(Long unitId, String category, List<ImagePath> filesAttached, Long subId) {
		UnivDataUnit unit = complexQuery.queryModel(unitId);
		Long changedEntity = se.getAttrValueByCode(
				unit.getUnitTypeId(), "CHANGED_ENTITY_TYPE").getRefDescriptorValueId();
		if (filesAttached.size() == 0) {
			return;
		}

		String dataBlock;
		if (category.equals("AUDIO_UNIT") || category.equals("AUDIO_ANNOTATION")) {
			dataBlock = formatHeader("Прикрепление аудиофайла");
			if (category.equals("AUDIO_UNIT")) {
				ArchPhonoStorageUnit storageUnit = em.find(ArchPhonoStorageUnit.class, subId);
				dataBlock += formatLabel("Единица хранения №" + storageUnit.getNumberNumber().toString()
						+ (storageUnit.getNumberText() == null ? "" : storageUnit.getNumberText()));
			}
			if (category.equals("AUDIO_ANNOTATION")) {
				ArchPerformanceAnnotation annotation = em.find(ArchPerformanceAnnotation.class, subId);
				dataBlock += formatLabel("Аннотация выступления №"
						+ annotation.getPerformanceNumber().toString());
			}
		} else if (category.equals("BOOK_COVER")) {
			dataBlock = formatHeader("Прикрепление обложки книги");
		} else {
			dataBlock = formatHeader("Прикрепление файлов");
			if (category.equals("FULL_SIZE")) {
				dataBlock += formatLabel("Полноразмерные файлы");
			}
			if (category.equals("PREVIEW")) {
				dataBlock += formatLabel("Сжатые файлы");
			}
		}
		for (ImagePath ip : filesAttached) {
			dataBlock += formatValue(ip.getCaption()
					+ ip.getFileName().substring(ip.getFileName().lastIndexOf('.')));
		}

		recordUnivDataUnit(unit, "ATTACH_FILES", changedEntity, dataBlock);
	}

	public void recordDetachFiles(Long unitId, String category, ImagePath detachedFile) {
		UnivDataUnit unit = complexQuery.queryModel(unitId);
		Long changedEntity = se.getAttrValueByCode(
				unit.getUnitTypeId(), "CHANGED_ENTITY_TYPE").getRefDescriptorValueId();

		String dataBlock;
		if (category.equals("BOOK_COVER")) {
			dataBlock = formatHeader("Удаление обложки книги");
		} else {
			dataBlock = formatHeader("Открепление файла");
			String fileCtg = "";
			if (category.equals("FULL_SIZE")) {
				fileCtg = " (Полноразмерный файл)";
			}
			if (category.equals("PREVIEW")) {
				fileCtg = " (Сжатый файл)";
			}
			dataBlock += formatValue(detachedFile.getCaption()
					+ detachedFile.getFileName().substring(detachedFile.getFileName().indexOf("."))
					+ fileCtg);
		}

		recordUnivDataUnit(unit, "ATTACH_FILES", changedEntity, dataBlock);
	}

	public void recordChangeFiles(Long unitId, String category, String oldFileName, String newFileName) {
		UnivDataUnit unit = complexQuery.queryModel(unitId);
		Long changedEntity = se.getAttrValueByCode(
				unit.getUnitTypeId(), "CHANGED_ENTITY_TYPE").getRefDescriptorValueId();

		String dataBlock;

		dataBlock = formatHeader("Изменение подписи");
		if (category.equals("FULL_SIZE")) {
			dataBlock += formatLabel("Полноразмерные файлы");
		}
		if (category.equals("PREVIEW")) {
			dataBlock += formatLabel("Сжатые файлы");
		}
		dataBlock += formatCrossedValue(oldFileName);
		dataBlock += formatValue(newFileName);

		recordUnivDataUnit(unit, "ATTACH_FILES", changedEntity, dataBlock);
	}

	public void recordEditVideodoc(UnivDataUnit model, VComplexDataUnit immModel, ArchVideodoc videodocModel, VComplexVideodoc videodocImmModel) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(copyImmModel.getFundNum(), immModel.getFundNum())
				|| !safeEquals(copyImmModel.getFundLetter(), immModel.getFundLetter())) {
			dataBlock += formatLabel("Фонд");
			dataBlock += formatCrossedValue(copyImmModel.getFundNum() == null ? empty()
					: copyImmModel.getFundNum().toString()
					+ (copyImmModel.getFundLetter() == null ? "" : copyImmModel.getFundLetter()));
			dataBlock += formatValue(immModel.getFundNum() == null ? empty()
					: immModel.getFundNum().toString()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (!safeEquals(copyImmModel.getSeriesNum(), immModel.getSeriesNum())
				|| !safeEquals(copyImmModel.getSeriesLetter(), immModel.getSeriesLetter())) {
			dataBlock += formatLabel("Опись");
			dataBlock += formatCrossedValue(copyImmModel.getSeriesNum() == null ? empty()
					: copyImmModel.getSeriesNum().toString()
					+ (copyImmModel.getSeriesLetter() == null ? "" : copyImmModel.getSeriesLetter()));
			dataBlock += formatValue(immModel.getSeriesNum() == null ? empty()
					: immModel.getSeriesNum().toString()
					+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		}
		if (!safeEquals(copyModel.getNumberNumber(), model.getNumberNumber())
				|| !safeEquals(copyModel.getNumberText(), model.getNumberText())) {
			dataBlock += formatLabel("Единица учёта");
			dataBlock += formatCrossedValue(copyModel.getNumberNumber().toString()
					+ (copyModel.getNumberText() == null ? "" : copyModel.getNumberText()));
			dataBlock += formatValue(model.getNumberNumber().toString()
					+ (model.getNumberText() == null ? "" : model.getNumberText()));
		}
		if (!safeEquals(copyModel.getPortalSectionId(), model.getPortalSectionId())) {
			dataBlock += formatLabel("Тематический раздел портала");
			dataBlock += formatCrossedValue(copyModel.getPortalSectionId() == null
					? empty() : copyImmModel.getPortalSection());
			dataBlock += formatValue(model.getPortalSectionId() == null
					? empty() : immModel.getPortalSection());
		}
		if (!safeEquals(copyVideodocModel.getVideodocTypeId(), videodocModel.getVideodocTypeId())) {
			dataBlock += formatLabel("Вид видеодокумента");
			dataBlock += formatCrossedValue(copyVideodocImmModel.getVideodocType());
			dataBlock += formatValue(videodocImmModel.getVideodocType());
		}
		if (!safeEquals(copyVideodocModel.getVideodocName(), videodocModel.getVideodocName())) {
			dataBlock += formatLabel("Название документа");
			dataBlock += formatCrossedValue(copyVideodocModel.getVideodocName());
			dataBlock += formatValue(videodocModel.getVideodocName());
		}
		if (!safeEquals(copyVideodocModel.getEventPlace(), videodocModel.getEventPlace())) {
			dataBlock += formatLabel("Место события");
			dataBlock += formatCrossedValue(copyVideodocModel.getEventPlace());
			dataBlock += formatValue(videodocModel.getEventPlace());
		}
		if (!safeEquals(copyVideodocModel.getFilmStudio(), videodocModel.getFilmStudio())) {
			dataBlock += formatLabel("Киностудия");
			dataBlock += formatCrossedValue(copyVideodocModel.getFilmStudio());
			dataBlock += formatValue(videodocModel.getFilmStudio());
		}
		if (!safeEquals(copyVideodocModel.getDirector(), videodocModel.getDirector())) {
			dataBlock += formatLabel("Режиссер");
			dataBlock += formatCrossedValue(copyVideodocModel.getDirector());
			dataBlock += formatValue(videodocModel.getDirector());
		}

		if (!safeEquals(copyVideodocModel.getOperators(), videodocModel.getOperators())) {
			dataBlock += formatLabel("Оператор(ы)");
			dataBlock += formatCrossedValue(copyVideodocModel.getOperators());
			dataBlock += formatValue(videodocModel.getOperators());
		}

		if (!safeEquals(copyVideodocModel.getOtherCreators(), videodocModel.getOtherCreators())) {
			dataBlock += formatLabel("Другие создатели");
			dataBlock += formatCrossedValue(copyVideodocModel.getOtherCreators());
			dataBlock += formatValue(videodocModel.getOtherCreators());
		}

		if (!safeEquals(copyVideodocModel.getCountryId(), videodocModel.getCountryId())) {
			dataBlock += formatLabel("Страна создатель");
			dataBlock += formatCrossedValue(copyVideodocImmModel.getCountry());
			dataBlock += formatValue(videodocImmModel.getCountry());
		}

		if (!safeEquals(copyVideodocImmModel.getStorageUnitCount(), videodocImmModel.getStorageUnitCount())) {
			dataBlock += formatLabel("Количество единиц хранения");
			dataBlock += formatCrossedValue(copyVideodocImmModel.getStorageUnitCount().toString());
			dataBlock += formatValue(videodocImmModel.getStorageUnitCount().toString());
		}

		boolean found;
		String suBlock = "";
		Integer copyTotalPlaytime = 0, totalPlaytime = 0;
		for (VComplexVideoStorageUnit copyStorageUnit : copyVideodocImmModel.getStorageUnits()) {
			found = false;
			for (VComplexVideoStorageUnit storageUnit : videodocImmModel.getStorageUnits()) {
				if (safeEquals(copyStorageUnit.getNumberNumber(), storageUnit.getNumberNumber())
						&& safeEquals(copyStorageUnit.getNumberText(), storageUnit.getNumberText())) {
					List<String> changes = Lists.newArrayList();
					if (!safeEquals(copyStorageUnit.getPlaytime(), storageUnit.getPlaytime())) {
						changes.add(String.format("время - <s>%1$d:%2$02d</s> —> %3$d:%4$02d",
								copyStorageUnit.getPlaytime() / 60, copyStorageUnit.getPlaytime() % 60,
								storageUnit.getPlaytime() / 60, storageUnit.getPlaytime() % 60));
					}

					if (!safeEquals(copyStorageUnit.getSound(), storageUnit.getSound())) {
						changes.add(String.format("звук - <s>%1$s</s> —> %2$s",
								copyStorageUnit.getSound(), storageUnit.getSound()));
					}
					if (!safeEquals(copyStorageUnit.getColor(), storageUnit.getColor())) {
						changes.add(String.format("цветность - <s>%1$s</s> —> %2$s",
								copyStorageUnit.getColor(), storageUnit.getColor()));
					}
					if (!safeEquals(copyStorageUnit.getColor(), storageUnit.getColor())) {
						changes.add(String.format("система записи - <s>%1$s</s> —> %2$s",
								(copyStorageUnit.getSystemRecord() == null ? empty() : copyStorageUnit.getSystemRecord().toString()),
								(storageUnit.getSystemRecord() == null ? empty() : storageUnit.getSystemRecord().toString())));
					}
					if (!safeEquals(copyStorageUnit.getQuality(), storageUnit.getQuality())) {
						changes.add(String.format("качество - <s>%1$s</s> —> %2$s",
								(copyStorageUnit.getQuality() == null ? empty() : copyStorageUnit.getQuality()),
								(storageUnit.getQuality() == null ? empty() : storageUnit.getQuality())));
					}
					if (!changes.isEmpty()) {
						suBlock += formatValue(storageUnit.getNumberNumber().toString()
								+ (storageUnit.getNumberText() == null ? "" : storageUnit.getNumberText())
								+ " (" + StringUtils.uniteString(changes, ", ") + ")");
					}
					found = true;
					break;
				}
			}
			if (!found) {
				suBlock += formatCrossedValue(copyStorageUnit.getNumberNumber().toString()
						+ (copyStorageUnit.getNumberText() == null ? "" : copyStorageUnit.getNumberText())
						+ " " + getVideoStorageUnitData(copyStorageUnit));
			}
			copyTotalPlaytime += copyStorageUnit.getPlaytime();
		}

		for (VComplexVideoStorageUnit storageUnit : videodocImmModel.getStorageUnits()) {
			found = false;
			for (VComplexVideoStorageUnit copyStorageUnit : copyVideodocImmModel.getStorageUnits()) {
				if (safeEquals(copyStorageUnit.getNumberNumber(), storageUnit.getNumberNumber())
						&& safeEquals(copyStorageUnit.getNumberText(), storageUnit.getNumberText())) {
					found = true;
					break;
				}
			}
			if (!found) {
				suBlock += formatValue(storageUnit.getNumberNumber().toString()
						+ (storageUnit.getNumberText() == null ? "" : storageUnit.getNumberText())
						+ " " + getVideoStorageUnitData(storageUnit));
			}
			totalPlaytime += storageUnit.getPlaytime();
		}

		if (!suBlock.isEmpty()) {
			dataBlock += formatLabel("Единицы хранения");
			dataBlock += suBlock;
		}

		if (!safeEquals(copyVideodocImmModel.getEventDate(), videodocImmModel.getEventDate())) {
			dataBlock += formatLabel("Дата события");
			dataBlock += formatCrossedValue(copyVideodocImmModel.getEventDate());
			dataBlock += formatValue(videodocImmModel.getEventDate());
		}

		if (!safeEquals(copyVideodocModel.getIssueDate(), videodocModel.getIssueDate())) {
			dataBlock += formatLabel("Дата выпуска");
			dataBlock += formatCrossedValue(copyVideodocImmModel.getIssueDate());
			dataBlock += formatValue(videodocImmModel.getIssueDate());
		}

		if (!safeEquals(copyTotalPlaytime, totalPlaytime)) {
			dataBlock += formatLabel("Время (продолжительность)");
			dataBlock += formatCrossedValue(String.format("%1$d:%2$02d",
					copyTotalPlaytime / 60, copyTotalPlaytime % 60));
			dataBlock += formatValue(String.format("%1$d:%2$02d", totalPlaytime / 60, totalPlaytime % 60));
		}
		if (!safeEquals(copyVideodocModel.getLanguageId(), videodocModel.getLanguageId())) {
			dataBlock += formatLabel("Язык");
			dataBlock += formatCrossedValue(copyVideodocImmModel.getLanguage() == null
					? empty() : copyVideodocImmModel.getLanguage());
			dataBlock += formatValue(videodocImmModel.getLanguage() == null
					? empty() : videodocImmModel.getLanguage());
		}
		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация (реферат)");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null
					? empty() : model.getAnnotation());
		}
		if (!safeEquals(copyVideodocModel.getNotes(), videodocModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copyVideodocModel.getNotes() == null
					? empty() : copyVideodocModel.getNotes());
			dataBlock += formatValue(videodocModel.getNotes() == null
					? empty() : videodocModel.getNotes());
		}
		dataBlock += getUnivClassifierChanges(
				copyImmModel.getClassifierValues(), immModel.getClassifierValues());

		String annBlock = "";
		for (VComplexVideoStoryDescription storyDescription : copyVideodocImmModel.getStoryDescriptions()) {
			found = false;
			for (VComplexVideoStoryDescription description : videodocImmModel.getStoryDescriptions()) {
				if (safeEquals(storyDescription.getStoryNumber(), description.getStoryNumber())) {
					List<String> changes = Lists.newArrayList();
					if (!safeEquals(storyDescription.getStorageUnitNumber(), description.getStorageUnitNumber())) {
						changes.add(String.format("ед.хр. - <s>%1$s</s> —> %2$s",
								storyDescription.getStorageUnitNumber(), description.getStorageUnitNumber()));
					}
					if (!safeEquals(storyDescription.getPart(), description.getPart())) {
						changes.add(String.format("часть - <s>%1$s</s> —> %2$s",
								(storyDescription.getPart() == null ? empty() : storyDescription.getPart()),
								(description.getPart() == null ? empty() : description.getPart())));
					}
					if (!safeEquals(storyDescription.getLanguage(), description.getLanguage())) {
						changes.add(String.format("язык - <s>%1$s</s> —> %2$s",
								(storyDescription.getLanguage() == null ? empty() : storyDescription.getLanguage()),
								(description.getLanguage() == null ? empty() : description.getLanguage())));
					}
					if (!safeEquals(storyDescription.getPlaytime(), description.getPlaytime())) {
						changes.add(String.format("время - <s>%1$d:%2$02d</s> —> %3$d:%4$02d",
								storyDescription.getPlaytime() / 60, storyDescription.getPlaytime() % 60,
								description.getPlaytime() / 60, description.getPlaytime() % 60));
					}
					if (!changes.isEmpty()) {
						annBlock += formatValue(description.getStoryNumber().toString() + ") "
								+ StringUtils.uniteString(changes, ", "));
					}

					if (!safeEquals(storyDescription.getAnnotation(), description.getAnnotation())) {
						annBlock += formatValue((changes.isEmpty()
								? String.format("%1$s) ", description.getStoryNumber()) : "&nbsp;&nbsp;&nbsp;")
								+ String.format("<s>%1$s</s> —> %2$s", storyDescription.getAnnotation(),
										description.getAnnotation()));
					}
					found = true;
					break;
				}
			}
			if (!found) {
				annBlock += formatCrossedValue(String.format("%1$d) %2$s",
						storyDescription.getStoryNumber(), getVideoStoryDescriptionData(storyDescription)));
				annBlock += "&nbsp;&nbsp;&nbsp;" + formatCrossedValue(storyDescription.getAnnotation());
			}
		}
		for (VComplexVideoStoryDescription description : videodocImmModel.getStoryDescriptions()) {
			found = false;
			for (VComplexVideoStoryDescription copyAnnotation : copyVideodocImmModel.getStoryDescriptions()) {
				if (safeEquals(copyAnnotation.getStoryNumber(), description.getStoryNumber())) {
					found = true;
					break;
				}
			}
			if (!found) {
				annBlock += formatValue(String.format("%1$d) %2$s",
						description.getStoryNumber(), getVideoStoryDescriptionData(description)));
				annBlock += formatValue("&nbsp;&nbsp;&nbsp;" + description.getAnnotation());
			}
		}
		if (!annBlock.isEmpty()) {
			dataBlock += formatLabel("Описания сюжетов") + annBlock;
		}

		recordUnivDataUnit(model, "EDIT",
				se.getDescValueByCodes("CHANGED_ENTITY_TYPE", "VIDEODOC").getDescriptorValueId(),
				dataBlock);

	}

	public void recordEditKinodoc(UnivDataUnit model, VComplexDataUnit immModel, ArchKinodoc kinodocModel, VComplexKinodoc kinodocImmModel) {
		String dataBlock = formatHeader("Редактируемые значения");
		if (!safeEquals(copyImmModel.getFundNum(), immModel.getFundNum())
				|| !safeEquals(copyImmModel.getFundLetter(), immModel.getFundLetter())) {
			dataBlock += formatLabel("Фонд");
			dataBlock += formatCrossedValue(copyImmModel.getFundNum() == null ? empty()
					: copyImmModel.getFundNum().toString()
					+ (copyImmModel.getFundLetter() == null ? "" : copyImmModel.getFundLetter()));
			dataBlock += formatValue(immModel.getFundNum() == null ? empty()
					: immModel.getFundNum().toString()
					+ (immModel.getFundLetter() == null ? "" : immModel.getFundLetter()));
		}
		if (!safeEquals(copyImmModel.getSeriesNum(), immModel.getSeriesNum())
				|| !safeEquals(copyImmModel.getSeriesLetter(), immModel.getSeriesLetter())) {
			dataBlock += formatLabel("Опись");
			dataBlock += formatCrossedValue(copyImmModel.getSeriesNum() == null ? empty()
					: copyImmModel.getSeriesNum().toString()
					+ (copyImmModel.getSeriesLetter() == null ? "" : copyImmModel.getSeriesLetter()));
			dataBlock += formatValue(immModel.getSeriesNum() == null ? empty()
					: immModel.getSeriesNum().toString()
					+ (immModel.getSeriesLetter() == null ? "" : immModel.getSeriesLetter()));
		}
		if (!safeEquals(copyModel.getNumberNumber(), model.getNumberNumber())
				|| !safeEquals(copyModel.getNumberText(), model.getNumberText())) {
			dataBlock += formatLabel("Единица учёта");
			dataBlock += formatCrossedValue(copyModel.getNumberNumber().toString()
					+ (copyModel.getNumberText() == null ? "" : copyModel.getNumberText()));
			dataBlock += formatValue(model.getNumberNumber().toString()
					+ (model.getNumberText() == null ? "" : model.getNumberText()));
		}
		if (!safeEquals(copyModel.getPortalSectionId(), model.getPortalSectionId())) {
			dataBlock += formatLabel("Тематический раздел портала");
			dataBlock += formatCrossedValue(copyModel.getPortalSectionId() == null
					? empty() : copyImmModel.getPortalSection());
			dataBlock += formatValue(model.getPortalSectionId() == null
					? empty() : immModel.getPortalSection());
		}
		if (!safeEquals(copyKinodocModel.getKinodocTypeId(), kinodocModel.getKinodocTypeId())) {
			dataBlock += formatLabel("Вид кинодокумента");
			dataBlock += formatCrossedValue(copyKinodocImmModel.getKinodocType());
			dataBlock += formatValue(kinodocImmModel.getKinodocType());
		}
		if (!safeEquals(copyKinodocModel.getKinodocName(), kinodocModel.getKinodocName())) {
			dataBlock += formatLabel("Название документа");
			dataBlock += formatCrossedValue(copyKinodocModel.getKinodocName());
			dataBlock += formatValue(kinodocModel.getKinodocName());
		}
		if (!safeEquals(copyKinodocModel.getEventPlace(), kinodocModel.getEventPlace())) {
			dataBlock += formatLabel("Место события");
			dataBlock += formatCrossedValue(copyKinodocModel.getEventPlace());
			dataBlock += formatValue(kinodocModel.getEventPlace());
		}
		if (!safeEquals(copyKinodocModel.getFilmStudio(), kinodocModel.getFilmStudio())) {
			dataBlock += formatLabel("Киностудия");
			dataBlock += formatCrossedValue(copyKinodocModel.getFilmStudio());
			dataBlock += formatValue(kinodocModel.getFilmStudio());
		}
		if (!safeEquals(copyKinodocModel.getDirector(), kinodocModel.getDirector())) {
			dataBlock += formatLabel("Режиссер");
			dataBlock += formatCrossedValue(copyKinodocModel.getDirector());
			dataBlock += formatValue(kinodocModel.getDirector());
		}

		if (!safeEquals(copyKinodocModel.getOperators(), kinodocModel.getOperators())) {
			dataBlock += formatLabel("Оператор(ы)");
			dataBlock += formatCrossedValue(copyKinodocModel.getOperators());
			dataBlock += formatValue(kinodocModel.getOperators());
		}

		if (!safeEquals(copyKinodocModel.getOtherCreators(), kinodocModel.getOtherCreators())) {
			dataBlock += formatLabel("Другие создатели");
			dataBlock += formatCrossedValue(copyKinodocModel.getOtherCreators());
			dataBlock += formatValue(kinodocModel.getOtherCreators());
		}

		if (!safeEquals(copyKinodocModel.getCountryId(), kinodocModel.getCountryId())) {
			dataBlock += formatLabel("Страна создатель");
			dataBlock += formatCrossedValue(copyKinodocImmModel.getCountry());
			dataBlock += formatValue(kinodocImmModel.getCountry());
		}

		if (!safeEquals(copyKinodocImmModel.getStorageUnitCount(), kinodocImmModel.getStorageUnitCount())) {
			dataBlock += formatLabel("Количество единиц хранения");
			dataBlock += formatCrossedValue(copyKinodocImmModel.getStorageUnitCount().toString());
			dataBlock += formatValue(kinodocImmModel.getStorageUnitCount().toString());
		}

		boolean found;
		String suBlock = "";
		Integer copyTotalPlaytime = 0, totalPlaytime = 0;
		for (VComplexKinoStorageUnit copyStorageUnit : copyKinodocImmModel.getStorageUnits()) {
			found = false;
			for (VComplexKinoStorageUnit storageUnit : kinodocImmModel.getStorageUnits()) {
				if (safeEquals(copyStorageUnit.getNumberNumber(), storageUnit.getNumberNumber())
						&& safeEquals(copyStorageUnit.getNumberText(), storageUnit.getNumberText())) {
					List<String> changes = Lists.newArrayList();
					if (!safeEquals(copyStorageUnit.getPlaytime(), storageUnit.getPlaytime())) {
						changes.add(String.format("время - <s>%1$d:%2$02d</s> —> %3$d:%4$02d",
								copyStorageUnit.getPlaytime() / 60, copyStorageUnit.getPlaytime() % 60,
								storageUnit.getPlaytime() / 60, storageUnit.getPlaytime() % 60));
					}

					if (!safeEquals(copyStorageUnit.getSound(), storageUnit.getSound())) {
						changes.add(String.format("звук - <s>%1$s</s> —> %2$s",
								copyStorageUnit.getSound(), storageUnit.getSound()));
					}
					if (!safeEquals(copyStorageUnit.getColor(), storageUnit.getColor())) {
						changes.add(String.format("цветность - <s>%1$s</s> —> %2$s",
								copyStorageUnit.getColor(), storageUnit.getColor()));
					}
					if (!safeEquals(copyStorageUnit.getColor(), storageUnit.getColor())) {
						changes.add(String.format("система записи - <s>%1$s</s> —> %2$s",
								(copyStorageUnit.getSystemRecord() == null ? empty() : copyStorageUnit.getSystemRecord().toString()),
								(storageUnit.getSystemRecord() == null ? empty() : storageUnit.getSystemRecord().toString())));
					}
					if (!safeEquals(copyStorageUnit.getQuality(), storageUnit.getQuality())) {
						changes.add(String.format("качество - <s>%1$s</s> —> %2$s",
								(copyStorageUnit.getQuality() == null ? empty() : copyStorageUnit.getQuality()),
								(storageUnit.getQuality() == null ? empty() : storageUnit.getQuality())));
					}
					if (!changes.isEmpty()) {
						suBlock += formatValue(storageUnit.getNumberNumber().toString()
								+ (storageUnit.getNumberText() == null ? "" : storageUnit.getNumberText())
								+ " (" + StringUtils.uniteString(changes, ", ") + ")");
					}
					found = true;
					break;
				}
			}
			if (!found) {
				suBlock += formatCrossedValue(copyStorageUnit.getNumberNumber().toString()
						+ (copyStorageUnit.getNumberText() == null ? "" : copyStorageUnit.getNumberText())
						+ " " + getKinoStorageUnitData(copyStorageUnit));
			}
			copyTotalPlaytime += copyStorageUnit.getPlaytime();
		}

		for (VComplexKinoStorageUnit storageUnit : kinodocImmModel.getStorageUnits()) {
			found = false;
			for (VComplexKinoStorageUnit copyStorageUnit : copyKinodocImmModel.getStorageUnits()) {
				if (safeEquals(copyStorageUnit.getNumberNumber(), storageUnit.getNumberNumber())
						&& safeEquals(copyStorageUnit.getNumberText(), storageUnit.getNumberText())) {
					found = true;
					break;
				}
			}
			if (!found) {
				suBlock += formatValue(storageUnit.getNumberNumber().toString()
						+ (storageUnit.getNumberText() == null ? "" : storageUnit.getNumberText())
						+ " " + getKinoStorageUnitData(storageUnit));
			}
			totalPlaytime += storageUnit.getPlaytime();
		}

		if (!suBlock.isEmpty()) {
			dataBlock += formatLabel("Единицы хранения");
			dataBlock += suBlock;
		}

		if (!safeEquals(copyKinodocImmModel.getEventDate(), kinodocImmModel.getEventDate())) {
			dataBlock += formatLabel("Дата события");
			dataBlock += formatCrossedValue(copyKinodocImmModel.getEventDate());
			dataBlock += formatValue(kinodocImmModel.getEventDate());
		}

		if (!safeEquals(copyKinodocModel.getIssueDate(), kinodocModel.getIssueDate())) {
			dataBlock += formatLabel("Дата выпуска");
			dataBlock += formatCrossedValue(copyKinodocImmModel.getIssueDate());
			dataBlock += formatValue(kinodocImmModel.getIssueDate());
		}

		if (!safeEquals(copyTotalPlaytime, totalPlaytime)) {
			dataBlock += formatLabel("Время (продолжительность)");
			dataBlock += formatCrossedValue(String.format("%1$d:%2$02d",
					copyTotalPlaytime / 60, copyTotalPlaytime % 60));
			dataBlock += formatValue(String.format("%1$d:%2$02d", totalPlaytime / 60, totalPlaytime % 60));
		}
		if (!safeEquals(copyKinodocModel.getLanguageId(), kinodocModel.getLanguageId())) {
			dataBlock += formatLabel("Язык");
			dataBlock += formatCrossedValue(copyKinodocImmModel.getLanguage() == null
					? empty() : copyKinodocImmModel.getLanguage());
			dataBlock += formatValue(kinodocImmModel.getLanguage() == null
					? empty() : kinodocImmModel.getLanguage());
		}
		if (!safeEquals(copyModel.getAnnotation(), model.getAnnotation())) {
			dataBlock += formatLabel("Аннотация (реферат)");
			dataBlock += formatCrossedValue(copyModel.getAnnotation() == null
					? empty() : copyModel.getAnnotation());
			dataBlock += formatValue(model.getAnnotation() == null
					? empty() : model.getAnnotation());
		}
		if (!safeEquals(copyKinodocModel.getNotes(), kinodocModel.getNotes())) {
			dataBlock += formatLabel("Примечание");
			dataBlock += formatCrossedValue(copyKinodocModel.getNotes() == null
					? empty() : copyKinodocModel.getNotes());
			dataBlock += formatValue(kinodocModel.getNotes() == null
					? empty() : kinodocModel.getNotes());
		}
		dataBlock += getUnivClassifierChanges(
				copyImmModel.getClassifierValues(), immModel.getClassifierValues());

		String annBlock = "";
		for (VComplexKinoStoryDescription storyDescription : copyKinodocImmModel.getStoryDescriptions()) {
			found = false;
			for (VComplexKinoStoryDescription description : kinodocImmModel.getStoryDescriptions()) {
				if (safeEquals(storyDescription.getStoryNumber(), description.getStoryNumber())) {
					List<String> changes = Lists.newArrayList();
					if (!safeEquals(storyDescription.getStorageUnitNumber(), description.getStorageUnitNumber())) {
						changes.add(String.format("ед.хр. - <s>%1$s</s> —> %2$s",
								storyDescription.getStorageUnitNumber(), description.getStorageUnitNumber()));
					}
					if (!safeEquals(storyDescription.getPart(), description.getPart())) {
						changes.add(String.format("часть - <s>%1$s</s> —> %2$s",
								(storyDescription.getPart() == null ? empty() : storyDescription.getPart()),
								(description.getPart() == null ? empty() : description.getPart())));
					}
					if (!safeEquals(storyDescription.getLanguage(), description.getLanguage())) {
						changes.add(String.format("язык - <s>%1$s</s> —> %2$s",
								(storyDescription.getLanguage() == null ? empty() : storyDescription.getLanguage()),
								(description.getLanguage() == null ? empty() : description.getLanguage())));
					}
					if (!safeEquals(storyDescription.getPlaytime(), description.getPlaytime())) {
						changes.add(String.format("время - <s>%1$d:%2$02d</s> —> %3$d:%4$02d",
								storyDescription.getPlaytime() / 60, storyDescription.getPlaytime() % 60,
								description.getPlaytime() / 60, description.getPlaytime() % 60));
					}
					if (!changes.isEmpty()) {
						annBlock += formatValue(description.getStoryNumber().toString() + ") "
								+ StringUtils.uniteString(changes, ", "));
					}

					if (!safeEquals(storyDescription.getAnnotation(), description.getAnnotation())) {
						annBlock += formatValue((changes.isEmpty()
								? String.format("%1$s) ", description.getStoryNumber()) : "&nbsp;&nbsp;&nbsp;")
								+ String.format("<s>%1$s</s> —> %2$s", storyDescription.getAnnotation(),
										description.getAnnotation()));
					}
					found = true;
					break;
				}
			}
			if (!found) {
				annBlock += formatCrossedValue(String.format("%1$d) %2$s",
						storyDescription.getStoryNumber(), getKinoStoryDescriptionData(storyDescription)));
				annBlock += "&nbsp;&nbsp;&nbsp;" + formatCrossedValue(storyDescription.getAnnotation());
			}
		}
		for (VComplexKinoStoryDescription description : kinodocImmModel.getStoryDescriptions()) {
			found = false;
			for (VComplexKinoStoryDescription copyAnnotation : copyKinodocImmModel.getStoryDescriptions()) {
				if (safeEquals(copyAnnotation.getStoryNumber(), description.getStoryNumber())) {
					found = true;
					break;
				}
			}
			if (!found) {
				annBlock += formatValue(String.format("%1$d) %2$s",
						description.getStoryNumber(), getKinoStoryDescriptionData(description)));
				annBlock += formatValue("&nbsp;&nbsp;&nbsp;" + description.getAnnotation());
			}
		}
		if (!annBlock.isEmpty()) {
			dataBlock += formatLabel("Описания сюжетов") + annBlock;
		}

		recordUnivDataUnit(model, "EDIT",
				se.getDescValueByCodes("CHANGED_ENTITY_TYPE", "KINODOC").getDescriptorValueId(),
				dataBlock);
	}

	private String getVideoStorageUnitData(VComplexVideoStorageUnit storageUnit) {
		String suInfo = String.format("(время - %1$d:%2$02d", storageUnit.getPlaytime() / 60,
				storageUnit.getPlaytime() % 60);
		suInfo += ", звук - " + storageUnit.getSound().toLowerCase();
		suInfo += ", цветность - " + storageUnit.getColor().toLowerCase();
		if (storageUnit.getSystemRecord() != null) {
			suInfo += ", система записи - " + storageUnit.getSystemRecord().toString();
		}
		if (storageUnit.getQuality() != null) {
			suInfo += ", качество - " + storageUnit.getQuality().toLowerCase();
		}
		if (storageUnit.getFilePathData() != null) {
			suInfo += ", файл - '" + storageUnit.getFilePathData().getCaption() + "'";
		}
		suInfo += ")";
		return suInfo;
	}

	private String getKinoStorageUnitData(VComplexKinoStorageUnit storageUnit) {
		String suInfo = String.format("(время - %1$d:%2$02d", storageUnit.getPlaytime() / 60,
				storageUnit.getPlaytime() % 60);
		suInfo += ", звук - " + storageUnit.getSound().toLowerCase();
		suInfo += ", цветность - " + storageUnit.getColor().toLowerCase();
		if (storageUnit.getSystemRecord() != null) {
			suInfo += ", система записи - " + storageUnit.getSystemRecord().toString();
		}
		if (storageUnit.getQuality() != null) {
			suInfo += ", качество - " + storageUnit.getQuality().toLowerCase();
		}
		if (storageUnit.getFilePathData() != null) {
			suInfo += ", файл - '" + storageUnit.getFilePathData().getCaption() + "'";
		}
		suInfo += ")";
		return suInfo;
	}

}
