package ru.insoft.sovdoc.model.web.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_WEB_PAGE_MULTIPLE_VALUE")
@Immutable
public class VWebPageMultipleValue {

	@Id
	@Column(name = "PAGE_ID")
	private Long pageId;
	
	@Column(name = "PAGE_TYPE_ID")
	private Long pageTypeId;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;
	
	@Column(name = "NAME")
	private String name;

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public Long getPageTypeId() {
		return pageTypeId;
	}

	public void setPageTypeId(Long pageTypeId) {
		this.pageTypeId = pageTypeId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
