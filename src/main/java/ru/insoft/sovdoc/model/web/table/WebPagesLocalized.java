package ru.insoft.sovdoc.model.web.table;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "WEB_PAGES_LOCALIZED")
public class WebPagesLocalized {

	@Id
	@SequenceGenerator(sequenceName="SEQ_WEB_PAGES_LOCALIZED",name="localizedpagesidgen", allocationSize = 1)
	@GeneratedValue(generator="localizedpagesidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "LOCALIZED_PAGE_ID")
	private Long localizedPageId;
	
	@Column(name = "PAGE_ID")
	private Long pageId;
	
	@Column(name = "NEWS_ID")
	private Long newsId;
	
	@Column(name = "LANGUAGE_CODE")
	private String languageCode;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "CONTENT")
	private String content;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "CONTENT_TYPE_ID")
	private Long contentTypeId;

	public Long getLocalizedPageId() {
		return localizedPageId;
	}

	public void setLocalizedPageId(Long localizedPageId) {
		this.localizedPageId = localizedPageId;
	}

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getContentTypeId() {
		return contentTypeId;
	}

	public void setContentTypeId(Long contentTypeId) {
		this.contentTypeId = contentTypeId;
	}
}
