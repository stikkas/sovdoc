package ru.insoft.sovdoc.model.web.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_WEB_PAGE_MULTIPLE_TYPE")
@Immutable
public class VWebPageMultipleType {

	@Id
	@Column(name = "PAGE_TYPE_ID")
	private Long pageTypeId;
	
	@Column(name = "PAGE_TYPE")
	private String pageType;
	
	@Column(name = "PAGES_COUNT")
	private Integer pagesCount; 

	public Long getPageTypeId() {
		return pageTypeId;
	}

	public void setPageTypeId(Long pageTypeId) {
		this.pageTypeId = pageTypeId;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public Integer getPagesCount() {
		return pagesCount;
	}

	public void setPagesCount(Integer pagesCount) {
		this.pagesCount = pagesCount;
	}
}
