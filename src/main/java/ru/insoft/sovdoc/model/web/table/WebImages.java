package ru.insoft.sovdoc.model.web.table;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Entity
@Table(name = "WEB_IMAGES")
public class WebImages {

	@Id
	@SequenceGenerator(sequenceName="SEQ_WEB_IMAGES",name="seqwebimgidgen", allocationSize = 1)
	@GeneratedValue(generator="seqwebimgidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "IMAGE_ID")
	private Long imageId;
	
	@Column(name = "FORMAT_ID")
	private Long formatId;
	
	@ManyToOne
	@JoinColumn(name = "FORMAT_ID", referencedColumnName = "DESCRIPTOR_VALUE_ID",
			insertable = false, updatable = false)
	private DescriptorValue format;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "IMAGE_DATA")
	private byte[] imageData;

	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public Long getFormatId() {
		return formatId;
	}

	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}

	public DescriptorValue getFormat() {
		return format;
	}

	public void setFormat(DescriptorValue format) {
		this.format = format;
	}

	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}
}
