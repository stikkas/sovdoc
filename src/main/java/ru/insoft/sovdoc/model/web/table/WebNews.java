package ru.insoft.sovdoc.model.web.table;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import ru.insoft.commons.service.JPAUtils;

@Entity
@Table(name = "WEB_NEWS")
public class WebNews {

	@Id
	@SequenceGenerator(sequenceName="SEQ_WEB_NEWS",name="webnewsidgen", allocationSize = 1)
	@GeneratedValue(generator="webnewsidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "NEWS_ID")
	private Long newsId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "NEWS_DATE")
	private Date newsDate;
	
	@Column(name = "NAME")
	private String name;

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Date getNewsDate() {
		return newsDate;
	}

	public void setNewsDate(Date newsDate) {
		this.newsDate = newsDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Transient
	public String getDateStr()
	{
		return JPAUtils.DateToString(getNewsDate());
	}
}
