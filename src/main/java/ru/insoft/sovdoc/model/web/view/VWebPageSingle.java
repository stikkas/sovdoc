package ru.insoft.sovdoc.model.web.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;
@Entity
@Table(name = "V_WEB_PAGE_SINGLE")
@Immutable
public class VWebPageSingle {

	@Id
	@Column(name = "PAGE_ID")
	private Long pageId;
	
	@Column(name = "PAGE_TYPE")
	private String pageType;

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
}
