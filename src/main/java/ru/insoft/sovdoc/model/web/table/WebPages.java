package ru.insoft.sovdoc.model.web.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WEB_PAGES")
public class WebPages {

	@Id
	@SequenceGenerator(sequenceName="SEQ_WEB_PAGES",name="webpagesidgen", allocationSize = 1)
	@GeneratedValue(generator="webpagesidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "PAGE_ID")
	private Long pageId;
	
	@Column(name = "PAGE_TYPE_ID")
	private Long pageTypeId;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public Long getPageTypeId() {
		return pageTypeId;
	}

	public void setPageTypeId(Long pageTypeId) {
		this.pageTypeId = pageTypeId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
}
