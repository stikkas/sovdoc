package ru.insoft.sovdoc.model.complex.table;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "ARCH_PHONO_STORAGE_UNIT")
public class ArchPhonoStorageUnit 
{
    @Id
    @SequenceGenerator(sequenceName = "SEQ_ARCH_PHONO_STORAGE_UNIT", name = "phonostorageunitidgen", 
                       allocationSize = 1)
    @GeneratedValue(generator = "phonostorageunitidgen", strategy = GenerationType.SEQUENCE)
    @Column(name = "PHONO_STORAGE_UNIT_ID")
    private Long phonoStorageUnitId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_DATA_UNIT_ID")
    private ArchPhonodoc phonodoc;
    
    @Column(name = "NUMBER_NUMBER")
    private Integer numberNumber;
    
    @Column(name = "NUMBER_TEXT")
    private String numberText;
    
    @Column(name = "PLAYTIME")
    private Integer playtime;
    
    @Column(name = "STORAGE_TYPE_ID")
    private Long storageTypeId;
    
    @Column(name = "CHANNEL_NUMBER_ID")
    private Long channelCountId;
    
    @Column(name = "SPEED")
    private Integer speed;
    
    @Column(name = "SOUND_QUALITY_ID")
    private Long soundQualityId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_IMAGE_PATH_ID")
    private ImagePath filePathData;
    
    @OneToMany(mappedBy = "storageUnit")
    private List<ArchPerformanceAnnotation> annotations;

    public Long getPhonoStorageUnitId() {
        return phonoStorageUnitId;
    }

    public void setPhonoStorageUnitId(Long phonoStorageUnitId) {
        this.phonoStorageUnitId = phonoStorageUnitId;
    }

    public ArchPhonodoc getPhonodoc() {
        return phonodoc;
    }

    public void setPhonodoc(ArchPhonodoc phonodoc) {
        this.phonodoc = phonodoc;
    }

    public Integer getNumberNumber() {
        return numberNumber;
    }

    public void setNumberNumber(Integer numberNumber) {
        this.numberNumber = numberNumber;
    }

    public String getNumberText() {
        return numberText;
    }

    public void setNumberText(String numberText) {
        this.numberText = numberText;
    }

    public Integer getPlaytime() {
        return playtime;
    }

    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    public Long getStorageTypeId() {
        return storageTypeId;
    }

    public void setStorageTypeId(Long storageTypeId) {
        this.storageTypeId = storageTypeId;
    }

    public Long getChannelCountId() {
        return channelCountId;
    }

    public void setChannelCountId(Long channelCountId) {
        this.channelCountId = channelCountId;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Long getSoundQualityId() {
        return soundQualityId;
    }

    public void setSoundQualityId(Long soundQualityId) {
        this.soundQualityId = soundQualityId;
    }

    public ImagePath getFilePathData() {
        return filePathData;
    }

    public void setFilePathData(ImagePath filePathData) {
        this.filePathData = filePathData;
    }

    public List<ArchPerformanceAnnotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<ArchPerformanceAnnotation> annotations) {
        this.annotations = annotations;
    }
}
