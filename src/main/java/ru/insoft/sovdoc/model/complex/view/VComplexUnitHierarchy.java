package ru.insoft.sovdoc.model.complex.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@SuppressWarnings("serial")
@Entity
@Table(name = "V_COMPLEX_UNIT_HIERARCHY")
public class VComplexUnitHierarchy implements Serializable {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "RES_OWNER_ID")
	private Long resOwnerId;
	
	@Column(name = "PORTAL_SECTION_ID")
	private Long portalSectionId;
	
	@Id
	@Column(name = "PARENT_UNIT_ID")
	private Long parentUnitId;
	
	@ManyToOne
	@JoinColumn(name = "PARENT_LEVEL_ID", referencedColumnName = "DESCRIPTOR_VALUE_ID")
	private DescriptorValue parentType;
	
	@Column(name = "PARENT_UNIT_NAME")
	private String parentUnitName;

	@Column(name = "PARENT_NUMBER_PREFIX")
	private String parentNumberPrefix;

	@Column(name = "PARENT_NUMBER_NUMBER")
	private Integer parentNumberNumber;
	
	@Column(name = "PARENT_NUMBER_TEXT")
	private String parentNumberText;
	
	@Column(name = "PARENT_LEVEL")
	private Integer parentLevel;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getResOwnerId() {
		return resOwnerId;
	}

	public void setResOwnerId(Long resOwnerId) {
		this.resOwnerId = resOwnerId;
	}

	public Long getPortalSectionId() {
		return portalSectionId;
	}

	public void setPortalSectionId(Long portalSectionId) {
		this.portalSectionId = portalSectionId;
	}

	public Long getParentUnitId() {
		return parentUnitId;
	}

	public void setParentUnitId(Long parentUnitId) {
		this.parentUnitId = parentUnitId;
	}

	public DescriptorValue getParentType() {
		return parentType;
	}

	public void setParentType(DescriptorValue parentType) {
		this.parentType = parentType;
	}

	public String getParentUnitName() {
		return parentUnitName;
	}

	public void setParentUnitName(String parentUnitName) {
		this.parentUnitName = parentUnitName;
	}

	public Integer getParentNumberNumber() {
		return parentNumberNumber;
	}

	public void setParentNumberNumber(Integer parentNumberNumber) {
		this.parentNumberNumber = parentNumberNumber;
	}

	public String getParentNumberText() {
		return parentNumberText;
	}

	public void setParentNumberText(String parentNumberText) {
		this.parentNumberText = parentNumberText;
	}

	public Integer getParentLevel() {
		return parentLevel;
	}

	public void setParentLevel(Integer parentLevel) {
		this.parentLevel = parentLevel;
	}

    public String getParentNumberPrefix() {
        return parentNumberPrefix;
    }

    public void setParentNumberPrefix(String parentNumberPrefix) {
        this.parentNumberPrefix = parentNumberPrefix;
    }

}
