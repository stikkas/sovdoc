package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "UNIV_CLASSIFIER_VALUE")
public class UnivClassifierValue implements Serializable
{
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_DATA_UNIT_ID")
    private UnivDataUnit dataUnit;
    
    @Id
    @Column(name = "DESCRIPTOR_VALUE_ID")
    private Long descriptorValueId;

    public UnivDataUnit getDataUnit() {
        return dataUnit;
    }

    public void setDataUnit(UnivDataUnit dataUnit) {
        this.dataUnit = dataUnit;
    }

    public Long getDescriptorValueId() {
        return descriptorValueId;
    }

    public void setDescriptorValueId(Long descriptorValueId) {
        this.descriptorValueId = descriptorValueId;
    }
}
