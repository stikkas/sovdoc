package ru.insoft.sovdoc.model.complex.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_ORG_STRUCTURE")
public class VOrgStructure implements Serializable {
	@Id
	@Column(name = "CHILD_VALUE_ID")
	private Long childValueId;
	
	@Id
	@Column(name = "PARENT_VALUE_ID")
	private Long parentValueId;
	
	@Column(name = "VALUE_LEVEL")
	private Integer valueLevel;

	public Long getChildValueId() {
		return childValueId;
	}

	public void setChildValueId(Long childValueId) {
		this.childValueId = childValueId;
	}

	public Long getParentValueId() {
		return parentValueId;
	}

	public void setParentValueId(Long parentValueId) {
		this.parentValueId = parentValueId;
	}

	public Integer getValueLevel() {
		return valueLevel;
	}

	public void setValueLevel(Integer valueLevel) {
		this.valueLevel = valueLevel;
	}
}
