package ru.insoft.sovdoc.model.complex.view;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_KINODOC")
@Immutable
public class VComplexKinodoc {
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "KINODOC_TYPE")
    private String kinodocType;

    @Column(name = "KINODOC_NAME")
    private String kinodocName;
    
    @Column(name = "EVENT_PLACE")
    private String eventPlace;
   
    @Column(name = "FILM_STUDIO")
    private String filmStudio;
    
    @Column(name = "DIRECTOR")
    private String director;
    
    @Column(name = "OPERATORS")
    private String operators;
 
    @Column(name = "OTHERS")
    private String others;
    
    @Column(name = "COUNTRY")
    private String country;
    
    @Column(name = "STORAGE_UNIT_COUNT")
    private Integer storageUnitCount;
   
    @Column(name = "EVENT_DATE")
    private String eventDate;
    
    @Column(name = "ISSUE_DATE")
    private String issueDate;
    
    @Column(name = "LANG")
    private String language;

    @Column(name = "NOTES")
    private String notes;
    
    @OneToMany(mappedBy = "kinodoc")
    @OrderBy("numberNumber, numberText")
    private List<VComplexKinoStorageUnit> storageUnits;
    
    @OneToMany(mappedBy = "kinodoc")
    @OrderBy("storyNumber")
    private List<VComplexKinoStoryDescription> storyDescriptions;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getKinodocType() {
		return kinodocType;
	}

	public void setKinodocType(String kinodocType) {
		this.kinodocType = kinodocType;
	}

	public String getKinodocName() {
		return kinodocName;
	}

	public void setKinodocName(String kinodocName) {
		this.kinodocName = kinodocName;
	}

	public Integer getStorageUnitCount() {
		return storageUnitCount;
	}

	public void setStorageUnitCount(Integer storageUnitCount) {
		this.storageUnitCount = storageUnitCount;
	}

	public List<VComplexKinoStorageUnit> getStorageUnits() {
		return storageUnits;
	}

	public void setStorageUnits(List<VComplexKinoStorageUnit> storageUnits) {
		this.storageUnits = storageUnits;
	}

	public String getEventPlace() {
		return eventPlace;
	}

	public void setEventPlace(String eventPlace) {
		this.eventPlace = eventPlace;
	}

	public String getFilmStudio() {
		return filmStudio;
	}

	public void setFilmStudio(String filmStudio) {
		this.filmStudio = filmStudio;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getOperators() {
		return operators;
	}

	public void setOperators(String operators) {
		this.operators = operators;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<VComplexKinoStoryDescription> getStoryDescriptions() {
		return storyDescriptions;
	}

	public void setStoryDescriptions(List<VComplexKinoStoryDescription> storyDescriptions) {
		this.storyDescriptions = storyDescriptions;
	}

}
