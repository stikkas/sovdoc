package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "UNIV_CARDS_LINKS")
public class UnivCardLinks {

  @Id
  @SequenceGenerator(sequenceName = "SEQ_CARD_LINKS", name = "cardLinks", allocationSize = 1)
  @GeneratedValue(generator = "cardLinks", strategy = GenerationType.SEQUENCE)
  @Column(name = "ID_LINKED_CARDS")
  private Long idLinkes;

  @Column(name = "UNIT_ID_ORIGINAL")
  private Long unitIdOriginal;

  @Column(name = "UNIT_ID_LINKED")
  private Long unitIdLinked;



//    @OneToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
//	private UnivDataUnit dataUnit;
  /**
   * @return the unitIdOriginal
   */
  public Long getUnitIdOriginal() {
    return unitIdOriginal;
  }

  /**
   * @param unitIdOriginal the unitIdOriginal to set
   */
  public void setUnitIdOriginal(Long unitIdOriginal) {
    this.unitIdOriginal = unitIdOriginal;
  }

  /**
   * @return the unitIdLinked
   */
  public Long getUnitIdLinked() {
    return unitIdLinked;
  }

  /**
   * @param unitIdLinked the unitIdLinked to set
   */
  public void setUnitIdLinked(Long unitIdLinked) {
    this.unitIdLinked = unitIdLinked;
  }

  /**
   * @return the idLinkes
   */
  public Long getIdLinkes() {
    return idLinkes;
  }

  /**
   * @param idLinkes the idLinkes to set
   */
  public void setIdLinkes(Long idLinkes) {
    this.idLinkes = idLinkes;
  }

 

}
