package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_VIDEO_STORAGE_UNIT")
@Immutable
public class VComplexVideoStorageUnit {
    @Id
    @Column(name = "VIDEO_STORAGE_UNIT_ID")
    private Long videoStorageUnitId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_DATA_UNIT_ID")
    private VComplexVideodoc videodoc;
    
    @Column(name = "NUMBER_NUMBER")
    private Integer numberNumber;
    
    @Column(name = "NUMBER_TEXT")
    private String numberText;
    
    @Column(name = "PLAYTIME")
    private Integer playtime;
    
    @Column(name = "SOUND")
    private String sound;
    
    @Column(name = "COLOR")
    private String color;
    
    @Column(name = "SYSTEM_RECORD")
    private String systemRecord;
      
    @Column(name = "QUALITY")
    private String quality;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_VIDEO_PATH_ID")
    private ImagePath filePathData;

	public Long getVideoStorageUnitId() {
		return videoStorageUnitId;
	}

	public void setVideoStorageUnitId(Long videoStorageUnitId) {
		this.videoStorageUnitId = videoStorageUnitId;
	}

	public VComplexVideodoc getVideodoc() {
		return videodoc;
	}

	public void setVideodoc(VComplexVideodoc videodoc) {
		this.videodoc = videodoc;
	}

	public Integer getNumberNumber() {
		return numberNumber;
	}

	public void setNumberNumber(Integer numberNumber) {
		this.numberNumber = numberNumber;
	}

	public String getNumberText() {
		return numberText;
	}

	public void setNumberText(String numberText) {
		this.numberText = numberText;
	}

	public Integer getPlaytime() {
		return playtime;
	}

	public void setPlaytime(Integer playtime) {
		this.playtime = playtime;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSystemRecord() {
		return systemRecord;
	}

	public void setSystemRecord(String systemRecord) {
		this.systemRecord = systemRecord;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public ImagePath getFilePathData() {
		return filePathData;
	}

	public void setFilePathData(ImagePath filePathData) {
		this.filePathData = filePathData;
	}

}
