package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_COMPLEX_FUND")
@Immutable
public class VComplexFund {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "FUND_TYPE")
	private String fundType;
	
	@Column(name = "SERIES_COUNT")
	private int seriesCount;
	
	@Column(name = "REAL_SERIES_COUNT")
	private int realSeriesCount;
	
	@Column(name = "STORAGE_UNIT_COUNT")
	private int storageUnitCount;
	
	@Column(name = "REAL_STORAGE_UNIT_COUNT")
	private int realStorageUnitCount;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public int getSeriesCount() {
		return seriesCount;
	}

	public void setSeriesCount(int seriesCount) {
		this.seriesCount = seriesCount;
	}

	public int getRealSeriesCount() {
		return realSeriesCount;
	}

	public void setRealSeriesCount(int realSeriesCount) {
		this.realSeriesCount = realSeriesCount;
	}

	public int getStorageUnitCount() {
		return storageUnitCount;
	}

	public void setStorageUnitCount(int storageUnitCount) {
		this.storageUnitCount = storageUnitCount;
	}

	public int getRealStorageUnitCount() {
		return realStorageUnitCount;
	}

	public void setRealStorageUnitCount(int realStorageUnitCount) {
		this.realStorageUnitCount = realStorageUnitCount;
	}
}
