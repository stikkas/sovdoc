package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "ARCH_STORY_DESCRIPTION")
public class ArchKinoStoryDescription {
    @Id
    @SequenceGenerator(sequenceName = "SEQ_ARCH_STORY_DESCRIPTION", name = "storydescgen",
                       allocationSize = 1)
    @GeneratedValue(generator = "storydescgen", strategy = GenerationType.SEQUENCE)
    @Column(name = "STORY_DESC_ID")
    private Long storyDescId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STORAGE_UNIT_ID")
    private ArchKinoStorageUnit storageUnit;
    
    @Column(name = "STORY_NUMBER")
    private Integer storyNumber;
    
    @Column(name = "PART")
    private Integer part;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "ANNOTATION")
    private String annotation;
    
    @Column(name = "LANGUAGE_ID")
    private Long languageId;
    
    @Column(name = "PLAYTIME")
    private Integer playtime;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_IMAGE_PATH_ID")
    private ImagePath filePathData;

	public Long getStoryDescId() {
		return storyDescId;
	}

	public void setStoryDescId(Long storyDescId) {
		this.storyDescId = storyDescId;
	}

	public ArchKinoStorageUnit getStorageUnit() {
		return storageUnit;
	}

	public void setStorageUnit(ArchKinoStorageUnit storageUnit) {
		this.storageUnit = storageUnit;
	}

	public Integer getStoryNumber() {
		return storyNumber;
	}

	public void setStoryNumber(Integer storyNumber) {
		this.storyNumber = storyNumber;
	}

	public Integer getPart() {
		return part;
	}

	public void setPart(Integer part) {
		this.part = part;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Integer getPlaytime() {
        return playtime;
    }

    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    public ImagePath getFilePathData() {
        return filePathData;
    }

    public void setFilePathData(ImagePath filePathData) {
        this.filePathData = filePathData;
    }
}
