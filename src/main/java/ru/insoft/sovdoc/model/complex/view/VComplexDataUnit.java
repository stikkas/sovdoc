package ru.insoft.sovdoc.model.complex.view;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import ru.insoft.sovdoc.model.HasDescriptors;

@Entity
@Table(name = "V_COMPLEX_DATA_UNIT")
@Immutable
public class VComplexDataUnit implements HasDescriptors {

  @Id
  @Column(name = "UNIV_DATA_UNIT_ID")
  private Long univDataUnitId;

  @Column(name = "ARCHIVE_ID")
  private Long archiveId;

  @Column(name = "ARCHIVE_NAME")
  private String archiveName;

  @Column(name = "ARCHIVE_NAME_SHORT")
  private String archiveNameShort;

  @Column(name = "DESCRIPTION_LEVEL_ID")
  private Long descriptionLevelId;

  @Column(name = "DESCRIPTION_LEVEL_CODE")
  private String descriptionLevelCode;

  @Column(name = "DESCRIPTION_LEVEL")
  private String descriptionLevel;

  @Column(name = "UNIT_TYPE_ID")
  private Long unitTypeId;

  @Column(name = "UNIT_TYPE_CODE")
  private String unitTypeCode;

  @Column(name = "UNIT_TYPE")
  private String unitType;

  @Column(name = "PORTAL_SECTION_ID")
  private Long portalSectionId;

  @Column(name = "PORTAL_SECTION")
  private String portalSection;

  @Column(name = "PARENT_UNIT_ID")
  private Long parentUnitId;

  @Column(name = "UNIT_NAME")
  private String unitName;

  @Column(name = "NUMBER_PREFIX")
  private String numberPrefix;

  @Column(name = "NUMBER_NUMBER")
  private Integer numberNumber;

  @Column(name = "NUMBER_TEXT")
  private String numberText;

  @Column(name = "FUND_NUM")
  private Integer fundNum;

  @Column(name = "FUND_PREFIX")
  private String fundPrefix;

  @Column(name = "FUND_LETTER")
  private String fundLetter;

  @Column(name = "SERIES_NUM")
  private Integer seriesNum;

  @Column(name = "SERIES_LETTER")
  private String seriesLetter;

  @Column(name = "UNIT_NUM")
  private Integer unitNum;

  @Column(name = "UNIT_LETTER")
  private String unitLetter;

  @Column(name = "UNIT_INFO")
  private String unitInfo;

  @Column(name = "UNIT_VOLUME")
  private Integer unitVolume;

  @Column(name = "UNIT_PART")
  private Integer unitPart;

  @Column(name = "START_PAGE")
  private Integer startPage;

  @Column(name = "PAGES")
  private String pages;

  @Column(name = "ARCH_NUMBER_CODE")
  private String archNumberCode;
  
  @Column(name = "IS_ACCESS_LIMITED")
  private boolean isAccessLimited;

  @Column(name = "ARCH_NUMBER")
  private String archNumber;

  @Column(name = "DATE_INTERVALS")
  private String dateIntervals;

  @Column(name = "IMAGE_COUNT")
  private int imageCount;

  @OneToMany(mappedBy = "dataUnit")
  @OrderBy("beginYear")
  private List<VComplexIntervalYears> years;

  @OneToMany(mappedBy = "dataUnit")
  @OrderBy("descriptor1")
  private List<VComplexDescriptor> descriptors;
  
  @OneToMany(mappedBy = "dataUnit")
  @OrderBy("valueIndex")
  private List<VComplexUnivClassifier> classifierValues;

  public Long getUnivDataUnitId() {
    return univDataUnitId;
  }

  public void setUnivDataUnitId(Long univDataUnitId) {
    this.univDataUnitId = univDataUnitId;
  }

  public Long getArchiveId() {
    return archiveId;
  }

  public void setArchiveId(Long archiveId) {
    this.archiveId = archiveId;
  }

  public String getArchiveName() {
    return archiveName;
  }

  public void setArchiveName(String archiveName) {
    this.archiveName = archiveName;
  }

  public String getArchiveNameShort() {
    return archiveNameShort;
  }

  public void setArchiveNameShort(String archiveNameShort) {
    this.archiveNameShort = archiveNameShort;
  }

  public Long getDescriptionLevelId() {
    return descriptionLevelId;
  }

  public void setDescriptionLevelId(Long descriptionLevelId) {
    this.descriptionLevelId = descriptionLevelId;
  }

  public String getDescriptionLevelCode() {
    return descriptionLevelCode;
  }

  public void setDescriptionLevelCode(String descriptionLevelCode) {
    this.descriptionLevelCode = descriptionLevelCode;
  }

  public String getDescriptionLevel() {
    return descriptionLevel;
  }

  public void setDescriptionLevel(String descriptionLevel) {
    this.descriptionLevel = descriptionLevel;
  }

  public Long getUnitTypeId() {
    return unitTypeId;
  }

  public void setUnitTypeId(Long unitTypeId) {
    this.unitTypeId = unitTypeId;
  }

  public String getUnitTypeCode() {
    return unitTypeCode;
  }

  public void setUnitTypeCode(String unitTypeCode) {
    this.unitTypeCode = unitTypeCode;
  }

  public String getUnitType() {
    return unitType;
  }

  public void setUnitType(String unitType) {
    this.unitType = unitType;
  }

  public Long getPortalSectionId() {
    return portalSectionId;
  }

  public void setPortalSectionId(Long portalSectionId) {
    this.portalSectionId = portalSectionId;
  }

  public String getPortalSection() {
    return portalSection;
  }

  public void setPortalSection(String portalSection) {
    this.portalSection = portalSection;
  }

  public Long getParentUnitId() {
    return parentUnitId;
  }

  public void setParentUnitId(Long parentUnitId) {
    this.parentUnitId = parentUnitId;
  }

  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }

  public Integer getNumberNumber() {
    return numberNumber;
  }

  public void setNumberNumber(Integer numberNumber) {
    this.numberNumber = numberNumber;
  }

  public String getNumberText() {
    return numberText;
  }

  public void setNumberText(String numberText) {
    this.numberText = numberText;
  }

  public Integer getFundNum() {
    return fundNum;
  }

  public void setFundNum(Integer fundNum) {
    this.fundNum = fundNum;
  }

  public String getFundLetter() {
    return fundLetter;
  }

  public void setFundLetter(String fundLetter) {
    this.fundLetter = fundLetter;
  }

  public Integer getSeriesNum() {
    return seriesNum;
  }

  public void setSeriesNum(Integer seriesNum) {
    this.seriesNum = seriesNum;
  }

  public String getSeriesLetter() {
    return seriesLetter;
  }

  public void setSeriesLetter(String seriesLetter) {
    this.seriesLetter = seriesLetter;
  }

  public Integer getUnitNum() {
    return unitNum;
  }

  public void setUnitNum(Integer unitNum) {
    this.unitNum = unitNum;
  }

  public String getUnitLetter() {
    return unitLetter;
  }

  public void setUnitLetter(String unitLetter) {
    this.unitLetter = unitLetter;
  }

  public String getUnitInfo() {
    return unitInfo;
  }

  public void setUnitInfo(String unitInfo) {
    this.unitInfo = unitInfo;
  }

  public Integer getUnitVolume() {
    return unitVolume;
  }

  public void setUnitVolume(Integer unitVolume) {
    this.unitVolume = unitVolume;
  }

  public Integer getUnitPart() {
    return unitPart;
  }

  public void setUnitPart(Integer unitPart) {
    this.unitPart = unitPart;
  }

  public Integer getStartPage() {
    return startPage;
  }

  public void setStartPage(Integer startPage) {
    this.startPage = startPage;
  }

  public String getPages() {
    return pages;
  }

  public void setPages(String pages) {
    this.pages = pages;
  }

  public String getArchNumberCode() {
    return archNumberCode;
  }

  public void setArchNumberCode(String archNumberCode) {
    this.archNumberCode = archNumberCode;
  }

  public boolean isAccessLimited() {
    return isAccessLimited;
  }

  public void setAccessLimited(boolean isAccessLimited) {
    this.isAccessLimited = isAccessLimited;
  }

  public String getArchNumber() {
    return archNumber;
  }

  public void setArchNumber(String archNumber) {
    this.archNumber = archNumber;
  }

  public String getDateIntervals() {
    return dateIntervals;
  }

  public void setDateIntervals(String dateIntervals) {
    this.dateIntervals = dateIntervals;
  }

  public int getImageCount() {
    return imageCount;
  }

  public void setImageCount(int imageCount) {
    this.imageCount = imageCount;
  }

  public List<VComplexIntervalYears> getYears() {
    return years;
  }

  public void setYears(List<VComplexIntervalYears> years) {
    this.years = years;
  }

  public List<VComplexDescriptor> getDescriptors() {
    return descriptors;
  }

  public void setDescriptors(List<VComplexDescriptor> descriptors) {
    this.descriptors = descriptors;
  }

    public List<VComplexUnivClassifier> getClassifierValues() {
        return classifierValues;
    }

    public void setClassifierValues(List<VComplexUnivClassifier> classifierValues) {
        this.classifierValues = classifierValues;
    }

    public String getFundPrefix() {
        return fundPrefix;
    }

    public void setFundPrefix(String fundPrefix) {
        this.fundPrefix = fundPrefix;
    }

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public void setNumberPrefix(String numberPrefix) {
        this.numberPrefix = numberPrefix;
    }
    
}
