package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_COMPLEX_ARCHIVE")
public class VComplexArchive {

	@Id
	@Column(name = "ARCHIVE_ID")
	private Long archiveId;
	
	@Column(name = "ARCHIVE_SHORT_NAME")
	private String archiveShortName;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;
	
	@Column(name = "PARENT_VALUE_ID")
	private Long parentValueId;
	
	@Column(name = "HAS_CHILDREN")
	private boolean hasChildren;

	public Long getArchiveId() {
		return archiveId;
	}

	public void setArchiveId(Long archiveId) {
		this.archiveId = archiveId;
	}

	public String getArchiveShortName() {
		return archiveShortName;
	}

	public void setArchiveShortName(String archiveShortName) {
		this.archiveShortName = archiveShortName;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Long getParentValueId() {
		return parentValueId;
	}

	public void setParentValueId(Long parentValueId) {
		this.parentValueId = parentValueId;
	}

	public boolean isHasChildren() {
		return hasChildren;
	}

	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}
}
