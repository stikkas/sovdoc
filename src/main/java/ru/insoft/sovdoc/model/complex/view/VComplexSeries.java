package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_COMPLEX_SERIES")
@Immutable
public class VComplexSeries {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "DOCUMENT_KIND")
	private String documentKind;
	
	@Column(name = "SERIES_NAME")
	private String seriesName;
	
	@Column(name = "STORAGE_UNIT_COUNT")
	private int storageUnitCount;
	
	@Column(name = "REAL_STORAGE_UNIT_COUNT")
	private int realStorageUnitCount;
	
	@Column(name = "REPRODUCTION_TYPE")
	private String reproductionType;
	
	@Column(name = "NOTES")
	private String notes;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getDocumentKind() {
		return documentKind;
	}

	public void setDocumentKind(String documentKind) {
		this.documentKind = documentKind;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public int getStorageUnitCount() {
		return storageUnitCount;
	}

	public void setStorageUnitCount(int storageUnitCount) {
		this.storageUnitCount = storageUnitCount;
	}

	public int getRealStorageUnitCount() {
		return realStorageUnitCount;
	}

	public void setRealStorageUnitCount(int realStorageUnitCount) {
		this.realStorageUnitCount = realStorageUnitCount;
	}

	public String getReproductionType() {
		return reproductionType;
	}

	public void setReproductionType(String reproductionType) {
		this.reproductionType = reproductionType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
