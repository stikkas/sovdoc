package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.desc.view.VDescAllRelations;

@SuppressWarnings("serial")
@Entity
@Table(name = "UNIV_DESCRIPTOR")
public class UnivDescriptor implements Serializable {

	@Id
	@SequenceGenerator(sequenceName="SEQ_UNIV_DESCRIPTOR",name="univdescriptoridgen", allocationSize = 1)
	@GeneratedValue(generator="univdescriptoridgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "UNIV_DESCRIPTOR_ID")
	private Long univDescriptorId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private UnivDataUnit dataUnit;
	
	@Column(name = "UNIV_DATA_UNIT_ID", insertable = false, updatable = false)
	private Long univDataUnitId;
	
	@Column(name = "DESCRIPTOR_VALUE_ID1")
	private Long descriptorValueId1;
	
	@Column(name = "DESCRIPTOR_VALUE_ID2")
	private Long descriptorValueId2;
	
	@Column(name = "DESCRIPTOR_RELATION_ID")
	private Long descriptorRelationId;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "DESCRIPTOR_VALUE_ID", referencedColumnName = "DESCRIPTOR_VALUE_ID1", 
			insertable = false, updatable = false)
	private List<VDescAllRelations> allRelations;

	public Long getUnivDescriptorId() {
		return univDescriptorId;
	}

	public void setUnivDescriptorId(Long univDescriptorId) {
		this.univDescriptorId = univDescriptorId;
	}

	public UnivDataUnit getDataUnit() {
		return dataUnit;
	}

	public void setDataUnit(UnivDataUnit dataUnit) {
		this.dataUnit = dataUnit;
	}

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getDescriptorValueId1() {
		return descriptorValueId1;
	}

	public void setDescriptorValueId1(Long descriptorValueId1) {
		this.descriptorValueId1 = descriptorValueId1;
	}

	public Long getDescriptorValueId2() {
		return descriptorValueId2;
	}

	public void setDescriptorValueId2(Long descriptorValueId2) {
		this.descriptorValueId2 = descriptorValueId2;
	}

	public Long getDescriptorRelationId() {
		return descriptorRelationId;
	}

	public void setDescriptorRelationId(Long descriptorRelationId) {
		this.descriptorRelationId = descriptorRelationId;
	}

	public List<VDescAllRelations> getAllRelations() {
		return allRelations;
	}

	public void setAllRelations(List<VDescAllRelations> allRelations) {
		this.allRelations = allRelations;
	}
}
