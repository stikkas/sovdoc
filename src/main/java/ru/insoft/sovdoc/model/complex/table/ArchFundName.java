package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ARCH_FUND_NAME")
public class ArchFundName {

	@Id
	@SequenceGenerator(sequenceName="SEQ_ARCH_FUND_NAME",name="archfundidgen", allocationSize = 1)
	@GeneratedValue(generator="archfundidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "FUND_NAME_ID")
	private Long fundNameId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private ArchFund fund;
	
	@Column(name = "FUND_NAME")
	private String fundName;
	
	@Column(name = "TEXT_DATE")
	private String textDate;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;

	public Long getFundNameId() {
		return fundNameId;
	}

	public void setFundNameId(Long fundNameId) {
		this.fundNameId = fundNameId;
	}

	public ArchFund getFund() {
		return fund;
	}

	public void setFund(ArchFund fund) {
		this.fund = fund;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getTextDate() {
		return textDate;
	}

	public void setTextDate(String textDate) {
		this.textDate = textDate;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
}
