package ru.insoft.sovdoc.model.complex.table;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.showfile.ImagePath;

@Entity
@Table(name = "ARCH_KINO_STORAGE_UNIT")
public class ArchKinoStorageUnit {
    @Id
    @SequenceGenerator(sequenceName = "SEQ_ARCH_KINO_STORAGE_UNIT", name = "kinostorageunitidgen", allocationSize = 1)
    @GeneratedValue(generator = "kinostorageunitidgen", strategy = GenerationType.SEQUENCE)
    @Column(name = "KINO_STORAGE_UNIT_ID")
    private Long kinoStorageUnitId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_DATA_UNIT_ID")
    private ArchKinodoc kinodoc;
    
    @Column(name = "NUMBER_NUMBER")
    private Integer numberNumber;
    
    @Column(name = "NUMBER_TEXT")
    private String numberText;
    
    @Column(name = "PLAYTIME")
    private Integer playtime;
    
    @Column(name = "SOUND_ID")
    private Long soundId;
   
    @Column(name = "COLOR_ID")
    private Long colorId;
    
    @Column(name = "SYSTEM_RECORD_ID")
    private Long systemRecordId;
    
    @Column(name = "QUALITY_ID")
    private Long qualityId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_VIDEO_PATH_ID")
    private ImagePath filePathData;
    
    @OneToMany(mappedBy = "storageUnit")
    private List<ArchKinoStoryDescription> storyDescriptions;

	public Long getKinoStorageUnitId() {
		return kinoStorageUnitId;
	}

	public void setKinoStorageUnitId(Long kinoStorageUnitId) {
		this.kinoStorageUnitId = kinoStorageUnitId;
	}

	public ArchKinodoc getKinodoc() {
		return kinodoc;
	}

	public void setKinodoc(ArchKinodoc kinodoc) {
		this.kinodoc = kinodoc;
	}

	public Integer getNumberNumber() {
		return numberNumber;
	}

	public void setNumberNumber(Integer numberNumber) {
		this.numberNumber = numberNumber;
	}

	public String getNumberText() {
		return numberText;
	}

	public void setNumberText(String numberText) {
		this.numberText = numberText;
	}

	public Integer getPlaytime() {
		return playtime;
	}

	public void setPlaytime(Integer playtime) {
		this.playtime = playtime;
	}

	public Long getSoundId() {
		return soundId;
	}

	public void setSoundId(Long soundId) {
		this.soundId = soundId;
	}

	public Long getColorId() {
		return colorId;
	}

	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}

	public Long getSystemRecordId() {
		return systemRecordId;
	}

	public void setSystemRecordId(Long systemRecordId) {
		this.systemRecordId = systemRecordId;
	}

	public Long getQualityId() {
		return qualityId;
	}

	public void setQualityId(Long qualityId) {
		this.qualityId = qualityId;
	}

	public ImagePath getFilePathData() {
		return filePathData;
	}

	public void setFilePathData(ImagePath filePathData) {
		this.filePathData = filePathData;
	}

	public List<ArchKinoStoryDescription> getStoryDescriptions() {
		return storyDescriptions;
	}

	public void setStoryDescriptions(List<ArchKinoStoryDescription> storyDescriptions) {
		this.storyDescriptions = storyDescriptions;
	}
}
