package ru.insoft.sovdoc.model.complex.table;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ARCH_VIDEODOC")
public class ArchVideodoc {
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "VIDEODOC_TYPE_ID")
    private Long videodocTypeId;
    
    @Column(name = "VIDEODOC_NAME")
    private String videodocName;
    
    @Column(name = "EVENT_PLACE")
    private String eventPlace;
    
    @Column(name = "FILM_STUDIO")
    private String filmStudio;
    
    @Column(name = "DIRECTOR")
    private String director;
    
    @Column(name = "OPERATORS")
    private String operators;
    
    @Column(name = "OTHERS")
    private String otherCreators;

    @Column(name = "COUNTRY_ID")
    private Long countryId; 

    @Temporal(TemporalType.DATE)
    @Column(name = "ISSUE_DATE")
    private Date issueDate;

    @Column(name = "LANGUAGE_ID")
    private Long languageId;  
    
    @Column(name = "NOTES")
    private String notes;
    
    @OneToMany(mappedBy = "videodoc")
    @OrderBy("numberNumber, numberText")
    private List<ArchVideoStorageUnit> storageUnits;

    public Long getUnivDataUnitId() {
        return univDataUnitId;
    }

    public void setUnivDataUnitId(Long univDataUnitId) {
        this.univDataUnitId = univDataUnitId;
    }

	public Long getVideodocTypeId() {
		return videodocTypeId;
	}

	public void setVideodocTypeId(Long videodocTypeId) {
		this.videodocTypeId = videodocTypeId;
	}

	public String getVideodocName() {
		return videodocName;
	}

	public void setVideodocName(String videodocName) {
		this.videodocName = videodocName;
	}

	public String getEventPlace() {
		return eventPlace;
	}

	public void setEventPlace(String eventPlace) {
		this.eventPlace = eventPlace;
	}

	public String getFilmStudio() {
		return filmStudio;
	}

	public void setFilmStudio(String filmStudio) {
		this.filmStudio = filmStudio;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getOperators() {
		return operators;
	}

	public void setOperators(String operators) {
		this.operators = operators;
	}

	public String getOtherCreators() {
		return otherCreators;
	}

	public void setOtherCreators(String otherCreators) {
		this.otherCreators = otherCreators;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<ArchVideoStorageUnit> getStorageUnits() {
		return storageUnits;
	}

	public void setStorageUnits(List<ArchVideoStorageUnit> storageUnits) {
		this.storageUnits = storageUnits;
	}

}
