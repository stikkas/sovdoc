package ru.insoft.sovdoc.model.complex.view;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_UNIV_CLASSIFIER")
@Immutable
public class VComplexUnivClassifier implements Serializable
{
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_DATA_UNIT_ID")
    private VComplexDataUnit dataUnit;
    
    @Id
    @Column(name = "DESCRIPTOR_VALUE_ID")
    private Long descriptorValueId;
    
    @Column(name = "VALUE_INDEX")
    private String valueIndex;
    
    @Column(name = "FULL_VALUE")
    private String fullValue;

    public VComplexDataUnit getDataUnit() {
        return dataUnit;
    }

    public void setDataUnit(VComplexDataUnit dataUnit) {
        this.dataUnit = dataUnit;
    }

    public Long getDescriptorValueId() {
        return descriptorValueId;
    }

    public void setDescriptorValueId(Long descriptorValueId) {
        this.descriptorValueId = descriptorValueId;
    }

    public String getValueIndex() {
        return valueIndex;
    }

    public void setValueIndex(String valueIndex) {
        this.valueIndex = valueIndex;
    }

    public String getFullValue() {
        return fullValue;
    }

    public void setFullValue(String fullValue) {
        this.fullValue = fullValue;
    }
}
