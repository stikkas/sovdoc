package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_COMPLEX_DESCRIPTOR")
@Immutable
public class VComplexDescriptor {

	@Id
	@Column(name = "UNIV_DESCRIPTOR_ID")
	private Long univDescriptorId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private VComplexDataUnit dataUnit;
	
	@Column(name = "DESCRIPTOR_VALUE_ID1")
	private Long descriptorValueId1;
	
	@Column(name = "GROUP_CODE")
	private String groupCode;
	
	@Column(name = "DESCRIPTOR_CODE")
	private String descriptorCode;
	
	@Column(name = "DESCRIPTOR1")
	private String descriptor1;
	
	@Column(name = "DESCRIPTOR_VALUE_ID2")
	private Long descriptorValueId2;
	
	@Column(name = "DESCRIPTOR2")
	private String descriptor2;
	
	@Column(name = "DESCRIPTOR_RELATION_ID")
	private Long descriptorRelationId;
	
	@Column(name = "DESC_RELATION")
	private String descRelation;

	public Long getUnivDescriptorId() {
		return univDescriptorId;
	}

	public void setUnivDescriptorId(Long univDescriptorId) {
		this.univDescriptorId = univDescriptorId;
	}

	public VComplexDataUnit getDataUnit() {
		return dataUnit;
	}

	public void setDataUnit(VComplexDataUnit dataUnit) {
		this.dataUnit = dataUnit;
	}

	public Long getDescriptorValueId1() {
		return descriptorValueId1;
	}

	public void setDescriptorValueId1(Long descriptorValueId1) {
		this.descriptorValueId1 = descriptorValueId1;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getDescriptorCode() {
		return descriptorCode;
	}

	public void setDescriptorCode(String descriptorCode) {
		this.descriptorCode = descriptorCode;
	}

	public String getDescriptor1() {
		return descriptor1;
	}

	public void setDescriptor1(String descriptor1) {
		this.descriptor1 = descriptor1;
	}

	public Long getDescriptorValueId2() {
		return descriptorValueId2;
	}

	public void setDescriptorValueId2(Long descriptorValueId2) {
		this.descriptorValueId2 = descriptorValueId2;
	}

	public String getDescriptor2() {
		return descriptor2;
	}

	public void setDescriptor2(String descriptor2) {
		this.descriptor2 = descriptor2;
	}

	public Long getDescriptorRelationId() {
		return descriptorRelationId;
	}

	public void setDescriptorRelationId(Long descriptorRelationId) {
		this.descriptorRelationId = descriptorRelationId;
	}

	public String getDescRelation() {
		return descRelation;
	}

	public void setDescRelation(String descRelation) {
		this.descRelation = descRelation;
	}
}
