package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_COMPLEX_TREE")
@Immutable
public class VComplexTree {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "ARCHIVE_ID")
	private Long archiveId;
	
	@Column(name = "ARCHIVE_NAME")
	private String archiveName;
	
	@Column(name = "ARCHIVE_NAME_SHORT")
	private String archiveNameShort;
	
	@Column(name = "DESCRIPTION_LEVEL_ID")
	private Long descriptionLevelId;
	
	@Column(name = "DESCRIPTION_LEVEL_CODE")
	private String descriptionLevelCode;
	
	@Column(name = "DESCRIPTION_LEVEL")
	private String descriptionLevel;
	
	@Column(name = "UNIT_TYPE_ID")
	private Long unitTypeId;
	
	@Column(name = "UNIT_TYPE_CODE")
	private String unitTypeCode;
	
	@Column(name = "UNIT_TYPE")
	private String unitType;
	
	@Column(name = "PORTAL_SECTION_ID")
	private Long portalSectionId;
	
	@Column(name = "PORTAL_SECTION")
	private String portalSection;
	
	@Column(name = "PARENT_UNIT_ID")
	private Long parentUnitId;
	
	@Column(name = "UNIT_NAME")
	private String unitName;
	
	@Column(name = "NUMBER_NUMBER")
	private Integer numberNumber;
	
	@Column(name = "NUMBER_TEXT")
	private String numberText;
	
	@Column(name = "ARCH_NUMBER_CODE")
	private String archNumberCode;
	
	@Column(name = "IS_ACCESS_LIMITED")
	private boolean isAccessLimited;
	
	@Column(name = "HAS_CHILDREN")
	private boolean hasChildren;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getArchiveId() {
		return archiveId;
	}

	public void setArchiveId(Long archiveId) {
		this.archiveId = archiveId;
	}

	public String getArchiveName() {
		return archiveName;
	}

	public void setArchiveName(String archiveName) {
		this.archiveName = archiveName;
	}

	public String getArchiveNameShort() {
		return archiveNameShort;
	}

	public void setArchiveNameShort(String archiveNameShort) {
		this.archiveNameShort = archiveNameShort;
	}

	public Long getDescriptionLevelId() {
		return descriptionLevelId;
	}

	public void setDescriptionLevelId(Long descriptionLevelId) {
		this.descriptionLevelId = descriptionLevelId;
	}

	public String getDescriptionLevelCode() {
		return descriptionLevelCode;
	}

	public void setDescriptionLevelCode(String descriptionLevelCode) {
		this.descriptionLevelCode = descriptionLevelCode;
	}

	public String getDescriptionLevel() {
		return descriptionLevel;
	}

	public void setDescriptionLevel(String descriptionLevel) {
		this.descriptionLevel = descriptionLevel;
	}

	public Long getUnitTypeId() {
		return unitTypeId;
	}

	public void setUnitTypeId(Long unitTypeId) {
		this.unitTypeId = unitTypeId;
	}

	public String getUnitTypeCode() {
		return unitTypeCode;
	}

	public void setUnitTypeCode(String unitTypeCode) {
		this.unitTypeCode = unitTypeCode;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public Long getPortalSectionId() {
		return portalSectionId;
	}

	public void setPortalSectionId(Long portalSectionId) {
		this.portalSectionId = portalSectionId;
	}

	public String getPortalSection() {
		return portalSection;
	}

	public void setPortalSection(String portalSection) {
		this.portalSection = portalSection;
	}

	public Long getParentUnitId() {
		return parentUnitId;
	}

	public void setParentUnitId(Long parentUnitId) {
		this.parentUnitId = parentUnitId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Integer getNumberNumber() {
		return numberNumber;
	}

	public void setNumberNumber(Integer numberNumber) {
		this.numberNumber = numberNumber;
	}

	public String getNumberText() {
		return numberText;
	}

	public void setNumberText(String numberText) {
		this.numberText = numberText;
	}

	public String getArchNumberCode() {
		return archNumberCode;
	}

	public void setArchNumberCode(String archNumberCode) {
		this.archNumberCode = archNumberCode;
	}

	public boolean isAccessLimited() {
		return isAccessLimited;
	}

	public void setAccessLimited(boolean isAccessLimited) {
		this.isAccessLimited = isAccessLimited;
	}

	public boolean isHasChildren() {
		return hasChildren;
	}

	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}
}
