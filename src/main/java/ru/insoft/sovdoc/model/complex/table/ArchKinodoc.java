package ru.insoft.sovdoc.model.complex.table;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ARCH_KINODOC")
public class ArchKinodoc {
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "KINODOC_TYPE_ID")
    private Long kinodocTypeId;
    
    @Column(name = "KINODOC_NAME")
    private String kinodocName;
    
    @Column(name = "EVENT_PLACE")
    private String eventPlace;
    
    @Column(name = "FILM_STUDIO")
    private String filmStudio;
    
    @Column(name = "DIRECTOR")
    private String director;
    
    @Column(name = "OPERATORS")
    private String operators;
    
    @Column(name = "OTHERS")
    private String otherCreators;

    @Column(name = "COUNTRY_ID")
    private Long countryId; 

    @Temporal(TemporalType.DATE)
    @Column(name = "ISSUE_DATE")
    private Date issueDate;

    @Column(name = "LANGUAGE_ID")
    private Long languageId;  
    
    @Column(name = "NOTES")
    private String notes;
    
    @OneToMany(mappedBy = "kinodoc")
    @OrderBy("numberNumber, numberText")
    private List<ArchKinoStorageUnit> storageUnits;

    public Long getUnivDataUnitId() {
        return univDataUnitId;
    }

    public void setUnivDataUnitId(Long univDataUnitId) {
        this.univDataUnitId = univDataUnitId;
    }

	public Long getKinodocTypeId() {
		return kinodocTypeId;
	}

	public void setKinodocTypeId(Long kinodocTypeId) {
		this.kinodocTypeId = kinodocTypeId;
	}

	public String getKinodocName() {
		return kinodocName;
	}

	public void setKinodocName(String kinodocName) {
		this.kinodocName = kinodocName;
	}

	public String getEventPlace() {
		return eventPlace;
	}

	public void setEventPlace(String eventPlace) {
		this.eventPlace = eventPlace;
	}

	public String getFilmStudio() {
		return filmStudio;
	}

	public void setFilmStudio(String filmStudio) {
		this.filmStudio = filmStudio;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getOperators() {
		return operators;
	}

	public void setOperators(String operators) {
		this.operators = operators;
	}

	public String getOtherCreators() {
		return otherCreators;
	}

	public void setOtherCreators(String otherCreators) {
		this.otherCreators = otherCreators;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<ArchKinoStorageUnit> getStorageUnits() {
		return storageUnits;
	}

	public void setStorageUnits(List<ArchKinoStorageUnit> storageUnits) {
		this.storageUnits = storageUnits;
	}

}
