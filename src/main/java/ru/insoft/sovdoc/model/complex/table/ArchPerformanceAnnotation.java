package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "ARCH_PERFORMANCE_ANNOTATION")
public class ArchPerformanceAnnotation 
{
    @Id
    @SequenceGenerator(sequenceName = "SEQ_ARCH_PERFORMANCE_ANNOTATIO", name = "performannotaidgen",
                       allocationSize = 1)
    @GeneratedValue(generator = "performannotaidgen", strategy = GenerationType.SEQUENCE)
    @Column(name = "ANNOTATION_ID")
    private Long annotationId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PHONO_STORAGE_UNIT_ID")
    private ArchPhonoStorageUnit storageUnit;
    
    @Column(name = "PERFORMANCE_NUMBER")
    private Integer performanceNumber;
    
    @Column(name = "SIDE_OR_TRACK")
    private String sideOrTrack;
    
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "ANNOTATION")
    private String annotation;
    
    @Column(name = "SPOKESPERSON")
    private String spokesperson;
    
    @Column(name = "LANGUAGE_ID")
    private Long languageId;
    
    @Column(name = "PLAYTIME")
    private Integer playtime;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_IMAGE_PATH_ID")
    private ImagePath filePathData;

    public Long getAnnotationId() {
        return annotationId;
    }

    public void setAnnotationId(Long annotationId) {
        this.annotationId = annotationId;
    }

    public ArchPhonoStorageUnit getStorageUnit() {
        return storageUnit;
    }

    public void setStorageUnit(ArchPhonoStorageUnit storageUnit) {
        this.storageUnit = storageUnit;
    }

    public Integer getPerformanceNumber() {
        return performanceNumber;
    }

    public void setPerformanceNumber(Integer performanceNumber) {
        this.performanceNumber = performanceNumber;
    }

    public String getSideOrTrack() {
        return sideOrTrack;
    }

    public void setSideOrTrack(String sideOrTrack) {
        this.sideOrTrack = sideOrTrack;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getSpokesperson() {
        return spokesperson;
    }

    public void setSpokesperson(String spokesperson) {
        this.spokesperson = spokesperson;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Integer getPlaytime() {
        return playtime;
    }

    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    public ImagePath getFilePathData() {
        return filePathData;
    }

    public void setFilePathData(ImagePath filePathData) {
        this.filePathData = filePathData;
    }
}
