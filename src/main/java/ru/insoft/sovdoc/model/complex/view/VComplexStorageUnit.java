package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_COMPLEX_STORAGE_UNIT")
@Immutable
public class VComplexStorageUnit {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "FILE_CAPTION")
	private String fileCaption;
	
	@Column(name = "PAGE_COUNT")
	private Integer pageCount;
	
	@Column(name = "REAL_DOCUMENT_COUNT")
	private int realDocumentCount;
	
	@Column(name = "BEGIN_DATE_STR")
	private String beginDateStr;
	
	@Column(name = "END_DATE_STR")
	private String endDateStr;
	
	@Column(name = "DATES_NOTE")
	private String datesNote;
	
	@Column(name = "WORK_INDEX")
	private String workIndex;
	
	@Column(name = "NOTES")
	private String notes;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getFileCaption() {
		return fileCaption;
	}

	public void setFileCaption(String fileCaption) {
		this.fileCaption = fileCaption;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public int getRealDocumentCount() {
		return realDocumentCount;
	}

	public void setRealDocumentCount(int realDocumentCount) {
		this.realDocumentCount = realDocumentCount;
	}

	public String getBeginDateStr() {
		return beginDateStr;
	}

	public void setBeginDateStr(String beginDateStr) {
		this.beginDateStr = beginDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getDatesNote() {
		return datesNote;
	}

	public void setDatesNote(String datesNote) {
		this.datesNote = datesNote;
	}

	public String getWorkIndex() {
		return workIndex;
	}

	public void setWorkIndex(String workIndex) {
		this.workIndex = workIndex;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
