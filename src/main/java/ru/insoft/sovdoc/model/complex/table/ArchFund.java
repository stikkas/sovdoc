package ru.insoft.sovdoc.model.complex.table;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "ARCH_FUND")
public class ArchFund {

  @Id
  @Column(name = "UNIV_DATA_UNIT_ID")
  private Long dataUnitId;

  @Column(name = "FUND_TYPE_ID")
  private Long fundTypeId;

  @Column(name = "SERIES_COUNT")
  private int seriesCount;

  @Column(name = "STORAGE_UNIT_COUNT")
  private int storageUnitCount;

  @Column(name = "NOTES")
  private String notes;

  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Column(name = "HISTORICAL_NOTE")
  private String historicalNote;

  @OneToMany(mappedBy = "fund", fetch = FetchType.EAGER)
  @OrderBy("sortOrder")
  private List<ArchFundName> fundNames;

  public Long getDataUnitId() {
    return dataUnitId;
  }

  public void setDataUnitId(Long dataUnit) {
    this.dataUnitId = dataUnit;
  }

  public Long getFundTypeId() {
    return fundTypeId;
  }

  public void setFundTypeId(Long fundTypeId) {
    this.fundTypeId = fundTypeId;
  }

  public Integer getSeriesCount() {
    return seriesCount;
  }

  public void setSeriesCount(Integer seriesCount) {
    if (seriesCount != null) {
      this.seriesCount = seriesCount;
    } else {
      this.seriesCount = 0;
    }
  }

  public Integer getStorageUnitCount() {
    return storageUnitCount;
  }

  public void setStorageUnitCount(Integer storageUnitCount) {
    if (storageUnitCount != null) {
      this.storageUnitCount = storageUnitCount;
    } else {
      this.storageUnitCount = 0;
    }
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public String getHistoricalNote() {
    return historicalNote;
  }

  public void setHistoricalNote(String historicalNote) {
    this.historicalNote = historicalNote;
  }

  public List<ArchFundName> getFundNames() {
    return fundNames;
  }

  public void setFundNames(List<ArchFundName> fundNames) {
    this.fundNames = fundNames;
  }
}
