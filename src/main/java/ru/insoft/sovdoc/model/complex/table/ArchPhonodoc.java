package ru.insoft.sovdoc.model.complex.table;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "ARCH_PHONODOC")
public class ArchPhonodoc 
{
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "RECORD_TYPE_ID")
    private Long recordTypeId;
    
    @Column(name = "PHONODOC_TYPE_ID")
    private Long phonodocTypeId;
    
    @Column(name = "PHONODOC_NAME")
    private String phonodocName;
    
    @Column(name = "AUTHORS")
    private String authors;
    
    @Column(name = "EVENT_PLACE")
    private String eventPlace;
    
    @Column(name = "PERFORMER_ORCHESTRA")
    private String performerOrchestra;
    
    @Column(name = "PERFORMER_CHOIR")
    private String performerChoir;
    
    @Column(name = "PERFORMERS")
    private String performers;
    
    @Column(name = "RUBRICS")
    private String rubrics;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "RECORD_DATE")
    private Date recordDate;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "REWRITE_DATE")
    private Date rewriteDate;
    
    @Column(name = "KIT_INFO")
    private String kitInfo;
    
    @Column(name = "MANUFACTURER_NAME")
    private String manufacturerName;
    
    @Column(name = "NOTES")
    private String notes;
    
    @OneToMany(mappedBy = "phonodoc")
    @OrderBy("numberNumber, numberText")
    private List<ArchPhonoStorageUnit> storageUnits;

    public Long getUnivDataUnitId() {
        return univDataUnitId;
    }

    public void setUnivDataUnitId(Long univDataUnitId) {
        this.univDataUnitId = univDataUnitId;
    }

    public Long getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(Long recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public Long getPhonodocTypeId() {
        return phonodocTypeId;
    }

    public void setPhonodocTypeId(Long phonodocTypeId) {
        this.phonodocTypeId = phonodocTypeId;
    }

    public String getPhonodocName() {
        return phonodocName;
    }

    public void setPhonodocName(String phonodocName) {
        this.phonodocName = phonodocName;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public String getPerformerOrchestra() {
        return performerOrchestra;
    }

    public void setPerformerOrchestra(String performerOrchestra) {
        this.performerOrchestra = performerOrchestra;
    }

    public String getPerformerChoir() {
        return performerChoir;
    }

    public void setPerformerChoir(String performerChoir) {
        this.performerChoir = performerChoir;
    }

    public String getPerformers() {
        return performers;
    }

    public void setPerformers(String performers) {
        this.performers = performers;
    }

    public String getRubrics() {
        return rubrics;
    }

    public void setRubrics(String rubrics) {
        this.rubrics = rubrics;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public Date getRewriteDate() {
        return rewriteDate;
    }

    public void setRewriteDate(Date rewriteDate) {
        this.rewriteDate = rewriteDate;
    }

    public String getKitInfo() {
        return kitInfo;
    }

    public void setKitInfo(String kitInfo) {
        this.kitInfo = kitInfo;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<ArchPhonoStorageUnit> getStorageUnits() {
        return storageUnits;
    }

    public void setStorageUnits(List<ArchPhonoStorageUnit> storageUnits) {
        this.storageUnits = storageUnits;
    }
}
