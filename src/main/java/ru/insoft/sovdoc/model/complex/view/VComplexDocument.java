package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_COMPLEX_DOCUMENT")
@Immutable
public class VComplexDocument {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "SINGLE_DOCUMENT_TYPE")
	private String singleDocumentType;
	
	@Column(name = "DOCUMENT_NUMBER")
	private String documentNumber;
	
	@Column(name = "DOCUMENT_NAME")
	private String documentName;
	
	@Column(name = "PAGE_COUNT")
	private Integer pageCount;
	
	@Column(name = "BEGIN_DATE_STR")
	private String beginDateStr;
	
	@Column(name = "END_DATE_STR")
	private String endDateStr;
	
	@Column(name = "DATES_NOTE")
	private String datesNote;
	
	@Column(name = "BASE_MATERIAL")
	private String baseMaterial;
	
	@Column(name = "REPRODUCTION_TYPE")
	private String reproductionType;
	
	@Column(name = "AUTHENTICITY_TYPE")
	private String authenticityType;
	
	@Column(name = "NOTES")
	private String notes;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getSingleDocumentType() {
		return singleDocumentType;
	}

	public void setSingleDocumentType(String singleDocumentType) {
		this.singleDocumentType = singleDocumentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public String getBeginDateStr() {
		return beginDateStr;
	}

	public void setBeginDateStr(String beginDateStr) {
		this.beginDateStr = beginDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getDatesNote() {
		return datesNote;
	}

	public void setDatesNote(String datesNote) {
		this.datesNote = datesNote;
	}

	public String getBaseMaterial() {
		return baseMaterial;
	}

	public void setBaseMaterial(String baseMaterial) {
		this.baseMaterial = baseMaterial;
	}

	public String getReproductionType() {
		return reproductionType;
	}

	public void setReproductionType(String reproductionType) {
		this.reproductionType = reproductionType;
	}

	public String getAuthenticityType() {
		return authenticityType;
	}

	public void setAuthenticityType(String authenticityType) {
		this.authenticityType = authenticityType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
