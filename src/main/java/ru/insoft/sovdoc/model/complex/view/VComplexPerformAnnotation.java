package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_PERFORM_ANNOTATION")
@Immutable
public class VComplexPerformAnnotation 
{
    @Id
    @Column(name = "ANNOTATION_ID")
    private Long annotationId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_DATA_UNIT_ID")
    private VComplexPhonodoc phonodoc;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PHONO_STORAGE_UNIT_ID")
    private ArchPhonoStorageUnit storageUnit;
    
    @Column(name = "PERFORMANCE_NUMBER")
    private Integer performanceNumber;
    
    @Column(name = "SIDE_OR_TRACK")
    private String sideOrTrack;
    
    @Column(name = "PLAYTIME")
    private Integer playtime;
    
    @Column(name = "STORAGE_UNIT_NUMBER")
    private String storageUnitNumber;
    
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "ANNOTATION")
    private String annotation;
    
    @Column(name = "SPOKESPERSON")
    private String spokesperson;
    
    @Column(name = "RECORD_LANGUAGE")
    private String recordLanguage;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_IMAGE_PATH_ID")
    private ImagePath filePathData;

    public Long getAnnotationId() {
        return annotationId;
    }

    public void setAnnotationId(Long annotationId) {
        this.annotationId = annotationId;
    }

    public VComplexPhonodoc getPhonodoc() {
        return phonodoc;
    }

    public void setPhonodoc(VComplexPhonodoc phonodoc) {
        this.phonodoc = phonodoc;
    }

    public ArchPhonoStorageUnit getStorageUnit() {
        return storageUnit;
    }

    public void setStorageUnit(ArchPhonoStorageUnit storageUnit) {
        this.storageUnit = storageUnit;
    }

    public Integer getPerformanceNumber() {
        return performanceNumber;
    }

    public void setPerformanceNumber(Integer performanceNumber) {
        this.performanceNumber = performanceNumber;
    }

    public String getSideOrTrack() {
        return sideOrTrack;
    }

    public void setSideOrTrack(String sideOrTrack) {
        this.sideOrTrack = sideOrTrack;
    }

    public Integer getPlaytime() {
        return playtime;
    }

    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    public String getStorageUnitNumber() {
        return storageUnitNumber;
    }

    public void setStorageUnitNumber(String storageUnitNumber) {
        this.storageUnitNumber = storageUnitNumber;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getSpokesperson() {
        return spokesperson;
    }

    public void setSpokesperson(String spokesperson) {
        this.spokesperson = spokesperson;
    }

    public String getRecordLanguage() {
        return recordLanguage;
    }

    public void setRecordLanguage(String recordLanguage) {
        this.recordLanguage = recordLanguage;
    }

    public ImagePath getFilePathData() {
        return filePathData;
    }

    public void setFilePathData(ImagePath filePathData) {
        this.filePathData = filePathData;
    }
}
