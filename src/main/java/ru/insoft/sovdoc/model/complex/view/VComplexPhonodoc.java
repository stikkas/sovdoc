package ru.insoft.sovdoc.model.complex.view;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_PHONODOC")
@Immutable
public class VComplexPhonodoc 
{
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "RECORD_TYPE")
    private String recordType;
    
    @Column(name = "RECORD_TYPE_CODE")
    private String recordTypeCode;
    
    @Column(name = "PHONODOC_TYPE")
    private String phonodocType;
    
    @Column(name = "PHONODOC_NAME")
    private String phonodocName;
    
    @Column(name = "AUTHORS")
    private String authors;
    
    @Column(name = "EVENT_PLACE")
    private String eventPlace;
    
    @Column(name = "PERFORMER_ORCHESTRA")
    private String performerOrchestra;
    
    @Column(name = "PERFORMER_CHOIR")
    private String performerChoir;
    
    @Column(name = "PERFORMERS")
    private String performers;
    
    @Column(name = "STORAGE_UNIT_COUNT")
    private Integer storageUnitCount;
    
    @Column(name = "EVENT_DATE_STR")
    private String eventDateStr;
    
    @Column(name = "RUBRICS")
    private String rubrics;
    
    @Column(name = "RECORD_DATE_STR")
    private String recordDateStr;
    
    @Column(name = "REWRITE_DATE_STR")
    private String rewriteDateStr;
    
    @Column(name = "KIT_INFO")
    private String kitInfo;
    
    @Column(name = "MANUFACTURER_NAME")
    private String manufacturerName;
    
    @Column(name = "NOTES")
    private String notes;
    
    @OneToMany(mappedBy = "phonodoc")
    @OrderBy("numberNumber, numberText")
    private List<VComplexPhonoStorageUnit> storageUnits;
    
    @OneToMany(mappedBy = "phonodoc")
    @OrderBy("performanceNumber")
    private List<VComplexPerformAnnotation> annotations;

    public Long getUnivDataUnitId() {
        return univDataUnitId;
    }

    public void setUnivDataUnitId(Long univDataUnitId) {
        this.univDataUnitId = univDataUnitId;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getRecordTypeCode() {
        return recordTypeCode;
    }

    public void setRecordTypeCode(String recordTypeCode) {
        this.recordTypeCode = recordTypeCode;
    }

    public String getPhonodocType() {
        return phonodocType;
    }

    public void setPhonodocType(String phonodocType) {
        this.phonodocType = phonodocType;
    }

    public String getPhonodocName() {
        return phonodocName;
    }

    public void setPhonodocName(String phonodocName) {
        this.phonodocName = phonodocName;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public String getPerformerOrchestra() {
        return performerOrchestra;
    }

    public void setPerformerOrchestra(String performerOrchestra) {
        this.performerOrchestra = performerOrchestra;
    }

    public String getPerformerChoir() {
        return performerChoir;
    }

    public void setPerformerChoir(String performerChoir) {
        this.performerChoir = performerChoir;
    }

    public String getPerformers() {
        return performers;
    }

    public void setPerformers(String performers) {
        this.performers = performers;
    }

    public Integer getStorageUnitCount() {
        return storageUnitCount;
    }

    public void setStorageUnitCount(Integer storageUnitCount) {
        this.storageUnitCount = storageUnitCount;
    }

    public String getEventDateStr() {
        return eventDateStr;
    }

    public void setEventDateStr(String eventDateStr) {
        this.eventDateStr = eventDateStr;
    }

    public String getRubrics() {
        return rubrics;
    }

    public void setRubrics(String rubrics) {
        this.rubrics = rubrics;
    }

    public String getRecordDateStr() {
        return recordDateStr;
    }

    public void setRecordDateStr(String recordDateStr) {
        this.recordDateStr = recordDateStr;
    }

    public String getRewriteDateStr() {
        return rewriteDateStr;
    }

    public void setRewriteDateStr(String rewriteDateStr) {
        this.rewriteDateStr = rewriteDateStr;
    }

    public String getKitInfo() {
        return kitInfo;
    }

    public void setKitInfo(String kitInfo) {
        this.kitInfo = kitInfo;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<VComplexPhonoStorageUnit> getStorageUnits() {
        return storageUnits;
    }

    public void setStorageUnits(List<VComplexPhonoStorageUnit> storageUnits) {
        this.storageUnits = storageUnits;
    }

    public List<VComplexPerformAnnotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<VComplexPerformAnnotation> annotations) {
        this.annotations = annotations;
    }
}
