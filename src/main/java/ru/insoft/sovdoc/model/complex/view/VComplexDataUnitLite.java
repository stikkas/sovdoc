package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Immutable;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_DATA_UNIT_LITE")
@Immutable
public class VComplexDataUnitLite 
{
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "ARCHIVE_ID")
    private Long archiveId;
    
    @Column(name = "ARCHIVE_NAME_SHORT")
    private String archiveNameShort;
    
    @Column(name = "DESCRIPTION_LEVEL_ID")
    private Long descriptionLevelId;
    
    @Column(name = "DESCRIPTION_LEVEL_CODE")
    private String descriptionLevelCode;
    
    @Column(name = "UNIT_TYPE_CODE")
    private String unitTypeCode;
    
    @Column(name = "UNIT_TYPE")
    private String unitType;

    @Column(name = "UNIT_TYPE_ID")
    private Long unitTypeId;
   
    @Column(name = "UNIT_NAME")
    private String unitName;
    
    @Column(name = "ARCH_NUMBER_CODE")
    private String archNumberCode;
    
    @Column(name = "ARCH_NUMBER_SORT")
    private String archNumberSort;
    
    @Column(name = "ANNOTATION")
    private String annotation;
    
    @Transient
    private String archNumber;
    
    @Transient
    private String dateIntervals;

    public Long getUnivDataUnitId() {
        return univDataUnitId;
    }

    public void setUnivDataUnitId(Long univDataUnitId) {
        this.univDataUnitId = univDataUnitId;
    }

    public Long getArchiveId() {
        return archiveId;
    }

    public void setArchiveId(Long archiveId) {
        this.archiveId = archiveId;
    }

    public String getArchiveNameShort() {
        return archiveNameShort;
    }

    public void setArchiveNameShort(String archiveNameShort) {
        this.archiveNameShort = archiveNameShort;
    }

    public Long getDescriptionLevelId() {
        return descriptionLevelId;
    }

    public void setDescriptionLevelId(Long descriptionLevelId) {
        this.descriptionLevelId = descriptionLevelId;
    }

    public String getDescriptionLevelCode() {
        return descriptionLevelCode;
    }

    public void setDescriptionLevelCode(String descriptionLevelCode) {
        this.descriptionLevelCode = descriptionLevelCode;
    }

    public String getUnitTypeCode() {
        return unitTypeCode;
    }

    public void setUnitTypeCode(String unitTypeCode) {
        this.unitTypeCode = unitTypeCode;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getArchNumberCode() {
        return archNumberCode;
    }

    public void setArchNumberCode(String archNumberCode) {
        this.archNumberCode = archNumberCode;
    }

    public String getArchNumberSort() {
        return archNumberSort;
    }

    public void setArchNumberSort(String archNumberSort) {
        this.archNumberSort = archNumberSort;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getArchNumber() {
        return archNumber;
    }

    public void setArchNumber(String archNumber) {
        this.archNumber = archNumber;
    }

    public String getDateIntervals() {
        return dateIntervals;
    }

    public void setDateIntervals(String dateIntervals) {
        this.dateIntervals = dateIntervals;
    }

	public Long getUnitTypeId() {
		return unitTypeId;
	}

	public void setUnitTypeId(Long unitTypeId) {
		this.unitTypeId = unitTypeId;
	}

}
