package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_PHONO_STORAGE_UNIT")
@Immutable
public class VComplexPhonoStorageUnit 
{
    @Id
    @Column(name = "PHONO_STORAGE_UNIT_ID")
    private Long phonoStorageUnitId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_DATA_UNIT_ID")
    private VComplexPhonodoc phonodoc;
    
    @Column(name = "NUMBER_NUMBER")
    private Integer numberNumber;
    
    @Column(name = "NUMBER_TEXT")
    private String numberText;
    
    @Column(name = "PLAYTIME")
    private Integer playtime;
    
    @Column(name = "STORAGE_TYPE")
    private String storageType;
    
    @Column(name = "CHANNEL_COUNT")
    private String channelCount;
    
    @Column(name = "SPEED")
    private Integer speed;
    
    @Column(name = "SOUND_QUALITY")
    private String soundQuality;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIV_IMAGE_PATH_ID")
    private ImagePath filePathData;

    public Long getPhonoStorageUnitId() {
        return phonoStorageUnitId;
    }

    public void setPhonoStorageUnitId(Long phonoStorageUnitId) {
        this.phonoStorageUnitId = phonoStorageUnitId;
    }

    public VComplexPhonodoc getPhonodoc() {
        return phonodoc;
    }

    public void setPhonodoc(VComplexPhonodoc phonodoc) {
        this.phonodoc = phonodoc;
    }

    public Integer getNumberNumber() {
        return numberNumber;
    }

    public void setNumberNumber(Integer numberNumber) {
        this.numberNumber = numberNumber;
    }

    public String getNumberText() {
        return numberText;
    }

    public void setNumberText(String numberText) {
        this.numberText = numberText;
    }

    public Integer getPlaytime() {
        return playtime;
    }

    public void setPlaytime(Integer playtime) {
        this.playtime = playtime;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getChannelCount() {
        return channelCount;
    }

    public void setChannelCount(String channelCount) {
        this.channelCount = channelCount;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public String getSoundQuality() {
        return soundQuality;
    }

    public void setSoundQuality(String soundQuality) {
        this.soundQuality = soundQuality;
    }

    public ImagePath getFilePathData() {
        return filePathData;
    }

    public void setFilePathData(ImagePath filePathData) {
        this.filePathData = filePathData;
    }
}
