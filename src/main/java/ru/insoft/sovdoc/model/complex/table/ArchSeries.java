package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARCH_SERIES")
public class ArchSeries {

  @Id
  @Column(name = "UNIV_DATA_UNIT_ID")
  private Long univDataUnitId;

  @Column(name = "DOCUMENT_KIND_ID")
  private Long documentKindId;

  @Column(name = "SERIES_NAME")
  private String seriesName;

  @Column(name = "STORAGE_UNIT_COUNT")
  private int storageUnitCount;

  @Column(name = "REPRODUCTION_TYPE_ID")
  private Long reproductionTypeId;

  @Column(name = "NOTES")
  private String notes;

  public Long getUnivDataUnitId() {
    return univDataUnitId;
  }

  public void setUnivDataUnitId(Long univDataUnitId) {
    this.univDataUnitId = univDataUnitId;
  }

  public Long getDocumentKindId() {
    return documentKindId;
  }

  public void setDocumentKindId(Long documentKindId) {
    this.documentKindId = documentKindId;
  }

  public String getSeriesName() {
    return seriesName;
  }

  public void setSeriesName(String seriesName) {
    this.seriesName = seriesName;
  }

  public Integer getStorageUnitCount() {
    return storageUnitCount;
  }

  public void setStorageUnitCount(Integer storageUnitCount) {
    if (storageUnitCount != null) {
      this.storageUnitCount = storageUnitCount;
    } else {
       this.storageUnitCount =0;
    }
  }

  public Long getReproductionTypeId() {
    return reproductionTypeId;
  }

  public void setReproductionTypeId(Long reproductionTypeId) {
    this.reproductionTypeId = reproductionTypeId;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }
}
