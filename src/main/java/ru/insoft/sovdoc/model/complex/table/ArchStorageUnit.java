package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARCH_STORAGE_UNIT")
public class ArchStorageUnit {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "VOLUME_NUM")
	private Integer volumeNum;
	
	@Column(name = "PART_NUM")
	private Integer partNum;
	
	@Column(name = "FILE_CAPTION")
	private String fileCaption;
	
	@Column(name = "PAGE_COUNT")
	private Integer pageCount;
	
	@Column(name = "DATES_NOTE")
	private String datesNote;
	
	@Column(name = "WORK_INDEX")
	private String workIndex;
	
	@Column(name = "NOTES")
	private String notes;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Integer getVolumeNum() {
		return volumeNum;
	}

	public void setVolumeNum(Integer volumeNum) {
		this.volumeNum = volumeNum;
	}

	public Integer getPartNum() {
		return partNum;
	}

	public void setPartNum(Integer partNum) {
		this.partNum = partNum;
	}

	public String getFileCaption() {
		return fileCaption;
	}

	public void setFileCaption(String fileCaption) {
		this.fileCaption = fileCaption;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public String getDatesNote() {
		return datesNote;
	}

	public void setDatesNote(String datesNote) {
		this.datesNote = datesNote;
	}

	public String getWorkIndex() {
		return workIndex;
	}

	public void setWorkIndex(String workIndex) {
		this.workIndex = workIndex;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
