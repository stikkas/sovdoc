package ru.insoft.sovdoc.model.complex.table;

import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "UNIV_DATA_UNIT")
public class UnivDataUnit {

	@Id
	@GeneratedValue(generator="undunidgen",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(sequenceName="SEQ_UNIV_DATA_UNIT",name="undunidgen", allocationSize = 1)
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "RES_OWNER_ID")
	private Long resOwnerId;
	
	@Column(name = "DESCRIPTION_LEVEL_ID")
	private Long descriptionLevelId;
	
	@Column(name = "PARENT_UNIT_ID")
	private Long parentUnitId;
	
	@Column(name = "UNIT_TYPE_ID")
	private Long unitTypeId;
	
	@Column(name = "PORTAL_SECTION_ID")
	private Long portalSectionId;
	
	@Column(name = "UNIT_NAME")
	private String unitName;
	
	@Column(name = "NUMBER_NUMBER")
	private Integer numberNumber;
	
	@Column(name = "NUMBER_PREFIX")
	private String numberPrefix;

	@Column(name = "NUMBER_TEXT")
	private String numberText;
	
	@Column(name = "ARCH_NUMBER_CODE")
	private String archNumberCode;
        
        @Lob
        @Basic(fetch = FetchType.LAZY)
        @Column(name = "ANNOTATION")
        private String annotation;
	
	@Column(name = "CONTENTS")
	private String contents;
	
	@Column(name = "IS_ACCESS_LIMITED")
	private boolean isAccessLimited;
	
	@Column(name = "ADD_USER_ID")
	private Long addUserId;
	
	@Column(name = "MOD_USER_ID")
	private Long modUserId;
	
	@Column(name = "INSERT_DATE")
	private Date insertDate;
	
	@Column(name = "LAST_UPDATE_DATE")
	private Date lastUpdateDate;
	
	@OneToMany(mappedBy = "dataUnit", fetch = FetchType.EAGER)
	private List<UnivDateInterval> dateIntervals;
	
	@OneToMany(mappedBy = "univDataUnit")
	private List<UnivDataLanguage> languages;
        
        @OneToMany(mappedBy = "dataUnit")
        private List<UnivClassifierValue> classifierValues;

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public void setNumberPrefix(String numberPrefix) {
        this.numberPrefix = numberPrefix;
    }

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getResOwnerId() {
		return resOwnerId;
	}

	public void setResOwnerId(Long resOwnerId) {
		this.resOwnerId = resOwnerId;
	}

	public Long getDescriptionLevelId() {
		return descriptionLevelId;
	}

	public void setDescriptionLevelId(Long descriptionLevelId) {
		this.descriptionLevelId = descriptionLevelId;
	}

	public Long getParentUnitId() {
		return parentUnitId;
	}

	public void setParentUnitId(Long parentUnitId) {
		this.parentUnitId = parentUnitId;
	}

	public Long getUnitTypeId() {
		return unitTypeId;
	}

	public void setUnitTypeId(Long unitTypeId) {
		this.unitTypeId = unitTypeId;
	}

	public Long getPortalSectionId() {
		return portalSectionId;
	}

	public void setPortalSectionId(Long portalSectionId) {
		this.portalSectionId = portalSectionId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Integer getNumberNumber() {
		return numberNumber;
	}

	public void setNumberNumber(Integer numberNumber) {
		this.numberNumber = numberNumber;
	}

	public String getNumberText() {
		return numberText;
	}

	public void setNumberText(String numberText) {
		this.numberText = numberText;
	}

	public String getArchNumberCode() {
		return archNumberCode;
	}

	public void setArchNumberCode(String archNumberCode) {
		this.archNumberCode = archNumberCode;
	}

        public String getAnnotation() {
            return annotation;
        }

        public void setAnnotation(String annotation) {
            this.annotation = annotation;
        }

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public boolean isAccessLimited() {
		return isAccessLimited;
	}

	public void setAccessLimited(boolean isAccessLimited) {
		this.isAccessLimited = isAccessLimited;
	}

	public Long getAddUserId() {
		return addUserId;
	}

	public void setAddUserId(Long addUserId) {
		this.addUserId = addUserId;
	}

	public Long getModUserId() {
		return modUserId;
	}

	public void setModUserId(Long modUserId) {
		this.modUserId = modUserId;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public List<UnivDateInterval> getDateIntervals() {
		return dateIntervals;
	}

	public void setDateIntervals(List<UnivDateInterval> dateIntervals) {
		this.dateIntervals = dateIntervals;
	}

	public List<UnivDataLanguage> getLanguages() {
		return languages;
	}

	public void setLanguages(List<UnivDataLanguage> languages) {
		this.languages = languages;
	}

    public List<UnivClassifierValue> getClassifierValues() {
        return classifierValues;
    }

    public void setClassifierValues(List<UnivClassifierValue> classifierValues) {
        this.classifierValues = classifierValues;
    }
}
