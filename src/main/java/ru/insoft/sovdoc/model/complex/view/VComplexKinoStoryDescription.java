package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;
import ru.insoft.sovdoc.model.complex.table.ArchKinoStorageUnit;
import ru.insoft.sovdoc.model.showfile.ImagePath;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_STORY_KINO_DESC")
@Immutable
public class VComplexKinoStoryDescription {

	@Id
	@Column(name = "STORY_DESC_ID")
	private Long storyDescriptionId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private VComplexKinodoc kinodoc;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "KINO_STORAGE_UNIT_ID")
	private ArchKinoStorageUnit storageUnit;

	@Column(name = "STORY_NUMBER")
	private Integer storyNumber;

	@Column(name = "STORAGE_UNIT_NUMBER")
	private String storageUnitNumber;

	@Column(name = "PART")
	private Integer part;

	@Column(name = "PLAYTIME")
	private Integer playtime;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "ANNOTATION")
	private String annotation;

	@Column(name = "LANG")
	private String language;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_IMAGE_PATH_ID")
	private ImagePath filePathData;

	public Long getStoryDescriptionId() {
		return storyDescriptionId;
	}

	public void setStoryDescriptionId(Long storyDescriptionId) {
		this.storyDescriptionId = storyDescriptionId;
	}

	public VComplexKinodoc getKinodoc() {
		return kinodoc;
	}

	public void setKinodoc(VComplexKinodoc kinodoc) {
		this.kinodoc = kinodoc;
	}

	public ArchKinoStorageUnit getStorageUnit() {
		return storageUnit;
	}

	public void setStorageUnit(ArchKinoStorageUnit storageUnit) {
		this.storageUnit = storageUnit;
	}

	public Integer getStoryNumber() {
		return storyNumber;
	}

	public void setStoryNumber(Integer storyNumber) {
		this.storyNumber = storyNumber;
	}

	public String getStorageUnitNumber() {
		return storageUnitNumber;
	}

	public void setStorageUnitNumber(String storageUnitNumber) {
		this.storageUnitNumber = storageUnitNumber;
	}

	public Integer getPart() {
		return part;
	}

	public void setPart(Integer part) {
		this.part = part;
	}

	public Integer getPlaytime() {
		return playtime;
	}

	public void setPlaytime(Integer playtime) {
		this.playtime = playtime;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public ImagePath getFilePathData() {
		return filePathData;
	}

	public void setFilePathData(ImagePath filePathData) {
		this.filePathData = filePathData;
	}

}
