package ru.insoft.sovdoc.model.complex.view;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_COMPLEX_VIDEODOC")
@Immutable
public class VComplexVideodoc {
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "VIDEODOC_TYPE")
    private String videodocType;

    @Column(name = "VIDEODOC_NAME")
    private String videodocName;
    
    @Column(name = "EVENT_PLACE")
    private String eventPlace;
   
    @Column(name = "FILM_STUDIO")
    private String filmStudio;
    
    @Column(name = "DIRECTOR")
    private String director;
    
    @Column(name = "OPERATORS")
    private String operators;
 
    @Column(name = "OTHERS")
    private String others;
    
    @Column(name = "COUNTRY")
    private String country;
    
    @Column(name = "STORAGE_UNIT_COUNT")
    private Integer storageUnitCount;
   
    @Column(name = "EVENT_DATE")
    private String eventDate;
    
    @Column(name = "ISSUE_DATE")
    private String issueDate;
    
    @Column(name = "LANG")
    private String language;

    @Column(name = "NOTES")
    private String notes;
    
    @OneToMany(mappedBy = "videodoc")
    @OrderBy("numberNumber, numberText")
    private List<VComplexVideoStorageUnit> storageUnits;
    
    @OneToMany(mappedBy = "videodoc")
    @OrderBy("storyNumber")
    private List<VComplexVideoStoryDescription> storyDescriptions;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getVideodocType() {
		return videodocType;
	}

	public void setVideodocType(String videodocType) {
		this.videodocType = videodocType;
	}

	public String getVideodocName() {
		return videodocName;
	}

	public void setVideodocName(String videodocName) {
		this.videodocName = videodocName;
	}

	public Integer getStorageUnitCount() {
		return storageUnitCount;
	}

	public void setStorageUnitCount(Integer storageUnitCount) {
		this.storageUnitCount = storageUnitCount;
	}

	public List<VComplexVideoStorageUnit> getStorageUnits() {
		return storageUnits;
	}

	public void setStorageUnits(List<VComplexVideoStorageUnit> storageUnits) {
		this.storageUnits = storageUnits;
	}

	public String getEventPlace() {
		return eventPlace;
	}

	public void setEventPlace(String eventPlace) {
		this.eventPlace = eventPlace;
	}

	public String getFilmStudio() {
		return filmStudio;
	}

	public void setFilmStudio(String filmStudio) {
		this.filmStudio = filmStudio;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getOperators() {
		return operators;
	}

	public void setOperators(String operators) {
		this.operators = operators;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<VComplexVideoStoryDescription> getStoryDescriptions() {
		return storyDescriptions;
	}

	public void setStoryDescriptions(List<VComplexVideoStoryDescription> storyDescriptions) {
		this.storyDescriptions = storyDescriptions;
	}

}
