package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "UNIV_DATA_LANGUAGE")
public class UnivDataLanguage implements Serializable {

	@Id
	@Column(name = "DOC_LANGUAGE_ID")
	private Long docLanguageId;
	
	@Id
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private UnivDataUnit univDataUnit;

	public Long getDocLanguageId() {
		return docLanguageId;
	}

	public void setDocLanguageId(Long docLanguageId) {
		this.docLanguageId = docLanguageId;
	}

	public UnivDataUnit getUnivDataUnit() {
		return univDataUnit;
	}

	public void setUnivDataUnit(UnivDataUnit univDataUnit) {
		this.univDataUnit = univDataUnit;
	}
}
