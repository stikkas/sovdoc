package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_COMPLEX_ARCHIVE_LIST")
public class VComplexArchiveList {

	@Id
	@Column(name = "ARCHIVE_ID")
	private Long archiveId;
	
	@Column(name = "ARCHIVE_NAME")
	private String archiveName;

	public Long getArchiveId() {
		return archiveId;
	}

	public void setArchiveId(Long archiveId) {
		this.archiveId = archiveId;
	}

	public String getArchiveName() {
		return archiveName;
	}

	public void setArchiveName(String archiveName) {
		this.archiveName = archiveName;
	}
}
