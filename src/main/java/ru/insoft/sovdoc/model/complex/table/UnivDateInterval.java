package ru.insoft.sovdoc.model.complex.table;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "UNIV_DATE_INTERVAL")
public class UnivDateInterval {

	
	
	@Id
	@SequenceGenerator(sequenceName="SEQ_UNIV_DATE_INTERVAL",name="univdateintidgen", allocationSize = 1)
	@GeneratedValue(generator="univdateintidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "DATE_INTERVAL_ID")
	private Long dateInetrvalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private UnivDataUnit dataUnit;
	
	@Column(name = "UNIV_DATA_UNIT_ID", insertable = false, updatable = false)
	private Long univDataUnitId;
	
	@Column(name = "BEGIN_DATE")
	private Date beginDate;
	
	@Column(name = "END_DATE")
	private Date endDate;

	public Long getDateInetrvalId() {
		return dateInetrvalId;
	}

	public void setDateInetrvalId(Long dateInetrvalId) {
		this.dateInetrvalId = dateInetrvalId;
	}

	public UnivDataUnit getDataUnit() {
		return dataUnit;
	}

	public void setDataUnit(UnivDataUnit dataUnit) {
		this.dataUnit = dataUnit;
	}

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
