package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_COMPLEX_INTERVAL_YEARS")
@Immutable
public class VComplexIntervalYears {

	@Id
	@Column(name = "DATE_INTERVAL_ID")
	private Long dateIntervalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private VComplexDataUnit dataUnit;
	
	@Column(name = "BEGIN_YEAR")
	private Integer beginYear;
	
	@Column(name = "END_YEAR")
	private Integer endYear;

	public Long getDateIntervalId() {
		return dateIntervalId;
	}

	public void setDateIntervalId(Long dateIntervalId) {
		this.dateIntervalId = dateIntervalId;
	}

	public VComplexDataUnit getDataUnit() {
		return dataUnit;
	}

	public void setDataUnit(VComplexDataUnit dataUnit) {
		this.dataUnit = dataUnit;
	}

	public Integer getBeginYear() {
		return beginYear;
	}

	public void setBeginYear(Integer beginYear) {
		this.beginYear = beginYear;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}
}
