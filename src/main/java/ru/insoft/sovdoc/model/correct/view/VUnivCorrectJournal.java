package ru.insoft.sovdoc.model.correct.view;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: vasilev
 * Date: 10.04.13
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "V_UNIV_CORRECT_JOURNAL")
@Immutable
public class VUnivCorrectJournal implements Serializable {

    @Id
    @Column(name = "UNIV_CORRECT_JOURNAL_ID")
    private Long univCorrectJournalId;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "ARCHIVE_ID")
    private Long archiveId;

    @Column(name = "ARCHIVE_NAME")
    private String archiveName;

    @Column(name = "ARCHIVE_NAME_FULL")
    private String archiveNameFull;

    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;

    @Column(name = "DESCRIPTOR_VALUE_ID")
    private Long descriptorValueId;

    @Column(name = "OPERATION_TYPE_ID")
    private Long operationTypeId;

    @Column(name = "OPERATION_TYPE")
    private String operationType;

    @Column(name = "CHANGED_ENTITY_TYPE_ID")
    private Long changedEntityTypeId;

    @Column(name = "CHANGED_ENTITY_TYPE")
    private String changedEntityType;

    @Column(name = "OPERATION_DATE")
    private Date operationDate;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "DATA_BLOCK")
    private String dataBlock;

    @Column(name = "IDENTIFICATION")
    private String identification;

    public Long getUnivCorrectJournalId() {
        return univCorrectJournalId;
    }

    public void setUnivCorrectJournalId(Long univCorrectJournalId) {
        this.univCorrectJournalId = univCorrectJournalId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getArchiveId() {
        return archiveId;
    }

    public void setArchiveId(Long archiveId) {
        this.archiveId = archiveId;
    }

    public String getArchiveName() {
        return archiveName;
    }

    public void setArchiveName(String archiveName) {
        this.archiveName = archiveName;
    }

    public String getArchiveNameFull() {
        return archiveNameFull;
    }

    public void setArchiveNameFull(String archiveNameFull) {
        this.archiveNameFull = archiveNameFull;
    }

    public Long getUnivDataUnitId() {
        return univDataUnitId;
    }

    public void setUnivDataUnitId(Long univDataUnitId) {
        this.univDataUnitId = univDataUnitId;
    }

    public Long getDescriptorValueId() {
        return descriptorValueId;
    }

    public void setDescriptorValueId(Long descriptorValueId) {
        this.descriptorValueId = descriptorValueId;
    }

    public Long getOperationTypeId() {
        return operationTypeId;
    }

    public void setOperationTypeId(Long operationTypeId) {
        this.operationTypeId = operationTypeId;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getChangedEntityTypeId() {
        return changedEntityTypeId;
    }

    public void setChangedEntityTypeId(Long changedEntityTypeId) {
        this.changedEntityTypeId = changedEntityTypeId;
    }

    public String getChangedEntityType() {
        return changedEntityType;
    }

    public void setChangedEntityType(String changedEntityType) {
        this.changedEntityType = changedEntityType;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getDataBlock() {
        return dataBlock;
    }

    public void setDataBlock(String dataBlock) {
        this.dataBlock = dataBlock;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }
}
