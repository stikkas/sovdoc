package ru.insoft.sovdoc.model.correct.table;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "UNIV_CORRECT_JOURNAL")
public class UnivCorrectJournal {

	@Id
	@SequenceGenerator(sequenceName="SEQ_UNIV_CORRECT_JOURNAL",name="univcorrectjournalidgen", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="univcorrectjournalidgen")
	@Column(name = "UNIV_CORRECT_JOURNAL_ID")
	private Long univCorrectJournalId;
	
	@Column(name = "USER_ID")
	private Long userId;
	
	@Column(name = "ARCHIVE_ID")
	private Long archiveId;
	
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "DESCRIPTOR_VALUE_ID")
	private Long descriptorValueId;
	
	@Column(name = "OPERATION_TYPE_ID")
	private Long operationTypeId;
	
	@Column(name = "CHANGED_ENTITY_TYPE_ID")
	private Long changedEntityTypeId;
	
	@Column(name = "OPERATION_DATE")
	private Date operationDate;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "DATA_BLOCK")
	private String dataBlock;

	public Long getUnivCorrectJournalId() {
		return univCorrectJournalId;
	}

	public void setUnivCorrectJournalId(Long univCorrectJournalId) {
		this.univCorrectJournalId = univCorrectJournalId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getArchiveId() {
		return archiveId;
	}

	public void setArchiveId(Long archiveId) {
		this.archiveId = archiveId;
	}

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public Long getOperationTypeId() {
		return operationTypeId;
	}

	public void setOperationTypeId(Long operationTypeId) {
		this.operationTypeId = operationTypeId;
	}

	public Long getChangedEntityTypeId() {
		return changedEntityTypeId;
	}

	public void setChangedEntityTypeId(Long changedEntityTypeId) {
		this.changedEntityTypeId = changedEntityTypeId;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getDataBlock() {
		return dataBlock;
	}

	public void setDataBlock(String dataBlock) {
		this.dataBlock = dataBlock;
	}
}
