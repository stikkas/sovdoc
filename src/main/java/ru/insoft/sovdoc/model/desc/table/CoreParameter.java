package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CORE_PARAMETER")
public class CoreParameter {

	@Id
	@Column(name = "PARAMETER_CODE")
	private String parameterCode;
	
	@Column(name = "SUBSYSTEM_NUMBER")
	private Integer subsystemNumber;
	
	@Column(name = "PARAMETER_NAME")
	private String parameterName;
	
	@Column(name = "PARAMETER_DESCRIPTION")
	private String parameterDescription;
	
	@Column(name = "PARAMETER_VALUE")
	private String parameterValue;

	public String getParameterCode() {
		return parameterCode;
	}

	public void setParameterCode(String parameterCode) {
		this.parameterCode = parameterCode;
	}

	public Integer getSubsystemNumber() {
		return subsystemNumber;
	}

	public void setSubsystemNumber(Integer subsystemNumber) {
		this.subsystemNumber = subsystemNumber;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterDescription() {
		return parameterDescription;
	}

	public void setParameterDescription(String parameterDescription) {
		this.parameterDescription = parameterDescription;
	}

	public String getParameterValue() {
		return parameterValue;
	}

	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}
}
