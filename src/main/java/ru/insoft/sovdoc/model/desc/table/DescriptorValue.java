package ru.insoft.sovdoc.model.desc.table;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DESCRIPTOR_VALUE")
public class DescriptorValue {

	@Id
	@SequenceGenerator(sequenceName="SEQ_DESCRIPTOR_VALUE",name="dvidgen", allocationSize = 1)
	@GeneratedValue(generator="dvidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "DESCRIPTOR_VALUE_ID")
	private Long descriptorValueId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DESCRIPTOR_GROUP_ID", insertable = false, updatable = false)
	private DescriptorGroup descriptorGroup;

	@Column(name = "DESCRIPTOR_GROUP_ID")
	private Long descriptorGroupId;
	
	@Column(name = "FULL_VALUE")
	private String fullValue;
	
	@Column(name = "SHORT_VALUE")
	private String shortValue;
	
	@Column(name = "VALUE_CODE")
	private String valueCode;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;
	
	@Column(name = "IS_ACTUAL")
	private boolean isActual;
	
	@Column(name = "PARENT_VALUE_ID")
	private Long parentValueId;
	
	@OneToMany(mappedBy = "descriptorValue")
	@OrderBy("descriptorValueAttrId")
	private List<DescriptorValueAttr> valueAttrList;
	
	@OneToMany(mappedBy = "descriptorValue")
	private List<DescValueInternational> multilangList;

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public DescriptorGroup getDescriptorGroup() {
		return descriptorGroup;
	}

	public void setDescriptorGroup(DescriptorGroup descriptorGroup) {
		this.descriptorGroup = descriptorGroup;
	}

	public Long getDescriptorGroupId() {
		return descriptorGroupId;
	}

	public void setDescriptorGroupId(Long descriptorGroupId) {
		this.descriptorGroupId = descriptorGroupId;
	}

	public String getFullValue() {
		return fullValue;
	}

	public void setFullValue(String fullValue) {
		this.fullValue = fullValue;
	}

	public String getShortValue() {
		return shortValue;
	}

	public void setShortValue(String shortValue) {
		this.shortValue = shortValue;
	}

	public String getValueCode() {
		return valueCode;
	}

	public void setValueCode(String valueCode) {
		this.valueCode = valueCode;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isActual() {
		return isActual;
	}

	public void setActual(boolean isActual) {
		this.isActual = isActual;
	}

	public Long getParentValueId() {
		return parentValueId;
	}

	public void setParentValueId(Long parentValueId) {
		this.parentValueId = parentValueId;
	}

	public List<DescriptorValueAttr> getValueAttrList() {
		return valueAttrList;
	}

	public void setValueAttrList(List<DescriptorValueAttr> valueAttrList) {
		this.valueAttrList = valueAttrList;
	}

	public List<DescValueInternational> getMultilangList() {
		return multilangList;
	}

	public void setMultilangList(List<DescValueInternational> multilangList) {
		this.multilangList = multilangList;
	}
}
