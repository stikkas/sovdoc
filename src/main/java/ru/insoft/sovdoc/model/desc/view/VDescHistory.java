package ru.insoft.sovdoc.model.desc.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "V_DESC_HISTORY")
public class VDescHistory implements Serializable {

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DESCRIPTOR_VALUE_ID")
	private VDescValue descriptorValue;
	
	@Id
	@Column(name = "DESCRIPTOR_HISTORY_ID")
	private Long descriptorHistoryId;
	
	@Column(name = "OLD_VALUE_ID")
	private Long oldValueId;
	
	@Column(name = "FULL_VALUE")
	private String fullValue;
	
	@Column(name = "IS_ACTUAL")
	private boolean isActual;
	
	@Column(name = "ACTUAL_VALUE_ID")
	private Long actualValueId;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;
	
	@Column(name = "CHANGE_DATE")
	private String changeDate;

	public VDescValue getDescriptorValue() {
		return descriptorValue;
	}

	public void setDescriptorValue(VDescValue descriptorValue) {
		this.descriptorValue = descriptorValue;
	}

	public Long getDescriptorHistoryId() {
		return descriptorHistoryId;
	}

	public void setDescriptorHistoryId(Long descriptorHistoryId) {
		this.descriptorHistoryId = descriptorHistoryId;
	}

	public Long getOldValueId() {
		return oldValueId;
	}

	public void setOldValueId(Long oldValueId) {
		this.oldValueId = oldValueId;
	}

	public String getFullValue() {
		return fullValue;
	}

	public void setFullValue(String fullValue) {
		this.fullValue = fullValue;
	}

	public boolean isActual() {
		return isActual;
	}

	public void setActual(boolean isActual) {
		this.isActual = isActual;
	}

	public Long getActualValueId() {
		return actualValueId;
	}

	public void setActualValueId(Long actualValueId) {
		this.actualValueId = actualValueId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
}
