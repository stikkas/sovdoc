package ru.insoft.sovdoc.model.desc.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "V_DESC_VALUE_PATH")
@Immutable
public class VDescValuePath {

	@Id
	@Column(name = "DESCRIPTOR_VALUE_ID")
	private Long descriptorValueId;
	
	@Column(name = "DESCRIPTOR_GROUP_ID")
	private Long descriptorGroupId;
	
	@Column(name = "FULL_VALUE")
	private String fullValue;
	
	@Column(name = "SHORT_VALUE")
	private String shortValue;
	
	@Column(name = "IS_ACTUAL")
	private boolean isActual;
	
	@Column(name = "FULL_PATH")
	private String fullPath;
	
	@Column(name = "SHORT_PATH")
	private String shortPath;
	
	@Column(name = "ID_PATH")
	private String idPath;

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public Long getDescriptorGroupId() {
		return descriptorGroupId;
	}

	public void setDescriptorGroupId(Long descriptorGroupId) {
		this.descriptorGroupId = descriptorGroupId;
	}

	public String getFullValue() {
		return fullValue;
	}

	public void setFullValue(String fullValue) {
		this.fullValue = fullValue;
	}

	public String getShortValue() {
		return shortValue;
	}

	public void setShortValue(String shortValue) {
		this.shortValue = shortValue;
	}

	public boolean isActual() {
		return isActual;
	}

	public void setActual(boolean isActual) {
		this.isActual = isActual;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getShortPath() {
		return shortPath;
	}

	public void setShortPath(String shortPath) {
		this.shortPath = shortPath;
	}

	public String getIdPath() {
		return idPath;
	}

	public void setIdPath(String idPath) {
		this.idPath = idPath;
	}
}
