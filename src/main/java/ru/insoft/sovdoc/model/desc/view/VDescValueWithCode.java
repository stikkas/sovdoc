package ru.insoft.sovdoc.model.desc.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_DESC_VALUE_WITH_CODE")
public class VDescValueWithCode {
	
	@Column(name = "GROUP_CODE")
	private String groupCode;
	
	@Id
	@Column(name = "DESCRIPTOR_VALUE_ID")
	private Long descriptorValueId;
	
	@Column(name = "FULL_VALUE")
	private String fullValue;
	
	@Column(name = "VALUE_CODE")
	private String valueCode;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public String getFullValue() {
		return fullValue;
	}

	public void setFullValue(String fullValue) {
		this.fullValue = fullValue;
	}

	public String getValueCode() {
		return valueCode;
	}

	public void setValueCode(String valueCode) {
		this.valueCode = valueCode;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
}
