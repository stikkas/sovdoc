package ru.insoft.sovdoc.model.desc.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_DESC_SUBSYSTEM")
public class VDescSubsystem {

	@Id
	@Column(name = "SUBSYSTEM_NUMBER")
	private Long subsystemNumber;
	
	@Column(name = "SUBSYSTEM_NAME")
	private String subsystemName;
	
	@Column(name = "HAS_GROUPS")
	private boolean hasGroups;
	
	@Column(name = "GROUP_CNT")
	private int groupCnt;

	public Long getSubsystemNumber() {
		return subsystemNumber;
	}

	public void setSubsystemNumber(Long subsystemNumber) {
		this.subsystemNumber = subsystemNumber;
	}

	public String getSubsystemName() {
		return subsystemName;
	}

	public void setSubsystemName(String subsystemName) {
		this.subsystemName = subsystemName;
	}

	public boolean isHasGroups() {
		return hasGroups;
	}

	public void setHasGroups(boolean hasGroups) {
		this.hasGroups = hasGroups;
	}

	public int getGroupCnt() {
		return groupCnt;
	}

	public void setGroupCnt(int groupCnt) {
		this.groupCnt = groupCnt;
	}
}
