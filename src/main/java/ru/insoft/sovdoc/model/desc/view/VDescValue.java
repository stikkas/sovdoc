package ru.insoft.sovdoc.model.desc.view;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "V_DESC_VALUE")
public class VDescValue {

	@Id
	@Column(name = "DESCRIPTOR_VALUE_ID")
	private Long descriptorValueId;
	
	@Column(name = "DESCRIPTOR_GROUP_ID")
	private Long descriptorGroupId;
	
	@Column(name = "GROUP_NAME")
	private String groupName;
	
	@Column(name = "FULL_VALUE")
	private String fullValue;
	
	@Column(name = "SHORT_VALUE")
	private String shortValue;
	
	@Column(name = "VALUE_CODE")
	private String valueCode;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;
	
	@Column(name = "IS_ACTUAL")
	private boolean isActual;
	
	@Column(name = "PARENT_VALUE_ID")
	private Long parentValueId;
	
	@Column(name = "HAS_CHILDREN")
	private boolean hasChildren;
	
	@Column(name = "CHILDREN_CNT")
	private int childrenCnt;
	
	@OneToMany(cascade = CascadeType.REFRESH, mappedBy = "descriptorValue")
	@OrderBy("sortOrder")
	private List<VDescHistory> historyList;
        
        public VDescValue() {}                
        
        public VDescValue(VDescValueUnivclss value)
        {
            descriptorValueId = value.getDescriptorValueId();
            descriptorGroupId = value.getDescriptorGroupId();
            groupName         = value.getGroupName();
            fullValue         = String.format("%1$s %2$s", value.getValueIndex(), value.getFullValue());
            shortValue        = String.format("%1$s %2$s", value.getValueIndex(), value.getShortValue());
            valueCode         = value.getValueCode();
            sortOrder         = value.getSortOrder();
            isActual          = value.isIsActual();
            parentValueId     = value.getParentValueId();
            hasChildren       = value.isHasChildren();
            childrenCnt       = value.getChildrenCnt();
        }

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public Long getDescriptorGroupId() {
		return descriptorGroupId;
	}

	public void setDescriptorGroupId(Long descriptorGroupId) {
		this.descriptorGroupId = descriptorGroupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getFullValue() {
		return fullValue;
	}

	public void setFullValue(String fullValue) {
		this.fullValue = fullValue;
	}

	public String getShortValue() {
		return shortValue;
	}

	public void setShortValue(String shortValue) {
		this.shortValue = shortValue;
	}

	public String getValueCode() {
		return valueCode;
	}

	public void setValueCode(String valueCode) {
		this.valueCode = valueCode;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isActual() {
		return isActual;
	}

	public void setActual(boolean isActual) {
		this.isActual = isActual;
	}

	public Long getParentValueId() {
		return parentValueId;
	}

	public void setParentValueId(Long parentValueId) {
		this.parentValueId = parentValueId;
	}

	public boolean isHasChildren() {
		return hasChildren;
	}

	public void setHasChildren(boolean hasChildren) {
		this.hasChildren = hasChildren;
	}

	public int getChildrenCnt() {
		return childrenCnt;
	}

	public void setChildrenCnt(int childrenCnt) {
		this.childrenCnt = childrenCnt;
	}

	public List<VDescHistory> getHistoryList() {
		return historyList;
	}

	public void setHistoryList(List<VDescHistory> historyList) {
		this.historyList = historyList;
	}
}
