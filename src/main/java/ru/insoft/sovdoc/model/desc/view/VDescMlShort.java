package ru.insoft.sovdoc.model.desc.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.insoft.sovdoc.ui.desc.MultilingualView;

@Entity
@Table(name = "V_DESC_ML_SHORT")
public class VDescMlShort implements MultilingualView {

	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "ROOT_ID")
	private Long rootId;
	
	@Column(name = "VALUE")
	private String value;
	
	@Column(name = "LANGUAGE_CODE")
	private String languageCode;
	
	@Column(name = "LANGUAGE_NAME")
	private String languageName;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRootId() {
		return rootId;
	}

	public void setRootId(Long rootId) {
		this.rootId = rootId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

}
