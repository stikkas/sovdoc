package ru.insoft.sovdoc.model.desc.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_DESC_ATTRVALUE_WITH_CODE")
public class VDescAttrvalueWithCode {

	@Id
	@Column(name = "DESCRIPTOR_VALUE_ATTR_ID")
	private Long descriptorValueAttrId;
	
	@Column(name = "DESCRIPTOR_VALUE_ID")
	private Long descriptorValueId;
	
	@Column(name = "DATATYPE_CODE")
	private String datatypeCode;
	
	@Column(name = "ATTR_CODE")
	private String attrCode;
	
	@Column(name = "ATTR_VALUE")
	private String attrValue;
	
	@Column(name = "REF_DESCRIPTOR_VALUE_ID")
	private Long refDescriptorValueId;

	public Long getDescriptorValueAttrId() {
		return descriptorValueAttrId;
	}

	public void setDescriptorValueAttrId(Long descriptorValueAttrId) {
		this.descriptorValueAttrId = descriptorValueAttrId;
	}

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public String getDatatypeCode() {
		return datatypeCode;
	}

	public void setDatatypeCode(String datatypeCode) {
		this.datatypeCode = datatypeCode;
	}

	public String getAttrCode() {
		return attrCode;
	}

	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	}

	public String getAttrValue() {
		return attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public Long getRefDescriptorValueId() {
		return refDescriptorValueId;
	}

	public void setRefDescriptorValueId(Long refDescriptorValueId) {
		this.refDescriptorValueId = refDescriptorValueId;
	}
}
