package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DESC_DATATYPE")
public class DescDatatype {

	@Id
	@Column(name = "DATATYPE_CODE")
	private String datatypeCode;
	
	@Column(name = "TYPE_NAME")
	private String typeName;
	
	@Column(name = "SORT_ORDER")
	private Long sortOrder;

	public String getDatatypeCode() {
		return datatypeCode;
	}

	public void setDatatypeCode(String datatypeCode) {
		this.datatypeCode = datatypeCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}
}
