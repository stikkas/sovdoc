package ru.insoft.sovdoc.model.desc.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "V_DESC_ALL_RELATIONS")
public class VDescAllRelations implements Serializable {

	@Id
	@Column(name = "DESCRIPTOR_VALUE_ID")
	private Long descriptorValueId;
	
	@Id
	@Column(name = "RELATED_VALUE_ID")
	private Long relatedValueId;

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public Long getRelatedValueId() {
		return relatedValueId;
	}

	public void setRelatedValueId(Long relatedValueId) {
		this.relatedValueId = relatedValueId;
	}
}
