package ru.insoft.sovdoc.model.desc.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_DESC_GROUP_ATTR")
public class VDescGroupAttr {

	@Id
	@Column(name = "DESCRIPTOR_GROUP_ATTR_ID")
	private Long descriptorGroupAttrId;
	
	@Column(name = "DESCRIPTOR_GROUP_ID")
	private Long descriptorGroupId;
	
	@Column(name = "ATTR_NAME")
	private String attrName;
	
	@Column(name = "ATTR_CODE")
	private String attrCode;
	
	@Column(name = "DATATYPE_CODE")
	private String datatypeCode;
	
	@Column(name = "DATATYPE")
	private String datatype;
	
	@Column(name = "SORT_ORDER")
	private Long sortOrder;
	
	@Column(name = "IS_REQUIRED")
	private boolean isRequired;
	
	@Column(name = "IS_COLLECTION")
	private boolean isCollection;
	
	@Column(name = "REF_DESCRIPTOR_GROUP_ID")
	private Long refDescriptorGroupId;
	
	@Column(name = "REF_GROUP_NAME")
	private String refGroupName;
	
	@Column(name = "HAS_VALUES")
	private boolean hasValues;

	public Long getDescriptorGroupAttrId() {
		return descriptorGroupAttrId;
	}

	public void setDescriptorGroupAttrId(Long descriptorGroupAttrId) {
		this.descriptorGroupAttrId = descriptorGroupAttrId;
	}

	public Long getDescriptorGroupId() {
		return descriptorGroupId;
	}

	public void setDescriptorGroupId(Long descriptorGroupId) {
		this.descriptorGroupId = descriptorGroupId;
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public String getAttrCode() {
		return attrCode;
	}

	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	}

	public String getDatatypeCode() {
		return datatypeCode;
	}

	public void setDatatypeCode(String datatypeCode) {
		this.datatypeCode = datatypeCode;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public boolean isCollection() {
		return isCollection;
	}

	public void setCollection(boolean isCollection) {
		this.isCollection = isCollection;
	}

	public Long getRefDescriptorGroupId() {
		return refDescriptorGroupId;
	}

	public void setRefDescriptorGroupId(Long refDescriptorGroupId) {
		this.refDescriptorGroupId = refDescriptorGroupId;
	}

	public String getRefGroupName() {
		return refGroupName;
	}

	public void setRefGroupName(String refGroupName) {
		this.refGroupName = refGroupName;
	}

	public boolean isHasValues() {
		return hasValues;
	}

	public void setHasValues(boolean hasValues) {
		this.hasValues = hasValues;
	}
}
