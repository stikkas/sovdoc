package ru.insoft.sovdoc.model.desc.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 *
 * @author melnikov
 */
@Entity
@Table(name = "V_DESC_VALUE_UNIVCLSS")
@Immutable
public class VDescValueUnivclss 
{
    @Id
    @Column(name = "DESCRIPTOR_VALUE_ID")
    private Long descriptorValueId;
    
    @Column(name = "DESCRIPTOR_GROUP_ID")
    private Long descriptorGroupId;
    
    @Column(name = "GROUP_NAME")
    private String groupName;
    
    @Column(name = "FULL_VALUE")
    private String fullValue;
    
    @Column(name = "SHORT_VALUE")
    private String shortValue;
    
    @Column(name = "VALUE_CODE")
    private String valueCode;
    
    @Column(name = "SORT_ORDER")
    private Integer sortOrder;
    
    @Column(name = "IS_ACTUAL")
    private boolean isActual;
    
    @Column(name = "PARENT_VALUE_ID")
    private Long parentValueId;
    
    @Column(name = "HAS_CHILDREN")
    private boolean hasChildren;
    
    @Column(name = "CHILDREN_CNT")
    private int childrenCnt;
    
    @Column(name = "VALUE_INDEX")
    private String valueIndex;

    public Long getDescriptorValueId() {
        return descriptorValueId;
    }

    public void setDescriptorValueId(Long descriptorValueId) {
        this.descriptorValueId = descriptorValueId;
    }

    public Long getDescriptorGroupId() {
        return descriptorGroupId;
    }

    public void setDescriptorGroupId(Long descriptorGroupId) {
        this.descriptorGroupId = descriptorGroupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFullValue() {
        return fullValue;
    }

    public void setFullValue(String fullValue) {
        this.fullValue = fullValue;
    }

    public String getShortValue() {
        return shortValue;
    }

    public void setShortValue(String shortValue) {
        this.shortValue = shortValue;
    }

    public String getValueCode() {
        return valueCode;
    }

    public void setValueCode(String valueCode) {
        this.valueCode = valueCode;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public boolean isIsActual() {
        return isActual;
    }

    public void setIsActual(boolean isActual) {
        this.isActual = isActual;
    }

    public Long getParentValueId() {
        return parentValueId;
    }

    public void setParentValueId(Long parentValueId) {
        this.parentValueId = parentValueId;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public int getChildrenCnt() {
        return childrenCnt;
    }

    public void setChildrenCnt(int childrenCnt) {
        this.childrenCnt = childrenCnt;
    }

    public String getValueIndex() {
        return valueIndex;
    }

    public void setValueIndex(String valueIndex) {
        this.valueIndex = valueIndex;
    }
}
