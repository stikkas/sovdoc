package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ru.insoft.sovdoc.ui.desc.MultilingualTable;

@Entity
@Table(name = "DESC_VALUE_INTERNATIONAL")
public class DescValueInternational implements MultilingualTable {

	@Id
	@SequenceGenerator(sequenceName="SEQ_DESC_VALUE_INTERNATIONAL",name="descvalueIntidgen", allocationSize = 1)
	@GeneratedValue(generator="descvalueIntidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "DESC_VALUE_INTERNATIONAL_ID")
	private Long descValueInternationalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DESCRIPTOR_VALUE_ID")
	private DescriptorValue descriptorValue;
	
	@Column(name = "LANGUAGE_CODE")
	private String languageCode;
	
	@Column(name = "INTERNATIONAL_FULL_VALUE")
	private String internationalFullValue;
	
	@Column(name = "INTERNATIONAL_SHORT_VALUE")
	private String internationalShortValue;
	
	@Column(name = "DESCRIPTOR_VALUE_ID", insertable = false, updatable = false)
	private Long rootId;

	public Long getDescValueInternationalId() {
		return descValueInternationalId;
	}

	public void setDescValueInternationalId(Long descValueInternationalId) {
		this.descValueInternationalId = descValueInternationalId;
	}

	public DescriptorValue getDescriptorValue() {
		return descriptorValue;
	}

	public void setDescriptorValue(DescriptorValue descriptorValue) {
		this.descriptorValue = descriptorValue;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getInternationalFullValue() {
		return internationalFullValue;
	}

	public void setInternationalFullValue(String internationFullValue) {
		this.internationalFullValue = internationFullValue;
	}

	public String getInternationalShortValue() {
		return internationalShortValue;
	}

	public void setInternationalShortValue(String internationalShortValue) {
		this.internationalShortValue = internationalShortValue;
	}

	@Override
	public Long getRootId() {
		return rootId;
	}

	@Override
	public void setRootId(Long rootId) {
		this.rootId = rootId;
	}
}
