package ru.insoft.sovdoc.model.desc.view;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "V_DESC_GROUP")
public class VDescGroup {

	@Id
	@Column(name = "DESCRIPTOR_GROUP_ID")
	private Long descriptorGroupId;
	
	@Column(name = "SUBSYSTEM_NUMBER")
	private Integer subsystemNumber;
	
	@Column(name = "SUBSYSTEM_NAME")
	private String subsystemName;
	
	@Column(name = "GROUP_NAME")
	private String groupName;
	
	@Column(name = "GROUP_CODE")
	private String groupCode;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;
	
	@Column(name = "IS_SYSTEM")
	private boolean isSystem;
	
	@Column(name = "IS_HIERARCHICAL")
	private boolean isHierarchical;
	
	@Column(name = "SHORT_VALUE_SUPPORTED")
	private boolean shortValueSupported;
	
	@Column(name = "HISTORY_SUPPORTED")
	private boolean historySupported;
	
	@Column(name = "ALPHABETIC_SORT")
	private boolean alphabeticSort;
	
	@Column(name = "VALUE_CNT")
	private int valueCnt;
	
	@OneToMany
	@JoinColumn(name = "DESCRIPTOR_GROUP_ID", insertable = false, updatable = false)
	@OrderBy("sortOrder")
	private List<VDescGroupAttr> attrList;

	public Long getDescriptorGroupId() {
		return descriptorGroupId;
	}

	public void setDescriptorGroupId(Long descriptorGroupId) {
		this.descriptorGroupId = descriptorGroupId;
	}

	public Integer getSubsystemNumber() {
		return subsystemNumber;
	}

	public void setSubsystemNumber(Integer subsystemNumber) {
		this.subsystemNumber = subsystemNumber;
	}

	public String getSubsystemName() {
		return subsystemName;
	}

	public void setSubsystemName(String subsystemName) {
		this.subsystemName = subsystemName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isSystem() {
		return isSystem;
	}

	public void setSystem(boolean isSystem) {
		this.isSystem = isSystem;
	}

	public boolean isHierarchical() {
		return isHierarchical;
	}

	public void setHierarchical(boolean isHierarchical) {
		this.isHierarchical = isHierarchical;
	}

	public boolean isShortValueSupported() {
		return shortValueSupported;
	}

	public void setShortValueSupported(boolean shortValueSupported) {
		this.shortValueSupported = shortValueSupported;
	}

	public boolean isHistorySupported() {
		return historySupported;
	}

	public void setHistorySupported(boolean historySupported) {
		this.historySupported = historySupported;
	}

	public int getValueCnt() {
		return valueCnt;
	}

	public void setValueCnt(int valueCnt) {
		this.valueCnt = valueCnt;
	}
	
	public boolean isAlphabeticSort() {
		return alphabeticSort;
	}

	public void setAlphabeticSort(boolean alphabeticSort) {
		this.alphabeticSort = alphabeticSort;
	}

	public List<VDescGroupAttr> getAttrList() {
		return attrList;
	}
}
