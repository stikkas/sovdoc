package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "DESCRIPTOR_GROUP_ATTR")
public class DescriptorGroupAttr {

	@Id
	@SequenceGenerator(sequenceName="SEQ_DESCRIPTOR_GROUP_ATTR",name="descrgroupAttrIdgen", allocationSize = 1)
	@GeneratedValue(generator="descrgroupAttrIdgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "DESCRIPTOR_GROUP_ATTR_ID")
	private Long descriptorGroupAttrId;
	
	@Column(name = "DESCRIPTOR_GROUP_ID")
	private Long descGroupId;
	
	@Column(name = "DATATYPE_CODE")
	private String datatype;
	
	@Column(name = "ATTR_NAME")
	private String attrName;
	
	@Column(name = "ATTR_CODE")
	private String attrCode;
	
	@Column(name = "SORT_ORDER")
	private Long sortOrder;
	
	@Column(name = "IS_COLLECTION")
	private boolean isCollection;
	
	@Column(name = "IS_REQUIRED")
	private boolean isRequired;
	
	@Column(name = "REF_DESCRIPTOR_GROUP_ID")
	private Long refDescGroupId;

	public Long getDescriptorGroupAttrId() {
		return descriptorGroupAttrId;
	}

	public void setDescriptorGroupAttrId(Long descriptorGroupAttrId) {
		this.descriptorGroupAttrId = descriptorGroupAttrId;
	}

	public Long getDescGroupId() {
		return descGroupId;
	}

	public void setDescGroupId(Long descGroupId) {
		this.descGroupId = descGroupId;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public String getAttrCode() {
		return attrCode;
	}

	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isCollection() {
		return isCollection;
	}

	public void setCollection(boolean isCollection) {
		this.isCollection = isCollection;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public Long getRefDescGroupId() {
		return refDescGroupId;
	}

	public void setRefDescGroupId(Long refDescGroup) {
		this.refDescGroupId = refDescGroup;
	}
}
