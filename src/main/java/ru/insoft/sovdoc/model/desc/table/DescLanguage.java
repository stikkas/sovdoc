package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DESC_LANGUAGE")
public class DescLanguage {

	@Id
	@Column(name = "LANGUAGE_CODE")
	private String languageCode;
	
	@Column(name = "LANGUAGE_NAME")
	private String languageName;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
}
