package ru.insoft.sovdoc.model.adm.view;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "V_ADM_USER")
public class VAdmUser {

	@Id
	@Column(name = "USER_ID")
	private Long userId;

	@Column(name = "DISPLAYED_NAME")
	private String displayedName;

	@Column(name = "LOGIN")
	private String login;

	@Column(name = "USER_TYPE_ID")
	private Long userTypeId;

	@Column(name = "USER_TYPE")
	private String userType;

	@Column(name = "IS_BLOCKED")
	private boolean isBlocked;

	@Column(name = "USER_STATUS")
	private String userStatus;

	@Column(name = "DEPARTMENT_ID")
	private Long departmentId;

	@Column(name = "ARCHIVE_ID")
	private Long archiveId;

	@Column(name = "LITERA_ARCHIVE")
	private String userArchive;

	@OneToMany(mappedBy = "user")
	private List<VAdmUserGroup> groups;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDisplayedName() {
		return displayedName;
	}

	public void setDisplayedName(String displayedName) {
		this.displayedName = displayedName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Long getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(Long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public List<VAdmUserGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<VAdmUserGroup> groups) {
		this.groups = groups;
	}

	public String getUserArchive() {
		return userArchive;
	}

	public void setUserArchive(String userArchive) {
		this.userArchive = userArchive;
	}

	public Long getArchiveId() {
		return archiveId;
	}

	public void setArchiveId(Long archiveId) {
		this.archiveId = archiveId;
	}

}
