package ru.insoft.sovdoc.model.adm.table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "ADM_USER_GROUP")
public class AdmUserGroup implements Serializable {

	@Id
	@Column(name = "USER_ID")
	private Long userId;
	
	@Id
	@Column(name = "GROUP_ID")
	private Long groupId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
}
