package ru.insoft.sovdoc.model.adm.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "V_ADM_USER_RULE")
public class VAdmUserRule implements Serializable {

	@Id
	@Column(name = "LOGIN")
	private String login;
	
	@Id
	@Column(name = "RULE_CODE")
	private String ruleCode;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getRuleCode() {
		return ruleCode;
	}

	public void setRuleCode(String ruleCode) {
		this.ruleCode = ruleCode;
	}
}
