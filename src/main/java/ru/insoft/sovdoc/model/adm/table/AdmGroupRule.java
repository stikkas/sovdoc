package ru.insoft.sovdoc.model.adm.table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@SuppressWarnings("serial")
@Entity
@Table(name = "ADM_GROUP_RULE")
public class AdmGroupRule implements Serializable {

	@Id
	@Column(name = "GROUP_ID")
	private Long groupId;
	
	@Id
	@Column(name = "ACCESS_RULE_ID")
	private Long accessRuleId;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getAccessRuleId() {
		return accessRuleId;
	}

	public void setAccessRuleId(Long accessRuleId) {
		this.accessRuleId = accessRuleId;
	}
}
