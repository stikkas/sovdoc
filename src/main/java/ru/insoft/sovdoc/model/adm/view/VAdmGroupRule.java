package ru.insoft.sovdoc.model.adm.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.adm.table.AdmGroup;

@SuppressWarnings("serial")
@Entity
@Table(name = "V_ADM_GROUP_RULE")
public class VAdmGroupRule implements Serializable {

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUP_ID")
	private AdmGroup group;
	
	@Id
	@Column(name = "ACCESS_RULE_ID")
	private Long accessRuleId;
	
	@Column(name = "SUBSYSTEM_NAME")
	private String subsystemName;
	
	@Column(name = "RULE_NAME")
	private String ruleName;

	public AdmGroup getGroup() {
		return group;
	}

	public void setGroup(AdmGroup group) {
		this.group = group;
	}

	public Long getAccessRuleId() {
		return accessRuleId;
	}

	public void setAccessRuleId(Long accessRuleId) {
		this.accessRuleId = accessRuleId;
	}

	public String getSubsystemName() {
		return subsystemName;
	}

	public void setSubsystemName(String subsystemName) {
		this.subsystemName = subsystemName;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
}
