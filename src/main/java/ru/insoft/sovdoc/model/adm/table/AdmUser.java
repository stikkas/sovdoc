package ru.insoft.sovdoc.model.adm.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ru.insoft.commons.security.DefaultUser;

@Entity
@Table(name = "ADM_USER")
public class AdmUser implements DefaultUser {

	@Id
	@SequenceGenerator(sequenceName="SEQ_ADM_USER",name="admuseridgen", allocationSize = 1)
	@GeneratedValue(generator="admuseridgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "USER_ID")
	private Long userId;
	
	@Column(name = "USER_TYPE_ID")
	private Long userTypeId;
	
	@Column(name = "LOGIN")
	private String login;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "IS_BLOCKED")
	private boolean isBlocked;
	
	@Column(name = "DISPLAYED_NAME")
	private String displayedName;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(Long userTypeId) {
		this.userTypeId = userTypeId;
	}

	@Override
	public String getLogin() {
		return login;
	}

	@Override
	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public String getDisplayedName() {
		return displayedName;
	}

	public void setDisplayedName(String displayedName) {
		this.displayedName = displayedName;
	}

	@Override
	public String getName() 
	{
		return displayedName;
	}
}
