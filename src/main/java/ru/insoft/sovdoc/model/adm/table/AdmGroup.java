package ru.insoft.sovdoc.model.adm.table;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.adm.view.VAdmGroupRule;

@Entity
@Table(name = "ADM_GROUP")
public class AdmGroup {

	@Id
	@SequenceGenerator(sequenceName="SEQ_ADM_GROUP",name="admgroupidgen", allocationSize = 1)
	@GeneratedValue(generator="admgroupidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "GROUP_ID")
	private Long groupId;
	
	@Column(name = "GROUP_NAME")
	private String groupName;
	
	@Column(name = "GROUP_NOTE")
	private String groupNote;
	
	@OneToMany(mappedBy = "group")
	private List<VAdmGroupRule> groupRules;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupNote() {
		return groupNote;
	}

	public void setGroupNote(String groupNote) {
		this.groupNote = groupNote;
	}

	public List<VAdmGroupRule> getGroupRules() {
		return groupRules;
	}

	public void setGroupRules(List<VAdmGroupRule> groupRules) {
		this.groupRules = groupRules;
	}
}
