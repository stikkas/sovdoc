package ru.insoft.sovdoc.model.adm.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ADM_ACCESS_RULE")
public class AdmAccessRule {

	@Id
	@Column(name = "ACCESS_RULE_ID")
	private Long accessRuleId;
	
	@Column(name = "SUBSYSTEM_NUMBER")
	private Long subsystemNumber;
	
	@Column(name = "RULE_CODE")
	private String ruleCode;
	
	@Column(name = "RULE_NAME")
	private String ruleName;

	public Long getAccessRuleId() {
		return accessRuleId;
	}

	public void setAccessRuleId(Long accessRuleId) {
		this.accessRuleId = accessRuleId;
	}

	public Long getSubsystemNumber() {
		return subsystemNumber;
	}

	public void setSubsystemNumber(Long subsystemNumber) {
		this.subsystemNumber = subsystemNumber;
	}

	public String getRuleCode() {
		return ruleCode;
	}

	public void setRuleCode(String ruleCode) {
		this.ruleCode = ruleCode;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
}
