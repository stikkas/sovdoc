package ru.insoft.sovdoc.model.ebook.table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@SuppressWarnings("serial")
@Entity
@Table(name = "ARCH_EBOOK_AUTHOR")
public class ArchEbookAuthor implements Serializable {

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private ArchEbook ebook;
	
	@Column(name = "UNIV_DATA_UNIT_ID", insertable = false, updatable = false)
	private Long univDataUnitId;
	
	@Id
	@Column(name = "AUTHOR_ID")
	private Long authorId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUTHOR_ID", referencedColumnName = "DESCRIPTOR_VALUE_ID",
			insertable = false, updatable = false)
	private DescriptorValue author;

	public ArchEbook getEbook() {
		return ebook;
	}

	public void setEbook(ArchEbook ebook) {
		this.ebook = ebook;
	}

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) 
	{
		this.authorId = authorId;
	}

	public DescriptorValue getAuthor() {
		return author;
	}

	public void setAuthor(DescriptorValue author) {
		this.author = author;
	}
}
