package ru.insoft.sovdoc.model.ebook.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ARCH_EBOOK_SERIES")
public class ArchEbookSeries {

	@Id
	@SequenceGenerator(sequenceName="SEQ_ARCH_EBOOK_SERIES",name="archebookidgen", allocationSize = 1)
	@GeneratedValue(generator="archebookidgen",strategy=GenerationType.SEQUENCE)
	@Column(name = "SERIES_ID")
	private Long seriesId;
	
	@Column(name = "SERIES_NAME")
	private String seriesName;
	
	@Column(name = "SERIES_INFO")
	private String seriesInfo;

	public Long getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(Long seriesId) {
		this.seriesId = seriesId;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getSeriesInfo() {
		return seriesInfo;
	}

	public void setSeriesInfo(String seriesInfo) {
		this.seriesInfo = seriesInfo;
	}
}
