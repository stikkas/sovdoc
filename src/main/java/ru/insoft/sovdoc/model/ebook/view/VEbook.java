package ru.insoft.sovdoc.model.ebook.view;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import ru.insoft.sovdoc.model.HasDescriptors;
import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookSeries;

@Entity
@Table(name = "V_EBOOK")
@Immutable
public class VEbook implements HasDescriptors {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "BOOK_TITLE")
	private String bookTitle;
	
	@Column(name = "AUTHORS")
	private String authors;
	
	@Column(name = "EDITION_TYPE_ID")
	private Long editionTypeId;
	
	@Column(name = "EDITION_TYPE")
	private String editionType;
	
	@Column(name = "LANGUAGE_NAME")
	private String languageName;
	
	@Column(name = "UDC")
	private String udc;
	
	@Column(name = "BBC")
	private String bbc;
	
	@Column(name = "ISBN")
	private String isbn;
	
	@Column(name = "EDITOR")
	private String editor;
	
	@Column(name = "EDITORIAL_BOARD")
	private String editorialBoard;
	
	@Column(name = "COMPILERS")
	private String compilers;
	
	@Column(name = "VOLUME1_INFO")
	private String volume1Info;
	
	@Column(name = "VOLUME2_INFO")
	private String volume2Info;
	
	@Column(name = "ATOPTITLE_DATA")
	private String atoptitleData;
	
	@Column(name = "SERIES_ID")
	private Long seriesId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SERIES_ID", insertable = false, updatable = false)
	private ArchEbookSeries series;
	
	@Column(name = "EDITION_NUMBER")
	private String editionNumber;
	
	@Column(name = "EDITION_NOTE")
	private String editionNote;
	
	@Column(name = "PUBLISHING_PLACE")
	private String publishingPlace;
	
	@Column(name = "PUBLISHER")
	private String publisher;
	
	@Column(name = "PUBLISHING_YEAR")
	private Integer publishingYear;
	
	@Column(name = "EDITION_SIZE")
	private String editionSize;
	
	@Column(name = "NOTES")
	private String notes;
	
	@Column(name = "HAS_EFILE")
	private Integer hasEfile;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID", insertable = false, updatable = false)
	@OrderBy("descriptor1")
	private List<VComplexDescriptor> descriptors;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public Long getEditionTypeId() {
		return editionTypeId;
	}

	public void setEditionTypeId(Long editionTypeId) {
		this.editionTypeId = editionTypeId;
	}

	public String getEditionType() {
		return editionType;
	}

	public void setEditionType(String editionType) {
		this.editionType = editionType;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getUdc() {
		return udc;
	}

	public void setUdc(String udc) {
		this.udc = udc;
	}

	public String getBbc() {
		return bbc;
	}

	public void setBbc(String bbc) {
		this.bbc = bbc;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getEditorialBoard() {
		return editorialBoard;
	}

	public void setEditorialBoard(String editorialBoard) {
		this.editorialBoard = editorialBoard;
	}

	public String getCompilers() {
		return compilers;
	}

	public void setCompilers(String compilers) {
		this.compilers = compilers;
	}

	public String getVolume1Info() {
		return volume1Info;
	}

	public void setVolume1Info(String volume1Info) {
		this.volume1Info = volume1Info;
	}

	public String getVolume2Info() {
		return volume2Info;
	}

	public void setVolume2Info(String volume2Info) {
		this.volume2Info = volume2Info;
	}

	public String getAtoptitleData() {
		return atoptitleData;
	}

	public void setAtoptitleData(String atoptitleData) {
		this.atoptitleData = atoptitleData;
	}

	public Long getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(Long seriesId) {
		this.seriesId = seriesId;
	}

	public ArchEbookSeries getSeries() {
		return series;
	}

	public void setSeries(ArchEbookSeries series) {
		this.series = series;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getEditionNote() {
		return editionNote;
	}

	public void setEditionNote(String editionNote) {
		this.editionNote = editionNote;
	}

	public String getPublishingPlace() {
		return publishingPlace;
	}

	public void setPublishingPlace(String publishingPlace) {
		this.publishingPlace = publishingPlace;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Integer getPublishingYear() {
		return publishingYear;
	}

	public void setPublishingYear(Integer publishingYear) {
		this.publishingYear = publishingYear;
	}

	public String getEditionSize() {
		return editionSize;
	}

	public void setEditionSize(String editionSize) {
		this.editionSize = editionSize;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getHasEfile() {
		return hasEfile;
	}

	public void setHasEfile(Integer hasEfile) {
		this.hasEfile = hasEfile;
	}

	public List<VComplexDescriptor> getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(List<VComplexDescriptor> descriptors) {
		this.descriptors = descriptors;
	}
}
