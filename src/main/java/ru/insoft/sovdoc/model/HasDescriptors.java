package ru.insoft.sovdoc.model;

import java.io.Serializable;
import java.util.List;

import ru.insoft.sovdoc.model.complex.view.VComplexDescriptor;

public interface HasDescriptors  extends Serializable {

	public List<VComplexDescriptor> getDescriptors();
	public void setDescriptors(List<VComplexDescriptor> descriptors);
}
