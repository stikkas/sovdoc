package ru.insoft.sovdoc.model.showfile;

import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

import javax.persistence.*;

@Entity
@Table(name = "UNIV_IMAGE_PATH")
public class ImagePath {
    public ImagePath() {}

    @Id
    @SequenceGenerator(sequenceName="SEQ_UNIV_IMAGE_PATH", name = "uipidgen", allocationSize = 1)
    @GeneratedValue(generator="uipidgen",strategy=GenerationType.SEQUENCE)
    @Column(name = "UNIV_IMAGE_PATH_ID")
    private Long imagePathId;

    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long dataUnitId;

    @Column(name = "SORT_ORDER")
    private Long sortOrder;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "FILE_NAME")
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FORMAT_ID")
    private DescriptorValue descriptorValueId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IMAGE_CATEGORY_ID")
    private DescriptorValue imageCategory;
    
    @Column(name = "CAPTION")
    private String caption;

    public Long getImagePathId() {
        return imagePathId;
    }

    public void setImagePathId(Long imagePathId) {
        this.imagePathId = imagePathId;
    }

    public Long getDataUnitId() {
        return dataUnitId;
    }

    public void setDataUnitId(Long dataUnitId) {
        this.dataUnitId = dataUnitId;
    }

    public Long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public DescriptorValue getDescriptorValueId() {
        return descriptorValueId;
    }

    public void setDescriptorValueId(DescriptorValue descriptorValueId) {
        this.descriptorValueId = descriptorValueId;
    }

	public DescriptorValue getImageCategory() {
		return imageCategory;
	}

	public void setImageCategory(DescriptorValue imageCategory) {
		this.imageCategory = imageCategory;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
}
