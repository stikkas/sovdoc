package ru.insoft.sovdoc.list.adm;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("groupUsersDL")
public class GroupUsersDataList extends UserDataList {

  @Override
  public boolean isShowResult() {
    return searchCriteria.getCriteria().getGroupId() != null;
  }

  public void reload() {
    paginator.setPageNum(1);
    setRowCount(null);
    resetPageData();
  }
}
