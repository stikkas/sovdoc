package ru.insoft.sovdoc.list.adm;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;
import ru.insoft.commons.jsf.ui.datalist.Paginator;
import ru.insoft.commons.jsf.ui.datalist.ViewQueryDataList;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.model.adm.table.AdmGroup;

@RequestScoped
@Named("userGroupDL")
public class GroupDataList extends ViewQueryDataList<AdmGroup> {

	@Inject
	EntityManager em;
	@Inject
	GroupSortingCriteria sortingCriteria;
	
	@Inject
	@RequestParam(value = "del_groupId")
	Long groupId;
	
	@Override
	public ViewQuerySearchCriteria<AdmGroup> getSearchCriteria() 
	{
		return null;
	}
	
	@Override
	public CommonSortingCriteria getSortingCriteria() 
	{
		return sortingCriteria;
	}
	
	@Override
	public Paginator getPaginator() 
	{
		return null;
	}
	
	@Override
	protected EntityManager getEntityManager() 
	{
		return em;
	}
	
	@Override
	public boolean isShowResult()
	{
		return true;
	}

	@Override
	public String getDataLabel() 
	{
		return null;
	}
	
	public void removeGroup()
	{
		try
		{
			setRowId(groupId.toString());
			em.remove(getCurrentRow());
			em.flush();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null || msg.indexOf("FK_ADMGRP_ADMUSRGRP") == -1)
				e.printStackTrace();
			else
				MessageUtils.ErrorMessage("Нельзя удалить группу, в которой есть пользователи");
			em.clear();
		}
	}
	
}
