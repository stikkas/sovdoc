package ru.insoft.sovdoc.list.adm;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.seam.security.IdentityImpl;
import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;
import ru.insoft.commons.jsf.ui.datalist.Paginator;
import ru.insoft.commons.jsf.ui.datalist.ViewQueryDataList;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.model.adm.table.AdmUser;
import ru.insoft.sovdoc.model.adm.table.AdmUser_;
import ru.insoft.sovdoc.model.adm.view.VAdmUser;

@RequestScoped
@Named("userDL")
public class UserDataList extends ViewQueryDataList<VAdmUser> {

	@Inject
	EntityManager em;
	@Inject
	UserSearchCriteria searchCriteria;
	@Inject
	Paginator paginator;
	@Inject
	@RequestParam(value = "userId")
	Long userId;
	@Inject
	IdentityImpl identity;
	
	private CommonSortingCriteria sortingCriteria;
	
	@Override
	public ViewQuerySearchCriteria<VAdmUser> getSearchCriteria() 
	{
		return searchCriteria;
	}

	@Override
	public CommonSortingCriteria getSortingCriteria() 
	{
		if (sortingCriteria == null)
		{
			sortingCriteria = new CommonSortingCriteria();
			sortingCriteria.init();
		}
		return sortingCriteria;
	}

	@Override
	public Paginator getPaginator() 
	{
		return paginator;
	}

	@Override
	protected EntityManager getEntityManager() 
	{
		return em;
	}
	
	@Override
	public boolean isShowResult()
	{
		return true;
	}
	
	public void blockUser()
	{
		AdmUser user = getUser(userId);
		if (user.getLogin().toUpperCase().equals(identity.getUser().getId().toUpperCase()))
		{
			MessageUtils.ErrorMessage("Вы не можете заблокировать сами себя");
			setRowIndexByKey(userId.toString());
			return;
		}
		
		user.setBlocked(!user.isBlocked());
		em.merge(user);
		em.flush();
		em.clear();
		resetPageData();
		
		setRowIndexByKey(userId.toString());
	}
	
	public void deleteUser()
	{
		AdmUser user = getUser(userId);
		if (user.getLogin().toUpperCase().equals(identity.getUser().getId().toUpperCase()))
		{
			MessageUtils.ErrorMessage("Вы не можете удалить сами себя");
			setRowIndexByKey(userId.toString());
			return;
		}
		
		em.remove(user);
		try
		{
			em.flush();
			
			em.clear();
			rowCount--;
			resetPageData();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null)
			{
				e.printStackTrace();
				return;
			}
			if (msg.indexOf("FK_ADMUSR_WEBUSR") >= 0)
			{
				MessageUtils.ErrorMessage("Пользователи портала не могут быть удалены");
				return;
			}
			MessageUtils.ErrorMessage("От имени этого пользователя в системе совершены определённые действия");
			MessageUtils.ErrorMessage("Пользователь не может быть удалён. Вы можете заблокировать этого пользователя");
			setRowIndexByKey(userId.toString());
		}
	}
	
	protected AdmUser getUser(Long id)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUser> cq = cb.createQuery(AdmUser.class);
		Root<AdmUser> root = cq.from(AdmUser.class);
		cq.select(root).where(cb.equal(root.get(AdmUser_.userId), id));
		return em.createQuery(cq).getSingleResult();
	}

}
