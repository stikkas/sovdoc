package ru.insoft.sovdoc.list.adm;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.sovdoc.model.adm.table.AdmUserGroup;
import ru.insoft.sovdoc.model.adm.table.AdmUserGroup_;
import ru.insoft.sovdoc.model.adm.view.VAdmUser;
import ru.insoft.sovdoc.model.adm.view.VAdmUser_;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

import com.google.common.collect.Lists;
import ru.insoft.sovdoc.model.complex.view.VOrgStructure;
import ru.insoft.sovdoc.model.complex.view.VOrgStructure_;

@RequestScoped
@Named("userlistSearch")
public class UserSearchCriteria implements ViewQuerySearchCriteria<VAdmUser> {

	@Inject
	ValueSelectDataListGeneral valueSelectDL;

	private Criteria criteria;

	public class Criteria {

		private String userName;
		private String login;
		private Long userTypeId;
		private Long departmentId;
		private Boolean status;
		private Long groupId;
		private Long archiveId;

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}

		public Long getUserTypeId() {
			return userTypeId;
		}

		public void setUserTypeId(Long userTypeId) {
			this.userTypeId = userTypeId;
		}

		public Long getDepartmentId() {
			return departmentId;
		}

		public void setDepartmentId(Long departmentId) {
			this.departmentId = departmentId;
		}

		public Boolean getStatus() {
			return status;
		}

		public void setStatus(Boolean status) {
			this.status = status;
		}

		public Long getGroupId() {
			return groupId;
		}

		public void setGroupId(Long groupId) {
			this.groupId = groupId;
		}

		public Long getArchiveId() {
			return archiveId;
		}

		public void setArchiveId(Long archiveId) {
			this.archiveId = archiveId;
		}

	}

	@Override
	public boolean validateCriteria() {
		return true;
	}

	@Override
	public <Q> Predicate getPredicateByCriteria(CriteriaBuilder builder,
			CriteriaQuery<Q> query, Root<VAdmUser> root) {
		List<Predicate> predicates = Lists.newArrayList();

		if (criteria.userName != null) {
			predicates.add(builder.like(builder.upper(root.get(VAdmUser_.displayedName)),
					criteria.userName.toUpperCase() + "%"));
		}
		if (criteria.login != null) {
			predicates.add(builder.like(builder.upper(root.get(VAdmUser_.login)),
					criteria.login.toUpperCase() + "%"));
		}
		if (criteria.userTypeId != null) {
			predicates.add(builder.equal(root.get(VAdmUser_.userTypeId), criteria.userTypeId));
		}
		if (criteria.status != null) {
			predicates.add(builder.equal(root.get(VAdmUser_.isBlocked), criteria.status));
		}

		if (criteria.departmentId != null) {
			Subquery<Long> sub = query.subquery(Long.class);
			Root<VOrgStructure> subroot = sub.from(VOrgStructure.class);
			sub.select(subroot.get(VOrgStructure_.childValueId));
			sub.where(builder.equal(subroot.get(VOrgStructure_.parentValueId), criteria.departmentId));
			predicates.add(builder.in(root.get(VAdmUser_.departmentId)).value(sub));

		}
		if (criteria.groupId != null) {
			Subquery<Long> sub = query.subquery(Long.class);
			Root<AdmUserGroup> subroot = sub.from(AdmUserGroup.class);
			sub.select(subroot.get(AdmUserGroup_.userId));
			sub.where(builder.equal(subroot.get(AdmUserGroup_.groupId), criteria.groupId));
			predicates.add(builder.in(root.get(VAdmUser_.userId)).value(sub));
		}
		if (criteria.archiveId != null) {
			predicates.add(builder.equal(root.get(VAdmUser_.archiveId), criteria.archiveId));
		}
		return builder.and(predicates.toArray(new Predicate[0]));
	}

	@PostConstruct
	private void init() {
		criteria = new Criteria();
	}

	public Criteria getCriteria() {
		return criteria;
	}

	public void selectDepartment() {
		criteria.setDepartmentId(valueSelectDL.getSelectedValue());
		valueSelectDL.setValueSelected(true);
	}

	public void clearDepartment() {
		criteria.setDepartmentId(null);
	}
}
