package ru.insoft.sovdoc.list.web;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import ru.insoft.commons.jsf.ui.datalist.Paginator;
import ru.insoft.commons.jsf.ui.datalist.ViewQueryDataList;
import ru.insoft.sovdoc.model.web.table.WebNews;
import ru.insoft.sovdoc.system.WebQuery;

@RequestScoped
@Named("newsDL")
public class NewsDataList extends ViewQueryDataList<WebNews> {

	@Inject
	Paginator paginator;
	@Inject
	NewsSearchCriteria searchCriteria;
	@Inject
	NewsSortingCriteria sortingCriteria;
	@Inject
	EntityManager em;
	@Inject
	WebQuery webQuery;
	
	@Override
	public Paginator getPaginator() 
	{
		return paginator;
	}

	@Override
	public NewsSearchCriteria getSearchCriteria() 
	{
		return searchCriteria;
	}

	@Override
	protected EntityManager getEntityManager() 
	{
		return em;
	}

	@Override
	public NewsSortingCriteria getSortingCriteria() 
	{
		return sortingCriteria;
	}
	
	@PostConstruct
	private void init()
	{
		setShowResult(true);
	}
	
	public void removeNews()
	{
		Long newsId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("argument"));
		WebNews news = webQuery.getWebNews(newsId);
		em.remove(news);
	}

}
