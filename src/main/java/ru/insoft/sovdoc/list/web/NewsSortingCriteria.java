package ru.insoft.sovdoc.list.web;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;

public class NewsSortingCriteria extends CommonSortingCriteria {

	@Override
	public <T> List<Order> getSortOrder(CriteriaBuilder builder, Root<T> root)
	{
		List<Order> ord = super.getSortOrder(builder, root);
		if (ord.size() == 0)
		{
			setSorting("newsDate", "descending", true);
			ord = super.getSortOrder(builder, root);
		}
		return ord;
	}
}
