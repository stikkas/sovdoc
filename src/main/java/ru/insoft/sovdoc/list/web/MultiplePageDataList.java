package ru.insoft.sovdoc.list.web;

import ru.insoft.commons.jsf.ui.datalist.EmbeddedDataList;
import ru.insoft.sovdoc.model.web.table.WebPages;
import ru.insoft.sovdoc.model.web.view.VWebPageMultipleValue;
import ru.insoft.sovdoc.system.WebQuery;

public class MultiplePageDataList extends EmbeddedDataList<VWebPageMultipleValue> {
	
	WebQuery webQuery;
	
	public void pageMoveUp()
	{
		VWebPageMultipleValue val;
		for (int i = 0; i < getRowCount(); i++)
		{
			Integer correctOrder = null;
			val = getWrappedData().get(i);
			if (i == getRowIndex() - 1)
				correctOrder = i + 2;
			if (i == getRowIndex())
				correctOrder = i;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!correctOrder.equals(val.getSortOrder()))
			{
				WebPages page = webQuery.getWebPage(val.getPageId());
				page.setSortOrder(correctOrder);
			}
		}
		val = getWrappedData().get(getRowIndex());
		getWrappedData().remove(val);
		getWrappedData().add(getRowIndex() - 1, val);
	}
	
	public void pageMoveDown()
	{
		VWebPageMultipleValue val;
		for (int i = 0; i < getRowCount(); i++)
		{
			Integer correctOrder = null;
			val = getWrappedData().get(i);
			if (i == getRowIndex())
				correctOrder = i + 2;
			if (i == getRowIndex() + 1)
				correctOrder = i;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!correctOrder.equals(val.getSortOrder()))
			{
				WebPages page = webQuery.getWebPage(val.getPageId());
				page.setSortOrder(correctOrder);
			}
		}
		val = getWrappedData().get(getRowIndex());
		getWrappedData().remove(val);
		getWrappedData().add(getRowIndex() + 1, val);
	}
	
	public int getPageIndex(Long pageId)
	{
		for (int i = 0; i < getWrappedData().size(); i++)
			if (getWrappedData().get(i).getPageId().equals(pageId))
				return i;
		return 0;
	}
}