package ru.insoft.sovdoc.list.web;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.commons.utils.ValidationUtils;
import ru.insoft.sovdoc.model.web.table.WebNews;
import ru.insoft.sovdoc.model.web.table.WebNews_;

import com.google.common.collect.Lists;
import java.util.Calendar;
import ru.insoft.sovdoc.model.correct.view.VUnivCorrectJournal_;

@RequestScoped
@Named("newsSearch")
public class NewsSearchCriteria implements ViewQuerySearchCriteria<WebNews> {

  private Criteria criteria;

  public class Criteria {

    private Date dateFrom;
    private Date dateTo;
    private String newsName;

    public Date getDateFrom() {
      return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
      this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
      return dateTo;
    }

    public void setDateTo(Date dateTo) {
      this.dateTo = dateTo;
    }

    public String getNewsName() {
      return newsName;
    }

    public void setNewsName(String newsName) {
      this.newsName = newsName;
    }
  }

  @Override
  public boolean validateCriteria() {
    return ValidationUtils.validateInterval("Дата новости",
            criteria.getDateFrom(), criteria.getDateTo());
  }

  @Override
  public <Q> Predicate getPredicateByCriteria(CriteriaBuilder builder,
          CriteriaQuery<Q> query, Root<WebNews> root) {
    List<Predicate> predicates = Lists.newArrayList();

    if (criteria.getDateFrom() != null) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(criteria.getDateTo() == null ? criteria.getDateFrom() : criteria.getDateTo());
      cal.set(Calendar.HOUR_OF_DAY, 23);
      cal.set(Calendar.MINUTE, 59);
      cal.set(Calendar.SECOND, 59);
      predicates.add(builder.between(root.get(WebNews_.newsDate), criteria.getDateFrom(), cal.getTime()));
    }

    return builder.and(predicates.toArray(new Predicate[0]));
  }

  @PostConstruct
  private void init() {
    criteria = new Criteria();
  }

  public Criteria getCriteria() {
    return criteria;
  }
}
