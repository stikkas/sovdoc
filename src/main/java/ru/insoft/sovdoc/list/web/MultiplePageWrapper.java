package ru.insoft.sovdoc.list.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.sovdoc.model.web.table.WebPages;
import ru.insoft.sovdoc.model.web.view.VWebPageMultipleValue;
import ru.insoft.sovdoc.system.WebQuery;

@RequestScoped
@Named
public class MultiplePageWrapper {

	@Inject
	WebQuery webQuery;
	
	@Inject
	@RequestParam(value = "pageId")
	Long pageId;
	
	private WebPages page;
	
	private Map<Long, MultiplePageDataList> pageCollectionMap = new HashMap<Long, MultiplePageDataList>();
	
	public MultiplePageDataList getDataListByType(Long pageTypeId)
	{
		if (pageCollectionMap.get(pageTypeId) == null)
		{
			List<VWebPageMultipleValue> pageList = webQuery.getMultipleWebPagesByType(pageTypeId);
			MultiplePageDataList dataList = new MultiplePageDataList();
			dataList.setWrappedData(pageList);
			dataList.webQuery = webQuery;
			pageCollectionMap.put(pageTypeId, dataList);
		}
		return pageCollectionMap.get(pageTypeId);
	}
	
	public WebPages getPage()
	{
		if (page == null && pageId != null)
			page = webQuery.getWebPage(pageId);
		return page;
	}
	
	public boolean isSelectedPageType(Long pageTypeId)
	{
		if (getPage() == null)
			return false;
		return getPage().getPageTypeId().equals(pageTypeId);
	}
	
	public void removePage()
	{
		pageId = Long.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("argument"));
		page = getPage();
		EntityManager em = webQuery.getEntityManager();
		
		Query q = em.createNativeQuery(
				"begin WEB_PACK.PAGE_ORDER_DECREASE(:p_page_id); end;");
		q.setParameter("p_page_id", page.getPageId());
		q.executeUpdate();
		
		em.remove(page);
	}
}
