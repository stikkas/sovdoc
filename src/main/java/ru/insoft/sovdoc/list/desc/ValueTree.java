package ru.insoft.sovdoc.list.desc;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jboss.solder.servlet.http.RequestParam;
import org.richfaces.model.DeclarativeModelKey;
import org.richfaces.model.SequenceRowKey;

import ru.insoft.commons.jsf.ui.CommonTree;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.constant.VALUE_DISPLAY_TYPE;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.model.desc.view.VDescValuePath;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.ui.desc.ValueSearchParam;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

import com.google.common.collect.Lists;

@RequestScoped
@Named
public class ValueTree extends CommonTree<Map<String, ValueTreeItem>> {

	@Inject
	ValueSearchParam valueSearch;
	@Inject
	SystemEntity se;
	@Inject
	EntityManager em;
	@Inject
	@RequestParam(value = "mode")
	String mode;
	@Inject
	@RequestParam(value = "childrenFetched")
	Integer childrenFetched;
	@Inject
	@RequestParam(value = "fetchLinkId")
	String fetchLinkId;
	
	private boolean hierarchical;
	private boolean alphabeticSort;
	private Integer valCount;
	private List<String> expandedVals;
	private boolean dirtyRoots;
	
	@PostConstruct
	void init()
	{
		if (valueSearch.getGroupId() != null)
		{
			VDescGroup dg = se.getImmDescGroup(valueSearch.getGroupId());
			hierarchical = dg.isHierarchical();
			alphabeticSort = dg.isAlphabeticSort();
			valCount = dg.getValueCnt();			
		}
		if (childrenFetched == null)
			childrenFetched = 0;
	}

	public boolean getHierarchical() 
	{
		return hierarchical;
	}

	public Integer getValCount() 
	{
		return valCount;
	}

	public List<String> getExpandedVals() {
		return expandedVals;
	}
	
	@Override
	public Map<String, ValueTreeItem> getRootNodes()
	{
		if (roots == null || dirtyRoots)
			initRoots();
		
		return roots;
	}

	@Override
	public void initRoots() 
	{
		if (valueSearch.getGroupId() == null || "selectNode".equals(mode))
			return;
		
		if (roots == null)
			roots = new LinkedHashMap<String, ValueTreeItem>();
		setExpandedVals();
		if (expandedVals == null)
			setExpandedValsInternal();
		valueSearch.setParentId(null);		
		
		Long expValue = null;
		if (getExpandedVals() != null && getExpandedVals().size() > 0)
			expValue = Long.valueOf(getExpandedVals().get(0));
		if (valueSearch.getValueId() != null && 
				(getExpandedVals() == null || getExpandedVals().size() == 0))
			expValue = valueSearch.getValueId();
		
		boolean expFound = false;
		if (expValue == null)
			expFound = true;
		int count = 0;
		if (fetchLinkId != null)
		{
			List<String> nodes = parseNodeId(fetchLinkId);
			if (nodes.size() == 1)
				count = childrenFetched;
		}
		int i = 0;
		do
		{
			count += getCHILDREN_PER_QUERY();
                        List<VDescValue> valList = valueSearch.searchValues(count);
			for (; i < count; i++)
			{
				if (i >= valList.size())
				{
					expFound = true;
					break;
				}
				
				VDescValue val = valList.get(i);
				if (val.getDescriptorValueId().equals(expValue))
					expFound = true;
				
				ValueTreeItem node = new ValueTreeItem(
						!val.isHasChildren(), val.getDescriptorValueId(),
						valueSearch.getDisplayedType().equals(VALUE_DISPLAY_TYPE.SHORT) ? 
								val.getShortValue() : val.getFullValue(), 
						"value");
				node.setData(val);
				node.setTree(this);
				if (!node.isLeaf() && getHierarchical())
				{
					if (expandedVals != null && expandedVals.contains(node.getId().toString()))
						node.setExpanded(true);
					/*else
						node.setExpanded(isNodeExpanded(val.getDescriptorValueId().toString()));*/
				}
				if (!roots.containsKey(val.getDescriptorValueId().toString()))
					roots.put(val.getDescriptorValueId().toString(), node);
			}
		}
		while (!expFound);
		
		if (i == count)
		{
			ValueTreeItem node = new ValueTreeItem(true, Long.valueOf(count), null, "fake");
			roots.put("fake" + node.getId().toString(), node);
		}
		dirtyRoots = false;
	}
	
	public Integer getChildrenFetched() {
		return childrenFetched;
	}

	public String getFetchLinkId() {
		return fetchLinkId;
	}

	protected void setExpandedVals()
	{
		if (valueSearch.getValueId() != null && getHierarchical())
		{
			VDescValuePath path = se.getValuePath(valueSearch.getValueId()); 
			expandedVals = Lists.newArrayList(path.getIdPath().split("->"));
			expandedVals.remove(expandedVals.size() - 1);
		}
	}
	
	protected void setExpandedValsInternal()
	{
		List<String> nodeIds = getExpandedNodeIds();
		if (nodeIds == null)
			return;
		
		expandedVals = Lists.newArrayList();
		for (String nodeId : nodeIds)
			if (!nodeId.startsWith("fake"))
				expandedVals.add(nodeId);
	}
	
	public void searchNext()
	{
		setSelectionKeys(null);
		if (valueSearch.getSearchString() == null)
		{
			MessageUtils.ErrorMessage("Не задана строка поиска");
			return;
		}
		
		valueSearch.setValueId(valueSearch.searchNext());
		setExpandedVals();
		if (valueSearch.getValueId() == null)
			MessageUtils.ErrorMessage("Значение не найдено");
		else
		{
			initRoots();
			selectNodes();
		}
		if (getCurrentNode() != null && getHierarchical())
			valCount = ((VDescValue)getCurrentNode().getData()).getChildrenCnt();
	}
	
	public void selectNodes()
	{
		if (valueSearch.getValueId() != null)
		{
			/*roots = null;
			setExpandedVals();*/
			Collection<Object> coll = Lists.newArrayList();
			if (getHierarchical())
			{
				Map<String, ValueTreeItem> nodes = getRootNodes();
				for (int i = 0; i < expandedVals.size(); i++)
				{
					coll.add(new DeclarativeModelKey(getAdaptorId(), expandedVals.get(i)));
					nodes.get(expandedVals.get(i)).setExpanded(true);
					nodes = nodes.get(expandedVals.get(i)).getChildren(valueSearch);
				}
			}
			DeclarativeModelKey dmk = new DeclarativeModelKey(getAdaptorId(), valueSearch.getValueId().toString());
			if (!coll.contains(dmk))
				coll.add(dmk);
			
			SequenceRowKey srk = new SequenceRowKey(coll.toArray());
			coll = Lists.newArrayList((Object)srk);
			setSelectionKeys(coll);
			super.resetCurrentNode();	
		}
	}
	
	@Override
	public void selectNode()
	{
		super.selectNode();
		if (getCurrentNode() == null)
			return;
		ValueTreeItem currentNode = new ValueTreeItem(
				getCurrentNode().getId(), getCurrentNode().getType());
		if (getHierarchical())
			currentNode.setData(se.getImmDescValue(getCurrentNode().getId()));
		setCurrentNode(currentNode);
		initSearchParams();
	}
	
	@Override
	public void resetCurrentNode()
	{
		super.resetCurrentNode();
		if (getCurrentNode() == null)
			return;
		initSearchParams();
	}
	
	protected void initSearchParams()
	{
		if (alphabeticSort)
		{
			VDescValuePath path = se.getValuePath(getCurrentNode().getId());
			if (valueSearch.getDisplayedType() != null && 
					valueSearch.getDisplayedType().equals(VALUE_DISPLAY_TYPE.SHORT))
				valueSearch.setLastPath(path.getShortPath());
			else
				valueSearch.setLastPath(path.getFullPath());
		}
		else
			valueSearch.setLastNumber(se.getOrderRowNum(getCurrentNode().getId()));
		
		if (getHierarchical())
			valCount = ((VDescValue)getCurrentNode().getData()).getChildrenCnt();
		
		valueSearch.setDataListId(String.valueOf(getCurrentNode().getId()));
	}

	@Override
	protected String getAdaptorId() 
	{
		return "value";
	}
	
	public void valueMoveUp()
	{
		DescriptorValue curValue = se.getDescValue(valueSearch.getValueId());
		List<DescriptorValue> values = se.getChildDescValues(
				curValue.getDescriptorGroupId(), curValue.getParentValueId());
		for (int i = 0; i < values.size(); i++)
		{
			DescriptorValue val = values.get(i);
			Integer correctOrder = null;
			if (i == curValue.getSortOrder() - 2)
				correctOrder = i + 2;
			if (i == curValue.getSortOrder() - 1 && i > 0)
				correctOrder = i;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!val.getSortOrder().equals(correctOrder))
			{
				val.setSortOrder(correctOrder);
				em.merge(val);
				em.flush();
			}
		}
		em.clear();
		refreshLastParent();		
	}
	
	public void valueMoveDown()
	{
		DescriptorValue curValue = se.getDescValue(valueSearch.getValueId());
		List<DescriptorValue> values = se.getChildDescValues(
				curValue.getDescriptorGroupId(), curValue.getParentValueId());
		int ord = curValue.getSortOrder();
		for (int i = 0; i < values.size(); i++)
		{
			DescriptorValue val = values.get(i);
			Integer correctOrder = null;
			if (i == ord)
				correctOrder = i;
			if (i == ord - 1 && i < values.size())
				correctOrder = i + 2;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!val.getSortOrder().equals(correctOrder))
			{
				val.setSortOrder(correctOrder);
				em.merge(val);
				em.flush();
			}
		}
		em.clear();
		refreshLastParent();
	}
	
	public void deleteValue()
	{
		DescriptorValue curValue = se.getDescValue(valueSearch.getValueId());
		if (curValue.isActual())
		{
			VDescValue view = se.getImmDescValue(valueSearch.getValueId());
			if (view.getHistoryList().size() > 1)
			{
				MessageUtils.ErrorMessage("Значение содержит цепочку переименований и является актуальным");
				MessageUtils.ErrorMessage("Перед удалением необходимо выбрать другое актуальное значение");
				return;
			}
		}
		Query q = em.createNativeQuery("begin DESCRIPTOR_PACK.VAL_ORDER_DECREASE(:p_value_id); end;");
		q.setParameter("p_value_id", valueSearch.getValueId());
		q.executeUpdate();
		em.remove(curValue);
		try
		{
			em.flush();
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null)
			{
				e.printStackTrace();
				return;
			}
			if (msg.indexOf("FK_DSCVAL_DSCVAL") >= 0)
			{
				MessageUtils.ErrorMessage("Значение содержит дочерние элементы");
				return;
			}
			MessageUtils.ErrorMessage("На значение имеются ссылки извне");
			return;
		}
		em.clear();
		if (curValue.getParentValueId() != null)
		{
			VDescValuePath path = se.getValuePath(curValue.getParentValueId());
			expandedVals = Lists.newArrayList(path.getIdPath().split("->"));
		}
		refreshLastParent();
		valueSearch.setValueId(curValue.getParentValueId());
		selectNodes();
	}
	
	public void clearForNewFetch()
	{
		if (roots != null)
		{
			Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			childrenFetched = Integer.valueOf(reqMap.get("childrenFetched"));
			fetchLinkId = reqMap.get("fetchLinkId");
			
			Map<String, ValueTreeItem> items = roots;
			ValueTreeItem item = null;
			for (String nodeId : parseNodeId(fetchLinkId))
			{
				ValueTreeItem currentItem = items.get(nodeId);
				if (currentItem != null && currentItem.getChildren(valueSearch) != null)
				{
					item = currentItem;
					items = item.getChildren(valueSearch);
				}
			}
			if (item == null)
			{
				roots.remove("fake" + childrenFetched.toString());
				dirtyRoots = true;
			}
			else
			{
				item.getChildren(valueSearch).remove("fake" + childrenFetched.toString());
				item.setDirtyChildren(true);
			}
		}
	}
	
	public void refreshLastParent()
	{
		if (expandedVals != null && expandedVals.size() > 0)
		{
			Map<String, ValueTreeItem> items = roots;
			ValueTreeItem item = null;
			for (String val : expandedVals)
			{
				item  = items.get(val);
				items = item.getChildren(valueSearch);
			}
			item.clearChildren();
			item.getChildren(valueSearch);
		}
		else
		{
			roots = null;
			initRoots();
		}
	}

}
