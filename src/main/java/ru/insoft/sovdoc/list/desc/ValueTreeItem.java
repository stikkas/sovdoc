package ru.insoft.sovdoc.list.desc;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.insoft.commons.jsf.ui.SimpleItemTreeNode;
import ru.insoft.sovdoc.constant.VALUE_DISPLAY_TYPE;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.ui.desc.ValueSearchParam;

public class ValueTreeItem extends SimpleItemTreeNode {

	private ValueTree tree;
	private Map<String, ValueTreeItem> children;
	private boolean dirtyChildren;
	
	public ValueTreeItem(Long id, String type)
	{
		super();
		setId(id);
		setType(type);
	}
	
	public ValueTreeItem(Boolean isLeaf, Long id, String label, String type) 
	{
		super(isLeaf, id, label, type);
	}
	
	public ValueTree getTree() {
		return tree;
	}

	public void setTree(ValueTree tree) {
		this.tree = tree;
	}

	public Map<String, ValueTreeItem> getChildren(ValueSearchParam vsp)
	{
		if (!isExpanded())
			return null;
		if (children == null || dirtyChildren)
		{
			if (children == null)
				children = new LinkedHashMap<String, ValueTreeItem>();
			vsp.setParentId(getId());			
			
			Long expValue = null;
			if (tree.getExpandedVals() != null)
			{
				int idx = tree.getExpandedVals().indexOf(getId().toString());
				if (idx != -1 && idx < tree.getExpandedVals().size() - 1)
					expValue = Long.valueOf(tree.getExpandedVals().get(idx + 1));
				if (vsp.getValueId() != null && tree.getExpandedVals().size() > 0 &&
						tree.getExpandedVals().get(tree.getExpandedVals().size() - 1).equals(getId().toString()))
					expValue = vsp.getValueId();
			}
			boolean expFound = false;
			if (expValue == null)
				expFound = true;
			int count = getChildrenFetched();
			int i = 0;
			do
			{
				count += tree.getCHILDREN_PER_QUERY();
                                List<VDescValue> valList = vsp.searchValues(count);
				for (; i < count; i++)
				{
					if (i >= valList.size())
					{
						expFound = true;
						break;
					}
					
					VDescValue val = valList.get(i);
					if (val.getDescriptorValueId().equals(expValue))
						expFound = true;
					
					ValueTreeItem node = new ValueTreeItem(
							!val.isHasChildren(), val.getDescriptorValueId(),
							vsp.getDisplayedType().equals(VALUE_DISPLAY_TYPE.SHORT) ? 
									val.getShortValue() : val.getFullValue(), 
							"value");
					node.setData(val);
					node.setTree(tree);
					if (!node.isLeaf() && tree.getHierarchical())
					{
						if (tree.getExpandedVals() != null &&
							tree.getExpandedVals().contains(node.getId().toString()))
							node.setExpanded(true);
						/*else
							node.setExpanded(tree.isNodeExpanded(val.getDescriptorValueId().toString()));*/
					}
						
					if (!children.containsKey(val.getDescriptorValueId().toString()))
						children.put(val.getDescriptorValueId().toString(), node);
				}
			}
			while (!expFound);
			
			if (i == count)
			{
				ValueTreeItem node = new ValueTreeItem(true, Long.valueOf(count), null, "fake");
				children.put("fake" + node.getId().toString(), node);
			}
			dirtyChildren = false;
		}
		return children;
	}
	
	public boolean isDirtyChildren() {
		return dirtyChildren;
	}

	public void setDirtyChildren(boolean dirtyChildren) {
		this.dirtyChildren = dirtyChildren;
	}
	
	public void clearChildren()
	{
		children = null;
	}
	
	public int getChildrenFetched()
	{
		if (tree.getFetchLinkId() == null)
			return 0;
		
		List<String> nodes = tree.parseNodeId(tree.getFetchLinkId());
		if (nodes.get(nodes.size() - 2).equals(getId().toString()))
			return tree.getChildrenFetched();
		else
			return 0;
	}

	@Override
	public VDescValue getData()
	{
		return (VDescValue)super.getData();
	}

}
