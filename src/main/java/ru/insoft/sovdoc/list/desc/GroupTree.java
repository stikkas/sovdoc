package ru.insoft.sovdoc.list.desc;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jboss.solder.servlet.http.RequestParam;
import org.richfaces.model.DeclarativeModelKey;
import org.richfaces.model.SequenceRowKey;

import ru.insoft.commons.jsf.ui.CommonTree;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescSubsystem;
import ru.insoft.sovdoc.security.AccessService;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.ui.desc.ValueSearchParam;

import com.google.common.collect.Lists;

@RequestScoped
@Named
public class GroupTree extends CommonTree<Map<String, GroupTreeItem>> {
	
	@Inject
	SystemEntity se;
	@Inject
	EntityManager em;
	@Inject
	ValueSearchParam valueSearch;
	@Inject
	ValueTree vTree;
	@Inject
	AccessService access;
	
	@Inject
	@RequestParam(value = "groupId")
	Long groupId;
	@Inject
	@RequestParam(value = "ss")
	Integer ssParam;
	@Inject
	@RequestParam(value = "ord")
	Integer ordParam;
	@Inject
	@RequestParam(value = "system")
	Boolean system;
	
	@PostConstruct
	private void init()
	{
		if (groupId == null)
			groupId = valueSearch.getGroupId();
	}
	
	@Override
	protected void initRoots()
	{
		roots = new LinkedHashMap<String, GroupTreeItem>();
		
		DescriptorGroup gr = null;
		if (groupId != null)
        gr = se.getDescGroup(groupId);
		GroupTreeItem node;
		
		List<VDescGroup> userGroups = se.getUserDescGroups();
		node = new GroupTreeItem(
				userGroups.size() == 0, 0L, "Указатели НСА", "userDesc");
		node.setData(userGroups.size());
		if (gr != null && !gr.isSystem() || 
				ssParam == null && ordParam != null && ordParam <= userGroups.size() ||
				system != null && system.equals(false))
			node.setExpanded(true);
		else
			node.setExpanded(isNodeExpanded("userDesc"));
		node.setChildren(new LinkedHashMap<String, GroupTreeItem>());
		for (VDescGroup dg : userGroups)
		{
			GroupTreeItem subnode = new GroupTreeItem(
					true, dg.getDescriptorGroupId(), dg.getGroupName(), "descGroup");
			subnode.setData(dg);
			node.getChildren().put("descGroup" + dg.getDescriptorGroupId().toString(), subnode);
		}
		roots.put("userDesc", node);
		
		if (access.hasAccess("DESC_GROUP_SYS"))
		{
			List<VDescSubsystem> ssList = se.getSubsystemModel();
			node = new GroupTreeItem(ssList.size() == 0, 1L, "Системные классификаторы по подсистемам", "systemDesc");
			node.setData(ssList.size());
			if (gr != null && gr.isSystem() || ssParam != null)
				node.setExpanded(true);
			else
				node.setExpanded(isNodeExpanded("systemDesc"));
			node.setChildren(new LinkedHashMap<String, GroupTreeItem>());
			for (VDescSubsystem ss : ssList)
			{
				GroupTreeItem subnode = new GroupTreeItem(
						!ss.isHasGroups(), ss.getSubsystemNumber(), 
						"Подсистема \"" + ss.getSubsystemName() + "\"", "subsystem");
				subnode.setData(ss);
				if (gr != null && gr.getSubsystemNumber() != null)
					subnode.setExpanded(gr.getSubsystemNumber().equals(ss.getSubsystemNumber()));
				if (ssParam != null)
					subnode.setExpanded(ssParam.equals(ss.getSubsystemNumber().intValue()));
				if ((gr == null || gr.getSubsystemNumber() == null) && ssParam == null)
					subnode.setExpanded(isNodeExpanded("subsystem" + ss.getSubsystemNumber().toString()));
				node.getChildren().put("subsystem" + ss.getSubsystemNumber().toString(), subnode);
			}
			roots.put("systemDesc", node);
		}
	}
	
	@Override
	protected String[] getNodeTypes()
	{
		return new String[]{"userDesc", "systemDesc", "subsystem", "descGroup"};
	}
	
	public void selectNodes()
	{
		roots = getRootNodes();
		if (groupId != null)
		{
			DescriptorGroup gr = se.getDescGroup(groupId);
			if (gr.getSubsystemNumber() != null)
				ssParam = gr.getSubsystemNumber().intValue();
		}
		if (groupId == null && ordParam != null)
		{
			Map<String, GroupTreeItem> map = null;
			if (ssParam == null)
				map = roots.get("userDesc").getChildren();
			else
				map = roots.get("systemDesc").getChildren().get("subsystem" + ssParam.toString()).getChildren(se);
			for (String id : map.keySet())
				if (map.get(id).getDescGroup().getSortOrder().equals(ordParam))
				{
					groupId = map.get(id).getId();
					break;
				}
		}
		if (groupId != null || ordParam != null)
		{
			Collection<Object> coll = Lists.newArrayList();
			if (ssParam == null)
				coll.add(new DeclarativeModelKey(getAdaptorId(), "userDesc"));
			else
			{
				coll.add(new DeclarativeModelKey(getAdaptorId(), "systemDesc"));
				coll.add(new DeclarativeModelKey(getAdaptorId(), "subsystem" + ssParam.toString()));
			}
			if (groupId != null)
				coll.add(new DeclarativeModelKey(getAdaptorId(), "descGroup" + groupId.toString()));
			
			SequenceRowKey srk = new SequenceRowKey(coll.toArray());
			coll = Lists.newArrayList((Object)srk);
			setSelectionKeys(coll);
			resetCurrentNode();
			initValueSearch();
		}
		vTree.selectNodes();
	}
	
	public void groupMoveUp()
	{
		DescriptorGroup curGroup = se.getDescGroup(groupId);
		int curOrder = curGroup.getSortOrder().intValue();
		List<VDescGroup> groupList = null;
		if (curGroup.getSubsystemNumber() == null)
			groupList = se.getUserDescGroups();
		else
			groupList = se.getDescGroups(curGroup.getSubsystemNumber());
		for (long i = 0; i < groupList.size(); i++)
		{
			VDescGroup dg = groupList.get((int)i);
			Long correctOrder = null;
			if (i == curOrder - 2)
				correctOrder = i + 2;
			if (i == curOrder - 1 && i > 0)
				correctOrder = i;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!dg.getSortOrder().equals(correctOrder))
			{
				DescriptorGroup modifGroup = se.getDescGroup(dg.getDescriptorGroupId());
				modifGroup.setSortOrder(correctOrder);
				em.merge(modifGroup);
				em.flush();
			}
		}
		initRoots();
	}
	
	public void groupMoveDown()
	{
		DescriptorGroup curGroup = se.getDescGroup(groupId);
		int curOrder = curGroup.getSortOrder().intValue();
		List<VDescGroup> groupList = null;
		if (curGroup.getSubsystemNumber() == null)
			groupList = se.getUserDescGroups();
		else
			groupList = se.getDescGroups(curGroup.getSubsystemNumber());
		for (long i = 0; i < groupList.size(); i++)
		{
			VDescGroup dg = groupList.get((int)i);
			Long correctOrder = null;
			if (i == curOrder)
				correctOrder = i;
			if (i == curOrder - 1 && i < groupList.size() - 1)
				correctOrder = i + 2;
			if (correctOrder == null)
				correctOrder = i + 1;
			if (!dg.getSortOrder().equals(correctOrder))
			{
				DescriptorGroup modifGroup = se.getDescGroup(dg.getDescriptorGroupId());
				modifGroup.setSortOrder(correctOrder);
				em.merge(modifGroup);
				em.flush();
			}
		}
		initRoots();
	}

	@Override
	public void selectNode()
	{
		super.selectNode();
		GroupTreeItem currentNode = new GroupTreeItem(
				getCurrentNode().getId(), getCurrentNode().getType());
		if (currentNode.getType().equals("userDesc"))
			currentNode.setData(se.getUserDescGroups().size());
		if (currentNode.getType().equals("subsystem"))
			currentNode.setData(se.getSubsystem(currentNode.getId()));
		if (currentNode.getType().equals("descGroup"))
			currentNode.setData(se.getImmDescGroup(currentNode.getId()));
		setCurrentNode(currentNode);
		initValueSearch();
	}
	
	private void initValueSearch()
	{
		if (getCurrentNode() != null && getCurrentNode().getType().equals("descGroup"))
		{
			valueSearch.setGroupId(getCurrentNode().getId());
			vTree.init();
		}
	}

	@Override
	protected String getAdaptorId() 
	{
		return "group";
	}
	
	public void removeGroup()
	{
		Map<String, String> reqMap = 
			FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		groupId = Long.valueOf(reqMap.get("argument"));
		DescriptorGroup gr = se.getDescGroup(groupId);
		
		Query q = em.createNativeQuery("begin DESCRIPTOR_PACK.REMOVE_GROUP(:p_group_id); end;");
		q.setParameter("p_group_id", groupId);
		try
		{
			q.executeUpdate();
			
			if (gr.getSubsystemNumber() != null)
				ssParam = gr.getSubsystemNumber().intValue();
			else
				system = false;
			groupId = null;
		}
		catch (Exception e)
		{
			String msg = ExceptionUtils.getConstraintViolationMessage(e);
			if (msg == null)
			{
				e.printStackTrace();
				return;
			}
			if (msg.indexOf("FK2_DSCGRP_DSCGRPATR") >= 0)
				MessageUtils.ErrorMessage("На данную группу имеется ссылка из атрибута");
			else
				MessageUtils.ErrorMessage("На значения данной группы имеются ссылки извне");
		}
		selectNodes();
	}
		
}
