package ru.insoft.sovdoc.list.desc;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.insoft.commons.jsf.ui.SimpleItemTreeNode;
import ru.insoft.sovdoc.model.desc.view.VDescGroup;
import ru.insoft.sovdoc.model.desc.view.VDescSubsystem;
import ru.insoft.sovdoc.system.SystemEntity;

public class GroupTreeItem extends SimpleItemTreeNode {
	
	private Map<String, GroupTreeItem> children;
	
	public GroupTreeItem(Long id, String type)
	{
		super();
		setId(id);
		setType(type);
	}
	
	public GroupTreeItem(Boolean isLeaf, Long id, String label, String type) 
	{
		super(isLeaf, id, label, type);
	}
	
	public String getAddParamString()
	{
		String res = "";
		if (getType().equals("userDesc"))
		{
			res = "system=false&amp;ord=" + String.valueOf(((Integer)getData() + 1)) +
				"&amp;mode=add";
		}
		if (getType().equals("systemDesc"))
			res = "system=true&amp;mode=add";
		if (getType().equals("subsystem"))
		{
			VDescSubsystem ss = (VDescSubsystem)getData();
			res = "system=true&amp;ss=" + getId().toString() + "&amp;ord=" + 
				((Integer)(ss.getGroupCnt() + 1)).toString() +
				"&amp;mode=add";
		}
		if (getType().equals("descGroup"))
		{
			VDescGroup dg = (VDescGroup)getData();
			if (dg.isSystem())
				res = "system=true&amp;ss=" + dg.getSubsystemNumber().toString() + "&amp;";
			res += "ord=" + dg.getSortOrder().toString() + "&amp;mode=add";
		}
		
		return res;
	}
	
	public String getEditParamString()
	{
		String res = null;
		if (getType().equals("descGroup"))
		{
			VDescGroup dg = (VDescGroup)getData();
			res = "groupId=" + dg.getDescriptorGroupId().toString();
		}
		
		return res;
	}
	
	public Map<String, GroupTreeItem> getChildren(SystemEntity se)
	{
		if (!isExpanded())
			return null;
		if (children == null && getType().equals("subsystem"))
		{
			children = new LinkedHashMap<String, GroupTreeItem>();
			List<VDescGroup> groupList = se.getDescGroups(getId());
			
			for (VDescGroup group : groupList)
			{
				GroupTreeItem newNode = new GroupTreeItem(
						true, group.getDescriptorGroupId(), group.getGroupName(),
						"descGroup");
				newNode.setData(group);
				children.put("descGroup" + group.getDescriptorGroupId().toString(), newNode);
			}
		}
		
		return children;
	}
	
	public Map<String, GroupTreeItem> getChildren()
	{
		return children;
	}
	
	public void setChildren(Map<String, GroupTreeItem> children)
	{
		this.children = children;
	}
	
	public VDescGroup getDescGroup()
	{
		return (VDescGroup)getData();
	}

}
