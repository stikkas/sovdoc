package ru.insoft.sovdoc.list.desc;

import com.google.common.collect.Lists;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.constant.VALUE_DISPLAY_TYPE;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss_;
import ru.insoft.sovdoc.ui.desc.ValueSearchParam;

/**
 *
 * @author melnikov
 */
@RequestScoped
@Named("univClssSearch")
public class UnivClssSearchCriteria implements ViewQuerySearchCriteria<VDescValueUnivclss>
{
    @Inject
    ValueSearchParam valueSearch;
    
    private Criteria criteria;
    
    public class Criteria
    {
        private boolean actual;
        private VALUE_DISPLAY_TYPE displayType;
        private String index;
        private String value;

        public boolean isActual() {
            return actual;
        }

        public void setActual(boolean actual) {
            this.actual = actual;
        }

        public VALUE_DISPLAY_TYPE getDisplayType() {
            return displayType;
        }

        public void setDisplayType(VALUE_DISPLAY_TYPE displayType) {
            this.displayType = displayType;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }        
    }
    
    @PostConstruct
    private void init()
    {
        criteria = new Criteria();
        criteria.setActual(valueSearch.getActual());
        criteria.setDisplayType(valueSearch.getDisplayedType());
    }
    
    public Criteria getCriteria()
    {
        return criteria;
    }
    
    @Override
    public boolean validateCriteria()
    {
        return true;
    }

    @Override
    public <Q> Predicate getPredicateByCriteria(CriteriaBuilder builder, CriteriaQuery<Q> query, Root<VDescValueUnivclss> root) 
    {
        List<Predicate> predicates = Lists.newArrayList();
        if (criteria.isActual())
            predicates.add(builder.equal(root.get(VDescValueUnivclss_.isActual), true));
        if (criteria.getIndex() != null)
            predicates.add(builder.like(builder.upper(root.get(VDescValueUnivclss_.valueIndex)), 
                    String.format("%1$s%%", criteria.getIndex().toUpperCase())));
        if (criteria.getValue() != null)
        {
            if (criteria.getDisplayType().equals(VALUE_DISPLAY_TYPE.SHORT))
                predicates.add(builder.greaterThan(builder.function("contains", Integer.class, 
                        root.get(VDescValueUnivclss_.shortValue),
                        builder.literal(StringUtils.textRequest(criteria.getValue()))), 0));
            if (criteria.getDisplayType().equals(VALUE_DISPLAY_TYPE.FULL))
                predicates.add(builder.greaterThan(builder.function("contains", Integer.class,
                        root.get(VDescValueUnivclss_.fullValue),
                        builder.literal(StringUtils.textRequest(criteria.getValue()))), 0));
        }
        
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
