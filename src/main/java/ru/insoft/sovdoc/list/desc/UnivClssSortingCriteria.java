package ru.insoft.sovdoc.list.desc;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;

/**
 *
 * @author melnikov
 */
public class UnivClssSortingCriteria extends CommonSortingCriteria
{
    @Override
    public <T> List<Order> getSortOrder(CriteriaBuilder builder, Root<T> root) 
    {
        List<Order> ord = super.getSortOrder(builder, root); 
        if (ord.isEmpty())
        {
            setSorting("valueIndex", "ascending", true);
            ord = super.getSortOrder(builder, root);
        }
        return ord;
    }    
}
