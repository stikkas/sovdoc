package ru.insoft.sovdoc.list.desc;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;
import ru.insoft.commons.jsf.ui.datalist.Paginator;
import ru.insoft.commons.jsf.ui.datalist.ViewQueryDataList;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataList;

/**
 *
 * @author melnikov
 */
@RequestScoped
@Named("univClssDL")
public class UnivClssDataList extends ViewQueryDataList<VDescValueUnivclss>
    implements ValueSelectDataList
{
    @Inject
    EntityManager em;
    @Inject
    Paginator paginator;
    @Inject
    UnivClssSearchCriteria searchCriteria;
    
    private UnivClssSortingCriteria sortingCriteria;
    private boolean valueSelected;

    @Override
    public ViewQuerySearchCriteria<VDescValueUnivclss> getSearchCriteria() 
    {
        return searchCriteria;
    }

    @Override
    public CommonSortingCriteria getSortingCriteria() 
    {
        if (sortingCriteria == null)
        {
            sortingCriteria = new UnivClssSortingCriteria();
            sortingCriteria.init();
        }
        return sortingCriteria;
    }

    @Override
    public Paginator getPaginator() 
    {
        return paginator;
    }

    @Override
    protected EntityManager getEntityManager() 
    {
        return em;
    }

    @Override
    public String getDataLabel() 
    {
        return null;
    }

    @Override
    public boolean isValueSelected() 
    {
        return valueSelected;
    }

    @Override
    public void setValueSelected(boolean valueSelected) 
    {
        this.valueSelected = valueSelected;
    }
    
    @Override
    public Long getSelectedValue()
    {
       if (getRowId() == null)
           return null;
       return Long.valueOf(getRowId());
    }
}
