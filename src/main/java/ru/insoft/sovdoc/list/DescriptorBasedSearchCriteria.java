package ru.insoft.sovdoc.list;

import java.util.List;

import javax.inject.Inject;

import org.jboss.solder.servlet.http.RequestParam;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.model.desc.view.VDescValue;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.ui.desc.ValueSelectDataListGeneral;

import com.google.common.collect.Lists;

public abstract class DescriptorBasedSearchCriteria {

	@Inject
	protected ValueSelectDataListGeneral valueSelectDL;
	@Inject
	protected SystemEntity se;
	
	@Inject
	@RequestParam(value = "descIds")
	protected String descIds;
	
	private List<VDescValue> selectedDescriptors;
	
	public String getDescIds() {
		return descIds;
	}

	public void setDescIds(String descIds) {
		this.descIds = descIds;
	}

	public List<VDescValue> getSelectedDescriptors()
	{
		if (selectedDescriptors == null && descIds != null && !descIds.isEmpty())
		{
			List<String> idList = Lists.newArrayList(descIds.split(","));
			selectedDescriptors = Lists.newArrayListWithExpectedSize(idList.size());
			for (String id : idList)
				selectedDescriptors.add(se.getImmDescValue(Long.valueOf(id)));
		}
		return selectedDescriptors;
	}
	
	public void addDescriptor()
	{
		if (valueSelectDL.getSelectedValue() == null)
		{
			MessageUtils.ErrorMessage("Значение должно быть выбрано");
			return;
		}
		if (descIds != null &&
			Lists.newArrayList(descIds.split(",")).contains(valueSelectDL.getSelectedValue().toString()))
		{
			MessageUtils.ErrorMessage("Это значение уже добавлено");
			return;
		}
		if (descIds == null || descIds.isEmpty())
			descIds = valueSelectDL.getSelectedValue().toString();
		else
			descIds = descIds + "," + valueSelectDL.getSelectedValue().toString();
		
		valueSelectDL.setValueSelected(true);
	}
	
	public void removeDescriptor(VDescValue value)
	{
		List<String> idList = Lists.newArrayList(descIds.split(","));
		idList.remove(value.getDescriptorValueId().toString());
		selectedDescriptors.remove(value);
		descIds = StringUtils.uniteString(idList, ",");
	}
}
