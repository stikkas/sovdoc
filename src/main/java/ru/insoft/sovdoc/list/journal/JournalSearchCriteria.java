package ru.insoft.sovdoc.list.journal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.*;
import javax.inject.*;
import javax.persistence.EntityManager;
import javax.persistence.criteria.*;

import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.sovdoc.list.DescriptorBasedSearchCriteria;
import ru.insoft.sovdoc.model.correct.view.VUnivCorrectJournal;
import ru.insoft.sovdoc.model.correct.view.VUnivCorrectJournal_;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue_;
import ru.insoft.sovdoc.system.SystemEntity;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: vasilev
 * Date: 08.04.13
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 */
@RequestScoped
@Named("journalSearch")
public class JournalSearchCriteria extends DescriptorBasedSearchCriteria
        implements ViewQuerySearchCriteria<VUnivCorrectJournal> {

    @Inject
    SystemEntity se;

    private Criteria criteria;

    public class Criteria {
        private Long archiveId;

        public Long getArchiveId() {
            return archiveId;
        }

        public void setArchiveId(Long archiveId) {
            this.archiveId = archiveId;
        }

        private Long changedEntityTypeId;

        public Long getChangedEntityTypeId() {
            return changedEntityTypeId;
        }

        public void setChangedEntityTypeId(Long changedEntityTypeId) {
            this.changedEntityTypeId = changedEntityTypeId;
        }

        private Long pointNsa;

        public Long getPointNsa() {
            return pointNsa;
        }

        public void setPointNsa(Long pointNsa) {
            this.pointNsa = pointNsa;
        }

        public Boolean getNsaVisible() {
            if (changedEntityTypeId != null) {
                Long valueCode = se.getImmDescValueByCodes("CHANGED_ENTITY_TYPE", "DESCRIPTOR_VALUE").getDescriptorValueId();
                return changedEntityTypeId.equals(valueCode);
            }
            return false;
        }

        private Date beginDate;
        private Date endDate;

        public Date getBeginDate() {
            return beginDate;
        }

        public void setBeginDate(Date beginDate) {
            this.beginDate = beginDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        private Long operationTypeId;

        public Long getOperationTypeId() {
            return operationTypeId;
        }

        public void setOperationTypeId(Long operationTypeId) {
            this.operationTypeId = operationTypeId;
        }

        private Long userId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((archiveId == null) ? 0 : archiveId.hashCode());
            result = prime * result + ((changedEntityTypeId == null) ? 0 : changedEntityTypeId.hashCode());
            result = prime * result + ((pointNsa == null) ? 0 : pointNsa.hashCode());
            result = prime * result + ((beginDate == null) ? 0 : beginDate.hashCode());
            result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
            result = prime * result + ((operationTypeId == null) ? 0 : operationTypeId.hashCode());
            result = prime * result + ((userId == null) ? 0 : userId.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            Criteria other = (Criteria) obj;
            if (!getOuterType().equals(other.getOuterType())) return false;

            if (archiveId == null) {
                if (other.archiveId != null) return false;
            } else {
                if (!archiveId.equals(other.archiveId)) return false;
            }

            if (changedEntityTypeId == null) {
                if (other.changedEntityTypeId != null) return false;
            } else {
                if (!changedEntityTypeId.equals(other.changedEntityTypeId)) return false;
            }

            if (pointNsa == null) {
                if (other.pointNsa != null) return false;
            } else {
                if (!pointNsa.equals(other.pointNsa)) return false;
            }

            if (beginDate == null) {
                if (other.beginDate != null) return false;
            } else {
                if (!beginDate.equals(other.beginDate)) return false;
            }

            if (endDate == null) {
                if (other.endDate != null) return false;
            } else {
                if (!endDate.equals(other.endDate)) return false;
            }

            if (operationTypeId == null) {
                if (other.operationTypeId != null) return false;
            } else {
                if (!operationTypeId.equals(other.operationTypeId)) return false;
            }

            if (userId == null) {
                if (other.userId != null) return false;
            } else {
                if (!userId.equals(other.userId)) return false;
            }

            return true;
        }

        private JournalSearchCriteria getOuterType() {
            return JournalSearchCriteria.this;
        }
    }

    public boolean isEmpty() {
        return criteria.equals(new Criteria()) && (descIds == null || descIds.isEmpty());
    }

    @PostConstruct
    private void init() {
        criteria = new Criteria();
    }

    public Criteria getCriteria() {
        return criteria;
    }

    @Override
    public boolean validateCriteria() {
//        if (isEmpty()) {
//            MessageUtils.ErrorMessage("Поиск без параметров невозможен");
//            return false;
//        }
        return true;
    }

    @Override
    public <Q> Predicate getPredicateByCriteria(CriteriaBuilder builder,
                                                CriteriaQuery<Q> query,
                                                Root<VUnivCorrectJournal> root) {
        List<Predicate> predicates = new ArrayList<Predicate>();

        if (criteria.getArchiveId() != null)
            predicates.add(builder.equal(root.get(VUnivCorrectJournal_.archiveId), criteria.archiveId));

        if (criteria.getChangedEntityTypeId() != null) {
            /*Long pointValue = se.getImmDescValueByCodes("CHANGED_ENTITY_TYPE" ,"DESCRIPTOR_VALUE").getDescriptorValueId();
            if (criteria.getPointNsa() != null && criteria.getChangedEntityTypeId().equals(pointValue)) {
                Subquery<Long> sub = query.subquery(Long.class);
                Root<DescriptorValue> subRoot = sub.from(DescriptorValue.class);
                sub.select(subRoot.get(DescriptorValue_.descriptorValueId));
                sub.where(builder.equal(subRoot.get(DescriptorValue_.descriptorGroupId), criteria.pointNsa));
                predicates.add(builder.in(root.get(VUnivCorrectJournal_.descriptorValueId)).value(sub));
            } else*/
                predicates.add(builder.equal(root.get(VUnivCorrectJournal_.changedEntityTypeId), criteria.changedEntityTypeId));
        }

        if (criteria.getBeginDate() != null)
        {
        	Calendar cal = Calendar.getInstance();
        	cal.setTime(criteria.getEndDate() == null ?	criteria.getBeginDate() : criteria.getEndDate());
        	cal.set(Calendar.HOUR_OF_DAY, 23);
        	cal.set(Calendar.MINUTE, 59);
        	cal.set(Calendar.SECOND, 59);
            predicates.add(builder.between(root.get(VUnivCorrectJournal_.operationDate), criteria.beginDate, cal.getTime()));
        }
        if (criteria.getOperationTypeId() != null)
            predicates.add(builder.equal(root.get(VUnivCorrectJournal_.operationTypeId), criteria.operationTypeId));
        if (criteria.getUserId() != null)
            predicates.add(builder.equal(root.get(VUnivCorrectJournal_.userId), criteria.userId));

        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
