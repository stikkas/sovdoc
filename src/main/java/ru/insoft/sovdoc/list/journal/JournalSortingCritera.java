/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.insoft.sovdoc.list.journal;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;
/**
 *
 * @author abukhov
 */
public class JournalSortingCritera extends CommonSortingCriteria{

	@Override
	public <T> List<Order> getSortOrder(CriteriaBuilder builder, Root<T> root)
	{
		List<Order> ord = super.getSortOrder(builder, root);
		if (ord.size() == 0)
		{
			setSorting("operationDate", "descending", true);
			ord = super.getSortOrder(builder, root);
		}
		return ord;
	}
}