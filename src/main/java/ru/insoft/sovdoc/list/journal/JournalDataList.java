package ru.insoft.sovdoc.list.journal;

import javax.enterprise.context.*;
import javax.inject.*;
import javax.persistence.*;

import com.google.common.collect.Lists;
import ru.insoft.commons.jsf.ui.datalist.*;
import ru.insoft.sovdoc.model.correct.view.VUnivCorrectJournal;

import java.util.List;
import ru.insoft.sovdoc.list.journal.JournalSortingCritera;

/**
 * Created with IntelliJ IDEA.
 * User: vasilev
 * Date: 08.04.13
 * Time: 11:10
 * To change this template use File | Settings | File Templates.
 */
@RequestScoped
@Named("journalDL")
public class JournalDataList extends ViewQueryDataList<VUnivCorrectJournal> {

    @Inject
    EntityManager em;
    @Inject
    JournalSearchCriteria searchCriteria;
    @Inject
    Paginator paginator;
    @Inject
	JournalSortingCritera sortingCriteria;

   // private CommonSortingCriteria sortingCriteria;
    private List<Long> wrappedIds;

    @Override
    public ViewQuerySearchCriteria<VUnivCorrectJournal> getSearchCriteria()
    {
        return searchCriteria;
    }

    @Override
    public JournalSortingCritera getSortingCriteria()
    {

        return sortingCriteria;
    }

    @Override
    public Paginator getPaginator()
    {
        return paginator;
    }

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    public List<Long> getWrappedIds()
    {
        if (wrappedIds == null)
        {
            List<VUnivCorrectJournal> data = getWrappedData();
            wrappedIds = Lists.newArrayListWithExpectedSize(data.size());
            for (VUnivCorrectJournal card : data)
                if (!wrappedIds.contains(card.getUnivCorrectJournalId()))
                    wrappedIds.add(card.getUnivCorrectJournalId());
        }
        return wrappedIds;
    }
}