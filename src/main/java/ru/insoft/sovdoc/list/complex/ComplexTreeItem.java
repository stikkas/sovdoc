package ru.insoft.sovdoc.list.complex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.SimpleItemTreeNode;
import ru.insoft.sovdoc.model.complex.view.VComplexArchive;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexTree;
import ru.insoft.sovdoc.model.complex.view.VComplexTree_;

public class ComplexTreeItem extends SimpleItemTreeNode {

	private ComplexTree tree;
	private Map<String, ComplexTreeItem> children;
	private boolean dirtyChildren;
	
	public ComplexTree getTree() {
		return tree;
	}

	public void setTree(ComplexTree tree) {
		this.tree = tree;
	}

    public ComplexTreeItem()
    {
        super();
    }

	public ComplexTreeItem(Boolean isLeaf, Long id, String label, String type) 
	{
		super(isLeaf, id, label, type);
	}
	
	public Map<String, ComplexTreeItem> getChildren()
	{
		if (!isExpanded())
			return null;
		if (children == null || dirtyChildren)
		{
			if (children == null)
				children = new LinkedHashMap<String, ComplexTreeItem>();
			List<VComplexTree> unitList = null;
			Long expUnit = null;
			if (getType().equals("archive"))
			{
				List<VComplexArchive> archList = tree.getArchives(getId());
				for (VComplexArchive arch : archList)
				{
					ComplexTreeItem node = new ComplexTreeItem(
							!arch.isHasChildren(), arch.getArchiveId(),
							arch.getArchiveShortName(), "archive");
					node.setData(arch);
					node.setTree(tree);
					if (!node.isLeaf())
					{
						if (tree.getExpArchives() != null && tree.getExpArchives().contains(node.getId()))
							node.setExpanded(true);
						/*else
							node.setExpanded(tree.isNodeExpanded("archive" + arch.getArchiveId().toString()));*/
					}
					if (!children.containsKey("archive" + arch.getArchiveId().toString()))
						children.put("archive" + arch.getArchiveId().toString(), node);
				}
				unitList = queryUnitList(getId(), null);
				if (tree.getExpArchives() != null && 
						tree.getExpArchives().indexOf(getId()) == tree.getExpArchives().size() - 1 &&
						tree.getExpUnits() != null &&
						tree.getExpUnits().size() > 0)
					expUnit = tree.getExpUnits().get(0);
				if (tree.unitId != null && tree.getExpUnits().size() == 0 && 
						tree.getExpArchives().get(tree.getExpArchives().size() - 1).equals(getId()))
					expUnit = tree.unitId;
			}
			else
			{
				VComplexTree unit = (VComplexTree)getData();
				unitList = queryUnitList(unit.getArchiveId(), getId());
				if (tree.getExpUnits() != null)
				{
					int idx = tree.getExpUnits().indexOf(getId());
					if (idx != -1 && idx < tree.getExpUnits().size() - 1)
						expUnit = tree.getExpUnits().get(idx + 1);
					if (tree.unitId != null && tree.getExpUnits().size() > 0 &&
							tree.getExpUnits().get(tree.getExpUnits().size() - 1).equals(getId()))
						expUnit = tree.unitId;
				}
			}
			
			boolean expFound = false;
			if (expUnit == null)
				expFound = true;
			int count = getChildrenFetched();
			int i = 0;
			do
			{
				count += tree.getCHILDREN_PER_QUERY();
				for (; i < count; i++)
				{
					if (i >= unitList.size())
						break;
					
					VComplexTree unit = unitList.get(i);
					if (unit.getUnivDataUnitId().equals(expUnit))
						expFound = true;
					
					ComplexTreeItem node = new ComplexTreeItem(
							!unit.isHasChildren(), unit.getUnivDataUnitId(),
							unit.getUnitName(), unit.getDescriptionLevelCode().toLowerCase());
					node.setData(unit);
					node.setTree(tree);
					if (!node.isLeaf())
					{
						if (tree.getExpUnits() != null && tree.getExpUnits().contains(node.getId()))
							node.setExpanded(true);
						/*else
							node.setExpanded(tree.isNodeExpanded("unit" + node.getId().toString()));*/
					}
					if (!children.containsKey("unit" + node.getId().toString()))
						children.put("unit" + node.getId().toString(), node);
				}
			}
			while (!expFound);
			
			if (i == count)
			{
				ComplexTreeItem node = new ComplexTreeItem(true, Long.valueOf(count), null, "fake");
				children.put("fake" + node.getId().toString(), node);
			}
			dirtyChildren = false;
		}
		return children;
	}
	
	public boolean isDirtyChildren() {
		return dirtyChildren;
	}

	public void setDirtyChildren(boolean dirtyChildren) {
		this.dirtyChildren = dirtyChildren;
	}

	public int getChildrenFetched()
	{
		if (tree.getFetchLinkId() == null)
			return 0;
		
		List<String> nodes = tree.parseNodeId(tree.getFetchLinkId());
		if (nodes.get(nodes.size() - 2).equals(
				(getType().equals("archive") ? "archive" : "unit") + getId().toString()))
			return tree.getChildrenFetched();
		else
			return 0;
	}
	
	protected List<VComplexTree> queryUnitList(Long archiveId, Long parentUnitId)
	{
		CriteriaBuilder cb = tree.em.getCriteriaBuilder();
		CriteriaQuery<VComplexTree> cq = cb.createQuery(VComplexTree.class);
		Root<VComplexTree> root = cq.from(VComplexTree.class);
		cq.select(root).orderBy(cb.asc(root.get(VComplexTree_.numberNumber)),
				cb.asc(root.get(VComplexTree_.numberText)), cb.asc(root.get(VComplexTree_.unitName)));
		Predicate predicate = null;
		if (parentUnitId == null)
			predicate = cb.isNull(root.get(VComplexTree_.parentUnitId));
		else
			predicate = cb.equal(root.get(VComplexTree_.parentUnitId), parentUnitId);
		cq.where(cb.and(cb.equal(root.get(VComplexTree_.archiveId), archiveId), 
				cb.isNotNull(root.get(VComplexTree_.descriptionLevelId)), predicate));
		return tree.em.createQuery(cq).getResultList();
	}
	
	public VComplexTree getDataUnit()
	{
		return (VComplexTree)getData();
	}

}