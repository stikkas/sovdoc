package ru.insoft.sovdoc.list.complex;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("complexSearch")
public class ComplexSearch1 extends ComplexSearchCriteria implements Serializable {

	@PostConstruct
	private void enumSet() {
		getCriteria().setEnum1(null);
	}
}
