package ru.insoft.sovdoc.list.complex;

import com.google.common.collect.Lists;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;
import ru.insoft.commons.jsf.ui.datalist.Paginator;
import ru.insoft.commons.jsf.ui.datalist.ViewQueryDataList;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnitLite;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnitLite_;


public class ComplexDataList extends ViewQueryDataList<VComplexDataUnitLite> 
{
    @Inject
    EntityManager em;

    @Inject
    ComplexSortingCriteria sortingCriteria;
    @Inject
    Paginator paginator;
	
    @Override
    public CommonSortingCriteria getSortingCriteria() 
    {
	return sortingCriteria;
    }
	
    @Override
    public Paginator getPaginator() 
    {
    	return paginator;
    }
	
    @Override
    protected EntityManager getEntityManager() 
    {
	return em;
    }

    @Override
    public ViewQuerySearchCriteria<VComplexDataUnitLite> getSearchCriteria() 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
        
    @Override
    public List<VComplexDataUnitLite> getWrappedData(Integer pageNum, Integer pageSize)
    {
        if (wrappedData == null && isShowResult())
        {
            List<VComplexDataUnitLite> queryRes = super.getWrappedData(pageNum, pageSize);
            List<Long> idList = Lists.newArrayListWithExpectedSize(queryRes.size());
            for (VComplexDataUnitLite dataUnit : queryRes)
                idList.add(dataUnit.getUnivDataUnitId());
            
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
            Root<VComplexDataUnitLite> root = cq.from(VComplexDataUnitLite.class);
            cq.multiselect(cb.function("get_readable_arch_number", String.class,
                            root.get(VComplexDataUnitLite_.archNumberCode), cb.literal("Д.")),
                    cb.function("get_date_intervals", String.class, 
                            root.get(VComplexDataUnitLite_.univDataUnitId)));
            cq.where(root.get(VComplexDataUnitLite_.univDataUnitId).in(idList));
            cq.orderBy(getSortingCriteria().<VComplexDataUnitLite>getSortOrder(cb, root));
            List<Tuple> queryRes2 = em.createQuery(cq).getResultList();
            for (int i = 0; i < queryRes2.size(); i++)
            {
                VComplexDataUnitLite dataUnit = queryRes.get(i);
                Tuple t = queryRes2.get(i);
                dataUnit.setArchNumber(t.get(0, String.class));
                dataUnit.setDateIntervals(t.get(1, String.class));
            }
            wrappedData = queryRes;
        }
        return wrappedData;
    }
}
