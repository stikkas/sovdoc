package ru.insoft.sovdoc.list.complex;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.commons.utils.ValidationUtils;
import ru.insoft.sovdoc.list.DescriptorBasedSearchCriteria;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval_;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor_;
import ru.insoft.sovdoc.model.complex.view.VComplexArchiveHierarchy;
import ru.insoft.sovdoc.model.complex.view.VComplexArchiveHierarchy_;
import ru.insoft.sovdoc.model.desc.view.VDescAllRelations;
import ru.insoft.sovdoc.model.desc.view.VDescAllRelations_;

import com.google.common.collect.Lists;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.list.desc.UnivClssDataList;
import ru.insoft.sovdoc.model.complex.table.ArchKinodoc;
import ru.insoft.sovdoc.model.complex.table.ArchKinodoc_;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc_;
import ru.insoft.sovdoc.model.complex.table.ArchVideodoc;
import ru.insoft.sovdoc.model.complex.table.ArchVideodoc_;
import ru.insoft.sovdoc.model.complex.table.UnivClassifierValue;
import ru.insoft.sovdoc.model.complex.table.UnivClassifierValue_;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit_;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnitLite;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnitLite_;
import ru.insoft.sovdoc.model.desc.view.VDescValueUnivclss;

public class ComplexSearchCriteria extends DescriptorBasedSearchCriteria
		implements ViewQuerySearchCriteria<VComplexDataUnitLite> {

	@Inject
	UnivClssDataList univClssDL;

	private Criteria criteria;

	public class Criteria {

		private Long descriptionLevelId;
		private String unitName;
		private Long unitTypeId;
		private Long archiveId;
		private Integer fundNum;
		private String fundLetter;
		private String fundPrefix;
		private Integer seriesNum;
		private String seriesLetter;
		private Integer unitNum;
		private String unitLetter;
		private Date beginDate;
		private Date endDate;
		private String enum1;
		private String univClssValueIds;
		private List<VDescValueUnivclss> univClssValues;
		private Long recordTypeId;
		private String annotation;
		private String eventPlace;
		private Long docTypeId;
		private String author;
		private Date recordDateFrom;
		private Date recordDateTo;

		private String vannotation;
		private String veventPlace;
		private Long vdocTypeId;
		private String vfilmStudio;
		private String vdirector;

		private String kannotation;
		private String keventPlace;
		private Long kdocTypeId;
		private String kfilmStudio;
		private String kdirector;

		public Long getDescriptionLevelId() {
			return descriptionLevelId;
		}

		public void setDescriptionLevelId(Long descriptionLevelId) {
			this.descriptionLevelId = descriptionLevelId;
		}

		public String getUnitName() {
			return unitName;
		}

		public void setUnitName(String unitName) {
			if (unitName == null || unitName.trim().equals("")) {
				this.unitName = null;
			} else {
				this.unitName = unitName;
			}
		}

		public Long getUnitTypeId() {
			return unitTypeId;
		}

		public void setUnitTypeId(Long unitTypeId) {
			this.unitTypeId = unitTypeId;
		}

		public Long getArchiveId() {
			return archiveId;
		}

		public void setArchiveId(Long archiveId) {
			this.archiveId = archiveId;
		}

		public Integer getFundNum() {
			return fundNum;
		}

		public void setFundNum(Integer fundNum) {
			this.fundNum = fundNum;
		}

		public String getFundLetter() {
			return fundLetter;
		}

		public void setFundLetter(String fundLetter) {
			this.fundLetter = fundLetter;
		}

		public String getFundPrefix() {
			return fundPrefix;
		}

		public void setFundPrefix(String fundPrefix) {
			this.fundPrefix = fundPrefix;
		}

		public Integer getSeriesNum() {
			return seriesNum;
		}

		public void setSeriesNum(Integer seriesNum) {
			this.seriesNum = seriesNum;
		}

		public String getSeriesLetter() {
			return seriesLetter;
		}

		public void setSeriesLetter(String seriesLetter) {
			this.seriesLetter = seriesLetter;
		}

		public Integer getUnitNum() {
			return unitNum;
		}

		public void setUnitNum(Integer unitNum) {
			this.unitNum = unitNum;
		}

		public String getUnitLetter() {
			return unitLetter;
		}

		public void setUnitLetter(String unitLetter) {
			this.unitLetter = unitLetter;
		}

		public Date getBeginDate() {
			return beginDate;
		}

		public void setBeginDate(Date beginDate) {
			this.beginDate = beginDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}

		public String getEnum1() {
			return enum1;
		}

		public void setEnum1(String enum1) {
			this.enum1 = enum1;
		}

		public String getUnivClssValueIds() {
			return univClssValueIds;
		}

		public void setUnivClssValueIds(String univClssValueIds) {
			this.univClssValueIds = univClssValueIds;
		}

		public Long getRecordTypeId() {
			return recordTypeId;
		}

		public void setRecordTypeId(Long recordTypeId) {
			this.recordTypeId = recordTypeId;
		}

		public String getAnnotation() {
			return annotation;
		}

		public void setAnnotation(String annotation) {
			this.annotation = annotation;
		}

		public String getEventPlace() {
			return eventPlace;
		}

		public void setEventPlace(String eventPlace) {
			this.eventPlace = eventPlace;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public Date getRecordDateFrom() {
			return recordDateFrom;
		}

		public void setRecordDateFrom(Date recordDateFrom) {
			this.recordDateFrom = recordDateFrom;
		}

		public Date getRecordDateTo() {
			return recordDateTo;
		}

		public void setRecordDateTo(Date recordDateTo) {
			this.recordDateTo = recordDateTo;
		}

		public Long getDocTypeId() {
			return docTypeId;
		}

		public void setDocTypeId(Long docTypeId) {
			this.docTypeId = docTypeId;
		}

		public String getVannotation() {
			return vannotation;
		}

		public void setVannotation(String vannotation) {
			this.vannotation = vannotation;
		}

		public String getVeventPlace() {
			return veventPlace;
		}

		public void setVeventPlace(String veventPlace) {
			this.veventPlace = veventPlace;
		}

		public Long getVdocTypeId() {
			return vdocTypeId;
		}

		public void setVdocTypeId(Long vdocTypeId) {
			this.vdocTypeId = vdocTypeId;
		}

		public String getVfilmStudio() {
			return vfilmStudio;
		}

		public void setVfilmStudio(String vfilmStudio) {
			this.vfilmStudio = vfilmStudio;
		}

		public String getVdirector() {
			return vdirector;
		}

		public void setVdirector(String vdirector) {
			this.vdirector = vdirector;
		}

		public String getKannotation() {
			return kannotation;
		}

		public void setKannotation(String kannotation) {
			this.kannotation = kannotation;
		}

		public String getKeventPlace() {
			return keventPlace;
		}

		public void setKeventPlace(String keventPlace) {
			this.keventPlace = keventPlace;
		}

		public Long getKdocTypeId() {
			return kdocTypeId;
		}

		public void setKdocTypeId(Long kdocTypeId) {
			this.kdocTypeId = kdocTypeId;
		}

		public String getKfilmStudio() {
			return kfilmStudio;
		}

		public void setKfilmStudio(String kfilmStudio) {
			this.kfilmStudio = kfilmStudio;
		}

		public String getKdirector() {
			return kdirector;
		}

		public void setKdirector(String kdirector) {
			this.kdirector = kdirector;
		}

		public List<VDescValueUnivclss> getUnivClssValues() {
			if (univClssValues == null) {
				univClssValues = queryUnivClssValues();
			}
			return univClssValues;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 59 * hash + Objects.hashCode(descriptionLevelId);
			hash = 59 * hash + Objects.hashCode(unitName);
			hash = 59 * hash + Objects.hashCode(unitTypeId);
			hash = 59 * hash + Objects.hashCode(archiveId);
			hash = 59 * hash + Objects.hashCode(fundNum);
			hash = 59 * hash + Objects.hashCode(fundLetter);
			hash = 59 * hash + Objects.hashCode(fundPrefix);
			hash = 59 * hash + Objects.hashCode(seriesNum);
			hash = 59 * hash + Objects.hashCode(seriesLetter);
			hash = 59 * hash + Objects.hashCode(unitNum);
			hash = 59 * hash + Objects.hashCode(unitLetter);
			hash = 59 * hash + Objects.hashCode(beginDate);
			hash = 59 * hash + Objects.hashCode(endDate);
			hash = 59 * hash + Objects.hashCode(enum1);
			hash = 59 * hash + Objects.hashCode(univClssValueIds);
			hash = 59 * hash + Objects.hashCode(univClssValues);
			hash = 59 * hash + Objects.hashCode(recordTypeId);
			hash = 59 * hash + Objects.hashCode(annotation);
			hash = 59 * hash + Objects.hashCode(eventPlace);
			hash = 59 * hash + Objects.hashCode(docTypeId);
			hash = 59 * hash + Objects.hashCode(author);
			hash = 59 * hash + Objects.hashCode(recordDateFrom);
			hash = 59 * hash + Objects.hashCode(recordDateTo);
			hash = 59 * hash + Objects.hashCode(vannotation);
			hash = 59 * hash + Objects.hashCode(veventPlace);
			hash = 59 * hash + Objects.hashCode(vdocTypeId);
			hash = 59 * hash + Objects.hashCode(vfilmStudio);
			hash = 59 * hash + Objects.hashCode(vdirector);
			hash = 59 * hash + Objects.hashCode(kannotation);
			hash = 59 * hash + Objects.hashCode(keventPlace);
			hash = 59 * hash + Objects.hashCode(kdocTypeId);
			hash = 59 * hash + Objects.hashCode(kfilmStudio);
			hash = 59 * hash + Objects.hashCode(kdirector);
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Criteria other = (Criteria) obj;
			if (!Objects.equals(unitName, other.unitName)) {
				return false;
			}
			if (!Objects.equals(fundLetter, other.fundLetter)) {
				return false;
			}
			if (!Objects.equals(fundPrefix, other.fundPrefix)) {
				return false;
			}
			if (!Objects.equals(seriesLetter, other.seriesLetter)) {
				return false;
			}
			if (!Objects.equals(unitLetter, other.unitLetter)) {
				return false;
			}
			if (!Objects.equals(enum1, other.enum1)) {
				return false;
			}
			if (!Objects.equals(univClssValueIds, other.univClssValueIds)) {
				return false;
			}
			if (!Objects.equals(annotation, other.annotation)) {
				return false;
			}
			if (!Objects.equals(eventPlace, other.eventPlace)) {
				return false;
			}
			if (!Objects.equals(docTypeId, other.docTypeId)) {
				return false;
			}
			if (!Objects.equals(author, other.author)) {
				return false;
			}
			if (!Objects.equals(vannotation, other.vannotation)) {
				return false;
			}
			if (!Objects.equals(veventPlace, other.veventPlace)) {
				return false;
			}
			if (!Objects.equals(vfilmStudio, other.vfilmStudio)) {
				return false;
			}
			if (!Objects.equals(vdirector, other.vdirector)) {
				return false;
			}
			if (!Objects.equals(kannotation, other.kannotation)) {
				return false;
			}
			if (!Objects.equals(keventPlace, other.keventPlace)) {
				return false;
			}
			if (!Objects.equals(kfilmStudio, other.kfilmStudio)) {
				return false;
			}
			if (!Objects.equals(kdirector, other.kdirector)) {
				return false;
			}
			if (!Objects.equals(descriptionLevelId, other.descriptionLevelId)) {
				return false;
			}
			if (!Objects.equals(unitTypeId, other.unitTypeId)) {
				return false;
			}
			if (!Objects.equals(archiveId, other.archiveId)) {
				return false;
			}
			if (!Objects.equals(fundNum, other.fundNum)) {
				return false;
			}
			if (!Objects.equals(seriesNum, other.seriesNum)) {
				return false;
			}
			if (!Objects.equals(unitNum, other.unitNum)) {
				return false;
			}
			if (!Objects.equals(beginDate, other.beginDate)) {
				return false;
			}
			if (!Objects.equals(endDate, other.endDate)) {
				return false;
			}
			if (!Objects.equals(univClssValues, other.univClssValues)) {
				return false;
			}
			if (!Objects.equals(recordTypeId, other.recordTypeId)) {
				return false;
			}
			if (!Objects.equals(recordDateFrom, other.recordDateFrom)) {
				return false;
			}
			if (!Objects.equals(recordDateTo, other.recordDateTo)) {
				return false;
			}
			if (!Objects.equals(vdocTypeId, other.vdocTypeId)) {
				return false;
			}
			if (!Objects.equals(kdocTypeId, other.kdocTypeId)) {
				return false;
			}
			return true;
		}
	}

	public boolean isEmpty() {
		return criteria.equals(new Criteria()) && (descIds == null || descIds.isEmpty());
	}

	@PostConstruct
	private void init() {
		criteria = new Criteria();
	}

	public Criteria getCriteria() {
		return criteria;
	}

	protected List<VDescValueUnivclss> queryUnivClssValues() {
		if (criteria.univClssValueIds == null) {
			Map<String, String> reqmap
					= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			criteria.univClssValueIds = reqmap.get("univClssValueIds");
		}
		if (criteria.univClssValueIds == null || criteria.univClssValueIds.isEmpty()) {
			return null;
		}
		List<String> idList = Lists.newArrayList(criteria.univClssValueIds.split(","));
		List<VDescValueUnivclss> res = Lists.newArrayListWithExpectedSize(idList.size());
		for (String id : idList) {
			res.add(se.getUnivClssValue(Long.valueOf(id)));
		}
		return res;
	}

	public void addUnivClssValue() {
		if (univClssDL.getSelectedValue() == null) {
			MessageUtils.ErrorMessage("Значение должно быть выбрано");
			return;
		}
		Map<String, String> reqmap
				= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		criteria.univClssValueIds = reqmap.get("univClssValueIds");
		if (criteria.univClssValueIds != null
				&& Lists.newArrayList(criteria.univClssValueIds.split(",")).contains(univClssDL.getSelectedValue().toString())) {
			MessageUtils.ErrorMessage("Это значение уже добавлено");
			return;
		}
		if (criteria.univClssValueIds == null || criteria.univClssValueIds.isEmpty()) {
			criteria.univClssValueIds = univClssDL.getSelectedValue().toString();
		} else {
			criteria.univClssValueIds += "," + univClssDL.getSelectedValue().toString();
		}
		univClssDL.setValueSelected(true);
	}

	public void removeUnivClssValue(VDescValueUnivclss value) {
		List<String> idList = Lists.newArrayList(criteria.univClssValueIds.split(","));
		idList.remove(value.getDescriptorValueId().toString());
		criteria.univClssValues.remove(value);
		criteria.univClssValueIds = StringUtils.uniteString(idList, ",");
	}

	@Override
	public boolean validateCriteria() {
		if (isEmpty()) {
			MessageUtils.ErrorMessage("Поиск без параметров невозможен");
			return false;
		}
		if (ValidationUtils.validateInterval("Даты документов", criteria.getBeginDate(), criteria.getEndDate())
				&& ValidationUtils.validateInterval("Дата записи", criteria.getRecordDateFrom(), criteria.getRecordDateTo())) {
			return true;
		}

		return false;
	}

	@Override
	public <Q> Predicate getPredicateByCriteria(CriteriaBuilder builder,
			CriteriaQuery<Q> query, Root<VComplexDataUnitLite> root) {
		List<Predicate> predicates = Lists.newArrayList();
		if (criteria.getEnum1() != null) {
			String descValueCode = null;
			String descValueCode1 = null;
			if (criteria.getEnum1().equals("SERIES")) {
				descValueCode = "SERIES";
				predicates.add(builder.equal(root.get(VComplexDataUnitLite_.descriptionLevelCode), descValueCode));
			}
			if (criteria.getEnum1().equals("STORAGE_UNIT") || criteria.getEnum1().equals("DOCUMENT")) {
				descValueCode = "STORAGE_UNIT";
				descValueCode1 = "DOCUMENT";
				predicates.add(builder.or(
						builder.equal(root.get(VComplexDataUnitLite_.descriptionLevelCode), descValueCode),
						builder.equal(root.get(VComplexDataUnitLite_.descriptionLevelCode), descValueCode1))
				);
			}
		}

		if (criteria.getDescriptionLevelId()
				!= null) {
			predicates.add(builder.equal(root.get(VComplexDataUnitLite_.descriptionLevelId), criteria.getDescriptionLevelId()));

		}

		if (criteria.getUnitName() != null) {
			predicates.add(builder.greaterThan(builder.function("contains", Integer.class,
					root.get(VComplexDataUnitLite_.unitName), builder.literal(StringUtils.textRequest(criteria.getUnitName()))), 0));
		}

		if (criteria.getUnitTypeId() != null) {
			predicates.add(builder.equal(root.get(VComplexDataUnitLite_.unitTypeId), criteria.getUnitTypeId()));
		}

		if (criteria.getArchiveId() != null) {
			Subquery<Long> sub = query.subquery(Long.class);
			Root<VComplexArchiveHierarchy> subroot = sub.from(VComplexArchiveHierarchy.class);
			sub.select(subroot.get(VComplexArchiveHierarchy_.childValueId));
			sub.where(builder.equal(subroot.get(VComplexArchiveHierarchy_.parentValueId), criteria.getArchiveId()));
			predicates.add(builder.in(root.get(VComplexDataUnitLite_.archiveId)).value(sub));
		}

		if (criteria.fundPrefix != null) {
			predicates.add(builder.equal(
					builder.function("COMPLEX_PACK.EXTRACT_ARCH_CODE_PART",
							String.class, builder.upper(root.get(VComplexDataUnitLite_.archNumberCode)), builder.literal(0)),
					criteria.fundPrefix.toUpperCase()));
		}
		if (criteria.fundLetter != null) {
			predicates.add(builder.equal(
					builder.function("COMPLEX_PACK.EXTRACT_ARCH_CODE_PART",
							String.class, builder.upper(root.get(VComplexDataUnitLite_.archNumberCode)), builder.literal(2)),
					criteria.fundLetter.toUpperCase()));
		}
		if (criteria.fundNum != null) {
			predicates.add(builder.equal(
					builder.function("COMPLEX_PACK.EXTRACT_ARCH_CODE_PART",
							Integer.class, root.get(VComplexDataUnitLite_.archNumberCode), builder.literal(1)),
					criteria.fundNum));
		}
		if (criteria.seriesNum != null) {
			predicates.add(builder.equal(
					builder.function("COMPLEX_PACK.EXTRACT_ARCH_CODE_PART",
							Integer.class, root.get(VComplexDataUnitLite_.archNumberCode), builder.literal(3)),
					criteria.seriesNum));
		}
		if (criteria.seriesLetter != null) {
			predicates.add(builder.equal(
					builder.function("COMPLEX_PACK.EXTRACT_ARCH_CODE_PART",
							String.class, builder.upper(root.get(VComplexDataUnitLite_.archNumberCode)), builder.literal(4)),
					criteria.seriesLetter.toUpperCase()));
		}
		if (criteria.unitNum != null) {
			predicates.add(builder.equal(
					builder.function("COMPLEX_PACK.EXTRACT_ARCH_CODE_PART",
							Integer.class, root.get(VComplexDataUnitLite_.archNumberCode), builder.literal(5)),
					criteria.unitNum));
		}
		if (criteria.unitLetter != null) {
			predicates.add(builder.equal(
					builder.function("COMPLEX_PACK.EXTRACT_ARCH_CODE_PART",
							String.class, builder.upper(root.get(VComplexDataUnitLite_.archNumberCode)), builder.literal(6)),
					criteria.unitLetter.toUpperCase()));
		}

		if (criteria.getBeginDate()
				!= null) {
			Subquery<Long> sub = query.subquery(Long.class);
			Root<UnivDateInterval> subroot = sub.from(UnivDateInterval.class);
			sub.select(subroot.get(UnivDateInterval_.univDataUnitId));

			if (criteria.getEndDate() == null) {
				sub.where(builder.and(
						builder.lessThanOrEqualTo(subroot.get(UnivDateInterval_.beginDate), criteria.getBeginDate()),
						builder.greaterThanOrEqualTo(subroot.get(UnivDateInterval_.endDate), criteria.getBeginDate())));
			} else {
				sub.where(builder.or(
						builder.and(
								builder.lessThanOrEqualTo(subroot.get(UnivDateInterval_.beginDate), criteria.getBeginDate()),
								builder.greaterThanOrEqualTo(subroot.get(UnivDateInterval_.endDate), criteria.getBeginDate())),
						builder.and(
								builder.lessThanOrEqualTo(subroot.get(UnivDateInterval_.beginDate), criteria.getEndDate()),
								builder.greaterThanOrEqualTo(subroot.get(UnivDateInterval_.endDate), criteria.getEndDate())),
						builder.and(
								builder.lessThanOrEqualTo(subroot.get(UnivDateInterval_.endDate), criteria.getEndDate()),
								builder.greaterThanOrEqualTo(subroot.get(UnivDateInterval_.beginDate), criteria.getBeginDate()))));
			}
			predicates.add(builder.in(root.get(VComplexDataUnitLite_.univDataUnitId)).value(sub));
		}
		if (descIds
				!= null && !descIds.isEmpty()) {
			for (String descValueId : descIds.split(",")) {
				Subquery<Long> sub = query.subquery(Long.class);
				Root<UnivDescriptor> subroot = sub.from(UnivDescriptor.class);
				sub.select(subroot.get(UnivDescriptor_.univDataUnitId));
				Join<UnivDescriptor, VDescAllRelations> join = subroot.join(UnivDescriptor_.allRelations);

				sub.where(builder.equal(join.get(VDescAllRelations_.relatedValueId), Long.valueOf(descValueId)));
				predicates.add(builder.in(root.get(VComplexDataUnitLite_.univDataUnitId)).value(sub));
			}
		}

		if (criteria.getUnivClssValueIds() != null && !criteria.getUnivClssValueIds().isEmpty()) {
			for (String univClssValueId : criteria.getUnivClssValueIds().split(",")) {
				Subquery<Long> sub = query.subquery(Long.class);
				Root<UnivClassifierValue> subroot1 = sub.from(UnivClassifierValue.class);
				Root<VDescAllRelations> subroot2 = sub.from(VDescAllRelations.class);
				sub.select(subroot1.get(UnivClassifierValue_.dataUnit).get(UnivDataUnit_.univDataUnitId));
				sub.where(builder.and(
						builder.equal(subroot2.get(VDescAllRelations_.relatedValueId), Long.valueOf(univClssValueId)),
						builder.equal(subroot1.get(UnivClassifierValue_.descriptorValueId),
								subroot2.get(VDescAllRelations_.descriptorValueId))));
				predicates.add(builder.in(root.get(VComplexDataUnitLite_.univDataUnitId)).value(sub));
			}
		}

		if (criteria.unitTypeId == 420465) { // Фонодокумент
			addPhonoPredicates(builder, root, query, predicates);
		} else if (criteria.unitTypeId == 420607 || criteria.unitTypeId == 420548) { // Видеодокумент
			addVideoPredicates(builder, root, query, predicates);
		} else if (criteria.unitTypeId == 420606 || criteria.unitTypeId == 420547) { // Кинодокумент
			addKinoPredicates(builder, root, query, predicates);
		}

		return builder.and(predicates.toArray(new Predicate[0]));
	}

	private <Q> void addPhonoPredicates(CriteriaBuilder builder, Root<VComplexDataUnitLite> root,
			CriteriaQuery<Q> query, List<Predicate> predicates) {
		if (criteria.annotation != null && !criteria.annotation.isEmpty()) {
			predicates.add(builder.greaterThan(builder.function("contains", Integer.class,
					root.get(VComplexDataUnitLite_.annotation),
					builder.literal(StringUtils.textRequest(criteria.annotation))), 0));
		}
		if (criteria.docTypeId != null || criteria.recordTypeId != null
				|| (criteria.eventPlace != null && !criteria.eventPlace.isEmpty())
				|| (criteria.author != null && !criteria.author.isEmpty())
				|| criteria.recordDateFrom != null || criteria.recordDateTo != null) {

			Subquery<Long> sub = query.subquery(Long.class);
			Root<ArchPhonodoc> subroot = sub.from(ArchPhonodoc.class);
			List<Predicate> subPredicates = Lists.newArrayList();

			if (criteria.docTypeId != null) {
				subPredicates.add(builder.equal(subroot.get(ArchPhonodoc_.phonodocTypeId), criteria.docTypeId));
			}
			if (criteria.recordTypeId != null) {
				subPredicates.add(builder.equal(subroot.get(ArchPhonodoc_.recordTypeId), criteria.recordTypeId));
			}
			if (criteria.eventPlace != null && !criteria.eventPlace.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchPhonodoc_.eventPlace),
						builder.literal(StringUtils.textRequest(criteria.eventPlace))), 0));
			}
			if (criteria.author != null && !criteria.author.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchPhonodoc_.authors),
						builder.literal(StringUtils.textRequest(criteria.author))), 0));
			}
			if (criteria.recordDateFrom != null) {
				if (criteria.recordDateTo == null) {
					subPredicates.add(builder.equal(
							subroot.get(ArchPhonodoc_.recordDate), criteria.recordDateFrom));
				} else {
					subPredicates.add(builder.and(
							builder.lessThanOrEqualTo(subroot.get(ArchPhonodoc_.recordDate), criteria.recordDateTo),
							builder.greaterThanOrEqualTo(subroot.get(ArchPhonodoc_.recordDate), criteria.recordDateFrom)));
				}
			}

			sub.select(subroot.get(ArchPhonodoc_.univDataUnitId));
			sub.where(builder.and(subPredicates.toArray(new Predicate[0])));
			predicates.add(builder.in(root.get(VComplexDataUnitLite_.univDataUnitId)).value(sub));
		}

	}

	private <Q> void addVideoPredicates(CriteriaBuilder builder, Root<VComplexDataUnitLite> root,
			CriteriaQuery<Q> query, List<Predicate> predicates) {
		if (criteria.vannotation != null && !criteria.vannotation.isEmpty()) {
			predicates.add(builder.greaterThan(builder.function("contains", Integer.class,
					root.get(VComplexDataUnitLite_.annotation),
					builder.literal(StringUtils.textRequest(criteria.vannotation))), 0));
		}

		if (criteria.vdocTypeId != null || (criteria.vdirector != null && !criteria.vdirector.isEmpty())
				|| (criteria.veventPlace != null && !criteria.veventPlace.isEmpty())
				|| (criteria.vfilmStudio != null && !criteria.vfilmStudio.isEmpty())) {

			Subquery<Long> sub = query.subquery(Long.class);
			Root<ArchVideodoc> subroot = sub.from(ArchVideodoc.class);
			List<Predicate> subPredicates = Lists.newArrayList();

			if (criteria.vdocTypeId != null) {
				subPredicates.add(builder.equal(subroot.get(ArchVideodoc_.videodocTypeId), criteria.vdocTypeId));
			}
			if (criteria.veventPlace != null && !criteria.veventPlace.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchVideodoc_.eventPlace),
						builder.literal(StringUtils.textRequest(criteria.veventPlace))), 0));
			}
			if (criteria.vdirector != null && !criteria.vdirector.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchVideodoc_.director),
						builder.literal(StringUtils.textRequest(criteria.vdirector))), 0));
			}
			if (criteria.vfilmStudio != null && !criteria.vfilmStudio.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchVideodoc_.filmStudio),
						builder.literal(StringUtils.textRequest(criteria.vfilmStudio))), 0));
			}

			sub.select(subroot.get(ArchVideodoc_.univDataUnitId));
			sub.where(builder.and(subPredicates.toArray(new Predicate[0])));
			predicates.add(builder.in(root.get(VComplexDataUnitLite_.univDataUnitId)).value(sub));
		}
	}

	private <Q> void addKinoPredicates(CriteriaBuilder builder, Root<VComplexDataUnitLite> root,
			CriteriaQuery<Q> query, List<Predicate> predicates) {
		if (criteria.kannotation != null && !criteria.kannotation.isEmpty()) {
			predicates.add(builder.greaterThan(builder.function("contains", Integer.class,
					root.get(VComplexDataUnitLite_.annotation),
					builder.literal(StringUtils.textRequest(criteria.kannotation))), 0));
		}

		if (criteria.kdocTypeId != null || (criteria.kdirector != null && !criteria.kdirector.isEmpty())
				|| (criteria.keventPlace != null && !criteria.keventPlace.isEmpty())
				|| (criteria.kfilmStudio != null && !criteria.kfilmStudio.isEmpty())) {

			Subquery<Long> sub = query.subquery(Long.class);
			Root<ArchKinodoc> subroot = sub.from(ArchKinodoc.class);
			List<Predicate> subPredicates = Lists.newArrayList();

			if (criteria.kdocTypeId != null) {
				subPredicates.add(builder.equal(subroot.get(ArchKinodoc_.kinodocTypeId), criteria.kdocTypeId));
			}
			if (criteria.keventPlace != null && !criteria.keventPlace.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchKinodoc_.eventPlace),
						builder.literal(StringUtils.textRequest(criteria.keventPlace))), 0));
			}
			if (criteria.kdirector != null && !criteria.kdirector.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchKinodoc_.director),
						builder.literal(StringUtils.textRequest(criteria.kdirector))), 0));
			}
			if (criteria.kfilmStudio != null && !criteria.kfilmStudio.isEmpty()) {
				subPredicates.add(builder.greaterThan(builder.function("contains", Integer.class,
						subroot.get(ArchKinodoc_.filmStudio),
						builder.literal(StringUtils.textRequest(criteria.kfilmStudio))), 0));
			}

			sub.select(subroot.get(ArchKinodoc_.univDataUnitId));
			sub.where(builder.and(subPredicates.toArray(new Predicate[0])));
			predicates.add(builder.in(root.get(VComplexDataUnitLite_.univDataUnitId)).value(sub));
		}
	}
}
