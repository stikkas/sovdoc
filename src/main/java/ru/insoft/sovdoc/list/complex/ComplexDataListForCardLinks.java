
package ru.insoft.sovdoc.list.complex;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnitLite;

@RequestScoped
@Named("complexDLForCardLinks")
public class ComplexDataListForCardLinks extends ComplexDataList{
  	@Inject
	ComplexSearchForCardLinks searchCriteria;
    
    	@Override
	public ViewQuerySearchCriteria<VComplexDataUnitLite> getSearchCriteria() 
	{
		return searchCriteria;
	}
}
