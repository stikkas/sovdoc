package ru.insoft.sovdoc.list.complex;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnitLite;

@RequestScoped
@Named("complexDL")
public class ComplexDataList1 extends ComplexDataList implements Serializable {

	@Inject
	ComplexSearch1 searchCriteria;
	private String unitTypeCode;

	public String getUnitTypeCode() {
		return unitTypeCode;
	}

	public void setUnitTypeCode(String unitTypeCode) {
		this.unitTypeCode = unitTypeCode;
	}

	public void changeUnitType() {
		Long unitTypeId = searchCriteria.getCriteria().getUnitTypeId();
		if (unitTypeId != null) {
			if (unitTypeId == 420465) {
				unitTypeCode = "PHONODOC";
			} else if (unitTypeId == 420607 || unitTypeId == 420548) {
				unitTypeCode = "VIDEODOC";
			} else if (unitTypeId == 420606 || unitTypeId == 420547) {
				unitTypeCode = "KINODOC";
			} else {
				unitTypeCode = null;
			}
		} else {
			unitTypeCode = null;
		}
	}

	@Override
	public ViewQuerySearchCriteria<VComplexDataUnitLite> getSearchCriteria() {
		return searchCriteria;
	}
}
