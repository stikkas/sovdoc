package ru.insoft.sovdoc.list.complex;

import java.io.File;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.solder.servlet.http.RequestParam;
import org.richfaces.model.DeclarativeModelKey;
import org.richfaces.model.SequenceRowKey;

import ru.insoft.commons.jsf.ui.CommonTree;
import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.panel.PanelMenuItem;
import ru.insoft.commons.utils.ExceptionUtils;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexArchive;
import ru.insoft.sovdoc.model.complex.view.VComplexArchive_;
import ru.insoft.sovdoc.model.complex.view.VComplexDataUnit;
import ru.insoft.sovdoc.model.complex.view.VComplexUnitHierarchy;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.service.correct.JournalHelper;
import ru.insoft.sovdoc.system.SystemEntity;
import ru.insoft.sovdoc.system.complex.SystemQuery;
import ru.insoft.sovdoc.ui.complex.AddComplex;
import ru.insoft.sovdoc.ui.complex.ComplexTabMenu;

import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Query;
import ru.insoft.sovdoc.model.complex.table.ApplicationToDocument;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit_;
import ru.insoft.sovdoc.model.complex.view.VComplexCardLinks;

import ru.insoft.sovdoc.security.AccessService;
import ru.insoft.sovdoc.security.SecurityServiceImpl;

@ViewScoped
@ManagedBean
public class ComplexTree extends CommonTree<Map<String, ComplexTreeItem>> implements Serializable {

  @Inject
  EntityManager em;
  @Inject
  AddComplex addComplex;
  @Inject
  SystemQuery complexQuery;
  @Inject
  SystemEntity se;
  @Inject
  JournalHelper journalHelper;
  @Inject
  ComplexTabMenu menu;

  @Inject
  AccessService access;

  @Inject
  @RequestParam(value = "unitId")
  Long unitId;

  @Inject
  @RequestParam(value = "owner")
  Long owner;

  @Inject
  @RequestParam(value = "argument")
  String argument;

  @Inject
  @RequestParam(value = "childrenFetched")
  Integer childrenFetched;

  @Inject
  @RequestParam(value = "fetchLinkId")
  String fetchLinkId;

  @Inject
  SecurityServiceImpl secServiceImpl;

  private List<Long> expArchives;
  private List<Long> expUnits;

  @PostConstruct
  protected void init() {
    if (childrenFetched == null) {
      childrenFetched = 0;
    }
  }

  @Override
  protected void initRoots() {
    roots = new LinkedHashMap<String, ComplexTreeItem>();
    List<VComplexArchive> archList = getArchives(null);

    setExpansionInternal();
    for (VComplexArchive arch : archList) {
      ComplexTreeItem node = new ComplexTreeItem(
              !arch.isHasChildren(), arch.getArchiveId(),
              arch.getArchiveShortName(), "archive");
      node.setData(arch);
      node.setTree(this);
      if (!node.isLeaf()) {
        if (expArchives != null && expArchives.contains(node.getId())) {
          node.setExpanded(true);
        }
        /*else
         node.setExpanded(isNodeExpanded("archive" + arch.getArchiveId().toString()));*/
      }
      roots.put("archive" + arch.getArchiveId().toString(), node);
    }
  }

  @Override
  public String[] getNodeTypes() {
    return new String[]{"archive", "unit"};
  }

  /*@Override
   public ComplexTreeItem getCurrentNode()
   {
   return (ComplexTreeItem)super.getCurrentNode();
   }*/
  @Override
  public void selectNode() {
    super.selectNode();
    if (getCurrentNode() != null && getCurrentNode().getType().equals("unit")) {
      for (PanelMenuItem pmi : menu.getItems()) {
        if (pmi.getName().equals("search")) {
          pmi.addParam("unitId", getCurrentNode().getId().toString());
          break;
        }
      }
    }
  }

  public List<Long> getExpArchives() {
    return expArchives;
  }

  public List<Long> getExpUnits() {
    return expUnits;
  }

  public Integer getChildrenFetched() {
    return childrenFetched;
  }

  public String getFetchLinkId() {
    return fetchLinkId;
  }

  public String getArgument() {
    if (argument == null) {
      Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext()
              .getRequestParameterMap();
      argument = reqMap.get("argument");
    }
    return argument;
  }

  protected void setExpansion() {
    if (unitId != null) {
      List<VComplexUnitHierarchy> hierarchy = complexQuery.queryHierarchy(unitId);
      expUnits = Lists.newArrayList();
      for (VComplexUnitHierarchy unit : hierarchy) {
        if (expArchives == null) {
          expArchives = complexQuery.queryArchiveHierarchy(unit.getResOwnerId());
        }
        if (!unit.getParentUnitId().equals(unitId)) {
          expUnits.add(unit.getParentUnitId());
        }
      }
    } else if (owner != null) {
      expArchives = complexQuery.queryArchiveHierarchy(owner);
      expArchives.remove(owner);
    }
  }

  protected void setExpansionInternal() {
    List<String> nodeIds = getExpandedNodeIds();
    if (nodeIds == null) {
      return;
    }

    expArchives = Lists.newArrayList();
    expUnits = Lists.newArrayList();
    for (String nodeId : nodeIds) {
      if (nodeId.startsWith("archive")) {
        Long id = Long.valueOf(nodeId.substring("archive".length()));
        expArchives.add(id);
      }
      if (nodeId.startsWith("unit")) {
        Long id = Long.valueOf(nodeId.substring("unit".length()));
        expUnits.add(id);
      }
    }
  }

  public void selectNodes() {
    if (unitId != null || owner != null) {
      roots = null;
      Collection<Object> coll = Lists.newArrayList();
      setExpansion();
      for (Long archId : expArchives) {
        coll.add(new DeclarativeModelKey(getAdaptorId(),
                "archive" + archId.toString()));
      }
      if (unitId != null) {
        for (Long unitId : expUnits) {
          coll.add(new DeclarativeModelKey(getAdaptorId(),
                  "unit" + unitId.toString()));
        }
        coll.add(new DeclarativeModelKey(getAdaptorId(), "unit" + unitId.toString()));
      } else {
        coll.add(new DeclarativeModelKey(getAdaptorId(), "archive" + owner.toString()));
      }

      SequenceRowKey srk = new SequenceRowKey(coll.toArray());
      coll = Lists.newArrayList((Object) srk);
      setSelectionKeys(coll);
      super.resetCurrentNode();
    }
  }

  @Override
  protected String getAdaptorId() {
    return "complex";
  }

  public List<VComplexArchive> getArchives(Long parentId) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<VComplexArchive> cq = cb.createQuery(VComplexArchive.class);
    Root<VComplexArchive> root = cq.from(VComplexArchive.class);
    cq.select(root).orderBy(cb.asc(root.get(VComplexArchive_.sortOrder)));
    if (parentId == null) {
      cq.where(cb.isNull(root.get(VComplexArchive_.parentValueId)));
    } else {
      cq.where(cb.equal(root.get(VComplexArchive_.parentValueId), parentId));
    }
    return em.createQuery(cq).getResultList();
  }

  public String redirectViewCard() {
    if (getCurrentNode() != null) {
      unitId = getCurrentNode().getId();
    }

    if (unitId != null) {
      VComplexDataUnit complex = complexQuery.queryImmModel(unitId);
      return addComplex.getPageName(complex.getUnitTypeCode())
              + "?faces-redirect=true&amp;unitId=" + unitId;
    }

    return null;
  }

  public boolean checkAccessRule() {
    Long archiveCardId = null;
    Long archiveUserId = null;

    if (access.hasAccess("COMPLEX_EDIT")) {
      return true;
    }
    if (access.hasAccess("COMPLEX_ARCHIVE_EDIT")) {

      if (getCurrentNode() != null) {
        unitId = getCurrentNode().getId();
      }
      Long result1 = null;
      if (unitId != null) {
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<VComplexDataUnit> cq = cb.createQuery(VComplexDataUnit.class);
//        Root<VComplexDataUnit> root = cq.from(VComplexDataUnit.class);
//        cq.(root).where(cb.equal(root.get(VComplexDataUnit_.), unitId));
//        String valueCode = em.createQuery(cq).getSingleResult().getValueCode();
//        if (valueCode == "ARCHIVE") {
//          archiveCardId = unitId;
//        } else {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<UnivDataUnit> root = cq.from(UnivDataUnit.class);
        cq.select(cb.count(root)).where(cb.equal(root.get(UnivDataUnit_.univDataUnitId), unitId));
        result1 = em.createQuery(cq).getSingleResult();
      }

      if (result1 != 0) {

        archiveCardId = complexQuery.queryModel(unitId).getResOwnerId();
      } else {
        archiveCardId = unitId;
      }

//        }
    }
    Long userId = secServiceImpl.getCurrentUser().getUserId();

    Query q = em.createNativeQuery("SELECT adm_pack.get_archive_for_user(:u_user_id) FROM DUAL");
    q.setParameter("u_user_id", userId);

    BigDecimal result = (BigDecimal) q.getSingleResult();
    archiveUserId = result.longValue();

    if (archiveUserId.equals(archiveCardId)) {
      return true;
    } else {
      return false;
    }
  }

  public String[] confirmRemove() 
  {
      argument = null;
    VComplexDataUnit dataUnit = complexQuery.queryImmModel(Long.valueOf(getArgument()));
    String msg = "Вы действительно хотите удалить \"" + dataUnit.getUnitName() + "\"?";
    if (dataUnit.getImageCount() > 0) {
      String msg1 = "К элементу прикреплено " + dataUnit.getImageCount() + " файлов.";
      return new String[]{msg1, msg};
    } else {
      return new String[]{msg};
    }
  }

  public void removeDataUnit() {
    unitId = Long.valueOf(getArgument());
    UnivDataUnit unit = complexQuery.queryModel(unitId);
    List<VComplexCardLinks> cardLinks = complexQuery.queryCardLinks(unit.getUnivDataUnitId());
    if (cardLinks == null) {
      cardLinks = new ArrayList<VComplexCardLinks>();
    }
    List<ApplicationToDocument> applToDocList = complexQuery.queryApplicationToDocumenttModel(unit.getUnivDataUnitId());
    if (applToDocList == null) {
      applToDocList = new ArrayList<ApplicationToDocument>();
    }
    try {
      List<ImagePath> allFiles = complexQuery.queryImagePathList(unitId);

      journalHelper.recordFullUnivDataUnit(unit, "DELETE", cardLinks, applToDocList);
      em.remove(unit);
      em.flush();

      for (ImagePath ip : allFiles) {
        File dir = new File(ip.getFilePath());
        File file = new File(dir, ip.getFileName());
        file.delete();
        if (dir.isDirectory() && dir.list().length == 0) {
          dir.delete();
        }
      }

      unitId = unit.getParentUnitId();
      owner = unit.getResOwnerId();
      initRoots();
    } catch (Exception e) {
      String msg = ExceptionUtils.getConstraintViolationMessage(e);
      if (msg == null) {
        e.printStackTrace();
        return;
      }
      if (msg.indexOf("FK_UNIDATUNT_UNIDATUNT") >= 0) {
        MessageUtils.ErrorMessage("Невозможно удалить ресурс, так как имеются вложенные данные");
      } else {
        MessageUtils.ErrorMessage("Невозможно удалить ресурс: обнаружены внешние ссылки");
      }
    }
    selectNodes();
  }

  public void clearChildrenForNewFetch() {
    if (roots != null) {
      Map<String, String> reqMap = FacesContext.getCurrentInstance().getExternalContext()
              .getRequestParameterMap();
      childrenFetched = Integer.valueOf(reqMap.get("childrenFetched"));
      fetchLinkId = reqMap.get("fetchLinkId");

      Map<String, ComplexTreeItem> items = roots;
      ComplexTreeItem item = null;
      for (String nodeId : parseNodeId(fetchLinkId)) {
        ComplexTreeItem currentItem = items.get(nodeId);
        if (currentItem != null && currentItem.getChildren() != null) {
          item = currentItem;
          items = item.getChildren();
        }
      }
      item.getChildren().remove("fake" + childrenFetched.toString());
      item.setDirtyChildren(true);
    }
  }

}
