package ru.insoft.sovdoc.list.complex;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;

public class ComplexSortingCriteria extends CommonSortingCriteria {

	@Override
	public <T> List<Order> getSortOrder(CriteriaBuilder builder, Root<T> root)
	{
		List<Order> ord = super.getSortOrder(builder, root);
		if (ord.size() == 0)
		{
			setSorting("archiveId", "ascending", true);
			setSorting("archNumberSort", "ascending", false);
			ord = super.getSortOrder(builder, root);
		}
		return ord;
	}

	@Override
	protected int getMaxSortingFields() 
	{
		return 2;
	}
}
