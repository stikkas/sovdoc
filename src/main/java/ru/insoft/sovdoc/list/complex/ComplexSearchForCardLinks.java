package ru.insoft.sovdoc.list.complex;

import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue_;

@RequestScoped
@Named("complexSearchForCardLinks")
public class ComplexSearchForCardLinks extends ComplexSearchCriteria {

  @Inject
  protected EntityManager em;

  @PostConstruct
  private void enumSet() {
    Map<String, String> reqmap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    Long descLevelForCarldLink = null;
    if (reqmap.containsKey("cardLinkDescLevel")) {
      descLevelForCarldLink = Long.valueOf(reqmap.get("cardLinkDescLevel"));
    }
    if (descLevelForCarldLink == null || descLevelForCarldLink.equals(0L)) {
      return;
    }

    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<DescriptorValue> cq = cb.createQuery(DescriptorValue.class);
    Root<DescriptorValue> root1 = cq.from(DescriptorValue.class);

    cq.select(root1)
            .where(cb.equal(root1.get(DescriptorValue_.descriptorValueId), descLevelForCarldLink));
    String descriptionValueCode = em.createQuery(cq).getSingleResult().getValueCode();
    getCriteria().setEnum1(descriptionValueCode);
  }

}
