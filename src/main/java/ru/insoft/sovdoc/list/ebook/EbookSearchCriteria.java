package ru.insoft.sovdoc.list.ebook;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.commons.utils.StringUtils;
import ru.insoft.sovdoc.list.DescriptorBasedSearchCriteria;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor;
import ru.insoft.sovdoc.model.complex.table.UnivDescriptor_;
import ru.insoft.sovdoc.model.desc.view.VDescAllRelations;
import ru.insoft.sovdoc.model.desc.view.VDescAllRelations_;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookAuthor;
import ru.insoft.sovdoc.model.ebook.table.ArchEbookAuthor_;
import ru.insoft.sovdoc.model.ebook.view.VEbook;
import ru.insoft.sovdoc.model.ebook.view.VEbook_;

import com.google.common.collect.Lists;

@RequestScoped
@Named("ebookSearch")
public class EbookSearchCriteria extends DescriptorBasedSearchCriteria 
	implements ViewQuerySearchCriteria<VEbook> {

	private Criteria criteria;
	
	public class Criteria
	{
		private Long authorId;
		private String bookTitle;
		private String isbn;
		private Long editionTypeId;
		private Long seriesId;
		private String publisher;
		private Integer publishingYear;
		
		public Long getAuthorId() {
			return authorId;
		}
		
		public void setAuthorId(Long authorId) {
			this.authorId = authorId;
		}
		
		public String getBookTitle() {
			return bookTitle;
		}
		
		public void setBookTitle(String bookTitle) {
          if (bookTitle == null || bookTitle.trim().equals("")) {
           this.bookTitle = null;
          } else {
           this.bookTitle = bookTitle;
          }
		}
		
		public String getIsbn() {
			return isbn;
		}

		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}

		public Long getEditionTypeId() {
			return editionTypeId;
		}

		public void setEditionTypeId(Long editionTypeId) {
			this.editionTypeId = editionTypeId;
		}

		public Long getSeriesId() {
			return seriesId;
		}

		public void setSeriesId(Long seriesId) {
			this.seriesId = seriesId;
		}

		public String getPublisher() {
			return publisher;
		}

		public void setPublisher(String publisher) {
          if (publisher == null || publisher.trim().equals("")) {
            this.publisher = null;
          } else {
            this.publisher = publisher;
          }
		}

		public Integer getPublishingYear() {
			return publishingYear;
		}
		
		public void setPublishingYear(Integer publishingYear) {
			this.publishingYear = publishingYear;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result
					+ ((authorId == null) ? 0 : authorId.hashCode());
			result = prime * result
					+ ((bookTitle == null) ? 0 : bookTitle.hashCode());
			result = prime * result
					+ ((editionTypeId == null) ? 0 : editionTypeId.hashCode());
			result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
			result = prime * result
					+ ((publisher == null) ? 0 : publisher.hashCode());
			result = prime
					* result
					+ ((publishingYear == null) ? 0 : publishingYear.hashCode());
			result = prime * result
					+ ((seriesId == null) ? 0 : seriesId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Criteria other = (Criteria) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (authorId == null) {
				if (other.authorId != null)
					return false;
			} else if (!authorId.equals(other.authorId))
				return false;
			if (bookTitle == null) {
				if (other.bookTitle != null)
					return false;
			} else if (!bookTitle.equals(other.bookTitle))
				return false;
			if (editionTypeId == null) {
				if (other.editionTypeId != null)
					return false;
			} else if (!editionTypeId.equals(other.editionTypeId))
				return false;
			if (isbn == null) {
				if (other.isbn != null)
					return false;
			} else if (!isbn.equals(other.isbn))
				return false;
			if (publisher == null) {
				if (other.publisher != null)
					return false;
			} else if (!publisher.equals(other.publisher))
				return false;
			if (publishingYear == null) {
				if (other.publishingYear != null)
					return false;
			} else if (!publishingYear.equals(other.publishingYear))
				return false;
			if (seriesId == null) {
				if (other.seriesId != null)
					return false;
			} else if (!seriesId.equals(other.seriesId))
				return false;
			return true;
		}

		private EbookSearchCriteria getOuterType() {
			return EbookSearchCriteria.this;
		}
	}
	
	public boolean isEmpty()
	{
		return criteria.equals(new Criteria()) && (descIds == null || descIds.isEmpty());
	}
	
	@PostConstruct
	private void init()
	{
		criteria = new Criteria();
	}
	
	public Criteria getCriteria()
	{
		return criteria;
	}
	
	public void selectAuthor()
	{
		criteria.setAuthorId(valueSelectDL.getSelectedValue());
		valueSelectDL.setValueSelected(true);
	}
	
	@Override
	public boolean validateCriteria() 
	{
		/*if (isEmpty())
		{
			MessageUtils.ErrorMessage("Поиск без параметров невозможен");
			return false;
		}*/
		return true;
	}
	
	@Override
	public <Q> Predicate getPredicateByCriteria(CriteriaBuilder builder,
			CriteriaQuery<Q> query, Root<VEbook> root) 
	{
		List<Predicate> predicates = Lists.newArrayList();
		
		if (criteria.getAuthorId() != null)
		{
			Subquery<Long> sub = query.subquery(Long.class);
			Root<ArchEbookAuthor> subroot = sub.from(ArchEbookAuthor.class);
			sub.select(subroot.get(ArchEbookAuthor_.univDataUnitId));
			sub.where(builder.equal(subroot.get(ArchEbookAuthor_.authorId), criteria.getAuthorId()));
			predicates.add(builder.in(root.get(VEbook_.univDataUnitId)).value(sub));
		}		
		if (criteria.getBookTitle() != null)
			predicates.add(builder.greaterThan(builder.function("contains", Integer.class, 
					root.get(VEbook_.bookTitle), builder.literal(StringUtils.textRequest(criteria.getBookTitle()))), 0));
		if (criteria.getIsbn() != null)
			predicates.add(builder.equal(root.get(VEbook_.isbn), criteria.getIsbn()));
		if (criteria.getEditionTypeId() != null)
			predicates.add(builder.equal(root.get(VEbook_.editionTypeId), criteria.getEditionTypeId()));
		if (criteria.getSeriesId() != null)
			predicates.add(builder.equal(root.get(VEbook_.seriesId), criteria.getSeriesId()));
		if (criteria.getPublisher() != null)
			predicates.add(builder.greaterThan(builder.function("contains", Integer.class, 
					root.get(VEbook_.publisher), builder.literal(StringUtils.textRequest(criteria.getPublisher()))), 0));
		if (criteria.getPublishingYear() != null)
			predicates.add(builder.equal(root.get(VEbook_.publishingYear), criteria.getPublishingYear()));
		
		if (descIds != null && !descIds.isEmpty())
			for (String descValueId : descIds.split(","))
			{
				Subquery<Long> sub = query.subquery(Long.class);
				Root<UnivDescriptor> subroot = sub.from(UnivDescriptor.class);
				sub.select(subroot.get(UnivDescriptor_.univDataUnitId));
				Join<UnivDescriptor, VDescAllRelations> join = subroot.join(UnivDescriptor_.allRelations);
				
				sub.where(builder.equal(join.get(VDescAllRelations_.relatedValueId), Long.valueOf(descValueId)));
				predicates.add(builder.in(root.get(VEbook_.univDataUnitId)).value(sub));
			}
		
		return builder.and(predicates.toArray(new Predicate[0]));
	}
}
