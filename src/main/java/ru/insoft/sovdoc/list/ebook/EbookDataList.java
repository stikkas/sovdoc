package ru.insoft.sovdoc.list.ebook;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import ru.insoft.commons.jsf.ui.datalist.CommonSortingCriteria;
import ru.insoft.commons.jsf.ui.datalist.Paginator;
import ru.insoft.commons.jsf.ui.datalist.ViewQueryDataList;
import ru.insoft.commons.jsf.ui.datalist.ViewQuerySearchCriteria;
import ru.insoft.sovdoc.card.complex.AttachedFiles;
import ru.insoft.sovdoc.model.ebook.view.VEbook;
import ru.insoft.sovdoc.model.showfile.ImagePath;
import ru.insoft.sovdoc.system.ebook.SystemQuery;

@RequestScoped
@Named("ebookDL")
public class EbookDataList extends ViewQueryDataList<VEbook> {

	@Inject
	EntityManager em;
	@Inject
	EbookSearchCriteria searchCriteria;
	@Inject
	Paginator paginator;
	@Inject
	SystemQuery ebookQuery;
	@Inject
	AttachedFiles attachedFiles;
	
	private CommonSortingCriteria sortingCriteria;
	
	@Override
	public ViewQuerySearchCriteria<VEbook> getSearchCriteria()
	{
		return searchCriteria;
	}
	
	@Override
	public CommonSortingCriteria getSortingCriteria() 
	{
		if (sortingCriteria == null)
		{
			sortingCriteria = new CommonSortingCriteria();
			sortingCriteria.init();
		}
		return sortingCriteria;
	}
	
	@Override
	public Paginator getPaginator() 
	{
		return paginator;
	}

	@Override
	protected EntityManager getEntityManager() 
	{
		return em;
	}
	
	public String getBookCoverURL(Long unitId)
	{
		ImagePath ip = ebookQuery.queryBookCover(unitId);
		if (ip == null)
			return null;
		else
			return attachedFiles.getFileURL(ip);
	}
}
