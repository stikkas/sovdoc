package ru.insoft.sovdoc.security;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.seam.security.IdentityImpl;
import ru.insoft.sovdoc.list.complex.ComplexTree;

import ru.insoft.sovdoc.model.adm.view.VAdmUserRule;
import ru.insoft.sovdoc.model.adm.view.VAdmUserRule_;

@RequestScoped
@Named("access")
public class AccessService {

  @Inject
  EntityManager em;
  @Inject
  IdentityImpl identity;
  @Inject
  ComplexTree complexTree;

  private List<String> availableRules;

  protected List<String> getAvailableRules() {
    if (availableRules == null && identity.getUser() != null) {
      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<String> cq = cb.createQuery(String.class);
      Root<VAdmUserRule> root = cq.from(VAdmUserRule.class);
      cq.select(root.get(VAdmUserRule_.ruleCode));
      cq.where(cb.equal(root.get(VAdmUserRule_.login), identity.getUser().getId().toUpperCase()));
      availableRules = em.createQuery(cq).getResultList();
    }
    return availableRules;
  }

  public boolean hasAccess(String rule) {
    if (getAvailableRules() == null) {
      return false;
    }

    return availableRules.contains(rule);
  }
 }
