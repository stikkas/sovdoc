package ru.insoft.sovdoc.security;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.jboss.seam.security.IdentityImpl;

import ru.insoft.commons.jsf.ui.MessageUtils;
import ru.insoft.commons.security.SecurityService;
import ru.insoft.sovdoc.model.adm.table.AdmUser;
import ru.insoft.sovdoc.model.adm.table.AdmUser_;
import ru.insoft.sovdoc.ui.core.MENU_PAGES;

@RequestScoped
@Named("security")
public class SecurityServiceImpl implements SecurityService<AdmUser> {

	@Inject
	EntityManager em;
	@Inject
	IdentityImpl identity;
	
	private AdmUser user;
	
	@Produces
	@Named("currentUser")
	public AdmUser getCurrentUser()
	{
		if (user == null)
		{
			user = getUser(identity.getUser().getId());
		}
		return user;
	}
	
	@Override
	public AdmUser getUser(String login) 
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUser> cq = cb.createQuery(AdmUser.class);
		Root<AdmUser> root = cq.from(AdmUser.class);
		cq.select(root).where(cb.equal(root.get(AdmUser_.login), login.toUpperCase()));
		try
		{
			user = em.createQuery(cq).getSingleResult();
			if (user.isBlocked())
			{
				MessageUtils.ErrorMessage("Пользователь заблокирован");
				return null;
			}
		}
		catch (NoResultException e)
		{
			MessageUtils.ErrorMessage("Пользователя с таким логином не существует");
		}
		return user;
	}
	
	public String logout()
	{
		identity.logout();
		return MENU_PAGES.LOGIN_PAGE.getId() + "?faces-redirect=true";
	}

}
