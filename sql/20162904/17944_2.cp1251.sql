
create or replace PACKAGE BODY          "SOVDOCPORTAL" AS
FUNCTION chekIntervalAonly(dateA in DATE, unitId in NUMBER)
return NUMBER AS
result boolean;
dateC DATE;
dateD DATE;
nnullDate DATE;
BEGIN
result:=false;
	for row_ in (select BEGIN_DATE,END_DATE from UNIV_DATE_INTERVAL where UNIV_DATA_UNIT_ID = unitId)
	loop
		dateC:=row_.BEGIN_DATE;
		dateD:=row_.END_DATE;
			if ((row_.BEGIN_DATE is not null) and (row_.END_DATE is not null)) then
			begin
			if (dateA>=dateC and dateA<=dateD) then
				result:=true;
				return 1;
			end if;
			end;
		else
			if (row_.BEGIN_DATE is not null) then
			nnullDate := row_.BEGIN_DATE;
			else
			nnullDate := row_.END_DATE;
			end if;
			if (dateA=nnullDate) then
			result:=true;
			return 1;
			end if;
		end if;
	end loop;
	if (result=true) then
	return 1;
	else
	return 0;
	end if;

end chekIntervalAonly;
FUNCTION checkInterval(dateA in DATE, dateB in DATE, unitId in NUMBER)
return NUMBER AS
result BOOLEAN;
dateC DATE;
dateD DATE;
nnullDate DATE;
BEGIN
	result:=false;
	for row_ in (select BEGIN_DATE,END_DATE from UNIV_DATE_INTERVAL where UNIV_DATA_UNIT_ID = unitId)
	loop
		dateC:=row_.BEGIN_DATE;
		dateD:=row_.END_DATE;
		if ((dateC is null)or(dateD is null)) then
			if (dateC is not null) then
				nnullDate := dateC;
			else
				nnullDate := dateD;
			end if;
		if ((nnullDate >= dateA) and (nnullDate <=dateB)) then
			result:=true;
			return 1;
		end if;

		end if;
		if (dateC=dateD) then
			if ( (dateC>=dateA) and (dateC<=dateB)) then
				result:=true;
				return 1;
			end if;
		else
			if ((dateA>=dateC and dateA<=dateD) or (dateB>=dateC and dateB<=dateD) or (dateA<=dateC and dateB>=dateD)) then
				result:=true;
				return 1;
			end if;
		end if;
	end loop;


	if (result=false) then
			return 0;
	else
			return 1;
	end if;

END checkInterval;
FUNCTION getChildsCount(univDataUnitId in NUMBER)
return NUMBER AS
cnt NUMBER;
begin
select count(*) into cnt from UNIV_DATA_UNIT where
PARENT_UNIT_ID = univDataUnitId;
return cnt;
end getChildsCount;
FUNCTION getFullArchiveCode(univDataUnitId IN NUMBER)
return VARCHAR2 AS
aName VARCHAR(4000);
aCode VARCHAR(4000);
dtCode DESCRIPTOR_VALUE.VALUE_CODE%TYPE;
docPrefix VARCHAR(20);
resOwId NUMBER;
begin
select v.VALUE_CODE into dtCode FROM UNIV_DATA_UNIT u left join DESCRIPTOR_VALUE v on u.UNIT_TYPE_ID = v.DESCRIPTOR_VALUE_ID
WHERE UNIV_DATA_UNIT_ID=univDataUnitId;
IF dtCode = 'PHONODOC' THEN
 docPrefix := '��.��.';
ELSE
 docPrefix := '�.';
END IF;
select COMPLEX_PACK.GET_READABLE_ARCH_NUMBER(ARCH_NUMBER_CODE, docPrefix), RES_OWNER_ID into aCode,resOwId
from UNIV_DATA_UNIT  
where UNIV_DATA_UNIT_ID=univDataUnitId;
select SHORT_VALUE into aName from DESCRIPTOR_VALUE where DESCRIPTOR_VALUE_ID=resOwId;
aName:=concat(aName,'. ');
return concat(aName,aCode);
end getFullArchiveCode;
FUNCTION getAuthors(univDataUnitId IN NUMBER)
return VARCHAR2 AS
authStr VARCHAR(4000);
begin
authStr:='';
for row_ in(
select DESCRIPTOR_VALUE.SHORT_VALUE dv from
ARCH_EBOOK_AUTHOR,DESCRIPTOR_VALUE where
ARCH_EBOOK_AUTHOR.AUTHOR_ID=DESCRIPTOR_VALUE.DESCRIPTOR_VALUE_ID and
UNIV_DATA_UNIT_ID=univDataUnitId)
loop
if length(authStr)>2 then
authStr:=concat(authStr,', ');
end if;
authStr:=concat(authStr,row_.dv);
end loop;
return authStr;
end getAuthors;
end sovdocportal;
