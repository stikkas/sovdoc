  CREATE TABLE "SOVDOC"."ARCH_VIDEODOC" (	
    "UNIV_DATA_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"VIDEODOC_TYPE_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"VIDEODOC_NAME" VARCHAR2(2500 CHAR) NOT NULL ENABLE, 
	"EVENT_PLACE" VARCHAR2(250 CHAR) NOT NULL ENABLE, 
	"FILM_STUDIO" VARCHAR2(250 CHAR) NOT NULL ENABLE, 
	"DIRECTOR" VARCHAR2(150 CHAR) NOT NULL ENABLE, 
	"OPERATORS" VARCHAR2(1000 CHAR), 
	"OTHERS" VARCHAR2(1000 CHAR), 
	"COUNTRY_ID" NUMBER(10,0), 
	"ISSUE_DATE" DATE NOT NULL ENABLE, 
	"LANGUAGE_ID" NUMBER(10,0), 
	"ANNOTATION" CLOB, 
	"NOTES" VARCHAR2(4000 CHAR), 
	 CONSTRAINT "PK_ARCH_VIDEODOC" PRIMARY KEY ("UNIV_DATA_UNIT_ID"), 
	 CONSTRAINT "FK_UNIDATUNT_ARHVDE" FOREIGN KEY ("UNIV_DATA_UNIT_ID")
	  REFERENCES "SOVDOC"."UNIV_DATA_UNIT" ("UNIV_DATA_UNIT_ID") ON DELETE CASCADE ENABLE, 
	 CONSTRAINT "FK_VDETYP_ARHVDE" FOREIGN KEY ("VIDEODOC_TYPE_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	 CONSTRAINT "FK_VDECNT_ARHVDE" FOREIGN KEY ("COUNTRY_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE,
	 CONSTRAINT "FK_VDELNG_ARHVDE" FOREIGN KEY ("LANGUAGE_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE
   );
 
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."UNIV_DATA_UNIT_ID" IS 'ID универсальной сущности';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."VIDEODOC_TYPE_ID" IS 'Вид видеодокумента';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."VIDEODOC_NAME" IS 'Название документа';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."EVENT_PLACE" IS 'Место события';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."FILM_STUDIO" IS 'Киностудия';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."DIRECTOR" IS 'Режиссер';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."OPERATORS" IS 'Операторы';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."OTHERS" IS 'Другие создатели';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."COUNTRY_ID" IS 'ID страны создателя';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."ISSUE_DATE" IS 'Дата выпуска';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."LANGUAGE_ID" IS 'ID языка';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."ANNOTATION" IS 'Аннотация (реферат)';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEODOC"."NOTES" IS 'Примечание';
   COMMENT ON TABLE "SOVDOC"."ARCH_VIDEODOC"  IS 'Видеодокумент';
 
  CREATE TABLE "SOVDOC"."ARCH_KINODOC" (	
    "UNIV_DATA_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"KINODOC_TYPE_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"KINODOC_NAME" VARCHAR2(2500 CHAR) NOT NULL ENABLE, 
	"EVENT_PLACE" VARCHAR2(250 CHAR) NOT NULL ENABLE, 
	"FILM_STUDIO" VARCHAR2(250 CHAR) NOT NULL ENABLE, 
	"DIRECTOR" VARCHAR2(150 CHAR) NOT NULL ENABLE, 
	"OPERATORS" VARCHAR2(1000 CHAR), 
	"OTHERS" VARCHAR2(1000 CHAR), 
	"COUNTRY_ID" NUMBER(10,0), 
	"ISSUE_DATE" DATE NOT NULL ENABLE, 
	"LANGUAGE_ID" NUMBER(10,0), 
	"ANNOTATION" CLOB, 
	"NOTES" VARCHAR2(4000 CHAR), 
	 CONSTRAINT "PK_ARCH_KINODOC" PRIMARY KEY ("UNIV_DATA_UNIT_ID"), 
	 CONSTRAINT "FK_UNIDATUNT_ARHKIN" FOREIGN KEY ("UNIV_DATA_UNIT_ID")
	  REFERENCES "SOVDOC"."UNIV_DATA_UNIT" ("UNIV_DATA_UNIT_ID") ON DELETE CASCADE ENABLE, 
	 CONSTRAINT "FK_KINTYP_ARHKIN" FOREIGN KEY ("KINODOC_TYPE_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	 CONSTRAINT "FK_KINCNT_ARHKIN" FOREIGN KEY ("COUNTRY_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE,
	 CONSTRAINT "FK_KINLNG_ARHKIN" FOREIGN KEY ("LANGUAGE_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE
   );
 
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."UNIV_DATA_UNIT_ID" IS 'ID универсальной сущности';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."KINODOC_TYPE_ID" IS 'Вид кинодокумента';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."KINODOC_NAME" IS 'Название документа';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."EVENT_PLACE" IS 'Место события';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."FILM_STUDIO" IS 'Киностудия';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."DIRECTOR" IS 'Режиссер';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."OPERATORS" IS 'Операторы';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."OTHERS" IS 'Другие создатели';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."COUNTRY_ID" IS 'ID страны создателя';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."ISSUE_DATE" IS 'Дата выпуска';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."LANGUAGE_ID" IS 'ID языка';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."ANNOTATION" IS 'Аннотация (реферат)';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINODOC"."NOTES" IS 'Примечание';
   COMMENT ON TABLE "SOVDOC"."ARCH_KINODOC"  IS 'Кинодокумент';
 
  CREATE TABLE "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT" (
	"VIDEO_STORAGE_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"UNIV_DATA_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"NUMBER_NUMBER" NUMBER(8,0) NOT NULL ENABLE, 
	"NUMBER_TEXT" VARCHAR2(4 CHAR), 
	"PLAYTIME" NUMBER(6,0) NOT NULL ENABLE, 
	"SOUND_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"COLOR_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"SYSTEM_RECORD_ID" NUMBER(10,0), 
	"QUALITY_ID" NUMBER(10,0), 
	"UNIV_VIDEO_PATH_ID" NUMBER(10,0), 
	CONSTRAINT "PK_ARCH_VIDEO_STORAGE_UNIT" PRIMARY KEY ("VIDEO_STORAGE_UNIT_ID"),
	CONSTRAINT "FK_ARHVDE_ARHVDESTGUNT" FOREIGN KEY ("UNIV_DATA_UNIT_ID")
	  REFERENCES "SOVDOC"."ARCH_VIDEODOC" ("UNIV_DATA_UNIT_ID") ON DELETE CASCADE ENABLE, 
	CONSTRAINT "FK_SND_ARHVDESTGUNT" FOREIGN KEY ("SOUND_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_CLR_ARHVDESTGUNT" FOREIGN KEY ("COLOR_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_SYSTYP_ARHVDESTGUNT" FOREIGN KEY ("SYSTEM_RECORD_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_QUA_ARHVDESTGUNT" FOREIGN KEY ("QUALITY_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_UNIVDEPTH_ARHVDESTGUNT" FOREIGN KEY ("UNIV_VIDEO_PATH_ID")
	  REFERENCES "SOVDOC"."UNIV_IMAGE_PATH" ("UNIV_IMAGE_PATH_ID") ON DELETE SET NULL ENABLE
   );

   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."VIDEO_STORAGE_UNIT_ID" IS 'ID единицы хранения видеоданных';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."UNIV_DATA_UNIT_ID" IS 'ID единицы информационного ресурса';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."NUMBER_NUMBER" IS 'Номер единицы хранения';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."NUMBER_TEXT" IS 'Литера единицы хранения';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."PLAYTIME" IS 'Время';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."SOUND_ID" IS 'Звук';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."COLOR_ID" IS 'Цветность';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."SYSTEM_RECORD_ID" IS 'Система записи';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."QUALITY_ID" IS 'Качество';
   COMMENT ON COLUMN "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"."UNIV_VIDEO_PATH_ID" IS 'ID записи о файле';
   COMMENT ON TABLE "SOVDOC"."ARCH_VIDEO_STORAGE_UNIT"  IS 'Единица хранения видеоданных';
 
  CREATE TABLE "SOVDOC"."ARCH_KINO_STORAGE_UNIT" (
	"KINO_STORAGE_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"UNIV_DATA_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"NUMBER_NUMBER" NUMBER(8,0) NOT NULL ENABLE, 
	"NUMBER_TEXT" VARCHAR2(4 CHAR), 
	"PLAYTIME" NUMBER(6,0) NOT NULL ENABLE, 
	"SOUND_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"COLOR_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"SYSTEM_RECORD_ID" NUMBER(10,0), 
	"QUALITY_ID" NUMBER(10,0), 
	"UNIV_VIDEO_PATH_ID" NUMBER(10,0), 
	CONSTRAINT "PK_ARCH_KINO_STORAGE_UNIT" PRIMARY KEY ("KINO_STORAGE_UNIT_ID"),
	CONSTRAINT "FK_ARHKIN_ARHKINSTGUNT" FOREIGN KEY ("UNIV_DATA_UNIT_ID")
	  REFERENCES "SOVDOC"."ARCH_KINODOC" ("UNIV_DATA_UNIT_ID") ON DELETE CASCADE ENABLE, 
	CONSTRAINT "FK_SND_ARHKINSTGUNT" FOREIGN KEY ("SOUND_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_CLR_ARHKINSTGUNT" FOREIGN KEY ("COLOR_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_SYSTYP_ARHKINSTGUNT" FOREIGN KEY ("SYSTEM_RECORD_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_QUA_ARHKINSTGUNT" FOREIGN KEY ("QUALITY_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
	CONSTRAINT "FK_UNIKINPTH_ARHKINSTGUNT" FOREIGN KEY ("UNIV_VIDEO_PATH_ID")
	  REFERENCES "SOVDOC"."UNIV_IMAGE_PATH" ("UNIV_IMAGE_PATH_ID") ON DELETE SET NULL ENABLE
   );

   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."KINO_STORAGE_UNIT_ID" IS 'ID единицы хранения киноданных';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."UNIV_DATA_UNIT_ID" IS 'ID единицы информационного ресурса';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."NUMBER_NUMBER" IS 'Номер единицы хранения';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."NUMBER_TEXT" IS 'Литера единицы хранения';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."PLAYTIME" IS 'Время';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."SOUND_ID" IS 'Звук';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."COLOR_ID" IS 'Цветность';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."SYSTEM_RECORD_ID" IS 'Система записи';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."QUALITY_ID" IS 'Качество';
   COMMENT ON COLUMN "SOVDOC"."ARCH_KINO_STORAGE_UNIT"."UNIV_VIDEO_PATH_ID" IS 'ID записи о файле';
   COMMENT ON TABLE "SOVDOC"."ARCH_KINO_STORAGE_UNIT"  IS 'Единица хранения киноданных';

CREATE SEQUENCE  "SOVDOC"."SEQ_ARCH_VIDEO_STORAGE_UNIT" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE  "SOVDOC"."SEQ_ARCH_KINO_STORAGE_UNIT" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1;

  CREATE TABLE "SOVDOC"."ARCH_STORY_DESCRIPTION" (	
	"STORY_DESC_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"STORAGE_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"STORY_NUMBER" NUMBER(5,0) NOT NULL ENABLE, 
	"PART" NUMBER(2,0), 
	"ANNOTATION" CLOB NOT NULL ENABLE, 
	"LANGUAGE_ID" NUMBER(10,0), 
	"PLAYTIME" NUMBER(6,0) NOT NULL ENABLE, 
	"UNIV_IMAGE_PATH_ID" NUMBER(10,0), 
	CONSTRAINT "PK_ARCH_STORY_DESCRIPTION" PRIMARY KEY ("STORY_DESC_ID"),
	CONSTRAINT "FK_UNIIMGPTH_ARHSTRDES" FOREIGN KEY ("UNIV_IMAGE_PATH_ID")
	  REFERENCES "SOVDOC"."UNIV_IMAGE_PATH" ("UNIV_IMAGE_PATH_ID") ON DELETE SET NULL ENABLE, 
	CONSTRAINT "FK_DOCLNG_ARHSTRDES" FOREIGN KEY ("LANGUAGE_ID")
	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE 
   ); 
 
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."STORY_DESC_ID" IS 'ID описания сюжета';
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."STORAGE_UNIT_ID" IS 'ID единицы хранения (ARCH_VIDEO или ARCH_KINO)';
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."STORY_NUMBER" IS 'Номер';
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."PART" IS 'Часть';
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."ANNOTATION" IS 'Аннотация';
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."LANGUAGE_ID" IS 'Язык';
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."PLAYTIME" IS 'Время';
   COMMENT ON COLUMN "SOVDOC"."ARCH_STORY_DESCRIPTION"."UNIV_IMAGE_PATH_ID" IS 'ID файла';
   COMMENT ON TABLE "SOVDOC"."ARCH_STORY_DESCRIPTION"  IS 'Описание сюжета';
 
CREATE SEQUENCE  "SOVDOC"."SEQ_ARCH_STORY_DESCRIPTION"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1;

-- CREATE TABLE "SOVDOC"."UNIV_VIDEO_PATH" (
-- 	"UNIV_VIDEO_PATH_ID" NUMBER(10,0) NOT NULL ENABLE, 
-- 	"UNIV_DATA_UNIT_ID" NUMBER(10,0) NOT NULL ENABLE, 
-- 	"SORT_ORDER" NUMBER(5,0) NOT NULL ENABLE, 
-- 	"FILE_PATH" VARCHAR2(500 CHAR) NOT NULL ENABLE, 
-- 	"FILE_NAME" VARCHAR2(100 CHAR) NOT NULL ENABLE, 
-- 	"FORMAT_ID" NUMBER(10,0) NOT NULL ENABLE, 
-- 	"VIDEO_CATEGORY_ID" NUMBER(10,0), 
-- 	"CAPTION" VARCHAR2(500 CHAR), 
-- 	CONSTRAINT "PK_UNIV_VIDEO_PATH" PRIMARY KEY ("UNIV_VIDEO_PATH_ID"),
-- 	CONSTRAINT "FK_FRM_UNIVDEPTH" FOREIGN KEY ("FORMAT_ID")
-- 	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
-- 	CONSTRAINT "FK_VDECTG_UNIVDEPTH" FOREIGN KEY ("VIDEO_CATEGORY_ID")
-- 	  REFERENCES "SOVDOC"."DESCRIPTOR_VALUE" ("DESCRIPTOR_VALUE_ID") ENABLE, 
-- 	CONSTRAINT "FK_UNIDATUNT_UNIVDEPTH" FOREIGN KEY ("UNIV_DATA_UNIT_ID")
-- 	  REFERENCES "SOVDOC"."UNIV_DATA_UNIT" ("UNIV_DATA_UNIT_ID") ON DELETE CASCADE ENABLE
--    ); 
-- 
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."UNIV_IMAGE_PATH_ID" IS 'ID записи о файле видео';
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."UNIV_DATA_UNIT_ID" IS 'ID единицы информационного ресурса';
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."SORT_ORDER" IS 'Порядковый номер';
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."FILE_PATH" IS 'Путь к файлу';
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."FILE_NAME" IS 'Имя файла';
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."FORMAT_ID" IS 'Формат';
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."IMAGE_CATEGORY_ID" IS 'Категория файла';
--    COMMENT ON COLUMN "SOVDOC"."UNIV_VIDEO_PATH"."CAPTION" IS 'Подпись';
--    COMMENT ON TABLE "SOVDOC"."UNIV_VIDEO_PATH"  IS 'Путь к прикреплённому файлу';
-- 
-- CREATE SEQUENCE  "SOVDOC"."SEQ_UNIV_VIDEO_PATH"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1;
  
